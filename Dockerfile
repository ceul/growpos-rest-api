FROM node:alpine as builder
WORKDIR '/app'
COPY ./package.json ./
RUN npm install
COPY  . .
RUN ionic run build --prod

FROM nginx
EXPOSE 8100
COPY --from=builder /app/build /usr/share/nginx/html