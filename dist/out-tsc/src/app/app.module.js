import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};
//import { AppComponent } from './app.component';
var APP_CONTAINERS = [
    DefaultLayoutComponent
];
import { AppAsideModule, AppBreadcrumbModule, AppHeaderModule, AppFooterModule, AppSidebarModule, } from '@coreui/angular';
// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { ReversePipe } from './pipes/reverse.pipe';
import { ToastMessagesComponent } from './components/toast-messages/toast-messages.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// Import containers
import { DefaultLayoutComponent } from './containers';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AppComponent } from './app.component';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                AppComponent
            ].concat(APP_CONTAINERS, [
                P404Component,
                P500Component,
                LoginComponent,
                RegisterComponent,
                ReversePipe,
                ToastMessagesComponent
            ]),
            entryComponents: [],
            imports: [
                BrowserModule,
                FormsModule,
                IonicModule.forRoot(),
                AppRoutingModule,
                AppAsideModule,
                AppBreadcrumbModule.forRoot(),
                AppFooterModule,
                AppHeaderModule,
                AppSidebarModule,
                PerfectScrollbarModule,
                BsDropdownModule.forRoot(),
                TabsModule.forRoot(),
                ChartsModule,
                HttpClientModule,
                AlertModule.forRoot(),
                BrowserAnimationsModule
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map