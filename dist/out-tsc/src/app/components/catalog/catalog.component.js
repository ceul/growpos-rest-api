import * as tslib_1 from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
var CatalogComponent = /** @class */ (function () {
    function CatalogComponent() {
        this.className = 'CatalogComponent';
        this.onClick = new EventEmitter();
    }
    CatalogComponent.prototype.ngOnInit = function () {
    };
    CatalogComponent.prototype.ngOnChanges = function (changes) {
        try {
            if (changes.items) {
                this.items = changes.items.currentValue;
            }
        }
        catch (e) {
            console.log('An error occurred changing the catalog list e', e);
        }
    };
    CatalogComponent.prototype.clickItem = function (item) {
        try {
            this.onClick.emit(item);
        }
        catch (e) {
            console.log('An error ocurred when clicked the item');
        }
    };
    tslib_1.__decorate([
        ViewChild(CdkVirtualScrollViewport),
        tslib_1.__metadata("design:type", CdkVirtualScrollViewport)
    ], CatalogComponent.prototype, "viewPort", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], CatalogComponent.prototype, "items", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], CatalogComponent.prototype, "listHeight", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], CatalogComponent.prototype, "itemName", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], CatalogComponent.prototype, "itemImage", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], CatalogComponent.prototype, "onClick", void 0);
    CatalogComponent = tslib_1.__decorate([
        Component({
            selector: 'app-catalog',
            templateUrl: './catalog.component.html',
            styleUrls: ['./catalog.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CatalogComponent);
    return CatalogComponent;
}());
export { CatalogComponent };
//# sourceMappingURL=catalog.component.js.map