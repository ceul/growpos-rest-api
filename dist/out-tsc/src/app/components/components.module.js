import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumericKeyboardComponent } from './numeric-keyboard/numeric-keyboard.component';
import { PaymentsComponent } from './payments/payments.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ModuleMenuComponent } from './module-menu/module-menu.component';
import { ItemListComponent } from './item-list/item-list.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateSupplierComponent } from './create-supplier/create-supplier.component';
import { FindCustomerComponent } from './find-customer/find-customer.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SalesKeyboardComponent } from './sales-keyboard/sales-keyboard.component';
import { CatalogComponent } from './catalog/catalog.component';
import { FindProductComponent } from './find-product/find-product.component';
var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                NumericKeyboardComponent,
                PaymentsComponent,
                FindCustomerComponent,
                CreateCustomerComponent,
                ProductListComponent,
                ModuleMenuComponent,
                ItemListComponent,
                CreateSupplierComponent,
                ToolBarComponent,
                SalesKeyboardComponent,
                CatalogComponent,
                FindProductComponent,
            ],
            imports: [
                CommonModule,
                ModalModule.forRoot(),
                TabsModule,
                AlertModule.forRoot(),
                RouterModule,
                FormsModule,
                ReactiveFormsModule,
                ScrollingModule
            ],
            exports: [
                NumericKeyboardComponent,
                PaymentsComponent,
                FindCustomerComponent,
                CreateCustomerComponent,
                ProductListComponent,
                ModuleMenuComponent,
                ItemListComponent,
                CreateSupplierComponent,
                ToolBarComponent,
                SalesKeyboardComponent,
                CatalogComponent,
                FindProductComponent
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());
export { ComponentsModule };
//# sourceMappingURL=components.module.js.map