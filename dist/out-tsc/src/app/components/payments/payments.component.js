import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
var PaymentsComponent = /** @class */ (function () {
    function PaymentsComponent(payments) {
        this.payments = payments;
    }
    PaymentsComponent.prototype.ngOnInit = function () {
        this.bigScreen = window.innerWidth >= 993;
    };
    PaymentsComponent.prototype.onResize = function (event) {
        this.bigScreen = window.innerWidth >= 993;
    };
    tslib_1.__decorate([
        HostListener('window:resize', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], PaymentsComponent.prototype, "onResize", null);
    PaymentsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-payments',
            templateUrl: './payments.component.html',
            styleUrls: ['./payments.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], PaymentsComponent);
    return PaymentsComponent;
}());
export { PaymentsComponent };
//# sourceMappingURL=payments.component.js.map