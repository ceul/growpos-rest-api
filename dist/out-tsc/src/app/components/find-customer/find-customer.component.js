import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
var FindCustomerComponent = /** @class */ (function () {
    function FindCustomerComponent(findCustomer) {
        this.findCustomer = findCustomer;
        this.customer = {
            name: "",
            keySearch: "",
            accountId: "",
            email: "",
            telephone: "",
            postal: ""
        };
        this.customerForm = new FormGroup({
            'name': new FormControl(''),
            'keySearch': new FormControl(''),
            'accountId': new FormControl(''),
            'email': new FormControl('', [Validators.email]),
            'telephone': new FormControl(''),
            'postal': new FormControl(''),
        });
        this.customerForm.setValue(this.customer);
    }
    FindCustomerComponent.prototype.ngOnInit = function () {
        this.customers = [];
    };
    FindCustomerComponent.prototype.selectCustomer = function (customer) {
        try {
            this.customer = customer;
            console.log(customer);
        }
        catch (e) {
            console.log('An error ocurred selecting a customer');
        }
    };
    FindCustomerComponent.prototype.execute = function () {
        try {
            for (var t = 0; t < 10; t++) {
                this.customers.push({
                    id: "" + t,
                    name: "Cliente " + t,
                    img: 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png',
                });
            }
        }
        catch (e) {
            console.log('An error occurred resetting the form');
        }
    };
    FindCustomerComponent.prototype.reset = function () {
        try {
            this.customers = [];
        }
        catch (e) {
            console.log('An error occurred resetting the form');
        }
    };
    FindCustomerComponent = tslib_1.__decorate([
        Component({
            selector: 'app-find-customer',
            templateUrl: './find-customer.component.html',
            styleUrls: ['./find-customer.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], FindCustomerComponent);
    return FindCustomerComponent;
}());
export { FindCustomerComponent };
//# sourceMappingURL=find-customer.component.js.map