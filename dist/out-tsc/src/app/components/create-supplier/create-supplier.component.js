import * as tslib_1 from "tslib";
import { Component, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
var CreateSupplierComponent = /** @class */ (function () {
    function CreateSupplierComponent(createSupplier) {
        this.createSupplier = createSupplier;
        this.supplier = {
            name: "",
            keySearch: "",
            accountId: "",
            firstName: "",
            lastName: "",
            email: "",
            phone: ""
        };
        this.submit = false;
        this.createdSupplier = new EventEmitter();
        this.supplierForm = new FormGroup({
            'name': new FormControl('', Validators.required),
            'keySearch': new FormControl('', Validators.required),
            'accountId': new FormControl(''),
            'firstName': new FormControl(''),
            'lastName': new FormControl(''),
            'email': new FormControl('', [Validators.email]),
            'phone': new FormControl('')
        });
        this.supplierForm.setValue(this.supplier);
    }
    CreateSupplierComponent.prototype.ngOnInit = function () {
        this.suppliers = [];
    };
    CreateSupplierComponent.prototype.saveSupplier = function () {
        console.log(this.supplierForm.valid);
        if (this.supplierForm.valid) {
            this.createdSupplier.emit(this.supplierForm.value);
            this.createSupplier.hide();
            this.submit = false;
            this.supplierForm.reset({
                name: "",
                keySearch: "",
                accountId: "",
                taxCat: "",
                creditLimit: 0,
                vip: false,
                discount: 0,
                firstName: "",
                lastName: "",
                email: "",
                telephone: "",
                cellPhone: ""
            });
        }
        else {
            this.submit = true;
        }
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], CreateSupplierComponent.prototype, "createdSupplier", void 0);
    CreateSupplierComponent = tslib_1.__decorate([
        Component({
            selector: 'app-create-supplier',
            templateUrl: './create-supplier.component.html',
            styleUrls: ['./create-supplier.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], CreateSupplierComponent);
    return CreateSupplierComponent;
}());
export { CreateSupplierComponent };
//# sourceMappingURL=create-supplier.component.js.map