import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToolBarItemListService } from '../../services/tool-bar-item-list/tool-bar-item-list.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
var ToolBarComponent = /** @class */ (function () {
    function ToolBarComponent(toolBarItemListService) {
        this.toolBarItemListService = toolBarItemListService;
        this.btnAdd = new EventEmitter();
        this.btnTrash = new EventEmitter();
        this.btnSave = new EventEmitter();
        this.edit = false;
    }
    ToolBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.toolBarItemListService.angleLeft.subscribe(function (state) {
            _this.angleLeft = state;
        });
        this.toolBarItemListService.left.subscribe(function (state) {
            _this.left = state;
        });
        this.toolBarItemListService.down.subscribe(function (state) {
            _this.down = state;
        });
        this.toolBarItemListService.right.subscribe(function (state) {
            _this.right = state;
        });
        this.toolBarItemListService.angleRight.subscribe(function (state) {
            _this.angleRight = state;
        });
        this.toolBarItemListService.itemSelected.subscribe(function (item) {
            _this.itemSelected = item;
        });
    };
    ToolBarComponent.prototype.btnAngleLeftOnClick = function () {
        try {
            this.toolBarItemListService.btnAngleLeftOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button AngleLeft');
        }
    };
    ToolBarComponent.prototype.btnLeftOnClick = function () {
        try {
            this.toolBarItemListService.btnLeftOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Left');
        }
    };
    ToolBarComponent.prototype.btnDownOnClick = function () {
        try {
            this.toolBarItemListService.btnDownOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Down');
        }
    };
    ToolBarComponent.prototype.btnRightOnClick = function () {
        try {
            this.toolBarItemListService.btnRightOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Right');
        }
    };
    ToolBarComponent.prototype.btnAngleRightOnClick = function () {
        try {
            this.toolBarItemListService.btnAngleRightOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button AngleRight');
        }
    };
    ToolBarComponent.prototype.btnRepeatOnClick = function () {
        try {
            this.toolBarItemListService.btnRepeatOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Repeat');
        }
    };
    ToolBarComponent.prototype.btnSearchOnClick = function () {
        try {
            this.toolBarItemListService.btnSearchOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Search');
        }
    };
    ToolBarComponent.prototype.btnSortOnClick = function () {
        try {
            this.toolBarItemListService.btnSortOnClick();
        }
        catch (e) {
            console.log('An error occured clicking button Sort');
        }
    };
    ToolBarComponent.prototype.btnAddOnClick = function () {
        try {
            this.btnAdd.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Add');
        }
    };
    ToolBarComponent.prototype.btnTrashOnClick = function () {
        try {
            this.confirmDeletion.hide();
            this.btnTrash.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Trash');
        }
    };
    ToolBarComponent.prototype.btnSaveOnClick = function () {
        try {
            this.btnSave.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Save');
        }
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ToolBarComponent.prototype, "add", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ToolBarComponent.prototype, "trash", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ToolBarComponent.prototype, "save", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ToolBarComponent.prototype, "edit", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarComponent.prototype, "btnAdd", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarComponent.prototype, "btnTrash", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarComponent.prototype, "btnSave", void 0);
    tslib_1.__decorate([
        ViewChild('confirmDeletion'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], ToolBarComponent.prototype, "confirmDeletion", void 0);
    ToolBarComponent = tslib_1.__decorate([
        Component({
            selector: 'app-tool-bar',
            templateUrl: './tool-bar.component.html',
            styleUrls: ['./tool-bar.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ToolBarItemListService])
    ], ToolBarComponent);
    return ToolBarComponent;
}());
export { ToolBarComponent };
//# sourceMappingURL=tool-bar.component.js.map