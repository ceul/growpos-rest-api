import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ToolBarItemListService } from '../../services/tool-bar-item-list/tool-bar-item-list.service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
var ItemListComponent = /** @class */ (function () {
    function ItemListComponent(toolBarItemListService) {
        this.toolBarItemListService = toolBarItemListService;
        this.className = 'ItemListComponent';
        this.onClick = new EventEmitter();
    }
    ItemListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.toolBarItemListService.itemSelectedFunction("*/" + this.items.length);
        if (this.items.length < 1) {
            this.toolBarItemListService.angleRightState(true);
            this.toolBarItemListService.rightState(true);
            this.toolBarItemListService.downState(true);
            this.toolBarItemListService.leftState(true);
            this.toolBarItemListService.angleLeftState(true);
        }
        else {
            this.toolBarItemListService.angleRightState(true);
            this.toolBarItemListService.rightState(true);
            this.toolBarItemListService.downState(false);
            this.toolBarItemListService.leftState(true);
            this.toolBarItemListService.angleLeftState(true);
        }
        this.toolBarItemListService.btnAngleLeft.subscribe(function () {
            _this.btnAngleLeft();
        });
        this.toolBarItemListService.btnLeft.subscribe(function () {
            _this.btnLeft();
        });
        this.toolBarItemListService.btnDown.subscribe(function () {
            _this.btnDown();
        });
        this.toolBarItemListService.btnRight.subscribe(function () {
            _this.btnRight();
        });
        this.toolBarItemListService.btnAngleRight.subscribe(function () {
            _this.btnAngleRight();
        });
        this.toolBarItemListService.btnRepeat.subscribe(function () {
            _this.btnRepeat();
        });
        this.toolBarItemListService.btnSearch.subscribe(function () {
            _this.btnSearch();
        });
        this.toolBarItemListService.btnSort.subscribe(function () {
            _this.btnSort();
        });
    };
    ItemListComponent.prototype.ngOnChanges = function (changes) {
        try {
            if (changes.newItem) {
                if (changes.newItem.currentValue) {
                    this.itemSelected(null);
                }
            }
            if (changes.items) {
                this.items = changes.items.currentValue;
                this.toolBarItemListService.itemSelectedFunction("*/" + this.items.length);
                if (this.items.length < 1) {
                    this.toolBarItemListService.angleRightState(true);
                    this.toolBarItemListService.rightState(true);
                    this.toolBarItemListService.downState(true);
                    this.toolBarItemListService.leftState(true);
                    this.toolBarItemListService.angleLeftState(true);
                }
                else {
                    this.toolBarItemListService.angleRightState(true);
                    this.toolBarItemListService.rightState(true);
                    this.toolBarItemListService.downState(false);
                    this.toolBarItemListService.leftState(true);
                    this.toolBarItemListService.angleLeftState(true);
                }
            }
        }
        catch (e) {
            console.log('An error occurred changing the list e', e);
        }
    };
    ItemListComponent.prototype.arrowsState = function () {
        try {
            var index = this.items.indexOf(this.selectedItem);
            if (index === 0) {
                this.toolBarItemListService.angleRightState(false);
                this.toolBarItemListService.rightState(false);
                this.toolBarItemListService.downState(false);
                this.toolBarItemListService.leftState(true);
                this.toolBarItemListService.angleLeftState(true);
            }
            else if (index > 0 && index < this.items.length - 1) {
                this.toolBarItemListService.angleRightState(false);
                this.toolBarItemListService.rightState(false);
                this.toolBarItemListService.downState(false);
                this.toolBarItemListService.leftState(false);
                this.toolBarItemListService.angleLeftState(false);
            }
            else {
                this.toolBarItemListService.angleRightState(true);
                this.toolBarItemListService.rightState(true);
                this.toolBarItemListService.downState(false);
                this.toolBarItemListService.leftState(false);
                this.toolBarItemListService.angleLeftState(false);
            }
        }
        catch (e) {
            console.log('An error occurred selecting the arrows state');
        }
    };
    ItemListComponent.prototype.btnAngleLeft = function () {
        try {
            this.selectedItem = this.items[0];
            this.itemSelected(this.selectedItem);
        }
        catch (e) {
            console.log("An error occurred on " + this.btnAngleLeft.name);
        }
    };
    ItemListComponent.prototype.btnLeft = function () {
        try {
            var index = this.items.indexOf(this.selectedItem);
            if (index > 0) {
                this.selectedItem = this.items[index - 1];
                this.itemSelected(this.selectedItem);
            }
        }
        catch (e) {
            console.log("An error occurred on " + this.btnLeft.name);
        }
    };
    ItemListComponent.prototype.btnDown = function () {
        try {
            this.selectedItem = this.items[0];
            this.itemSelected(this.selectedItem);
        }
        catch (e) {
            console.log("An error occurred on " + this.btnDown.name);
        }
    };
    ItemListComponent.prototype.btnRight = function () {
        try {
            var index = this.items.indexOf(this.selectedItem);
            if (index < this.items.length - 1) {
                this.selectedItem = this.items[index + 1];
                this.itemSelected(this.selectedItem);
            }
        }
        catch (e) {
            console.log("An error occurred on " + this.btnRight.name);
        }
    };
    ItemListComponent.prototype.btnAngleRight = function () {
        try {
            this.selectedItem = this.items[this.items.length - 1];
            this.itemSelected(this.selectedItem);
        }
        catch (e) {
            console.log("An error occurred on " + this.btnAngleRight.name);
        }
    };
    ItemListComponent.prototype.btnRepeat = function () {
        try {
            this.selectedItem = this.items[0];
            this.itemSelected(this.selectedItem);
        }
        catch (e) {
            console.log("An error occurred on " + this.btnRepeat.name);
        }
    };
    ItemListComponent.prototype.btnSearch = function () {
        try {
        }
        catch (e) {
            console.log("An error occurred on " + this.btnSearch.name);
        }
    };
    ItemListComponent.prototype.btnSort = function () {
        try {
        }
        catch (e) {
            console.log("An error occurred on " + this.btnSort.name);
        }
    };
    ItemListComponent.prototype.clickItem = function (item) {
        try {
            this.selectedItem = item;
            this.itemSelected(item);
        }
        catch (e) {
            console.log('An error ocurred when clicked the item');
        }
    };
    ItemListComponent.prototype.itemSelected = function (item) {
        try {
            if (item === null) {
                this.toolBarItemListService.itemSelectedFunction("*/" + this.items.length);
                this.onClick.emit(item);
                this.arrowsState();
            }
            else {
                var index = this.items.indexOf(item) + 1;
                this.viewPort.scrollToIndex(index - 1, 'smooth');
                this.toolBarItemListService.itemSelectedFunction(index + "/" + this.items.length);
                this.onClick.emit(item);
                this.arrowsState();
            }
            this.selectedItem = item;
        }
        catch (e) {
            console.log("An error occurred " + this.itemSelected.name + " -> " + this.className);
        }
    };
    tslib_1.__decorate([
        ViewChild(CdkVirtualScrollViewport),
        tslib_1.__metadata("design:type", CdkVirtualScrollViewport)
    ], ItemListComponent.prototype, "viewPort", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], ItemListComponent.prototype, "items", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], ItemListComponent.prototype, "listHeight", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], ItemListComponent.prototype, "itemName", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], ItemListComponent.prototype, "itemImage", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ItemListComponent.prototype, "newItem", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ItemListComponent.prototype, "onClick", void 0);
    ItemListComponent = tslib_1.__decorate([
        Component({
            selector: 'app-item-list',
            templateUrl: './item-list.component.html',
            styleUrls: ['./item-list.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ToolBarItemListService])
    ], ItemListComponent);
    return ItemListComponent;
}());
export { ItemListComponent };
//# sourceMappingURL=item-list.component.js.map