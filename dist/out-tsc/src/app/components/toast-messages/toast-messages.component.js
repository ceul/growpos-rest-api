import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ToastService } from '../../services/base-services/toast.service';
import { AlertConfig } from 'ngx-bootstrap/alert';
export function getAlertConfig() {
    return Object.assign(new AlertConfig(), { type: 'success' });
}
var ToastMessagesComponent = /** @class */ (function () {
    function ToastMessagesComponent(toast) {
        this.toast = toast;
        this.messages = [];
    }
    ToastMessagesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.toast.addMessage.subscribe(function (message) {
            _this.messages.push(message);
        });
    };
    ToastMessagesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-toast-messages',
            templateUrl: './toast-messages.component.html',
            styleUrls: ['./toast-messages.component.scss'],
            providers: [{ provide: AlertConfig, useFactory: getAlertConfig }]
        }),
        tslib_1.__metadata("design:paramtypes", [ToastService])
    ], ToastMessagesComponent);
    return ToastMessagesComponent;
}());
export { ToastMessagesComponent };
//# sourceMappingURL=toast-messages.component.js.map