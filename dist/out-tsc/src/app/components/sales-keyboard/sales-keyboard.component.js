import * as tslib_1 from "tslib";
import { Component, EventEmitter, Output } from '@angular/core';
var SalesKeyboardComponent = /** @class */ (function () {
    function SalesKeyboardComponent() {
        this.btnEqual = new EventEmitter();
        this.barcode = "";
    }
    SalesKeyboardComponent.prototype.ngOnInit = function () {
    };
    SalesKeyboardComponent.prototype.numClicked = function (num) {
        try {
            console.log(num);
            this.barcode = this.barcode.concat(num.toString());
        }
        catch (e) {
            console.log("An error occurred in " + this.numClicked.name);
        }
    };
    SalesKeyboardComponent.prototype.equalClicked = function () {
        try {
            this.btnEqual.emit();
        }
        catch (e) {
            console.log("An error occurred in " + this.equalClicked.name);
        }
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], SalesKeyboardComponent.prototype, "btnEqual", void 0);
    SalesKeyboardComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sales-keyboard',
            templateUrl: './sales-keyboard.component.html',
            styleUrls: ['./sales-keyboard.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SalesKeyboardComponent);
    return SalesKeyboardComponent;
}());
export { SalesKeyboardComponent };
//# sourceMappingURL=sales-keyboard.component.js.map