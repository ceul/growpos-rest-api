import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import PeopleModel from '../../models/people.model';
var LoginComponent = /** @class */ (function () {
    function LoginComponent() {
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.people = new PeopleModel();
        this.people.id = 'uno';
        this.people.apppassword = 'uno';
        this.people.card = '';
        this.people.image = '';
        this.people.name = 'uno';
        this.people.role = '1';
        this.people.visible = true;
    };
    LoginComponent.prototype.onSubmit = function (form) {
        if (form.invalid) {
            return;
        }
        console.log('Enviado');
        console.log(this.people);
        console.log(form);
    };
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: 'login.component.html',
            styleUrls: ['login.component.css'],
        })
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map