import * as tslib_1 from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import ProductFilterModel from '../../models/product-filter.model';
import { CategoryBusiness } from '../../business/category/category.business';
import { ProductBusiness } from '../../business/product/product.business';
import FilterOperatorEnum from '../../models/filter-operator.enum';
import { BsModalRef } from 'ngx-bootstrap/modal';
var FindProductComponent = /** @class */ (function () {
    function FindProductComponent(findProduct, categoryBusiness, productBusiness) {
        this.findProduct = findProduct;
        this.categoryBusiness = categoryBusiness;
        this.productBusiness = productBusiness;
        this.onClick = new EventEmitter();
        this.filterOperators = FilterOperatorEnum;
        this.keys = Object.keys(this.filterOperators).filter(Number);
        this.filter = new ProductFilterModel();
        this.filterForm = new FormGroup({
            'barcode': new FormControl(''),
            'name.filter': new FormControl(2),
            'name.value': new FormControl(''),
            'pricebuy.filter': new FormControl(1),
            'pricebuy.value': new FormControl(''),
            'pricesell.filter': new FormControl(1),
            'pricesell.value': new FormControl(''),
            'category': new FormControl(''),
        });
    }
    FindProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        //this.selectedProduct = new Subject();
        this.categories = [];
        this.products = [];
        this.categoryBusiness.get().then(function (categories) {
            _this.categories = Object.values(categories);
        });
    };
    FindProductComponent.prototype.okBtn = function () {
        try {
            this.onClick.emit(this.selectedProduct);
            this.findProduct.hide();
        }
        catch (error) {
            console.log('An error occurred in ok button');
        }
    };
    FindProductComponent.prototype.selectProduct = function (product) {
        try {
            this.selectedProduct = product;
        }
        catch (error) {
            console.log('An error occurred selecting the product');
        }
    };
    FindProductComponent.prototype.clearForm = function () {
        try {
            var form = {
                'barcode': '',
                'name.filter': 2,
                'name.value': '',
                'pricebuy.filter': 1,
                'pricebuy.value': null,
                'pricesell.filter': 1,
                'pricesell.value': null,
                'category': ''
            };
            this.filterForm.patchValue(form);
        }
        catch (error) {
            console.log('An error occurred clearing the form');
        }
    };
    FindProductComponent.prototype.getProducts = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var products, error_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        debugger;
                        this.filter = new ProductFilterModel(this.filterForm.value);
                        if (this.filter.name.value === '') {
                            this.filter.name.filter = 1;
                        }
                        return [4 /*yield*/, this.productBusiness.getFiltered(this.filter)];
                    case 1:
                        products = _a.sent();
                        this.products = Object.values(products);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log('An error occurred getting the products: ', error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], FindProductComponent.prototype, "onClick", void 0);
    FindProductComponent = tslib_1.__decorate([
        Component({
            selector: 'app-find-product',
            templateUrl: './find-product.component.html',
            styleUrls: ['./find-product.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef,
            CategoryBusiness,
            ProductBusiness])
    ], FindProductComponent);
    return FindProductComponent;
}());
export { FindProductComponent };
//# sourceMappingURL=find-product.component.js.map