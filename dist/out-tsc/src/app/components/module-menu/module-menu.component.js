import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var ModuleMenuComponent = /** @class */ (function () {
    function ModuleMenuComponent() {
    }
    ModuleMenuComponent.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ModuleMenuComponent.prototype, "module", void 0);
    ModuleMenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-module-menu',
            templateUrl: './module-menu.component.html',
            styleUrls: ['./module-menu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ModuleMenuComponent);
    return ModuleMenuComponent;
}());
export { ModuleMenuComponent };
//# sourceMappingURL=module-menu.component.js.map