import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var NumericKeyboardComponent = /** @class */ (function () {
    function NumericKeyboardComponent() {
    }
    NumericKeyboardComponent.prototype.ngOnInit = function () {
    };
    NumericKeyboardComponent = tslib_1.__decorate([
        Component({
            selector: 'app-numeric-keyboard',
            templateUrl: './numeric-keyboard.component.html',
            styleUrls: ['./numeric-keyboard.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], NumericKeyboardComponent);
    return NumericKeyboardComponent;
}());
export { NumericKeyboardComponent };
//# sourceMappingURL=numeric-keyboard.component.js.map