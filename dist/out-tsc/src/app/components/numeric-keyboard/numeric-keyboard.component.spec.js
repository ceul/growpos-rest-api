import { async, TestBed } from '@angular/core/testing';
import { NumericKeyboardComponent } from './numeric-keyboard.component';
describe('NumericKeyboardComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [NumericKeyboardComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(NumericKeyboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=numeric-keyboard.component.spec.js.map