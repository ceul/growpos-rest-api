import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
var ProductListComponent = /** @class */ (function () {
    function ProductListComponent(productList) {
        this.productList = productList;
    }
    ProductListComponent.prototype.ngOnInit = function () {
    };
    ProductListComponent = tslib_1.__decorate([
        Component({
            selector: 'app-product-list',
            templateUrl: './product-list.component.html',
            styleUrls: ['./product-list.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], ProductListComponent);
    return ProductListComponent;
}());
export { ProductListComponent };
//# sourceMappingURL=product-list.component.js.map