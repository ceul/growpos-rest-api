import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
var FindSupplierComponent = /** @class */ (function () {
    function FindSupplierComponent(findSupplier) {
        this.findSupplier = findSupplier;
    }
    FindSupplierComponent.prototype.ngOnInit = function () {
        this.suppliers = [];
    };
    FindSupplierComponent.prototype.selectSupplier = function (supplier) {
        try {
            console.log(supplier);
        }
        catch (e) {
            console.log('An error ocurred selecting a supplier');
        }
    };
    FindSupplierComponent.prototype.execute = function () {
        try {
            for (var t = 0; t < 10; t++) {
                this.suppliers.push({
                    id: "" + t,
                    name: "Proveedor " + t,
                    img: 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png',
                });
            }
        }
        catch (e) {
            console.log('An error occurred executting the form');
        }
    };
    FindSupplierComponent.prototype.reset = function () {
        try {
            this.suppliers = [];
        }
        catch (e) {
            console.log('An error occurred resetting the form');
        }
    };
    FindSupplierComponent = tslib_1.__decorate([
        Component({
            selector: 'app-find-supplier',
            templateUrl: './find-supplier.component.html',
            styleUrls: ['./find-supplier.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], FindSupplierComponent);
    return FindSupplierComponent;
}());
export { FindSupplierComponent };
//# sourceMappingURL=find-supplier.component.js.map