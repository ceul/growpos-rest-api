import * as tslib_1 from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
var CreateCustomerComponent = /** @class */ (function () {
    function CreateCustomerComponent(createCustomer) {
        this.createCustomer = createCustomer;
        this.customer = {
            name: "",
            keySearch: "",
            accountId: "",
            taxCat: "",
            creditLimit: 0,
            vip: false,
            discount: 0,
            firstName: "",
            lastName: "",
            email: "",
            telephone: "",
            cellPhone: ""
        };
        this.taxCategories = [];
        this.createdCustomer = new EventEmitter();
        this.submit = false;
        this.customerForm = new FormGroup({
            'name': new FormControl('', Validators.required),
            'keySearch': new FormControl('', Validators.required),
            'accountId': new FormControl(''),
            'taxCat': new FormControl(''),
            'creditLimit': new FormControl(0, Validators.pattern("^[0-9]*$")),
            'vip': new FormControl(true),
            'discount': new FormControl(0, [Validators.pattern("^[0-9]+$")]),
            'firstName': new FormControl(''),
            'lastName': new FormControl(''),
            'email': new FormControl('', [Validators.email]),
            'telephone': new FormControl(''),
            'cellPhone': new FormControl('')
        });
        this.customerForm.setValue(this.customer);
    }
    CreateCustomerComponent.prototype.ngOnInit = function () {
        for (var t = 0; t < 10; t++) {
            this.taxCategories.push({
                id: "" + t,
                name: "Categoria " + t
            });
        }
    };
    CreateCustomerComponent.prototype.saveCustomer = function () {
        console.log(this.customerForm.valid);
        if (this.customerForm.valid) {
            this.createdCustomer.emit(this.customerForm.value);
            this.createCustomer.hide();
            this.submit = false;
            this.customerForm.reset({
                name: "",
                keySearch: "",
                accountId: "",
                taxCat: "",
                creditLimit: 0,
                vip: false,
                discount: 0,
                firstName: "",
                lastName: "",
                email: "",
                telephone: "",
                cellPhone: ""
            });
        }
        else {
            this.submit = true;
        }
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], CreateCustomerComponent.prototype, "createdCustomer", void 0);
    CreateCustomerComponent = tslib_1.__decorate([
        Component({
            selector: 'app-create-customer',
            templateUrl: './create-customer.component.html',
            styleUrls: ['./create-customer.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalRef])
    ], CreateCustomerComponent);
    return CreateCustomerComponent;
}());
export { CreateCustomerComponent };
//# sourceMappingURL=create-customer.component.js.map