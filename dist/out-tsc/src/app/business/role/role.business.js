import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { RoleService } from '../../services/role/role.service';
var RoleBusiness = /** @class */ (function () {
    function RoleBusiness(roleService) {
        this.roleService = roleService;
        this.className = 'RoleBusiness';
    }
    RoleBusiness.prototype.save = function (data) {
        try {
            return this.roleService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleBusiness.prototype.get = function () {
        try {
            return this.roleService.get();
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleBusiness.prototype.getById = function (id) {
        try {
            return this.roleService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleBusiness.prototype.update = function (data) {
        try {
            return this.roleService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleBusiness.prototype.delete = function (id) {
        try {
            return this.roleService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [RoleService])
    ], RoleBusiness);
    return RoleBusiness;
}());
export { RoleBusiness };
//# sourceMappingURL=role.business.js.map