import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StockService } from '../../services/stock/stock.service';
var StockBusiness = /** @class */ (function () {
    function StockBusiness(stockService) {
        this.stockService = stockService;
        this.className = 'StockBusiness';
    }
    StockBusiness.prototype.saveStockDiary = function (data) {
        try {
            return this.stockService.saveStockDiary(data);
        }
        catch (e) {
            console.log("An error occurred saving stock diary: " + this.saveStockDiary.name + " --> " + this.className);
        }
    };
    StockBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [StockService])
    ], StockBusiness);
    return StockBusiness;
}());
export { StockBusiness };
//# sourceMappingURL=stock.business.js.map