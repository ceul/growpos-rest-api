import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { CustomerService } from '../../services/customer/customer.service';
var CustomerBusiness = /** @class */ (function () {
    function CustomerBusiness(customerService) {
        this.customerService = customerService;
        this.className = 'CustomerBusiness';
    }
    CustomerBusiness.prototype.save = function (data) {
        try {
            return this.customerService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerBusiness.prototype.get = function () {
        try {
            return this.customerService.get();
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerBusiness.prototype.getById = function (id) {
        try {
            return this.customerService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerBusiness.prototype.update = function (data) {
        try {
            return this.customerService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerBusiness.prototype.delete = function (id) {
        try {
            return this.customerService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [CustomerService])
    ], CustomerBusiness);
    return CustomerBusiness;
}());
export { CustomerBusiness };
//# sourceMappingURL=customer.business.js.map