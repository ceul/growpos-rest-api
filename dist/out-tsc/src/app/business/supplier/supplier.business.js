import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { SupplierService } from '../../services/supplier/supplier.service';
var SupplierBusiness = /** @class */ (function () {
    function SupplierBusiness(supplierService) {
        this.supplierService = supplierService;
        this.className = 'SupplierBusiness';
    }
    SupplierBusiness.prototype.save = function (data) {
        try {
            return this.supplierService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving supplier " + this.save.name + " --> " + this.className);
        }
    };
    SupplierBusiness.prototype.get = function () {
        try {
            return this.supplierService.get();
        }
        catch (e) {
            console.log("An error occurred getting suppliers: " + this.get.name + " --> " + this.className);
        }
    };
    SupplierBusiness.prototype.getVisible = function () {
        try {
            return this.supplierService.getVisible();
        }
        catch (e) {
            console.log("An error occurred getting visible suppliers: " + this.getVisible.name + " --> " + this.className);
        }
    };
    SupplierBusiness.prototype.getById = function (id) {
        try {
            return this.supplierService.getById(id);
        }
        catch (e) {
            console.log("An error occurred getting supplier by id: " + this.getById.name + " --> " + this.className);
        }
    };
    SupplierBusiness.prototype.update = function (data) {
        try {
            return this.supplierService.update(data);
        }
        catch (e) {
            console.log("An error occurred updatting supplier: " + this.update.name + " --> " + this.className);
        }
    };
    SupplierBusiness.prototype.delete = function (id) {
        try {
            return this.supplierService.delete(id);
        }
        catch (e) {
            console.log("An error occurred deletting supplier: " + this.delete.name + " --> " + this.className);
        }
    };
    SupplierBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [SupplierService])
    ], SupplierBusiness);
    return SupplierBusiness;
}());
export { SupplierBusiness };
//# sourceMappingURL=supplier.business.js.map