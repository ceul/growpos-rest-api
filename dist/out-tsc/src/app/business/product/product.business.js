import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ProductService } from '../../services/product/product.service';
var ProductBusiness = /** @class */ (function () {
    function ProductBusiness(productService) {
        this.productService = productService;
        this.className = 'ProductBusiness';
    }
    ProductBusiness.prototype.save = function (data) {
        try {
            return this.productService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving product " + this.save.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.get = function () {
        try {
            return this.productService.get();
        }
        catch (e) {
            console.log("An error occurred getting products: " + this.get.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.getById = function (id) {
        try {
            return this.productService.getById(id);
        }
        catch (e) {
            console.log("An error occurred getting product by id: " + this.getById.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.getByCategory = function (categoryId) {
        try {
            return this.productService.getByCategory(categoryId);
        }
        catch (e) {
            console.log("An error occurred getting products by category: " + this.getByCategory.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.getConstCat = function () {
        try {
            return this.productService.getConstCat();
        }
        catch (e) {
            console.log("An error occurred getting constant products in category: " + this.getConstCat.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.getFiltered = function (filter) {
        try {
            return this.productService.getFiltered(filter);
        }
        catch (e) {
            console.log("An error occurred getting product filtered: " + this.getFiltered.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.update = function (data) {
        try {
            return this.productService.update(data);
        }
        catch (e) {
            console.log("An error occurred updating product: " + this.update.name + " --> " + this.className);
        }
    };
    ProductBusiness.prototype.delete = function (id) {
        try {
            return this.productService.delete(id);
        }
        catch (e) {
            console.log("An error occurred deletting product: " + this.delete.name + " --> " + this.className);
        }
    };
    ProductBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [ProductService])
    ], ProductBusiness);
    return ProductBusiness;
}());
export { ProductBusiness };
//# sourceMappingURL=product.business.js.map