import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AttributeSetService } from '../../services/attribute-set/attribute-set.service';
var AttributeSetBusiness = /** @class */ (function () {
    function AttributeSetBusiness(attributeSetService) {
        this.attributeSetService = attributeSetService;
        this.className = 'AttributeSetBusiness';
    }
    AttributeSetBusiness.prototype.save = function (data) {
        try {
            return this.attributeSetService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving attribute set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetBusiness.prototype.get = function () {
        try {
            return this.attributeSetService.get();
        }
        catch (e) {
            console.log("An error occurred saving attribute set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetBusiness.prototype.getById = function (id) {
        try {
            return this.attributeSetService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving attribute set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetBusiness.prototype.update = function (data) {
        try {
            return this.attributeSetService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving attribute set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetBusiness.prototype.delete = function (id) {
        try {
            return this.attributeSetService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving attribute set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AttributeSetService])
    ], AttributeSetBusiness);
    return AttributeSetBusiness;
}());
export { AttributeSetBusiness };
//# sourceMappingURL=attribute-set.business.js.map