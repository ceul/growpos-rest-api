import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { UnitMeasureService } from '../../services/uom/unit-measure.service';
var UnitOfMeasureBusiness = /** @class */ (function () {
    function UnitOfMeasureBusiness(unitMeasureService) {
        this.unitMeasureService = unitMeasureService;
        this.className = 'UnitOfMeasureBusiness';
    }
    UnitOfMeasureBusiness.prototype.save = function (data) {
        try {
            return this.unitMeasureService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    UnitOfMeasureBusiness.prototype.get = function () {
        try {
            return this.unitMeasureService.get();
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    UnitOfMeasureBusiness.prototype.getById = function (id) {
        try {
            return this.unitMeasureService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    UnitOfMeasureBusiness.prototype.update = function (data) {
        try {
            return this.unitMeasureService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    UnitOfMeasureBusiness.prototype.delete = function (id) {
        try {
            return this.unitMeasureService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    UnitOfMeasureBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [UnitMeasureService])
    ], UnitOfMeasureBusiness);
    return UnitOfMeasureBusiness;
}());
export { UnitOfMeasureBusiness };
//# sourceMappingURL=unit-measure.business.js.map