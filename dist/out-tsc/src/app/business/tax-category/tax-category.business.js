import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { TaxCategoryService } from '../../services/tax-category/tax-category.service';
var TaxCategoryBusiness = /** @class */ (function () {
    function TaxCategoryBusiness(taxCategoryService) {
        this.taxCategoryService = taxCategoryService;
        this.className = 'TaxCategoryBusiness';
    }
    TaxCategoryBusiness.prototype.save = function (data) {
        try {
            return this.taxCategoryService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryBusiness.prototype.get = function () {
        try {
            return this.taxCategoryService.get();
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryBusiness.prototype.getById = function (id) {
        try {
            return this.taxCategoryService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryBusiness.prototype.update = function (data) {
        try {
            return this.taxCategoryService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryBusiness.prototype.delete = function (id) {
        try {
            return this.taxCategoryService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving tax category : " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [TaxCategoryService])
    ], TaxCategoryBusiness);
    return TaxCategoryBusiness;
}());
export { TaxCategoryBusiness };
//# sourceMappingURL=tax-category.business.js.map