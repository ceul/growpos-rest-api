import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { UserService } from '../../services/user/user.service';
var UserBusiness = /** @class */ (function () {
    function UserBusiness(userService) {
        this.userService = userService;
        this.className = 'UserBusiness';
    }
    UserBusiness.prototype.save = function (data) {
        try {
            return this.userService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserBusiness.prototype.get = function () {
        try {
            return this.userService.get();
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserBusiness.prototype.getById = function (id) {
        try {
            return this.userService.getById(id);
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserBusiness.prototype.update = function (data) {
        try {
            return this.userService.update(data);
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserBusiness.prototype.delete = function (id) {
        try {
            return this.userService.delete(id);
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [UserService])
    ], UserBusiness);
    return UserBusiness;
}());
export { UserBusiness };
//# sourceMappingURL=user.business.js.map