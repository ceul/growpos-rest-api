import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { LocationService } from '../../services/location/location.service';
var LocationBusiness = /** @class */ (function () {
    function LocationBusiness(locationService) {
        this.locationService = locationService;
        this.className = 'locationBusiness';
    }
    LocationBusiness.prototype.save = function (data) {
        try {
            return this.locationService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving location " + this.save.name + " --> " + this.className);
        }
    };
    LocationBusiness.prototype.get = function () {
        try {
            return this.locationService.get();
        }
        catch (e) {
            console.log("An error occurred getting locations: " + this.get.name + " --> " + this.className);
        }
    };
    LocationBusiness.prototype.getById = function (id) {
        try {
            return this.locationService.getById(id);
        }
        catch (e) {
            console.log("An error occurred getting location by id: " + this.getById.name + " --> " + this.className);
        }
    };
    LocationBusiness.prototype.update = function (data) {
        try {
            return this.locationService.update(data);
        }
        catch (e) {
            console.log("An error occurred updatting location: " + this.update.name + " --> " + this.className);
        }
    };
    LocationBusiness.prototype.delete = function (id) {
        try {
            return this.locationService.delete(id);
        }
        catch (e) {
            console.log("An error occurred deletting location: " + this.delete.name + " --> " + this.className);
        }
    };
    LocationBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [LocationService])
    ], LocationBusiness);
    return LocationBusiness;
}());
export { LocationBusiness };
//# sourceMappingURL=location.business.js.map