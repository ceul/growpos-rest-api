import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { CategoryService } from '../../services/category/category.service';
var CategoryBusiness = /** @class */ (function () {
    function CategoryBusiness(categoryService) {
        this.categoryService = categoryService;
        this.className = 'CategoryBusiness';
    }
    CategoryBusiness.prototype.save = function (data) {
        try {
            return this.categoryService.save(data);
        }
        catch (e) {
            console.log("An error occurred saving category " + this.save.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.get = function () {
        try {
            return this.categoryService.get();
        }
        catch (e) {
            console.log("An error occurred getting categories " + this.get.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.getRoot = function () {
        try {
            return this.categoryService.getRoot();
        }
        catch (e) {
            console.log("An error occurred getting root categories: " + this.getRoot.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.getChild = function (id) {
        try {
            return this.categoryService.getChild(id);
        }
        catch (e) {
            console.log("An error occurred getting child categories: " + this.getChild.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.getById = function (id) {
        try {
            return this.categoryService.getById(id);
        }
        catch (e) {
            console.log("An error occurred getting category by id: " + this.getById.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.update = function (data) {
        try {
            return this.categoryService.update(data);
        }
        catch (e) {
            console.log("An error occurred updatting category " + this.update.name + " --> " + this.className);
        }
    };
    CategoryBusiness.prototype.delete = function (id) {
        try {
            return this.categoryService.delete(id);
        }
        catch (e) {
            console.log("An error occurred deletting category " + this.delete.name + " --> " + this.className);
        }
    };
    CategoryBusiness = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [CategoryService])
    ], CategoryBusiness);
    return CategoryBusiness;
}());
export { CategoryBusiness };
//# sourceMappingURL=category.business.js.map