import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomerMenuComponent } from './customer-menu/customer-menu.component';
import { CustomerManageComponent } from './customer-manage/customer-manage.component';
var routes = [
    {
        path: '',
        data: {
            title: 'Customers'
        },
        children: [
            {
                path: 'menu',
                component: CustomerMenuComponent,
                data: {
                    title: 'Menu de clientes'
                }
            },
            {
                path: 'customer-manage',
                component: CustomerManageComponent,
                data: {
                    title: 'Gestion de clientes'
                }
            }
        ]
    }
];
var CustomerRoutingModule = /** @class */ (function () {
    function CustomerRoutingModule() {
    }
    CustomerRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], CustomerRoutingModule);
    return CustomerRoutingModule;
}());
export { CustomerRoutingModule };
//# sourceMappingURL=customer-routing.module.js.map