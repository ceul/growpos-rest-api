import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import CustomerModel from '../../../models/customer.model';
import { CustomerBusiness } from '../../../business/customer/customer.business';
import { ToastService } from '../../../services/base-services/toast.service';
var CustomerManageComponent = /** @class */ (function () {
    function CustomerManageComponent(customerBusiness, toast) {
        this.customerBusiness = customerBusiness;
        this.toast = toast;
        this.customers = [];
        this.transactions = [];
        this.displayedColumns = ['ticketid', 'date', 'product', 'quantity', 'total'];
        this.trash = true;
        this.save = false;
        this.add = false;
        this.edit = false;
        this.submit = false;
        this.newItem = true;
        this.customerForm = new FormGroup({
            'id': new FormControl(''),
            'searchkey': new FormControl('', Validators.required),
            'taxid': new FormControl(''),
            'name': new FormControl('', Validators.required),
            'taxcategory': new FormControl(''),
            'card': new FormControl(''),
            'maxdebt': new FormControl(0, Validators.min(0)),
            'address': new FormControl(''),
            'address2': new FormControl(''),
            'postal': new FormControl(''),
            'city': new FormControl(''),
            'region': new FormControl(''),
            'country': new FormControl(''),
            'firstname': new FormControl(''),
            'lastname': new FormControl(''),
            'email': new FormControl('', Validators.email),
            'phone': new FormControl(''),
            'phone2': new FormControl(''),
            'fax': new FormControl(''),
            'notes': new FormControl(''),
            'visible': new FormControl(true),
            'curdate': new FormControl({ value: '', disabled: true }),
            'curdebt': new FormControl({ value: '', disabled: true }, Validators.min(0)),
            'image': new FormControl(''),
            'isvip': new FormControl(false),
            'discount': new FormControl('', Validators.min(0)),
        });
    }
    CustomerManageComponent.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var customers;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.customers = [];
                        return [4 /*yield*/, this.customerBusiness.get()];
                    case 1:
                        customers = _a.sent();
                        this.customers = Object.values(customers);
                        this.customerForm.valueChanges.subscribe(function () {
                            if (_this.customerForm.dirty) {
                                if (!_this.newItem) {
                                    _this.edit = true;
                                }
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerManageComponent.prototype.selectCustomer = function (customer) {
        if (this.customer !== customer && customer !== null) {
            this.customer = customer;
            this.customerForm.reset();
            this.customerForm.patchValue(this.customer);
            this.submit = false;
            this.newItem = false;
            this.trash = false;
            this.edit = false;
        }
        if (customer === null) {
            this.clearForm();
            this.submit = false;
            this.newItem = true;
            this.trash = true;
            this.edit = false;
        }
    };
    CustomerManageComponent.prototype.saveCustomer = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, response;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.customerForm.valid) return [3 /*break*/, 4];
                        this.customer = new CustomerModel(this.customerForm.value);
                        if (!this.newItem) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.customerBusiness.save(this.customer)];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        this.toast.showMessage('El cliente ah sido añadido con exito!!', 'success');
                        this.edit = false;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.customerBusiness.update(this.customer)];
                    case 3:
                        response = _a.sent();
                        this.toast.showMessage('El cliente ah sido actualizado con exito!!', 'success');
                        this.edit = false;
                        console.log(response);
                        _a.label = 4;
                    case 4:
                        this.submit = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    CustomerManageComponent.prototype.deleteCustomer = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.customerBusiness.delete(this.customer.id)];
                    case 1:
                        response = _a.sent();
                        this.toast.showMessage('El cliente ah sido eliminado con exito!!', 'success');
                        index = this.customers.map(function (customer) { return customer.id; }).indexOf(this.customer.id);
                        this.customers.splice(index, 1);
                        this.customers = this.customers.slice();
                        this.clearForm();
                        this.edit = false;
                        console.log(this.customers);
                        console.log(response);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CustomerManageComponent.prototype.clearForm = function () {
        try {
            var customer = new CustomerModel();
            customer.visible = true;
            customer.isvip = false;
            customer.maxdebt = 0;
            this.customer = customer;
            this.customerForm.reset();
            this.customerForm.patchValue(this.customer);
            this.trash = true;
        }
        catch (e) {
        }
    };
    CustomerManageComponent = tslib_1.__decorate([
        Component({
            selector: 'app-customer-manage',
            templateUrl: './customer-manage.component.html',
            styleUrls: ['./customer-manage.component.scss'],
            providers: [
                CustomerBusiness
            ]
        }),
        tslib_1.__metadata("design:paramtypes", [CustomerBusiness, ToastService])
    ], CustomerManageComponent);
    return CustomerManageComponent;
}());
export { CustomerManageComponent };
//# sourceMappingURL=customer-manage.component.js.map