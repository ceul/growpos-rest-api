import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerMenuComponent } from './customer-menu/customer-menu.component';
import { CustomerManageComponent } from './customer-manage/customer-manage.component';
import { ComponentsModule } from '../../components/components.module';
import { CustomerRoutingModule } from './customer-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { MatInputModule, MatTableModule, MatPaginatorModule } from '@angular/material';
var CustomerModule = /** @class */ (function () {
    function CustomerModule() {
    }
    CustomerModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                CustomerMenuComponent,
                CustomerManageComponent,
            ],
            imports: [
                CommonModule,
                ComponentsModule,
                CustomerRoutingModule,
                TabsModule,
                FormsModule,
                ReactiveFormsModule,
                CdkTableModule,
                MatInputModule,
                MatTableModule,
                MatPaginatorModule,
            ]
        })
    ], CustomerModule);
    return CustomerModule;
}());
export { CustomerModule };
//# sourceMappingURL=customer.module.js.map