import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var CustomerMenuComponent = /** @class */ (function () {
    function CustomerMenuComponent() {
        this.module = {
            name: 'Clientes',
            manage: 'Gestion',
            reports: 'Reportes',
            manageItems: [],
            reportsItems: []
        };
        this.module.manageItems.push({
            name: "Clientes",
            icon: 'fa fa-address-card-o fa-lg',
            url: '/customer/customer-manage'
        });
        for (var i = 0; i < 11; i++) {
            this.module.reportsItems.push({
                name: "Option " + i,
                icon: 'fa fa-home fa-lg',
                url: '/sales'
            });
        }
    }
    CustomerMenuComponent.prototype.ngOnInit = function () {
    };
    CustomerMenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-customer-menu',
            templateUrl: './customer-menu.component.html',
            styleUrls: ['./customer-menu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CustomerMenuComponent);
    return CustomerMenuComponent;
}());
export { CustomerMenuComponent };
//# sourceMappingURL=customer-menu.component.js.map