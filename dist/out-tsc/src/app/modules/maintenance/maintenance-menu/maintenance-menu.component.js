import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var MaintenanceMenuComponent = /** @class */ (function () {
    function MaintenanceMenuComponent() {
        this.module = {
            name: 'Mantenimiento',
            manage: 'Gestion',
            reports: 'Reportes',
            manageItems: [],
            reportsItems: []
        };
        this.module.manageItems.push({
            name: "Usuarios",
            icon: 'fa fa-users fa-lg',
            url: '/maintenance/user-manage'
        });
        for (var i = 0; i < 11; i++) {
            this.module.reportsItems.push({
                name: "Option " + i,
                icon: 'fa fa-home fa-lg',
                url: '/sales'
            });
        }
    }
    MaintenanceMenuComponent.prototype.ngOnInit = function () {
    };
    MaintenanceMenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-maintenance-menu',
            templateUrl: './maintenance-menu.component.html',
            styleUrls: ['./maintenance-menu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], MaintenanceMenuComponent);
    return MaintenanceMenuComponent;
}());
export { MaintenanceMenuComponent };
//# sourceMappingURL=maintenance-menu.component.js.map