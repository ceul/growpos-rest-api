import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import PeopleModel from '../../../models/people.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { UserBusiness } from '../../../business/user/user.business';
import { RoleBusiness } from '../../../business/role/role.business';
var UserManageComponent = /** @class */ (function () {
    function UserManageComponent(userBusiness, roleBusiness, toast) {
        this.userBusiness = userBusiness;
        this.roleBusiness = roleBusiness;
        this.toast = toast;
        this.users = [];
        this.roles = [];
        this.trash = true;
        this.save = false;
        this.add = false;
        this.edit = false;
        this.submit = false;
        this.newItem = true;
        this.userForm = new FormGroup({
            'id': new FormControl(''),
            'name': new FormControl('', Validators.required),
            'apppassword': new FormControl(''),
            'card': new FormControl(''),
            'role': new FormControl('', Validators.required),
            'visible': new FormControl(true, Validators.required),
            'image': new FormControl(''),
        });
    }
    UserManageComponent.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var users, roles;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.users = [];
                        this.roles = [];
                        return [4 /*yield*/, this.userBusiness.get()];
                    case 1:
                        users = _a.sent();
                        this.users = Object.values(users);
                        return [4 /*yield*/, this.roleBusiness.get()];
                    case 2:
                        roles = _a.sent();
                        this.roles = Object.values(roles);
                        this.userForm.valueChanges.subscribe(function () {
                            if (_this.userForm.dirty) {
                                if (!_this.newItem) {
                                    _this.edit = true;
                                }
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    UserManageComponent.prototype.selectUser = function (user) {
        if (this.user !== user && user !== null) {
            this.user = user;
            this.userForm.reset();
            this.userForm.patchValue(this.user);
            this.submit = false;
            this.newItem = false;
            this.trash = false;
            this.edit = false;
        }
        if (user === null) {
            this.clearForm();
            this.submit = false;
            this.newItem = true;
            this.trash = true;
            this.edit = false;
        }
    };
    UserManageComponent.prototype.saveUser = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, response;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.userForm.valid) return [3 /*break*/, 4];
                        this.user = new PeopleModel(this.userForm.value);
                        if (!this.newItem) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.userBusiness.save(this.user)];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        this.toast.showMessage('El usuario ah sido añadido con exito!!', 'success');
                        this.edit = false;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.userBusiness.update(this.user)];
                    case 3:
                        response = _a.sent();
                        this.toast.showMessage('El usuario ah sido actualizado con exito!!', 'success');
                        this.edit = false;
                        console.log(response);
                        _a.label = 4;
                    case 4:
                        this.submit = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    UserManageComponent.prototype.deleteUser = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userBusiness.delete(this.user.id)];
                    case 1:
                        response = _a.sent();
                        this.toast.showMessage('El usuario ah sido eliminado con exito!!', 'success');
                        index = this.users.map(function (user) { return user.id; }).indexOf(this.user.id);
                        this.users.splice(index, 1);
                        this.users = this.users.slice();
                        this.clearForm();
                        this.edit = false;
                        console.log(this.users);
                        console.log(response);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserManageComponent.prototype.clearForm = function () {
        try {
            var user = new PeopleModel();
            user.visible = true;
            this.user = user;
            this.userForm.reset();
            this.userForm.patchValue(this.user);
            this.trash = true;
        }
        catch (e) {
        }
    };
    UserManageComponent = tslib_1.__decorate([
        Component({
            selector: 'app-user-manage',
            templateUrl: './user-manage.component.html',
            styleUrls: ['./user-manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [UserBusiness, RoleBusiness, ToastService])
    ], UserManageComponent);
    return UserManageComponent;
}());
export { UserManageComponent };
//# sourceMappingURL=user-manage.component.js.map