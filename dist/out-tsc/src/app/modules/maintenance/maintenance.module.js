import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceMenuComponent } from './maintenance-menu/maintenance-menu.component';
import { MaintenanceRoutingModule } from './maintenance-routing.madule';
import { ComponentsModule } from '../../components/components.module';
import { UserManageComponent } from './user-manage/user-manage.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
var MaintenanceModule = /** @class */ (function () {
    function MaintenanceModule() {
    }
    MaintenanceModule = tslib_1.__decorate([
        NgModule({
            declarations: [MaintenanceMenuComponent, UserManageComponent],
            imports: [
                CommonModule,
                MaintenanceRoutingModule,
                ComponentsModule,
                FormsModule,
                ReactiveFormsModule,
            ]
        })
    ], MaintenanceModule);
    return MaintenanceModule;
}());
export { MaintenanceModule };
//# sourceMappingURL=maintenance.module.js.map