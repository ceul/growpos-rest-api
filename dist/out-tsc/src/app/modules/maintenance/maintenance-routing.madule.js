import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaintenanceMenuComponent } from './maintenance-menu/maintenance-menu.component';
import { UserManageComponent } from './user-manage/user-manage.component';
var routes = [
    {
        path: '',
        data: {
            title: 'Maintenance'
        },
        children: [
            {
                path: 'menu',
                component: MaintenanceMenuComponent,
                data: {
                    title: 'Maintenance menu'
                }
            },
            {
                path: 'user-manage',
                component: UserManageComponent,
                data: {
                    title: 'User Manage'
                }
            }
        ]
    }
];
var MaintenanceRoutingModule = /** @class */ (function () {
    function MaintenanceRoutingModule() {
    }
    MaintenanceRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], MaintenanceRoutingModule);
    return MaintenanceRoutingModule;
}());
export { MaintenanceRoutingModule };
//# sourceMappingURL=maintenance-routing.madule.js.map