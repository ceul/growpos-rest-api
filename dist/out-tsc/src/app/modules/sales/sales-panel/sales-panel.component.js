import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ProductBusiness } from 'src/app/business/product/product.business';
import { CategoryBusiness } from 'src/app/business/category/category.business';
import StockDiaryModel from 'src/app/models/stock-diary.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ProductListComponent } from 'src/app/components/product-list/product-list.component';
import { CreateCustomerComponent } from 'src/app/components/create-customer/create-customer.component';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
var SalesPanelComponent = /** @class */ (function () {
    function SalesPanelComponent(modalService, productBusiness, categoryBusiness, toast) {
        this.modalService = modalService;
        this.productBusiness = productBusiness;
        this.categoryBusiness = categoryBusiness;
        this.toast = toast;
        this.displayedColumns = ['item', 'pricesell', 'units', 'value'];
    }
    SalesPanelComponent.prototype.onResize = function (event) {
        this.catalogHeight = (window.innerHeight - 65).toString() + 'px';
        this.tableHeight = (window.innerHeight - (65 + 197)).toString() + 'px';
    };
    SalesPanelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.total = 0;
        this.subTotal = 0;
        this.taxes = 0;
        this.catalogHeight = (window.innerHeight - 65).toString() + 'px';
        this.tableHeight = (window.innerHeight - (65 + 197)).toString() + 'px';
        this.tableData = [];
        this.products = [];
        this.categories = [];
        this.constProducts = [];
        this.categoryBusiness.get().then(function (categories) {
            _this.categories = Object.values(categories);
        });
        this.productBusiness.getConstCat().then(function (products) {
            _this.constProducts = Object.values(products);
            _this.products = _this.constProducts;
            _this.products = _this.products.slice();
        });
    };
    SalesPanelComponent.prototype.selectCategory = function (category) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var products, filterConstProducts, categories, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        if (!(category.products == null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productBusiness.getByCategory(category.id)];
                    case 1:
                        products = _a.sent();
                        category.products = Object.values(products);
                        filterConstProducts = this.constProducts.filter(function (product) { return product.category !== category.id; });
                        category.products = category.products.concat(filterConstProducts);
                        _a.label = 2;
                    case 2:
                        if (!(category.childCategories == null)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.categoryBusiness.getChild(category.id)];
                    case 3:
                        categories = _a.sent();
                        category.childCategories = Object.values(categories);
                        _a.label = 4;
                    case 4:
                        index = this.categories.map(function (category) { return category.id; }).indexOf(category.id);
                        this.categories[index] = category;
                        this.products = category.products;
                        this.products = this.products.slice();
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        console.log('An error occurred selecting a category');
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    SalesPanelComponent.prototype.addProduct = function (product) {
        try {
            var productInline = new StockDiaryModel();
            productInline.product = product.id;
            productInline.productObject = product;
            productInline.units = 1;
            productInline.price = product.pricebuy;
            this.tableData.unshift(productInline);
            this.tableData = this.tableData.slice();
            this.selectedItem = 0;
            console.log(this.tableData);
        }
        catch (e) {
            console.log('An error occurred adding a product');
        }
    };
    SalesPanelComponent.prototype.getSellValue = function (units, price) {
        try {
            debugger;
            /*this.subTotal = this.subTotal+(units * price)
            this.total = this.subTotal + this.taxes*/
            return units * price;
        }
        catch (error) {
            console.log('An error occurred getting sell value');
        }
    };
    SalesPanelComponent.prototype.getRecord = function (row) {
        console.log(row);
    };
    //---------------------------- Modals ---------------------------------------------------------//
    SalesPanelComponent.prototype.openPaymentsModal = function () {
        try {
            this.paymentsModal = this.modalService.show(PaymentsComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening payments modal: ', error);
        }
    };
    SalesPanelComponent.prototype.openFindCustomerModal = function () {
        try {
            this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening find customer modal: ', error);
        }
    };
    SalesPanelComponent.prototype.openCreateCustomerModal = function () {
        try {
            this.createCustomerModal = this.modalService.show(CreateCustomerComponent, { class: 'modal-lg' });
            this.createCustomerModal.content.createdCustomer.subscribe(function (customer) {
                console.log('Customer Created: ', customer);
            });
        }
        catch (error) {
            console.log('An error ocurred opening create customer modal: ', error);
        }
    };
    SalesPanelComponent.prototype.openProductListModal = function () {
        try {
            this.productListModal = this.modalService.show(ProductListComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening product list modal: ', error);
        }
    };
    tslib_1.__decorate([
        HostListener('window:resize', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], SalesPanelComponent.prototype, "onResize", null);
    SalesPanelComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sales-panel',
            templateUrl: './sales-panel.component.html',
            styleUrls: ['./sales-panel.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalService,
            ProductBusiness,
            CategoryBusiness,
            ToastService])
    ], SalesPanelComponent);
    return SalesPanelComponent;
}());
export { SalesPanelComponent };
//# sourceMappingURL=sales-panel.component.js.map