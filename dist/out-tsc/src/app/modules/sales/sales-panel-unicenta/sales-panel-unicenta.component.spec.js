import { async, TestBed } from '@angular/core/testing';
import { SalesPanelUnicentaComponent } from './sales-panel-unicenta.component';
describe('SalesPanelUnicentaComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [SalesPanelUnicentaComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(SalesPanelUnicentaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=sales-panel-unicenta.component.spec.js.map