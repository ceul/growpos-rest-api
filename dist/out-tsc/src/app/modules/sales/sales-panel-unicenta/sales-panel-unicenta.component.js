import * as tslib_1 from "tslib";
import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ModalDirective, BsModalService } from 'ngx-bootstrap/modal';
import { PaymentsComponent } from '../../../components/payments/payments.component';
import { CreateCustomerComponent } from '../../../components/create-customer/create-customer.component';
import { ProductListComponent } from '../../../components/product-list/product-list.component';
import { ActivatedRoute } from '@angular/router';
import { FindCustomerComponent } from '../../../components/find-customer/find-customer.component';
import ProductModel from '../../../models/product.model';
var SalesPanelUnicentaComponent = /** @class */ (function () {
    function SalesPanelUnicentaComponent(modalService, _activatedRoute, changeDetectorRefs) {
        this.modalService = modalService;
        this._activatedRoute = _activatedRoute;
        this.changeDetectorRefs = changeDetectorRefs;
        this.displayedColumns = ['name', 'pricesell', 'units', 'taxes', 'value', 'printer'];
        this.tableData = [];
        this.categories = [];
        this.products = [];
        this._activatedRoute.params.subscribe(function (parametros) {
            console.log(parametros);
        });
        var catName;
        var catId;
        var products;
        for (var l = 0; l < 8; l++) {
            products = [];
            catId = "" + l;
            catName = "Category " + l;
            for (var t = 0; t < 24; t++) {
                var product = new ProductModel();
                product.id = "" + l + t;
                product.image = 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png'; //'https://www.w3schools.com/bootstrap4/paris.jpg'
                product.name = "Producto " + t;
                product.pricesell = t;
                products.push(product);
            }
            this.categories.push({
                catId: catId,
                catName: catName,
                catImg: 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png',
                products: products
            });
        }
        /*for (let i = 0; i < 8; i++) {
          this.tableData.push({
            item: `Cafe ${i}`,
            price: i,
            units: i,
            taxes: `${i} %`,
            value: i * i,
            printer: i
          });
        }*/
    }
    SalesPanelUnicentaComponent.prototype.ngOnInit = function () {
    };
    SalesPanelUnicentaComponent.prototype.getRecord = function (row) {
        console.log(row);
    };
    SalesPanelUnicentaComponent.prototype.selectCategory = function (category) {
        try {
            this.products = category.products;
        }
        catch (e) {
            console.log('An error ocurred selecting a category');
        }
    };
    SalesPanelUnicentaComponent.prototype.addProduct = function (product) {
        try {
            this.tableData = this.tableData.concat(product);
            this.changeDetectorRefs.detectChanges();
        }
        catch (e) {
            console.log('An error ocurred adding a product');
        }
    };
    SalesPanelUnicentaComponent.prototype.selectCustomer = function (customer) {
        try {
            console.log(customer);
        }
        catch (e) {
            console.log('An error occurred selecting a customer');
        }
    };
    SalesPanelUnicentaComponent.prototype.deleteTicket = function () {
        try {
            this.tableData = [];
            this.confirmDeletionReceipt.hide();
        }
        catch (e) {
            console.log('An error ocurred deleting the ticket');
        }
    };
    //---------------------------- Modals ---------------------------------------------------------//
    SalesPanelUnicentaComponent.prototype.openPaymentsModal = function () {
        try {
            this.paymentsModal = this.modalService.show(PaymentsComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening payments modal: ', error);
        }
    };
    SalesPanelUnicentaComponent.prototype.openFindCustomerModal = function () {
        try {
            this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening find customer modal: ', error);
        }
    };
    SalesPanelUnicentaComponent.prototype.openCreateCustomerModal = function () {
        try {
            this.createCustomerModal = this.modalService.show(CreateCustomerComponent, { class: 'modal-lg' });
            this.createCustomerModal.content.createdCustomer.subscribe(function (customer) {
                console.log('Customer Created: ', customer);
            });
        }
        catch (error) {
            console.log('An error ocurred opening create customer modal: ', error);
        }
    };
    SalesPanelUnicentaComponent.prototype.openProductListModal = function () {
        try {
            this.productListModal = this.modalService.show(ProductListComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening product list modal: ', error);
        }
    };
    tslib_1.__decorate([
        ViewChild('editLine'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "editLine", void 0);
    tslib_1.__decorate([
        ViewChild('confirmDeletionLine'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "confirmDeletionLine", void 0);
    tslib_1.__decorate([
        ViewChild('noAttributesInfo'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "noAttributesInfo", void 0);
    tslib_1.__decorate([
        ViewChild('productInfo'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "productInfo", void 0);
    tslib_1.__decorate([
        ViewChild('splitReceipt'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "splitReceipt", void 0);
    tslib_1.__decorate([
        ViewChild('numberGuest'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "numberGuest", void 0);
    tslib_1.__decorate([
        ViewChild('confirmDeletionReceipt'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], SalesPanelUnicentaComponent.prototype, "confirmDeletionReceipt", void 0);
    SalesPanelUnicentaComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sales-panel--unicenta',
            templateUrl: './sales-panel-unicenta.component.html',
            styleUrls: ['./sales-panel-unicenta.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalService, ActivatedRoute,
            ChangeDetectorRef])
    ], SalesPanelUnicentaComponent);
    return SalesPanelUnicentaComponent;
}());
export { SalesPanelUnicentaComponent };
//# sourceMappingURL=sales-panel-unicenta.component.js.map