import { async, TestBed } from '@angular/core/testing';
import { RestaurantSalesComponent } from './restaurant-sales.component';
describe('RestaurantSalesComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [RestaurantSalesComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(RestaurantSalesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=restaurant-sales.component.spec.js.map