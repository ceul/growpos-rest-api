import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var RestaurantSalesComponent = /** @class */ (function () {
    function RestaurantSalesComponent(_router) {
        this._router = _router;
        this.floors = [];
        try {
            var floor = void 0;
            var tables = void 0;
            for (var i = 0; i < 2; i++) {
                tables = [];
                floor = "Piso " + i;
                for (var t = 0; t < 24; t++) {
                    tables.push({
                        id: "" + t,
                        customer: '',
                        waiter: 'Admininstrador',
                        table: "Mesa " + t + "/" + i,
                        occupancy: true
                    });
                }
                this.floors.push({
                    floor: floor,
                    tables: tables
                });
            }
        }
        catch (e) {
            console.log('severo error ', e);
        }
    }
    RestaurantSalesComponent.prototype.ngOnInit = function () {
    };
    RestaurantSalesComponent.prototype.navigateSales = function (id) {
        try {
            this._router.navigate(['/sales/table', id]);
        }
        catch (e) {
            console.log('An error ocurred navifating to the sales panel');
        }
    };
    RestaurantSalesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-restaurant-sales',
            templateUrl: './restaurant-sales.component.html',
            styleUrls: ['./restaurant-sales.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], RestaurantSalesComponent);
    return RestaurantSalesComponent;
}());
export { RestaurantSalesComponent };
//# sourceMappingURL=restaurant-sales.component.js.map