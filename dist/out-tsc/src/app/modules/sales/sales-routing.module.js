import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SalesPanelComponent } from './sales-panel/sales-panel.component';
import { RestaurantSalesComponent } from './restaurant-sales/restaurant-sales.component';
import { ReservationsComponent } from './reservations/reservations.component';
var routes = [
    {
        path: '',
        data: {
            title: 'Sales'
        },
        children: [
            {
                path: '',
                component: SalesPanelComponent,
                data: {
                    title: 'Sales panel'
                }
            },
            {
                path: 'table/:id',
                component: SalesPanelComponent,
                data: {
                    title: 'Sales panel'
                }
            },
            {
                path: 'restaurant',
                component: RestaurantSalesComponent,
                data: {
                    title: 'Tables'
                }
            },
            {
                path: 'reservation',
                component: ReservationsComponent,
                data: {
                    title: 'Reservations'
                }
            }
        ]
    }
];
var SalesRoutingModule = /** @class */ (function () {
    function SalesRoutingModule() {
    }
    SalesRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], SalesRoutingModule);
    return SalesRoutingModule;
}());
export { SalesRoutingModule };
//# sourceMappingURL=sales-routing.module.js.map