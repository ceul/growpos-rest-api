import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { SalesRoutingModule } from './sales-routing.module';
import { SalesPanelComponent } from './sales-panel/sales-panel.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkTableModule } from '@angular/cdk/table';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule } from "@angular/material";
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AlertModule } from 'ngx-bootstrap/alert';
import { PaymentsComponent } from '../../components/payments/payments.component';
import { FindCustomerComponent } from '../../components/find-customer/find-customer.component';
import { CreateCustomerComponent } from '../../components/create-customer/create-customer.component';
import { ProductListComponent } from '../../components/product-list/product-list.component';
import { RestaurantSalesComponent } from './restaurant-sales/restaurant-sales.component';
import { CommonModule } from '@angular/common';
import { ReservationsComponent } from './reservations/reservations.component';
import { ComponentsModule } from '../../components/components.module';
import { SalesPanelUnicentaComponent } from './sales-panel-unicenta/sales-panel-unicenta.component';
import { FormsModule } from '@angular/forms';
var SalesModule = /** @class */ (function () {
    function SalesModule() {
    }
    SalesModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                SalesRoutingModule,
                FormsModule,
                BsDropdownModule.forRoot(),
                ScrollingModule,
                CdkTableModule,
                MatInputModule,
                MatTableModule,
                MatPaginatorModule,
                MatSortModule,
                MatProgressSpinnerModule,
                ModalModule,
                TabsModule,
                AlertModule.forRoot(),
                ComponentsModule
            ],
            declarations: [
                SalesPanelComponent,
                SalesPanelUnicentaComponent,
                RestaurantSalesComponent,
                ReservationsComponent,
            ],
            providers: [
                BsModalService
            ],
            bootstrap: [
                PaymentsComponent,
                FindCustomerComponent,
                CreateCustomerComponent,
                ProductListComponent
            ]
        })
    ], SalesModule);
    return SalesModule;
}());
export { SalesModule };
//# sourceMappingURL=sales.module.js.map