import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FindCustomerComponent } from '../../../components/find-customer/find-customer.component';
var ReservationsComponent = /** @class */ (function () {
    function ReservationsComponent(modalService) {
        this.modalService = modalService;
    }
    ReservationsComponent.prototype.ngOnInit = function () {
    };
    ReservationsComponent.prototype.openFindCustomerModal = function () {
        try {
            this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening find customer modal: ', error);
        }
    };
    ReservationsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-reservations',
            templateUrl: './reservations.component.html',
            styleUrls: ['./reservations.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalService])
    ], ReservationsComponent);
    return ReservationsComponent;
}());
export { ReservationsComponent };
//# sourceMappingURL=reservations.component.js.map