import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import CategoryModel from '../../../models/category.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryBusiness } from '../../../business/category/category.business';
import { ToastService } from '../../../services/base-services/toast.service';
var CategoryManageComponent = /** @class */ (function () {
    function CategoryManageComponent(categoryBusiness, toast) {
        this.categoryBusiness = categoryBusiness;
        this.toast = toast;
        this.categories = [];
        this.trash = true;
        this.save = false;
        this.add = false;
        this.edit = false;
        this.submit = false;
        this.newItem = true;
        this.categoryForm = new FormGroup({
            'id': new FormControl(''),
            'name': new FormControl('', Validators.required),
            'parentid': new FormControl(''),
            'image': new FormControl(''),
            'texttip': new FormControl(''),
            'catshowname': new FormControl(true),
            'catorder': new FormControl('', Validators.min(0)),
        });
    }
    CategoryManageComponent.prototype.ngOnInit = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var categories;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.categories = [];
                        return [4 /*yield*/, this.categoryBusiness.get()];
                    case 1:
                        categories = _a.sent();
                        this.categories = Object.values(categories);
                        this.categoryForm.valueChanges.subscribe(function () {
                            if (_this.categoryForm.dirty) {
                                if (!_this.newItem) {
                                    _this.edit = true;
                                }
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    CategoryManageComponent.prototype.selectCategory = function (category) {
        if (this.category !== category && category !== null) {
            this.category = category;
            this.categoryForm.reset();
            this.categoryForm.patchValue(this.category);
            this.submit = false;
            this.newItem = false;
            this.trash = false;
            this.edit = false;
        }
        if (category === null) {
            this.clearForm();
            this.submit = false;
            this.newItem = true;
            this.trash = true;
            this.edit = false;
        }
    };
    CategoryManageComponent.prototype.saveCategory = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, response;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.categoryForm.valid) return [3 /*break*/, 4];
                        this.category = new CategoryModel(this.categoryForm.value);
                        if (this.category.parentid === '') {
                            this.category.parentid = null;
                        }
                        if (!this.newItem) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.categoryBusiness.save(this.category)];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        this.toast.showMessage('La categoria ah sido añadida con exito!!', 'success');
                        this.edit = false;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.categoryBusiness.update(this.category)];
                    case 3:
                        response = _a.sent();
                        this.toast.showMessage('La categoria ah sido actualizada con exito!!', 'success');
                        this.edit = false;
                        console.log(response);
                        _a.label = 4;
                    case 4:
                        this.submit = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    CategoryManageComponent.prototype.deleteCategory = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.categoryBusiness.delete(this.category.id)];
                    case 1:
                        response = _a.sent();
                        this.toast.showMessage('La categoria ah sido eliminada con exito!!', 'success');
                        index = this.categories.map(function (category) { return category.id; }).indexOf(this.category.id);
                        this.categories.splice(index, 1);
                        this.categories = this.categories.slice();
                        this.clearForm();
                        this.edit = false;
                        console.log(this.categories);
                        console.log(response);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CategoryManageComponent.prototype.clearForm = function () {
        try {
            var category = new CategoryModel();
            category.catshowname = true;
            category.parentid = '';
            this.category = category;
            this.categoryForm.reset();
            this.categoryForm.patchValue(this.category);
            this.trash = true;
        }
        catch (e) {
        }
    };
    CategoryManageComponent = tslib_1.__decorate([
        Component({
            selector: 'app-category-manage',
            templateUrl: './category-manage.component.html',
            styleUrls: ['./category-manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [CategoryBusiness, ToastService])
    ], CategoryManageComponent);
    return CategoryManageComponent;
}());
export { CategoryManageComponent };
//# sourceMappingURL=category-manage.component.js.map