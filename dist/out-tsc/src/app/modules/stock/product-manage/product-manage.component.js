import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreateSupplierComponent } from '../../../components/create-supplier/create-supplier.component';
import ProductModel from '../../../models/product.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductBusiness } from '../../../business/product/product.business';
import { ToastService } from '../../../services/base-services/toast.service';
import { CategoryBusiness } from '../../../business/category/category.business';
import { TaxCategoryBusiness } from '../../../business/tax-category/tax-category.business';
import { UnitOfMeasureBusiness } from '../../../business/uom/unit-measure.business';
import { AttributeSetBusiness } from '../../../business/attribute-set/attribute-set.business';
import { SupplierBusiness } from '../../../business/supplier/supplier.business';
var ProductManageComponent = /** @class */ (function () {
    function ProductManageComponent(modalService, productBusiness, categoryBusiness, taxCategoryBusiness, unitMeasureBusiness, attributeSetBusiness, supplierBusiness, toast) {
        this.modalService = modalService;
        this.productBusiness = productBusiness;
        this.categoryBusiness = categoryBusiness;
        this.taxCategoryBusiness = taxCategoryBusiness;
        this.unitMeasureBusiness = unitMeasureBusiness;
        this.attributeSetBusiness = attributeSetBusiness;
        this.supplierBusiness = supplierBusiness;
        this.toast = toast;
        this.products = [];
        this.categories = [];
        this.attributes = [];
        this.taxCategories = [];
        this.uoms = [];
        this.suppliers = [];
        this.displayedColumns = ['location', 'current', 'minimum', 'maximun'];
        this.tableData = [];
        this.trash = true;
        this.save = false;
        this.add = false;
        this.edit = false;
        this.submit = false;
        this.newItem = true;
        this.productForm = new FormGroup({
            'id': new FormControl(''),
            'reference': new FormControl('', Validators.required),
            'code': new FormControl('', Validators.required),
            'codetype': new FormControl('', Validators.required),
            'name': new FormControl('', Validators.required),
            'pricebuy': new FormControl('', [Validators.required, Validators.min(0)]),
            'pricesell': new FormControl('', [Validators.required, Validators.min(0)]),
            'category': new FormControl('', Validators.required),
            'taxcat': new FormControl('', Validators.required),
            'attributeset_id': new FormControl(''),
            'stockcost': new FormControl(0),
            'stockvolume': new FormControl(0),
            'image': new FormControl(''),
            'iscom': new FormControl(false),
            'isscale': new FormControl(false),
            'isconstant': new FormControl(false),
            'printkb': new FormControl(false),
            'sendstatus': new FormControl(false),
            'isservice': new FormControl(false),
            'attributes': new FormControl(''),
            'display': new FormControl(''),
            'isvprice': new FormControl(false),
            'isverpatrib': new FormControl(false),
            'texttip': new FormControl(''),
            'warranty': new FormControl(false),
            'stockunits': new FormControl(0),
            'printto': new FormControl('null'),
            'supplier': new FormControl('null'),
            'uom': new FormControl('null'),
            'flag': new FormControl(false),
            'description': new FormControl(''),
            'margin': new FormControl(''),
            'grossProfit': new FormControl({ value: '', disabled: true }, Validators.min(0)),
            'priceSellNoTax': new FormControl('', Validators.min(0)),
            'weigth': new FormControl(0, Validators.min(0)),
            'width': new FormControl(0, Validators.min(0)),
            'height': new FormControl(0, Validators.min(0)),
            'length': new FormControl(0, Validators.min(0)),
            'catorder': new FormControl(0, Validators.min(0)),
            'inCat': new FormControl(true),
        });
    }
    ProductManageComponent_1 = ProductManageComponent;
    ProductManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.products = [];
        this.categories = [];
        this.productBusiness.get().then(function (products) {
            _this.products = Object.values(products);
        });
        this.categoryBusiness.get().then(function (categories) {
            _this.categories = Object.values(categories);
        });
        this.taxCategoryBusiness.get().then(function (taxCat) {
            _this.taxCategories = Object.values(taxCat);
        });
        this.supplierBusiness.get().then(function (supplier) {
            _this.suppliers = Object.values(supplier);
        });
        this.attributeSetBusiness.get().then(function (attributeSet) {
            _this.attributes = Object.values(attributeSet);
        });
        this.unitMeasureBusiness.get().then(function (uom) {
            _this.uoms = Object.values(uom);
        });
        this.productForm.valueChanges.subscribe(function () {
            if (_this.productForm.dirty) {
                if (!_this.newItem) {
                    _this.edit = true;
                }
            }
        });
    };
    ProductManageComponent.prototype.openCreateSupplierModal = function () {
        try {
            this.createSupplierModal = this.modalService.show(CreateSupplierComponent, { class: 'modal-lg' });
        }
        catch (error) {
            console.log('An error ocurred opening create Supplier modal: ', error);
        }
    };
    ProductManageComponent.prototype.calculateMargin = function () {
        var dPriceBuy = this.productForm.controls['pricebuy'].value;
        var dPriceSell = this.productForm.controls['priceSellNoTax'].value;
        if (dPriceBuy == null || dPriceSell == null) {
            this.productForm.controls['margin'].setValue(null);
        }
        else {
            this.productForm.controls['margin'].setValue((dPriceSell / dPriceBuy - 1.0) * 100);
        }
    };
    ProductManageComponent.prototype.calculatePriceSellTax = function () {
        var dPriceSell = this.productForm.controls['priceSellNoTax'].value;
        if (dPriceSell == null) {
            this.productForm.controls['pricesell'].setValue(null);
        }
        else {
            var dTaxRate = 0; // selected tax reate
            this.productForm.controls['pricesell'].setValue(dPriceSell * (1.0 + dTaxRate));
        }
    };
    ProductManageComponent.prototype.calculateGP = function () {
        var dPriceBuy = this.productForm.controls['pricebuy'].value;
        var dPriceSell = this.productForm.controls['priceSellNoTax'].value;
        if (dPriceBuy == null || dPriceSell == null) {
            this.productForm.controls['grossProfit'].setValue(null);
        }
        else {
            this.productForm.controls['grossProfit'].setValue(((dPriceSell - dPriceBuy) / dPriceSell) * 100);
        }
    };
    ProductManageComponent.prototype.calculatePriceSellfromMargin = function () {
        var dPriceBuy = this.productForm.controls['pricebuy'].value;
        var dMargin = this.productForm.controls['margin'].value / 100;
        if (dMargin == null || dPriceBuy == null) {
            this.productForm.controls['priceSellNoTax'].setValue(null);
        }
        else {
            var a = dPriceBuy * (1.0 + dMargin);
            console.log(a);
            this.productForm.controls['priceSellNoTax'].setValue(dPriceBuy * (1.0 + dMargin));
        }
    };
    ProductManageComponent.prototype.calculatePriceSellfromPST = function () {
        var dPriceSellTax = this.productForm.controls['pricesell'].value;
        if (dPriceSellTax == null) {
            this.productForm.controls['priceSellNoTax'].setValue(null);
        }
        else {
            var dTaxRate = 0; //get tax;
            this.productForm.controls['priceSellNoTax'].setValue(dPriceSellTax / (1.0 + dTaxRate));
        }
    };
    ProductManageComponent.prototype.priceNoTaxSellManager = function () {
        this.calculateMargin();
        this.calculatePriceSellTax();
        this.calculateGP();
    };
    ProductManageComponent.prototype.priceBuyManager = function () {
        this.calculateMargin();
        this.calculatePriceSellTax();
        this.calculateGP();
    };
    ProductManageComponent.prototype.priceTaxSellManager = function () {
        this.calculatePriceSellfromPST();
        this.calculateMargin();
        this.calculateGP();
    };
    ProductManageComponent.prototype.marginManager = function () {
        this.calculatePriceSellfromMargin();
        this.calculatePriceSellTax();
        this.calculateGP();
    };
    // --------------------------------------- Tool bar logic --------------------------------------//
    ProductManageComponent.prototype.selectProduct = function (product) {
        if (this.product !== product && product !== null) {
            this.product = product;
            this.productForm.reset();
            this.productForm.patchValue(this.product);
            this.submit = false;
            this.newItem = false;
            this.trash = false;
            this.edit = false;
            this.priceTaxSellManager();
        }
        if (product === null) {
            this.clearForm();
            this.submit = false;
            this.newItem = true;
            this.trash = true;
            this.edit = false;
        }
    };
    ProductManageComponent.prototype.saveProduct = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, response, err_1;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        if (!this.productForm.valid) return [3 /*break*/, 4];
                        this.product = new ProductModel(this.productForm.value);
                        Object.keys(this.product).map(function (key, index) {
                            _this.product[key] = _this.product[key] === '' || _this.product[key] === 'null' ? null : _this.product[key];
                        });
                        if (!this.newItem) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productBusiness.save(this.product)];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        this.toast.showMessage('El producto ah sido añadido con exito!!', 'success');
                        this.edit = false;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.productBusiness.update(this.product)];
                    case 3:
                        response = _a.sent();
                        this.toast.showMessage('El producto ah sido actualizado con exito!!', 'success');
                        this.edit = false;
                        console.log(response);
                        _a.label = 4;
                    case 4:
                        this.submit = true;
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        this.toast.showMessage("Ocurrio un error al a\u00F1adir el producto: " + err_1, 'danger');
                        console.log("An error occurred saving product: " + this.saveProduct.name + " --> " + ProductManageComponent_1.name);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ProductManageComponent.prototype.deleteProduct = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var response, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.productBusiness.delete(this.product.id)];
                    case 1:
                        response = _a.sent();
                        this.toast.showMessage('El producto ah sido eliminado con exito!!', 'success');
                        index = this.products.map(function (product) { return product.id; }).indexOf(this.product.id);
                        this.products.splice(index, 1);
                        this.products = this.products.slice();
                        this.clearForm();
                        this.edit = false;
                        console.log(this.products);
                        console.log(response);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log("An error occurred deleting the form: " + this.deleteProduct.name + " --> " + ProductManageComponent_1.name);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProductManageComponent.prototype.clearForm = function () {
        try {
            var product = new ProductModel();
            product.stockcost = 0;
            product.stockvolume = 0;
            product.iscom = false;
            product.isscale = false;
            product.isconstant = false;
            product.printkb = false;
            product.sendstatus = false;
            product.isservice = false;
            product.isvprice = false;
            product.isverpatrib = false;
            product.warranty = false;
            product.stockunits = 0;
            product.printto = 'null';
            product.supplier = 'null';
            product.uom = 'null';
            product.flag = false;
            product.weigth = 0;
            product.width = 0;
            product.height = 0;
            product.length = 0;
            product.catorder = 0;
            product.inCat = true;
            this.product = product;
            this.productForm.reset();
            this.productForm.patchValue(this.product);
            this.trash = true;
        }
        catch (e) {
            console.log("An error occurred cleaning the form: " + this.clearForm.name + " --> " + ProductManageComponent_1.name);
        }
    };
    var ProductManageComponent_1;
    ProductManageComponent = ProductManageComponent_1 = tslib_1.__decorate([
        Component({
            selector: 'app-product-manage',
            templateUrl: './product-manage.component.html',
            styleUrls: ['./product-manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalService,
            ProductBusiness,
            CategoryBusiness,
            TaxCategoryBusiness,
            UnitOfMeasureBusiness,
            AttributeSetBusiness,
            SupplierBusiness,
            ToastService])
    ], ProductManageComponent);
    return ProductManageComponent;
}());
export { ProductManageComponent };
//# sourceMappingURL=product-manage.component.js.map