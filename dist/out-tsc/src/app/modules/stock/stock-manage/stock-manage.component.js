import * as tslib_1 from "tslib";
import { Component, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../../services/base-services/toast.service';
import { ProductBusiness } from '../../../business/product/product.business';
import { CategoryBusiness } from '../../../business/category/category.business';
import { SupplierBusiness } from '../../../business/supplier/supplier.business';
import { LocationBusiness } from '../../../business/location/location.business';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import StockDiaryModel from '../../../models/stock-diary.model';
import { ModalDirective, BsModalService } from 'ngx-bootstrap/modal';
import { FindProductComponent } from '../../../components/find-product/find-product.component';
import MovementReasonEnum from '../../../models/movement-reason.enum';
import { StockBusiness } from '../../../business/stock/stock.business';
var StockManageComponent = /** @class */ (function () {
    function StockManageComponent(modalService, productBusiness, categoryBusiness, supplierBusiness, locationBusiness, stockBusiness, toast) {
        this.modalService = modalService;
        this.productBusiness = productBusiness;
        this.categoryBusiness = categoryBusiness;
        this.supplierBusiness = supplierBusiness;
        this.locationBusiness = locationBusiness;
        this.stockBusiness = stockBusiness;
        this.toast = toast;
        this.displayedColumns = ['item', 'pricesell', 'units', 'value'];
        this.movementReasons = MovementReasonEnum;
        this.movementKeys = Object.keys(this.movementReasons).filter(Number);
        this.manageForm = new FormGroup({
            'date': new FormControl(new Date(), Validators.required),
            'reason': new FormControl('', Validators.required),
            'location': new FormControl('', Validators.required),
            'supplier': new FormControl('', Validators.required),
            'document': new FormControl(''),
        });
    }
    StockManageComponent.prototype.onScrollEvent = function () {
        this.datepicker.hide();
    };
    StockManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.submit = false;
        this.newPriceBuy = 1;
        this.tableData = [];
        this.products = [];
        this.categories = [];
        this.constProducts = [];
        this.locations = [];
        this.suppliers = [];
        this.categoryBusiness.get().then(function (categories) {
            _this.categories = Object.values(categories);
        });
        this.productBusiness.getConstCat().then(function (products) {
            _this.constProducts = Object.values(products);
            _this.products = _this.constProducts;
            _this.products = _this.products.slice();
        });
        this.locationBusiness.get().then(function (locations) {
            _this.locations = Object.values(locations);
        });
        this.supplierBusiness.get().then(function (suppliers) {
            _this.suppliers = Object.values(suppliers);
        });
    };
    StockManageComponent.prototype.selectCategory = function (category) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var products, filterConstProducts, categories, index, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        if (!(category.products == null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.productBusiness.getByCategory(category.id)];
                    case 1:
                        products = _a.sent();
                        category.products = Object.values(products);
                        filterConstProducts = this.constProducts.filter(function (product) { return product.category !== category.id; });
                        category.products = category.products.concat(filterConstProducts);
                        _a.label = 2;
                    case 2:
                        if (!(category.childCategories == null)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.categoryBusiness.getChild(category.id)];
                    case 3:
                        categories = _a.sent();
                        category.childCategories = Object.values(categories);
                        _a.label = 4;
                    case 4:
                        index = this.categories.map(function (category) { return category.id; }).indexOf(category.id);
                        this.categories[index] = category;
                        this.products = category.products;
                        this.products = this.products.slice();
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        console.log('An error occurred selecting a category');
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    StockManageComponent.prototype.addProduct = function (product) {
        try {
            var productInline = new StockDiaryModel();
            productInline.product = product.id;
            productInline.productObject = product;
            productInline.units = 1;
            productInline.price = product.pricebuy;
            this.tableData.unshift(productInline);
            this.tableData = this.tableData.slice();
            this.selectedItem = 0;
            console.log(this.tableData);
        }
        catch (e) {
            console.log('An error occurred adding a product');
        }
    };
    StockManageComponent.prototype.getBuyValue = function (units, price) {
        try {
            return units * price;
        }
        catch (error) {
            console.log('An error occurred getting buy value');
        }
    };
    StockManageComponent.prototype.getRecord = function (row) {
        try {
            this.selectedItem = row;
        }
        catch (error) {
            console.log('An error occurred getting the record');
        }
    };
    StockManageComponent.prototype.deleteRecord = function () {
        try {
            this.tableData.splice(this.selectedItem, 1);
            this.tableData = this.tableData.slice();
        }
        catch (error) {
            console.log('An error occurred deletting the record');
        }
    };
    StockManageComponent.prototype.deleteOrder = function () {
        try {
            this.tableData = [];
            this.tableData = this.tableData.slice();
            this.confirmDeletionOrder.hide();
        }
        catch (error) {
            console.log('An error occurred deletting the order');
        }
    };
    StockManageComponent.prototype.saveOrder = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var save, error_1;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.submit = true;
                        if (!this.manageForm.valid) return [3 /*break*/, 2];
                        debugger;
                        this.tableData.forEach(function (row) {
                            row.supplier = _this.manageForm.controls['supplier'].value;
                            row.reason = parseInt(_this.manageForm.controls['reason'].value);
                            row.location = _this.manageForm.controls['location'].value;
                            row.datenew = _this.manageForm.controls['date'].value;
                            row.supplierdoc = _this.manageForm.controls['document'].value;
                        });
                        return [4 /*yield*/, this.stockBusiness.saveStockDiary(this.tableData)];
                    case 1:
                        save = _a.sent();
                        this.toast.showMessage('La transacción ah sido almacenada con exito!!', 'success');
                        this.clearForm();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        console.log('An error occurred saving the order');
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    StockManageComponent.prototype.editPriceBuy = function () {
        try {
            this.tableData[this.selectedItem].price = this.newPriceBuy;
            this.editPriceBuyModal.hide();
            this.newPriceBuy = 1;
        }
        catch (error) {
            console.log('An error occurred editing the order');
        }
    };
    StockManageComponent.prototype.clearForm = function () {
        try {
            this.tableData = [];
            this.manageForm.reset();
            this.manageForm.controls['date'].setValue(new Date());
            this.manageForm.controls['reason'].setValue('');
            this.manageForm.controls['location'].setValue('');
            this.manageForm.controls['supplier'].setValue('');
            this.manageForm.controls['document'].setValue('');
            this.submit = false;
        }
        catch (err) {
            console.log('An error ocurred clearing the form');
        }
    };
    StockManageComponent.prototype.openFindProductModal = function () {
        var _this = this;
        try {
            this.findProductModal = this.modalService.show(FindProductComponent, { class: 'modal-lg' });
            this.findProductModal.content.onClick.subscribe(function (product) {
                _this.addProduct(product);
            });
        }
        catch (error) {
            console.log('An error ocurred opening find product modal: ', error);
        }
    };
    tslib_1.__decorate([
        ViewChild(BsDatepickerDirective),
        tslib_1.__metadata("design:type", BsDatepickerDirective)
    ], StockManageComponent.prototype, "datepicker", void 0);
    tslib_1.__decorate([
        ViewChild('confirmDeletionOrder'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], StockManageComponent.prototype, "confirmDeletionOrder", void 0);
    tslib_1.__decorate([
        ViewChild('editPriceBuyModal'),
        tslib_1.__metadata("design:type", ModalDirective)
    ], StockManageComponent.prototype, "editPriceBuyModal", void 0);
    tslib_1.__decorate([
        HostListener('window:scroll'),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], StockManageComponent.prototype, "onScrollEvent", null);
    StockManageComponent = tslib_1.__decorate([
        Component({
            selector: 'app-stock-manage',
            templateUrl: './stock-manage.component.html',
            styleUrls: ['./stock-manage.component.scss'],
            animations: [],
        }),
        tslib_1.__metadata("design:paramtypes", [BsModalService,
            ProductBusiness,
            CategoryBusiness,
            SupplierBusiness,
            LocationBusiness,
            StockBusiness,
            ToastService])
    ], StockManageComponent);
    return StockManageComponent;
}());
export { StockManageComponent };
//# sourceMappingURL=stock-manage.component.js.map