import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var StockMenuComponent = /** @class */ (function () {
    function StockMenuComponent() {
        this.module = {
            name: 'Inventory',
            manage: 'Manage',
            reports: 'Reports',
            manageItems: [],
            reportsItems: []
        };
        this.module.manageItems.push({
            name: "Productos",
            icon: 'fa fa-shopping-cart fa-lg',
            url: '/stock/product-manage'
        });
        this.module.manageItems.push({
            name: "Categorias",
            icon: 'fa fa-shopping-cart fa-lg',
            url: '/stock/category-manage'
        });
        this.module.manageItems.push({
            name: "Gestion de Inventario",
            icon: 'fa fa-shopping-cart fa-lg',
            url: '/stock/stock-manage'
        });
        for (var i = 0; i < 11; i++) {
            this.module.reportsItems.push({
                name: "Option " + i,
                icon: 'fa fa-home fa-lg',
                url: '/sales'
            });
        }
    }
    StockMenuComponent.prototype.ngOnInit = function () {
    };
    StockMenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-stock-menu',
            templateUrl: './stock-menu.component.html',
            styleUrls: ['./stock-menu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], StockMenuComponent);
    return StockMenuComponent;
}());
export { StockMenuComponent };
//# sourceMappingURL=stock-menu.component.js.map