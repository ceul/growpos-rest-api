import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StockMenuComponent } from './stock-menu/stock-menu.component';
import { ProductManageComponent } from './product-manage/product-manage.component';
import { CategoryManageComponent } from './category-manage/category-manage.component';
import { StockManageComponent } from './stock-manage/stock-manage.component';
var routes = [
    {
        path: '',
        data: {
            title: 'Stock'
        },
        children: [
            {
                path: 'menu',
                component: StockMenuComponent,
                data: {
                    title: 'Stock menu'
                }
            },
            {
                path: 'product-manage',
                component: ProductManageComponent,
                data: {
                    title: 'Product Manage'
                }
            },
            {
                path: 'category-manage',
                component: CategoryManageComponent,
                data: {
                    title: 'Category Manage'
                }
            },
            {
                path: 'stock-manage',
                component: StockManageComponent,
                data: {
                    title: 'Stock Manage'
                }
            }
        ]
    }
];
var StockRoutingModule = /** @class */ (function () {
    function StockRoutingModule() {
    }
    StockRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], StockRoutingModule);
    return StockRoutingModule;
}());
export { StockRoutingModule };
//# sourceMappingURL=stock-routing.module.js.map