import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockRoutingModule } from './stock-routing.module';
import { StockMenuComponent } from './stock-menu/stock-menu.component';
import { ProductManageComponent } from './product-manage/product-manage.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ComponentsModule } from '../../components/components.module';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { CreateSupplierComponent } from '../../components/create-supplier/create-supplier.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { CategoryManageComponent } from './category-manage/category-manage.component';
import { StockManageComponent } from './stock-manage/stock-manage.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FindProductComponent } from '../../components/find-product/find-product.component';
var StockModule = /** @class */ (function () {
    function StockModule() {
    }
    StockModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                StockMenuComponent,
                ProductManageComponent,
                CategoryManageComponent,
                StockManageComponent,
            ],
            imports: [
                CommonModule,
                StockRoutingModule,
                ModalModule.forRoot(),
                TabsModule,
                ComponentsModule,
                FormsModule,
                ReactiveFormsModule,
                CdkTableModule,
                MatInputModule,
                MatTableModule,
                MatPaginatorModule,
                BsDatepickerModule.forRoot(),
            ],
            providers: [
                BsModalService,
            ],
            bootstrap: [
                CreateSupplierComponent,
                FindProductComponent
            ]
        })
    ], StockModule);
    return StockModule;
}());
export { StockModule };
//# sourceMappingURL=stock.module.js.map