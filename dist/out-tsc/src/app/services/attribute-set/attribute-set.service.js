import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var AttributeSetService = /** @class */ (function (_super) {
    tslib_1.__extends(AttributeSetService, _super);
    function AttributeSetService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'AttributeSetService';
        return _this;
    }
    AttributeSetService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/attribute-set', data);
        }
        catch (e) {
            console.log("An error occurred saving attribute-set " + this.save.name + " --> " + this.className);
        }
    };
    AttributeSetService.prototype.get = function () {
        try {
            return this.consumeAPI('/attribute-set/get');
        }
        catch (e) {
            console.log("An error occurred getting attribute-sets: " + this.get.name + " --> " + this.className);
        }
    };
    AttributeSetService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/attribute-set/getById');
        }
        catch (e) {
            console.log("An error occurred getting attribute-set by id: " + this.getById.name + " --> " + this.className);
        }
    };
    AttributeSetService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/attribute-set/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting attribute-set: " + this.update.name + " --> " + this.className);
        }
    };
    AttributeSetService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/attribute-set/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting attribute-set: " + this.delete.name + " --> " + this.className);
        }
    };
    AttributeSetService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], AttributeSetService);
    return AttributeSetService;
}(BaseServiceService));
export { AttributeSetService };
//# sourceMappingURL=attribute-set.service.js.map