import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var SupplierService = /** @class */ (function (_super) {
    tslib_1.__extends(SupplierService, _super);
    function SupplierService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'SupplierService';
        return _this;
    }
    SupplierService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/supplier', data);
        }
        catch (e) {
            console.log("An error occurred saving supplier " + this.save.name + " --> " + this.className);
        }
    };
    SupplierService.prototype.get = function () {
        try {
            return this.consumeAPI('/supplier/get');
        }
        catch (e) {
            console.log("An error occurred getting suppliers: " + this.get.name + " --> " + this.className);
        }
    };
    SupplierService.prototype.getVisible = function () {
        try {
            return this.consumeAPI('/supplier/getVisible');
        }
        catch (e) {
            console.log("An error occurred getting visible suppliers: " + this.getVisible.name + " --> " + this.className);
        }
    };
    SupplierService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/supplier/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting supplier by id: " + this.getById.name + " --> " + this.className);
        }
    };
    SupplierService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/supplier/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting supplier " + this.update.name + " --> " + this.className);
        }
    };
    SupplierService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/supplier/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting supplier: " + this.delete.name + " --> " + this.className);
        }
    };
    SupplierService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], SupplierService);
    return SupplierService;
}(BaseServiceService));
export { SupplierService };
//# sourceMappingURL=supplier.service.js.map