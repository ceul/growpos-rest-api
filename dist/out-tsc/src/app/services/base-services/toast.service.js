import * as tslib_1 from "tslib";
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Message } from '../../models/message.model';
var ToastService = /** @class */ (function () {
    function ToastService() {
        this.addMessage = new EventEmitter();
    }
    ToastService.prototype.showMessage = function (content, style) {
        var message = new Message(content, style);
        this.addMessage.emit(message);
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToastService.prototype, "addMessage", void 0);
    ToastService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ToastService);
    return ToastService;
}());
export { ToastService };
//# sourceMappingURL=toast.service.js.map