import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var BaseServiceService = /** @class */ (function () {
    function BaseServiceService(http) {
        this.http = http;
        this.headers = {};
        this.headers['Content-type'] = 'application/json';
    }
    BaseServiceService.prototype.consumeAPI = function (url, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                _this.http.post('http://192.168.1.103:3000' + url, data, { headers: _this.headers })
                    .subscribe(function (response) {
                    resolve(response);
                }, function (error) {
                    console.log('An error occurred consumming the api: ', error);
                    reject(error);
                });
            }
            catch (err) {
                console.log('An error occurred consumming the api: ', err);
            }
        });
    };
    BaseServiceService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], BaseServiceService);
    return BaseServiceService;
}());
export { BaseServiceService };
//# sourceMappingURL=base-service.service.js.map