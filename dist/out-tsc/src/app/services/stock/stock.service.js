import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var StockService = /** @class */ (function (_super) {
    tslib_1.__extends(StockService, _super);
    function StockService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'StockService';
        return _this;
    }
    StockService.prototype.saveStockDiary = function (data) {
        try {
            return this.consumeAPI('/stock/stockDiary', data);
        }
        catch (e) {
            console.log("An error occurred saving stock diary: " + this.saveStockDiary.name + " --> " + this.className);
        }
    };
    StockService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], StockService);
    return StockService;
}(BaseServiceService));
export { StockService };
//# sourceMappingURL=stock.service.js.map