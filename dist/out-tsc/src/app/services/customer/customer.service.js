import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var CustomerService = /** @class */ (function (_super) {
    tslib_1.__extends(CustomerService, _super);
    function CustomerService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'CustomerService';
        return _this;
    }
    CustomerService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/customer', data);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerService.prototype.get = function () {
        try {
            return this.consumeAPI('/customer/get');
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/customer/getById');
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/customer/update', data);
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/customer/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred saving customer " + this.save.name + " --> " + this.className);
        }
    };
    CustomerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], CustomerService);
    return CustomerService;
}(BaseServiceService));
export { CustomerService };
//# sourceMappingURL=customer.service.js.map