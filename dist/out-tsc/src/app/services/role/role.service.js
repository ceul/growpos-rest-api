import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var RoleService = /** @class */ (function (_super) {
    tslib_1.__extends(RoleService, _super);
    function RoleService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'RoleService';
        return _this;
    }
    RoleService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/role', data);
        }
        catch (e) {
            console.log("An error occurred saving role " + this.save.name + " --> " + this.className);
        }
    };
    RoleService.prototype.get = function () {
        try {
            return this.consumeAPI('/role/get');
        }
        catch (e) {
            console.log("An error occurred getting roles: " + this.get.name + " --> " + this.className);
        }
    };
    RoleService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/role/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting role by id: " + this.getById.name + " --> " + this.className);
        }
    };
    RoleService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/role/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting role: " + this.update.name + " --> " + this.className);
        }
    };
    RoleService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/role/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting role: " + this.delete.name + " --> " + this.className);
        }
    };
    RoleService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], RoleService);
    return RoleService;
}(BaseServiceService));
export { RoleService };
//# sourceMappingURL=role.service.js.map