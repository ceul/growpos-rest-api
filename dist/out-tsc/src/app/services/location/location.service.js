import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var LocationService = /** @class */ (function (_super) {
    tslib_1.__extends(LocationService, _super);
    function LocationService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'LocationService';
        return _this;
    }
    LocationService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/location', data);
        }
        catch (e) {
            console.log("An error occurred saving location " + this.save.name + " --> " + this.className);
        }
    };
    LocationService.prototype.get = function () {
        try {
            return this.consumeAPI('/location/get');
        }
        catch (e) {
            console.log("An error occurred getting locations: " + this.get.name + " --> " + this.className);
        }
    };
    LocationService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/location/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting location by id: " + this.getById.name + " --> " + this.className);
        }
    };
    LocationService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/location/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting location: " + this.update.name + " --> " + this.className);
        }
    };
    LocationService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/location/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting location: " + this.delete.name + " --> " + this.className);
        }
    };
    LocationService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], LocationService);
    return LocationService;
}(BaseServiceService));
export { LocationService };
//# sourceMappingURL=location.service.js.map