import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var TaxCategoryService = /** @class */ (function (_super) {
    tslib_1.__extends(TaxCategoryService, _super);
    function TaxCategoryService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'TaxCategoryService';
        return _this;
    }
    TaxCategoryService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/tax-category', data);
        }
        catch (e) {
            console.log("An error occurred saving tax-category " + this.save.name + " --> " + this.className);
        }
    };
    TaxCategoryService.prototype.get = function () {
        try {
            return this.consumeAPI('/tax-category/get');
        }
        catch (e) {
            console.log("An error occurred getting tax-categories " + this.get.name + " --> " + this.className);
        }
    };
    TaxCategoryService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/tax-category/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting tax-category by id " + this.getById.name + " --> " + this.className);
        }
    };
    TaxCategoryService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/tax-category/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting tax-category " + this.update.name + " --> " + this.className);
        }
    };
    TaxCategoryService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/tax-category/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting tax-category " + this.delete.name + " --> " + this.className);
        }
    };
    TaxCategoryService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], TaxCategoryService);
    return TaxCategoryService;
}(BaseServiceService));
export { TaxCategoryService };
//# sourceMappingURL=tax-category.service.js.map