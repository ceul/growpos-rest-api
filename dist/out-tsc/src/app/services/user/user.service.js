import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var UserService = /** @class */ (function (_super) {
    tslib_1.__extends(UserService, _super);
    function UserService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'UserService';
        return _this;
    }
    UserService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/user', data);
        }
        catch (e) {
            console.log("An error occurred saving user " + this.save.name + " --> " + this.className);
        }
    };
    UserService.prototype.get = function () {
        try {
            return this.consumeAPI('/user/get');
        }
        catch (e) {
            console.log("An error occurred getting users " + this.get.name + " --> " + this.className);
        }
    };
    UserService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/user/getById');
        }
        catch (e) {
            console.log("An error occurred getting user by id " + this.getById.name + " --> " + this.className);
        }
    };
    UserService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/user/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting user " + this.update.name + " --> " + this.className);
        }
    };
    UserService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/user/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting user " + this.delete.name + " --> " + this.className);
        }
    };
    UserService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], UserService);
    return UserService;
}(BaseServiceService));
export { UserService };
//# sourceMappingURL=user.service.js.map