import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var UnitMeasureService = /** @class */ (function (_super) {
    tslib_1.__extends(UnitMeasureService, _super);
    function UnitMeasureService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'UnitMeasureService';
        return _this;
    }
    UnitMeasureService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/unit-measure', data);
        }
        catch (e) {
            console.log("An error occurred saving unit-measure " + this.save.name + " --> " + this.className);
        }
    };
    UnitMeasureService.prototype.get = function () {
        try {
            return this.consumeAPI('/unit-measure/get');
        }
        catch (e) {
            console.log("An error occurred getting units-measure " + this.get.name + " --> " + this.className);
        }
    };
    UnitMeasureService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/unit-measure/getById');
        }
        catch (e) {
            console.log("An error occurred getting unit-measure by id " + this.getById.name + " --> " + this.className);
        }
    };
    UnitMeasureService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/unit-measure/update', data);
        }
        catch (e) {
            console.log("An error occurred updatting unit-measure " + this.update.name + " --> " + this.className);
        }
    };
    UnitMeasureService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/unit-measure/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred delleting unit-measure " + this.delete.name + " --> " + this.className);
        }
    };
    UnitMeasureService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], UnitMeasureService);
    return UnitMeasureService;
}(BaseServiceService));
export { UnitMeasureService };
//# sourceMappingURL=unit-measure.service.js.map