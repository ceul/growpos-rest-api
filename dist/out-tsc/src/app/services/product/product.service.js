import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var ProductService = /** @class */ (function (_super) {
    tslib_1.__extends(ProductService, _super);
    function ProductService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'ProductService';
        return _this;
    }
    ProductService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/product', data);
        }
        catch (e) {
            console.log("An error occurred saving product " + this.save.name + " --> " + this.className);
        }
    };
    ProductService.prototype.get = function () {
        try {
            return this.consumeAPI('/product/get');
        }
        catch (e) {
            console.log("An error occurred saving product " + this.get.name + " --> " + this.className);
        }
    };
    ProductService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/product/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred saving product " + this.getById.name + " --> " + this.className);
        }
    };
    ProductService.prototype.getByCategory = function (id) {
        try {
            return this.consumeAPI('/product/getByCategory', { id: id });
        }
        catch (e) {
            console.log("An error occurred saving product " + this.getByCategory.name + " --> " + this.className);
        }
    };
    ProductService.prototype.getFiltered = function (filter) {
        try {
            return this.consumeAPI('/product/getFiltered', filter);
        }
        catch (e) {
            console.log("An error occurred getting product filtered: " + this.getFiltered.name + " --> " + this.className);
        }
    };
    ProductService.prototype.getConstCat = function () {
        try {
            return this.consumeAPI('/product/getConstCat');
        }
        catch (e) {
            console.log("An error occurred saving product " + this.getConstCat.name + " --> " + this.className);
        }
    };
    ProductService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/product/update', data);
        }
        catch (e) {
            console.log("An error occurred saving product " + this.update.name + " --> " + this.className);
        }
    };
    ProductService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/product/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred saving product " + this.delete.name + " --> " + this.className);
        }
    };
    ProductService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ProductService);
    return ProductService;
}(BaseServiceService));
export { ProductService };
//# sourceMappingURL=product.service.js.map