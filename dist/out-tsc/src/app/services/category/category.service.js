import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
var CategoryService = /** @class */ (function (_super) {
    tslib_1.__extends(CategoryService, _super);
    function CategoryService(http) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.className = 'CategoryService';
        return _this;
    }
    CategoryService.prototype.save = function (data) {
        try {
            return this.consumeAPI('/category', data);
        }
        catch (e) {
            console.log("An error occurred saving category " + this.save.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.get = function () {
        try {
            return this.consumeAPI('/category/get');
        }
        catch (e) {
            console.log("An error occurred getting categories " + this.get.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.getById = function (id) {
        try {
            return this.consumeAPI('/category/getById', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting category by id " + this.getById.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.getRoot = function () {
        try {
            return this.consumeAPI('/category/getRoot');
        }
        catch (e) {
            console.log("An error occurred getting root categories " + this.getRoot.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.getChild = function (id) {
        try {
            return this.consumeAPI('/category/getChild', { id: id });
        }
        catch (e) {
            console.log("An error occurred getting child categories " + this.getChild.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.update = function (data) {
        try {
            return this.consumeAPI('/category/update', data);
        }
        catch (e) {
            console.log("An error occurred updating category " + this.update.name + " --> " + this.className);
        }
    };
    CategoryService.prototype.delete = function (id) {
        try {
            return this.consumeAPI('/category/delete', { id: id });
        }
        catch (e) {
            console.log("An error occurred deletting category " + this.delete.name + " --> " + this.className);
        }
    };
    CategoryService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], CategoryService);
    return CategoryService;
}(BaseServiceService));
export { CategoryService };
//# sourceMappingURL=category.service.js.map