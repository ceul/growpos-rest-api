import * as tslib_1 from "tslib";
import { Injectable, Output, EventEmitter } from '@angular/core';
var ToolBarItemListService = /** @class */ (function () {
    function ToolBarItemListService() {
        this.className = "ToolBarItemListService";
        this.itemSelected = new EventEmitter();
        this.btnAngleLeft = new EventEmitter();
        this.btnLeft = new EventEmitter();
        this.btnDown = new EventEmitter();
        this.btnRight = new EventEmitter();
        this.btnAngleRight = new EventEmitter();
        this.btnRepeat = new EventEmitter();
        this.btnSearch = new EventEmitter();
        this.btnSort = new EventEmitter();
        this.angleLeft = new EventEmitter();
        this.left = new EventEmitter();
        this.down = new EventEmitter();
        this.right = new EventEmitter();
        this.angleRight = new EventEmitter();
    }
    //------------------------------- item Selected ---------------------------------
    ToolBarItemListService.prototype.itemSelectedFunction = function (item) {
        try {
            this.itemSelected.emit(item);
        }
        catch (e) {
            console.log("An error ocorrued on " + this.itemSelectedFunction.name + " -> " + this.className);
        }
    };
    //------------------------------- btn disabled state --------------------------
    ToolBarItemListService.prototype.angleLeftState = function (state) {
        try {
            this.angleLeft.emit(state);
        }
        catch (e) {
            console.log("An error occurred on " + this.angleLeftState.name + " -> " + this.className);
        }
    };
    ToolBarItemListService.prototype.leftState = function (state) {
        try {
            this.left.emit(state);
        }
        catch (e) {
            console.log("An error occurred on " + this.leftState.name + " -> " + this.className);
        }
    };
    ToolBarItemListService.prototype.downState = function (state) {
        try {
            this.down.emit(state);
        }
        catch (e) {
            console.log("An error occurred on " + this.downState.name + " -> " + this.className);
        }
    };
    ToolBarItemListService.prototype.rightState = function (state) {
        try {
            this.right.emit(state);
        }
        catch (e) {
            console.log("An error occurred on " + this.rightState.name + " -> " + this.className);
        }
    };
    ToolBarItemListService.prototype.angleRightState = function (state) {
        try {
            this.angleRight.emit(state);
        }
        catch (e) {
            console.log("An error occurred on " + this.angleRightState.name + " -> " + this.className);
        }
    };
    //------------------------------ On click action --------------------------------------
    ToolBarItemListService.prototype.btnAngleLeftOnClick = function () {
        try {
            this.btnAngleLeft.emit();
        }
        catch (e) {
            console.log('An error occured clicking button AngleLeft');
        }
    };
    ToolBarItemListService.prototype.btnLeftOnClick = function () {
        try {
            this.btnLeft.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Left');
        }
    };
    ToolBarItemListService.prototype.btnDownOnClick = function () {
        try {
            this.btnDown.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Down');
        }
    };
    ToolBarItemListService.prototype.btnRightOnClick = function () {
        try {
            this.btnRight.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Right');
        }
    };
    ToolBarItemListService.prototype.btnAngleRightOnClick = function () {
        try {
            this.btnAngleRight.emit();
        }
        catch (e) {
            console.log('An error occured clicking button AngleRight');
        }
    };
    ToolBarItemListService.prototype.btnRepeatOnClick = function () {
        try {
            this.btnRepeat.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Repeat');
        }
    };
    ToolBarItemListService.prototype.btnSearchOnClick = function () {
        try {
            this.btnSearch.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Search');
        }
    };
    ToolBarItemListService.prototype.btnSortOnClick = function () {
        try {
            this.btnSort.emit();
        }
        catch (e) {
            console.log('An error occured clicking button Sort');
        }
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "itemSelected", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnAngleLeft", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnLeft", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnDown", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnRight", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnAngleRight", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnRepeat", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnSearch", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "btnSort", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "angleLeft", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "left", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "down", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "right", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], ToolBarItemListService.prototype, "angleRight", void 0);
    ToolBarItemListService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ToolBarItemListService);
    return ToolBarItemListService;
}());
export { ToolBarItemListService };
//# sourceMappingURL=tool-bar-item-list.service.js.map