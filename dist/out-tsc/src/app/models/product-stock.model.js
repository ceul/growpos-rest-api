var ProductStockModel = /** @class */ (function () {
    function ProductStockModel(init) {
        if (init) {
            this.id = init.id; //|| this.id;
            this.location = init.location;
            this.units = init.units;
            this.minimum = init.minimum;
            this.maximum = init.maximum;
            this.pricebuy = init.pricebuy;
            this.pricesell = init.pricesell;
        }
        else {
            Object.assign(this, init);
        }
    }
    return ProductStockModel;
}());
export default ProductStockModel;
//# sourceMappingURL=product-stock.model.js.map