var ProductModel = /** @class */ (function () {
    function ProductModel(init) {
        if (init) {
            this.id = init.id; //|| this.id;
            this.reference = init.reference; //|| this.reference;
            this.code = init.code; //|| this.code;
            this.codetype = init.codetype; //|| this.codetype;
            this.name = init.name; //|| this.name;
            this.pricebuy = init.pricebuy; //|| this.pricebuy;
            this.pricesell = init.pricesell; //|| this.pricesell;
            this.category = init.category; //|| this.category;
            this.taxcat = init.taxcat; //|| this.taxcat;
            this.attributeset_id = init.attributeset_id; //|| this.attributeset_id;
            this.stockcost = init.stockcost; //|| this.stockcost;
            this.stockvolume = init.stockvolume; //|| this.stockvolume;
            this.image = init.image; //|| this.image;
            this.iscom = init.iscom; //|| this.iscom;
            this.isscale = init.isscale; //|| this.isscale;
            this.isconstant = init.isconstant; //|| this.isconstant;
            this.printkb = init.printkb; //|| this.printkb;
            this.sendstatus = init.sendstatus; //|| this.sendstatus;
            this.isservice = init.isservice; //|| this.isservice;
            this.attributes = init.attributes; //|| this.attributes;
            this.display = init.display; //|| this.display;
            this.isvprice = init.isvprice; //|| this.isvprice;
            this.isverpatrib = init.isverpatrib; //|| this.isverpatrib;
            this.texttip = init.texttip; //|| this.texttip;
            this.warranty = init.warranty; //|| this.warranty;
            this.stockunits = init.stockunits; //|| this.stockunits;
            this.printto = init.printto; //|| this.printto;
            this.supplier = init.supplier; //|| this.supplier;
            this.uom = init.uom; //|| this.uom;
            this.flag = init.flag; //|| this.flag;
            this.description = init.description; //|| this.description;
            this.short_description = init.short_description; //|| this.short_description;
            this.weigth = init.weigth; //|| this.weigth;
            this.length = init.length; //|| this.length;
            this.height = init.height; //|| this.height;
            this.width = init.width; //|| this.width;
            this.catorder = init.catorder;
            this.inCat = init.inCat;
        }
        else {
            Object.assign(this, init);
        }
    }
    return ProductModel;
}());
export default ProductModel;
//# sourceMappingURL=product.model.js.map