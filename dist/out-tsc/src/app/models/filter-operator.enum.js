var FilterOperatorEnum;
(function (FilterOperatorEnum) {
    FilterOperatorEnum[FilterOperatorEnum["None"] = 1] = "None";
    FilterOperatorEnum[FilterOperatorEnum["Equals"] = 2] = "Equals";
    FilterOperatorEnum[FilterOperatorEnum["Distinct"] = 3] = "Distinct";
    FilterOperatorEnum[FilterOperatorEnum["Greater"] = 4] = "Greater";
    FilterOperatorEnum[FilterOperatorEnum["Less"] = 5] = "Less";
    FilterOperatorEnum[FilterOperatorEnum["GreaterOrEqual"] = 6] = "GreaterOrEqual";
    FilterOperatorEnum[FilterOperatorEnum["LessOrEqual"] = 7] = "LessOrEqual";
})(FilterOperatorEnum || (FilterOperatorEnum = {}));
export default FilterOperatorEnum;
//# sourceMappingURL=filter-operator.enum.js.map