var Message = /** @class */ (function () {
    function Message(content, style) {
        this.dismissed = false;
        this.content = content;
        this.style = style || 'info';
    }
    return Message;
}());
export { Message };
//# sourceMappingURL=message.model.js.map