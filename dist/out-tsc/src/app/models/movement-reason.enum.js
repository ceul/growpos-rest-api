var MovementReasonEnum;
(function (MovementReasonEnum) {
    // IN values
    MovementReasonEnum[MovementReasonEnum["IN_PURCHASE"] = 1] = "IN_PURCHASE";
    MovementReasonEnum[MovementReasonEnum["IN_REFUND"] = 2] = "IN_REFUND";
    MovementReasonEnum[MovementReasonEnum["IN_MOVEMENT"] = 4] = "IN_MOVEMENT";
    // OUT values   
    MovementReasonEnum[MovementReasonEnum["OUT_SALE"] = -1] = "OUT_SALE";
    MovementReasonEnum[MovementReasonEnum["OUT_REFUND"] = -2] = "OUT_REFUND";
    MovementReasonEnum[MovementReasonEnum["OUT_BREAK"] = -3] = "OUT_BREAK";
    MovementReasonEnum[MovementReasonEnum["OUT_MOVEMENT"] = -4] = "OUT_MOVEMENT";
    MovementReasonEnum[MovementReasonEnum["OUT_SAMPLE"] = -5] = "OUT_SAMPLE";
    MovementReasonEnum[MovementReasonEnum["OUT_FREE"] = -6] = "OUT_FREE";
    MovementReasonEnum[MovementReasonEnum["OUT_USED"] = -7] = "OUT_USED";
    MovementReasonEnum[MovementReasonEnum["OUT_SUBTRACT"] = -8] = "OUT_SUBTRACT";
    // TRANSFER
    MovementReasonEnum[MovementReasonEnum["OUT_CROSSING"] = 1000] = "OUT_CROSSING";
})(MovementReasonEnum || (MovementReasonEnum = {}));
export default MovementReasonEnum;
//# sourceMappingURL=movement-reason.enum.js.map