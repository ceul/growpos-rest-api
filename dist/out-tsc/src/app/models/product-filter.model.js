var ProductFilterModel = /** @class */ (function () {
    function ProductFilterModel(init) {
        debugger;
        if (init) {
            this.barcode = init.barcode;
            this.category = init.category; //|| this.category;
            this.name = {
                filter: parseFloat(init['name.filter']),
                value: init['name.value']
            }; //|| this.name;
            this.pricebuy = {
                filter: parseFloat(init['pricebuy.filter']),
                value: parseFloat(init['pricebuy.value'])
            }; //|| this.pricebuy;
            this.pricesell = {
                filter: parseFloat(init['pricesell.filter']),
                value: parseFloat(init['pricesell.value'])
            }; //|| this.pricesell;
        }
        else {
            Object.assign(this, init);
        }
    }
    return ProductFilterModel;
}());
export default ProductFilterModel;
//# sourceMappingURL=product-filter.model.js.map