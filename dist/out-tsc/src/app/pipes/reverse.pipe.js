import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { reverse } from 'lodash';
var ReversePipe = /** @class */ (function () {
    function ReversePipe() {
    }
    ReversePipe.prototype.transform = function (value) {
        if (!value)
            return;
        return reverse(value);
    };
    ReversePipe = tslib_1.__decorate([
        Pipe({
            name: 'reverse'
        })
    ], ReversePipe);
    return ReversePipe;
}());
export { ReversePipe };
//# sourceMappingURL=reverse.pipe.js.map