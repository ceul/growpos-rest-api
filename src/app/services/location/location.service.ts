import { Injectable } from '@angular/core';
import LocationModel from '../../models/location.model';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class LocationService extends BaseServiceService {

	className = 'LocationService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: LocationModel) {
		try {
			return this.consumeAPI('/location', data);
		} catch (e) {
			console.log(`An error occurred saving location ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/location/get');
		} catch (e) {
			console.log(`An error occurred getting locations: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/location/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting location by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: LocationModel) {
		try {
			return this.consumeAPI('/location/update', data);
		} catch (e) {
			console.log(`An error occurred updatting location: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/location/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting location: ${this.delete.name} --> ${this.className}`)
		}
	}
}
