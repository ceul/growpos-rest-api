import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import TaxModel from '../../models/tax.model'
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class TaxService extends BaseServiceService {

	className = 'TaxService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: TaxModel) {
		try {
			return this.consumeAPI('/tax', data);
		} catch (e) {
			console.log(`An error occurred saving tax ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/tax/get');
		} catch (e) {
			console.log(`An error occurred getting tax ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/tax/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting tax by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: TaxModel) {
		try {
			return this.consumeAPI('/tax/update', data);
		} catch (e) {
			console.log(`An error occurred updatting tax ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/tax/delete', { id });
		} catch (e) {
			console.log(`An error occurred delleting tax ${this.delete.name} --> ${this.className}`)
		}
	}

	getTaxesByCategory(category: string) {
		try {
			return this.consumeAPI('/tax/getByCategory', { category });
		} catch (e) {
			console.log(`An error occurred getting total taxes by close cash ${this.getTaxesByCategory.name} --> ${this.className}`)
		}
	}

	getTotalTaxesByCloseCash(id: string) {
		try {
			return this.consumeAPI('/tax/getTotalByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting total taxes by close cash ${this.getTotalTaxesByCloseCash.name} --> ${this.className}`)
		}
	}
}
