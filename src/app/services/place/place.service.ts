import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import PlaceModel from 'src/app/models/place.model';
import { Storage } from '@ionic/storage';
@Injectable({
	providedIn: 'root'
})
export class PlaceService extends BaseServiceService {

	className = 'PlaceService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}


	save(data: PlaceModel) {
		try {
			return this.consumeAPI('/place', data);
		} catch (e) {
			console.log(`An error occurred saving place ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/place/get');
		} catch (e) {
			console.log(`An error occurred getting place ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/place/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting place by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: PlaceModel) {
		try {
			return this.consumeAPI('/place/update', data);
		} catch (e) {
			console.log(`An error occurred updatting place ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/place/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting place ${this.delete.name} --> ${this.className}`)
		}
	}
}