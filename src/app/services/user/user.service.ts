import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import PeopleModel from '../../models/people.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class UserService extends BaseServiceService {

	className = 'UserService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}


	save(data: PeopleModel) {
		try {
			return this.consumeAPI('/user', data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/user/get');
		} catch (e) {
			console.log(`An error occurred getting users ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/user/getById', this.getById.name);
		} catch (e) {
			console.log(`An error occurred getting user by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: PeopleModel) {
		try {
			return this.consumeAPI('/user/update', data);
		} catch (e) {
			console.log(`An error occurred updatting user ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/user/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting user ${this.delete.name} --> ${this.className}`)
		}
	}

	getMenu(id: string, op?: any) {
		try {
			return this.consumeAPI('/user/get_menu', { id, op });
		} catch (e) {
			console.log(`An error occurred getting menu by id ${this.getMenu.name} --> ${this.className}`)
		}
	}

	getSubMenu(id: string) {
		try {
			return this.consumeAPI('/user/get_sub_menu', { id });
		} catch (e) {
			console.log(`An error occurred getting sub menu by id ${this.getSubMenu.name} --> ${this.className}`)
		}
	}
}
