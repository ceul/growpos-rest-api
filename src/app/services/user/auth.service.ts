import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import PeopleModel from '../../models/people.model';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseServiceService {

  className = 'AuthService';

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}

  val(data: PeopleModel) {
    try {
      return this.consumeAPI('/user/val', { name: data.id, apppassword: data.apppassword });
    } catch (e) {
      console.log(`An error has occurred validating the user: ${this.val.name} --> ${this.className}`)
    }
  }


}
