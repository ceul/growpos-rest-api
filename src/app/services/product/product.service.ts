import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import ProductModel from '../../models/product.model';
import ProductFilterModel from '../../models/product-filter.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class ProductService extends BaseServiceService {

	className = 'ProductService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}


	save(data: ProductModel) {
		try {
			return this.consumeAPI('/product', data);
		} catch (e) {
			console.log(`An error occurred saving product ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/product/get');
		} catch (e) {
			console.log(`An error occurred saving product ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/product/getById', { id });
		} catch (e) {
			console.log(`An error occurred saving product ${this.getById.name} --> ${this.className}`)
		}
	}

	getTotalByCloseCash(id: string) {
		try {
			return this.consumeAPI('/product/getTotalByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting product by close cash ${this.getTotalByCloseCash.name} --> ${this.className}`)
		}
	}

	getByBarCode(id: string) {
		try {
			return this.consumeAPI('/product/getByBarCode', { barCode: id });
		} catch (e) {
			console.log(`An error occurred saving product ${this.getByBarCode.name} --> ${this.className}`)
		}
	}

	getByCategory(id: string) {
		try {
			return this.consumeAPI('/product/getByCategory', { id });
		} catch (e) {
			console.log(`An error occurred saving product ${this.getByCategory.name} --> ${this.className}`)
		}
	}

	getFiltered(filter: ProductFilterModel) {
		try {
			return this.consumeAPI('/product/getFiltered', filter);
		} catch (e) {
			console.log(`An error occurred getting product filtered: ${this.getFiltered.name} --> ${this.className}`)
		}
	}

	getConstCat() {
		try {
			return this.consumeAPI('/product/getConstCat');
		} catch (e) {
			console.log(`An error occurred saving product ${this.getConstCat.name} --> ${this.className}`)
		}
	}

	getProductSalesReport(datestart: Date, dateend: Date) {
		try {
			return this.consumeAPI('/product/getProductSalesReport', { datestart, dateend });
		} catch (e) {
			console.log(`An error occurred getting product sales report ${this.getProductSalesReport.name} --> ${this.className}`)
		}
	}

	getProfitSales(dateStart: Date, dateEnd: Date) {
		try {
			return this.consumeAPI('/product/getProfitSales', { dateStart, dateEnd });
		} catch (e) {
			console.log(`An error occurred getting product sales report ${this.getProfitSales.name} --> ${this.className}`)
		}
	}

	update(data: ProductModel) {
		try {
			return this.consumeAPI('/product/update', data);
		} catch (e) {
			console.log(`An error occurred saving product ${this.update.name} --> ${this.className}`)
		}
	}

	uploadImage(img: File, id: string) {
		try {
			return this.consumeAPI(`/product/${id}/uploadPicture`, img, 'post', true);
		} catch (e) {
			console.log(`An error occurred uploading image of product ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/product/delete', { id });
		} catch (e) {
			console.log(`An error occurred saving product ${this.delete.name} --> ${this.className}`)
		}
	}

	getProductBestSelling() {
		try {
			return this.consumeAPI('/product/bs');
		} catch (e) {
			console.log(`An error occurred getting best selling: ${this.getProductBestSelling.name} --> ${this.className}`)
		}
	}

	getWithLowerInventory() {
		try {
			return this.consumeAPI('/product/li');
		} catch (e) {
			console.log(`An error occurred getting lower inventory: ${this.getFiltered.name} --> ${this.className}`)
		}
	}

	print() {

	}
}
