import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import ContractModel from 'src/app/models/contract.model';
@Injectable({
  providedIn: 'root'
})
export class ContractService extends BaseServiceService {

	className = 'ContractService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: ContractModel) {
		try {
			return this.consumeAPI('/contract', data);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/contract/get');
		} catch (e) {
			console.log(`An error occurred getting contracts: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/contract/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting contract by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getByPlate(plate: string) {
		try {
			return this.consumeAPI('/contract/getByPlate', { plate });
		} catch (e) {
			console.log(`An error occurred getting contract by plate: ${this.getByPlate.name} --> ${this.className}`)
		}
	}

	update(data: ContractModel) {
		try {
			return this.consumeAPI('/contract/update', data);
		} catch (e) {
			console.log(`An error occurred updatting contract: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/contract/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting contract: ${this.delete.name} --> ${this.className}`)
		}
  }
}