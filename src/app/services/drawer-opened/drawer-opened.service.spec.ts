import { TestBed } from '@angular/core/testing';

import { DrawerOpenedService } from './drawer-opened.service';

describe('DrawerOpenedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawerOpenedService = TestBed.get(DrawerOpenedService);
    expect(service).toBeTruthy();
  });
});
