import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import DrawerOpenedModel from 'src/app/models/drawer-opened';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class DrawerOpenedService extends BaseServiceService {

	className = 'DrawerOpenedService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: DrawerOpenedModel) {
		try {
			return this.consumeAPI('/drawer-opened', data);
		} catch (e) {
			console.log(`An error occurred saving drawer-opened ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/drawer-opened/get');
		} catch (e) {
			console.log(`An error occurred getting drawer-opened ${this.get.name} --> ${this.className}`)
		}
	}

	getByUser(userName: string) {
		try {
			return this.consumeAPI('/drawer-opened/getByUser', { userName });
		} catch (e) {
			console.log(`An error occurred getting drawer-opened by user name ${this.getByUser.name} --> ${this.className}`)
		}
	}

	getByTicket(product: string) {
		try {
			return this.consumeAPI('/drawer-opened/getByTicket', { product });
		} catch (e) {
			console.log(`An error occurred getting drawer-opened by product ${this.getByTicket.name} --> ${this.className}`)
		}
	}
}