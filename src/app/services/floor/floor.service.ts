import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import FloorModel from 'src/app/models/floor.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class FloorService extends BaseServiceService {

	className = 'FloorService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: FloorModel) {
		try {
			return this.consumeAPI('/floor', data);
		} catch (e) {
			console.log(`An error occurred saving floor ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/floor/get');
		} catch (e) {
			console.log(`An error occurred getting floor ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/floor/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting floor by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getWithPlace(type: string) {
		try {
			return this.consumeAPI('/floor/getWithPlace',{type});
		} catch (e) {
			console.log(`An error occurred getting floor by id ${this.getWithPlace.name} --> ${this.className}`)
		}
	}

	update(data: FloorModel) {
		try {
			return this.consumeAPI('/floor/update', data);
		} catch (e) {
			console.log(`An error occurred updatting floor ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/floor/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting floor ${this.delete.name} --> ${this.className}`)
		}
	}
}
