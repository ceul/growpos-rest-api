import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import ParkingMovModel from 'src/app/models/parking_mov.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class ParkingService extends BaseServiceService {

	className = 'ParkingService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: ParkingMovModel) {
		try {
			return this.consumeAPI('/parking_mov', data);
		} catch (e) {
			console.log(`An error occurred saving parking mov ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/parking_mov/get');
		} catch (e) {
			console.log(`An error occurred getting parking mov ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/parking_mov/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting parking mov by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getWithPlace(type: string) {
		try {
			return this.consumeAPI('/parking_mov/getWithPlace',{type});
		} catch (e) {
			console.log(`An error occurred getting parking mov by id ${this.getWithPlace.name} --> ${this.className}`)
		}
	}

	update(data: ParkingMovModel) {
		try {
			return this.consumeAPI('/parking_mov/update', data);
		} catch (e) {
			console.log(`An error occurred updatting parking mov ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/parking_mov/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting parking mov ${this.delete.name} --> ${this.className}`)
		}
	}
}
