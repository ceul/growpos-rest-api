import { TestBed } from '@angular/core/testing';

import { HeaderBodyService } from './header-body.service';

describe('HeaderBodyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HeaderBodyService = TestBed.get(HeaderBodyService);
    expect(service).toBeTruthy();
  });
});
