import { Injectable, Output, EventEmitter } from '@angular/core';

export interface NavData {
  url?: string;
  title?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class HeaderBodyService {

  className= "HeaderBodyService"

  @Output() table: EventEmitter<null> = new EventEmitter();
  @Output() customer: EventEmitter<null> = new EventEmitter();
  @Output() user: EventEmitter<null> = new EventEmitter();
  @Output() discount: EventEmitter<null> = new EventEmitter();

  @Output() navBar: EventEmitter<null> = new EventEmitter();

  constructor() { }

  tableFunction(item){
    try{
      this.table.emit(item)
    } catch(e){
      console.log(`An error ocorrued on ${this.tableFunction.name} -> ${this.className}`)
    }
  }

  customerFunction(item){
    try{
      this.customer.emit(item)
    } catch(e){
      console.log(`An error ocorrued on ${this.customerFunction.name} -> ${this.className}`)
    }
  }

  discountFunction(item){
    try{
      this.discount.emit(item)
    } catch(e){
      console.log(`An error ocorrued on ${this.discountFunction.name} -> ${this.className}`)
    }
  }

  userFunction(item){
    try{
      this.user.emit(item)
    } catch(e){
      console.log(`An error ocorrued on ${this.userFunction.name} -> ${this.className}`)
    }
  }

  navbarFunction(){
    try{
      this.navBar.emit()
    } catch(e){
      console.log(`An error ocorrued on ${this.navbarFunction.name} -> ${this.className}`)
    }
  }

}
