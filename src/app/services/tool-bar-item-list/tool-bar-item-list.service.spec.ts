import { TestBed } from '@angular/core/testing';

import { ToolBarItemListService } from './tool-bar-item-list.service';

describe('ToolBarItemListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToolBarItemListService = TestBed.get(ToolBarItemListService);
    expect(service).toBeTruthy();
  });
});
