import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToolBarItemListService {

  className= "ToolBarItemListService"

  constructor() { }

  @Output() itemSelected: EventEmitter<null> = new EventEmitter();

  @Output() btnAngleLeft: EventEmitter<null> = new EventEmitter();
  @Output() btnLeft: EventEmitter<null> = new EventEmitter();
  @Output() btnDown: EventEmitter<null> = new EventEmitter();
  @Output() btnRight: EventEmitter<null> = new EventEmitter();
  @Output() btnAngleRight: EventEmitter<null> = new EventEmitter();
  @Output() btnRepeat: EventEmitter<null> = new EventEmitter();
  @Output() btnSearch: EventEmitter<null> = new EventEmitter();
  @Output() btnSort: EventEmitter<null> = new EventEmitter();

  @Output() angleLeft: EventEmitter<null> = new EventEmitter();
  @Output() left: EventEmitter<null> = new EventEmitter();
  @Output() down: EventEmitter<null> = new EventEmitter();
  @Output() right: EventEmitter<null> = new EventEmitter();
  @Output() angleRight: EventEmitter<null> = new EventEmitter();

  //------------------------------- item Selected ---------------------------------

  itemSelectedFunction(item){
    try{
      this.itemSelected.emit(item)
    } catch(e){
      console.log(`An error ocorrued on ${this.itemSelectedFunction.name} -> ${this.className}`)
    }
  }
  //------------------------------- btn disabled state --------------------------
  
  angleLeftState(state){
    try{
      this.angleLeft.emit(state)
    } catch(e){
      console.log(`An error occurred on ${this.angleLeftState.name} -> ${this.className}`)
    }
  }

  leftState(state){
    try{
      this.left.emit(state)
    } catch(e){
      console.log(`An error occurred on ${this.leftState.name} -> ${this.className}`)
    }
  }

  downState(state){
    try{
      this.down.emit(state)
    } catch(e){
      console.log(`An error occurred on ${this.downState.name} -> ${this.className}`)
    }
  }

  rightState(state){
    try{
      this.right.emit(state)
    } catch(e){
      console.log(`An error occurred on ${this.rightState.name} -> ${this.className}`)
    }
  }

  angleRightState(state){
    try{
      this.angleRight.emit(state)
    } catch(e){
      console.log(`An error occurred on ${this.angleRightState.name} -> ${this.className}`)
    }
  }

  //------------------------------ On click action --------------------------------------
  btnAngleLeftOnClick() {
    try{
      this.btnAngleLeft.emit()
    }catch(e){
      console.log('An error occured clicking button AngleLeft')
    }
  }

  btnLeftOnClick() {
    try{
      this.btnLeft.emit()
    }catch(e){
      console.log('An error occured clicking button Left')
    }
  }

  btnDownOnClick() {
    try{
      this.btnDown.emit()
    }catch(e){
      console.log('An error occured clicking button Down')
    }
  }

  btnRightOnClick() {
    try{
      this.btnRight.emit()
    }catch(e){
      console.log('An error occured clicking button Right')
    }
  }

  btnAngleRightOnClick() {
    try{
      this.btnAngleRight.emit()
    }catch(e){
      console.log('An error occured clicking button AngleRight')
    }
  }

  btnRepeatOnClick() {
    try{
      this.btnRepeat.emit()
    }catch(e){
      console.log('An error occured clicking button Repeat')
    }
  }

  btnSearchOnClick() {
    try{
      this.btnSearch.emit()
    }catch(e){
      console.log('An error occured clicking button Search')
    }
  }

  btnSortOnClick() {
    try{
      this.btnSort.emit()
    }catch(e){
      console.log('An error occured clicking button Sort')
    }
  }

}
