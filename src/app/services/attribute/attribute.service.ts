import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import AttributeModel from '../../models/attribute.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AttributeService extends BaseServiceService{

  className = 'AttributeService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}

	save(data: AttributeModel) {
		try {
			return this.consumeAPI('/attribute',data);
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/attribute/get');
		} catch (e) {
			console.log(`An error occurred getting attribut: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/attribute/getById', {id});
		} catch (e) {
			console.log(`An error occurred getting attribute by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getByAttributeSet(id: string) {
		try {
			return this.consumeAPI('/attribute/getByAttributeSet', {id});
		} catch (e) {
			console.log(`An error occurred getting attribute by attribute set: ${this.getByAttributeSet.name} --> ${this.className}`)
		}
	}

	update(data: AttributeModel) {
		try {
			return this.consumeAPI('/attribute/update', data);
		} catch (e) {
			console.log(`An error occurred updatting attribute: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/attribute/delete', {id});
		} catch (e) {
			console.log(`An error occurred deletting attribute: ${this.delete.name} --> ${this.className}`)
		}
	}
}
