import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import CategoryModel from '../../models/category.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class CategoryService extends BaseServiceService {

	className = 'CategoryService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}

	save(data: CategoryModel) {
		try {
			return this.consumeAPI('/category', data);
		} catch (e) {
			console.log(`An error occurred saving category ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/category/get');
		} catch (e) {
			console.log(`An error occurred getting categories ${this.get.name} --> ${this.className}`)
		}
	}

	getParking() {
		try {
			return this.consumeAPI('/category/getParking');
		} catch (e) {
			console.log(`An error occurred getting parking categories ${this.getParking.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/category/getById',{ id });
		} catch (e) {
			console.log(`An error occurred getting category by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getRoot() {
		try {
			return this.consumeAPI('/category/getRoot');
		} catch (e) {
			console.log(`An error occurred getting root categories ${this.getRoot.name} --> ${this.className}`)
		}
	}

	getChild(id: string) {
		try {
			return this.consumeAPI('/category/getChild',{ id });
		} catch (e) {
			console.log(`An error occurred getting child categories ${this.getChild.name} --> ${this.className}`)
		}
	}

	getSalesReport(datestart: Date, dateend: Date) {
		try {
			return this.consumeAPI('/category/getSalesReport',{ datestart, dateend });
		} catch (e) {
			console.log(`An error occurred getting sales categories report ${this.getSalesReport.name} --> ${this.className}`)
		}
	}

	update(data: CategoryModel) {
		try {
			return this.consumeAPI('/category/update', data);
		} catch (e) {
			console.log(`An error occurred updating category ${this.update.name} --> ${this.className}`)
		}
	}

	uploadImage(img: File, id: string) {
		try {
			return this.consumeAPI(`/category/${id}/uploadPicture`, img, 'post', true);
		} catch (e) {
			console.log(`An error occurred uploading image of category ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/category/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting category ${this.delete.name} --> ${this.className}`)
		}
	}

	getBySales() {
		try {
			return this.consumeAPI('/category/getBySales');
		} catch (e) {
			console.log(`An error occurred getting category by sales ${this.className}`)
		}
	}
}
