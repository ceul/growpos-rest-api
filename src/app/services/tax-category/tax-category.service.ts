import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import TaxCategoryModel from '../../models/tax-category.model';
import { Storage } from '@ionic/storage';
@Injectable({
	providedIn: 'root'
})
export class TaxCategoryService extends BaseServiceService {

	className = 'TaxCategoryService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: TaxCategoryModel) {
		try {
			return this.consumeAPI('/tax-category', data);
		} catch (e) {
			console.log(`An error occurred saving tax-category ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/tax-category/get');
		} catch (e) {
			console.log(`An error occurred getting tax-categories ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/tax-category/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting tax-category by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: TaxCategoryModel) {
		try {
			return this.consumeAPI('/tax-category/update', data);
		} catch (e) {
			console.log(`An error occurred updatting tax-category ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/tax-category/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting tax-category ${this.delete.name} --> ${this.className}`)
		}
	}

	getTotalTaxesByCloseCash(id: string) {
		try {
			return this.consumeAPI('/tax-category/getTotalByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting total taxes by close cash ${this.getTotalTaxesByCloseCash.name} --> ${this.className}`)
		}
	}
}
