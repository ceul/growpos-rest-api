import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import StockDiaryModel from '../../models/stock-diary.model';
import { Storage } from '@ionic/storage';


@Injectable({
	providedIn: 'root'
})
export class StockService extends BaseServiceService {

	className = 'StockService'
	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	saveStockDiary(data: StockDiaryModel[]) {
		try {
			return this.consumeAPI('/stock/stockDiary', data);
		} catch (e) {
			console.log(`An error occurred saving stock diary: ${this.saveStockDiary.name} --> ${this.className}`)
		}

	}

	getStock() {
		try {
			return this.consumeAPI('/stock/getStock');
		} catch (e) {
			console.log(`An error occurred getting stock: ${this.getStock.name} --> ${this.className}`)
		}

	}

	getLowStock() {
		try {
			return this.consumeAPI('/stock/getLowStock');
		} catch (e) {
			console.log(`An error occurred getting low stock: ${this.getLowStock.name} --> ${this.className}`)
		}

	}

	getLocationStock() {
		try {
			return this.consumeAPI('/stock/getLocationStock');
		} catch (e) {
			console.log(`An error occurred getting location stock: ${this.getLocationStock.name} --> ${this.className}`)
		}
	}

	getProductList() {
		try {
			return this.consumeAPI('/product/getList');
		} catch (e) {
			console.log(`An error occurred getting location stock: ${this.getProductList.name} --> ${this.className}`)
		}
	}
}
