import { TestBed } from '@angular/core/testing';

import { MoveTableService } from './move-table.service';

describe('MoveTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MoveTableService = TestBed.get(MoveTableService);
    expect(service).toBeTruthy();
  });
});
