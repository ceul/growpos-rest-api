import { Injectable } from '@angular/core';
import PlaceModel from 'src/app/models/place.model';

@Injectable({
  providedIn: 'root'
})
export class MoveTableService {

  private table: PlaceModel = null

  constructor() { }

  public setTable(_table: PlaceModel) {
    this.table = _table
  }

  public getTable(): PlaceModel {
    return this.table
  }

}
