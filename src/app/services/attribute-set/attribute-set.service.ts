import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import AttributeSetModel from '../../models/attribute-set.model';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class AttributeSetService extends BaseServiceService {

  className = 'AttributeSetService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}

	save(data: AttributeSetModel) {
		try {
			return this.consumeAPI('/attribute-set',data);
		} catch (e) {
			console.log(`An error occurred saving attribute-set ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/attribute-set/get');
		} catch (e) {
			console.log(`An error occurred getting attribute-sets: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/attribute-set/getById');
		} catch (e) {
			console.log(`An error occurred getting attribute-set by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AttributeSetModel) {
		try {
			return this.consumeAPI('/attribute-set/update', data);
		} catch (e) {
			console.log(`An error occurred updatting attribute-set: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/attribute-set/delete', {id});
		} catch (e) {
			console.log(`An error occurred deletting attribute-set: ${this.delete.name} --> ${this.className}`)
		}
	}
}
