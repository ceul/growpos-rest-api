import { TestBed } from '@angular/core/testing';

import { AttributeSetService } from './attribute-set.service';

describe('AttributeSetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttributeSetService = TestBed.get(AttributeSetService);
    expect(service).toBeTruthy();
  });
});
