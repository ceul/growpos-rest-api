import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseServiceService } from '../base-services/base-service.service';
import TicketModel from 'src/app/models/ticket.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class SharedTicketService extends BaseServiceService {

	className = 'SharedTicketService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}
	save(id: string, ticket: TicketModel) {
		try {
			return this.consumeAPI('/shared-ticket', { id, ticket });
		} catch (e) {
			console.log(`An error occurred saving sharedticket ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/shared-ticket/get');
		} catch (e) {
			console.log(`An error occurred getting sharedtickets: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/shared-ticket/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting sharedticket by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(id: string, ticket: TicketModel) {
		try {
			return this.consumeAPI('/shared-ticket/update', { id, ticket });
		} catch (e) {
			console.log(`An error occurred updatting sharedticket: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/shared-ticket/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting sharedticket: ${this.delete.name} --> ${this.className}`)
		}
	}
}
