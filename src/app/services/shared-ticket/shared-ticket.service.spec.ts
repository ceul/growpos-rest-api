import { TestBed } from '@angular/core/testing';

import { SharedTicketService } from './shared-ticket.service';

describe('SharedTicketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedTicketService = TestBed.get(SharedTicketService);
    expect(service).toBeTruthy();
  });
});
