import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import ResourceModel from 'src/app/models/resource.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class ResourceService extends BaseServiceService {

	className = 'RoleService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: ResourceModel) {
		try {
			return this.consumeAPI('/resource', data);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/resource/get');
		} catch (e) {
			console.log(`An error occurred getting roles: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/resource/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting resource by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getByHost(name: string) {
		try {
			return this.consumeAPI('/resource/getByHost', { name });
		} catch (e) {
			console.log(`An error occurred getting resource by host: ${this.getByHost.name} --> ${this.className}`)
		}
	}

	getCompanyLogo() {
		try {
			return this.consumeAPI('/resource/getCompanyLogo', null, 'get', true);
		} catch (e) {
			console.log(`An error occurred getting company logo: ${this.getCompanyLogo.name} --> ${this.className}`)
		}
	}

	update(data: ResourceModel) {
		try {
			return this.consumeAPI('/resource/update', data);
		} catch (e) {
			console.log(`An error occurred updatting resource: ${this.update.name} --> ${this.className}`)
		}
	}

	uploadCompanyLogo(data: File) {
		try {
			return this.consumeAPI('/resource/uploadCompanyLogo', data, 'post', true);
		} catch (e) {
			console.log(`An error occurred updatting resource: ${this.uploadCompanyLogo.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/resource/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting resource: ${this.delete.name} --> ${this.className}`)
		}
	}
}
