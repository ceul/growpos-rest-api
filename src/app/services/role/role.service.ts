import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import RoleModel from '../../models/roles.model';
import { Storage } from '@ionic/storage';
@Injectable({
	providedIn: 'root'
})
export class RoleService extends BaseServiceService {

	className = 'RoleService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: RoleModel) {
		try {
			return this.consumeAPI('/role', data);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/role/get');
		} catch (e) {
			console.log(`An error occurred getting roles: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/role/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting role by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: RoleModel) {
		try {
			return this.consumeAPI('/role/update', data);
		} catch (e) {
			console.log(`An error occurred updatting role: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/role/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting role: ${this.delete.name} --> ${this.className}`)
		}
	}
}
