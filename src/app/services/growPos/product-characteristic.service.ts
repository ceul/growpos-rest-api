import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { ProductCharacteristicModel } from 'src/app/models/growPos/product.characteristic.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class ProductCharacteristicsService extends BaseServiceService {

	className = 'ProductCharacteristicsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: ProductCharacteristicModel) {
		try {
			return this.consumeAPI('/prod_characteristic', data);
		} catch (e) {
			console.log(`An error occurred saving product characteristic ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/prod_characteristics/get');
		} catch (e) {
			console.log(`An error occurred getting product characteristics ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id_p: string, id_c: string) {
		try {
			return this.consumeAPI('/prod_characteristics/getById', { id_p, id_c });
		} catch (e) {
			console.log(`An error occurred getting product characteristics by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getByProduct(id_p: string) {
		try {
			return this.consumeAPI('/prod_characteristics/getByProduct', { id_p});
		} catch (e) {
			console.log(`An error occurred getting product characteristics by product ${this.getByProduct.name} --> ${this.className}`)
		}
	}

	update(data: ProductCharacteristicModel) {
		try {
			return this.consumeAPI('/prod_characteristic/update', data);
		} catch (e) {
			console.log(`An error occurred updating product characteristic ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id_p: string, id_c: string) {
		try {
			return this.consumeAPI('/prod_characteristic/delete', { id_p, id_c });
		} catch (e) {
			console.log(`An error occurred deleting product characteristics ${this.delete.name} --> ${this.className}`)
		}
	}
}
