import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { CharacteristicModel } from 'src/app/models/growPos/characteristic.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class CharacteristicsService extends BaseServiceService {

	className = 'CharacteristicsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: CharacteristicModel) {
		try {
			return this.consumeAPI('/characteristic', data);
		} catch (e) {
			console.log(`An error occurred saving characteristic ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/characteristics/get');
		} catch (e) {
			console.log(`An error occurred getting characteristics ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/characteristics/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting characteristics by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: CharacteristicModel) {
		try {
			return this.consumeAPI('/characteristic/update', data);
		} catch (e) {
			console.log(`An error occurred updatting characteristics ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/characteristic/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting characteristics ${this.delete.name} --> ${this.className}`)
		}
	}
}
