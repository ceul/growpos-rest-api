import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { ThirdPartyTypesModel } from 'src/app/models/growPos/_third_party_type.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class ThirdPartyTypesService extends BaseServiceService {

	className = 'ThirdPartyTypesService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: ThirdPartyTypesModel) {
		try {
			return this.consumeAPI('/third_party_type', data);
		} catch (e) {
			console.log(`An error occurred saving _account_types ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/third_party_type/get');
		} catch (e) {
			console.log(`An error occurred getting _third_party_type ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/third_party_type/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _third_party_type by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: ThirdPartyTypesModel) {
		try {
			return this.consumeAPI('/third_party_type/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _third_party_type ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/third_party_type/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _third_party_type ${this.delete.name} --> ${this.className}`)
		}
	}
}
