import { Injectable, Output, EventEmitter } from '@angular/core';
import PrintModel from '../../models/print.model';
import { Router } from '@angular/router';

import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  isPrinting: boolean
  constructor(private router: Router, private btSerial:BluetoothSerial) { }

  public processReport(printModel: PrintModel) {

  }

  printDocument(documentName: string, documentData: any) {
    this.isPrinting = true;
    this.router.navigate(['/',
      {
        outlets: {
          'print': ['print', documentName]
        }
      }], { state: { data: documentData } });
  }

  onDataReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate(['/', { outlets: { print: null } }]);
    });
  }

  searchBt()
  {
    return this.btSerial.list();
  }

  connectBT(address)
  {
    return this.btSerial.connect(address);
  }

  testPrint(address)
  {
    let printData="Test hello this is a test \n\n\n\n Hello Test 123 123 123\n\n\n"

    
    let xyz=this.connectBT(address).subscribe(data=>{
      this.btSerial.write(printData).then(dataz=>{
        console.log("WRITE SUCCESS",dataz);
        xyz.unsubscribe();
      },errx=>{
        console.log("WRITE FAILED",errx);
      });
      },err=>{
        console.log("CONNECTION ERROR",err);
      });

  }
}
