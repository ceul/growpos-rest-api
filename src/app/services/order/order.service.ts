import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import OrderModel from 'src/app/models/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseServiceService {

	className = 'OrderService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: OrderModel) {
		try {
			return this.consumeAPI('/order', data);
		} catch (e) {
			console.log(`An error occurred saving order ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/order/get');
		} catch (e) {
			console.log(`An error occurred getting orders: ${this.get.name} --> ${this.className}`)
		}
	}

	getProcess() {
		try {
			return this.consumeAPI('/order/getProcess');
		} catch (e) {
			console.log(`An error occurred getting on process orders: ${this.getProcess.name} --> ${this.className}`)
		}
	}

	getReadyToDeliver() {
		try {
			return this.consumeAPI('/order/getReadyToDeliver');
		} catch (e) {
			console.log(`An error occurred getting on process orders: ${this.getReadyToDeliver.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/order/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting order by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: OrderModel) {
		try {
			return this.consumeAPI('/order/update', data);
		} catch (e) {
			console.log(`An error occurred updatting order: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/order/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting order: ${this.delete.name} --> ${this.className}`)
		}
	}
}
