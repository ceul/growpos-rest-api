import { TestBed } from '@angular/core/testing';

import { CloseCashService } from './close-cash.service';

describe('CloseCashService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CloseCashService = TestBed.get(CloseCashService);
    expect(service).toBeTruthy();
  });
});
