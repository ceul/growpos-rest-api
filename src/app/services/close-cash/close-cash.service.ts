import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import CloseCashModel from 'src/app/models/close-cash';
import PaymentModel from 'src/app/models/payment.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class CloseCashService extends BaseServiceService {

	className = 'CloseCashService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(closeCash: CloseCashModel) {
		try {
			return this.consumeAPI('/close-cash', { closeCash });
		} catch (e) {
			console.log(`An error occurred saving close-cash ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/close-cash/get');
		} catch (e) {
			console.log(`An error occurred getting close-cash ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/close-cash/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting close-cash by id ${this.getById.name} --> ${this.className}`)
		}
	}

	getLastByHost(host: string) {
		try {
			return this.consumeAPI('/close-cash/getLastByHost', { host });
		} catch (e) {
			console.log(`An error occurred getting close-cash by host ${this.getLastByHost.name} --> ${this.className}`)
		}
	}

	getByHostAndBySequence(host: string,sequence: number) {
		try {
			return this.consumeAPI('/close-cash/getByHostAndBySequence', { host,sequence });
		} catch (e) {
			console.log(`An error occurred getting close-cash by host and by sequence ${this.getByHostAndBySequence.name} --> ${this.className}`)
		}
	}

	getCloseCashListByDate(dateStart: Date, dateEnd: Date) {
		try {
			return this.consumeAPI('/close-cash/getCloseCashListByDate', { dateStart, dateEnd });
		} catch (e) {
			console.log(`An error occurred getting close-cash list by date ${this.getCloseCashListByDate.name} --> ${this.className}`)
		}
	}

	getCashFlowReport(dateStart: Date, dateEnd: Date) {
		try {
			return this.consumeAPI('/close-cash/getCashFlowReport', { dateStart, dateEnd });
		} catch (e) {
			console.log(`An error occurred getting cash flow report ${this.getCashFlowReport.name} --> ${this.className}`)
		}
	}

	update(data: CloseCashModel) {
		try {
			return this.consumeAPI('/close-cash/update', data);
		} catch (e) {
			console.log(`An error occurred updatting close-cash ${this.update.name} --> ${this.className}`)
		}
	}

	isActive(id: string) {
		try {
			return this.consumeAPI('/close-cash/isActive', id);
		} catch (e) {
			console.log(`An error occurred updatting close-cash ${this.isActive.name} --> ${this.className}`)
		}
	}

	moveCash(closeCash: string, payment: PaymentModel) {
		try {
			return this.consumeAPI('/close-cash/moveCash', { closeCash, payment });
		} catch (e) {
			console.log(`An error occurred moving cash ${this.moveCash.name} --> ${this.className}`)
		}
	}
}
