import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
@Injectable({
	providedIn: 'root'
})
export class PaymentService extends BaseServiceService {

	className = 'PaymentService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}


	getByCloseCash(id: string) {
		try {
			return this.consumeAPI('/payment/getByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting payments by close cash ${this.getByCloseCash.name} --> ${this.className}`)
		}
	}

	getTotalPaymentsByCloseCash(id: string) {
		try {
			return this.consumeAPI('/payment/getTotalPaymentsByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting Total payments by close cash ${this.getTotalPaymentsByCloseCash.name} --> ${this.className}`)
		}
	}

	getTransactions(datestart: Date, dateend: Date) {
		try {
			return this.consumeAPI('/payment/getTransactionsReport', { datestart, dateend });
		} catch (e) {
			console.log(`An error occurred getting transactions ${this.getTransactions.name} --> ${this.className}`)
		}
	}

	getTransactionsByPayment(datestart: Date, dateend: Date) {
		try {
			return this.consumeAPI('/payment/getTransactionsReportByPayment', { datestart, dateend });
		} catch (e) {
			console.log(`An error occurred getting transactions by people ${this.getTransactions.name} --> ${this.className}`)
		}
	}
}
