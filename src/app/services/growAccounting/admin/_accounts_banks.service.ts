import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { AccountsBanksModel } from 'src/app/models/growAccounting/_accounts_banks';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AccountsBanksService extends BaseServiceService {

	className = 'AccountsBanksService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: AccountsBanksModel) {
		try {
			return this.consumeAPI('/accounts_banks', data);
		} catch (e) {
			console.log(`An error occurred saving accounts_banks ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/accounts_banks/get');
		} catch (e) {
			console.log(`An error occurred getting accounts_banks ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/accounts_banks/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting accounts_banks by id ${this.getById.name} --> ${this.className}`)
		}
	}
	
	delete(id: string) {
		try {
			return this.consumeAPI('/accounts_banks/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting accounts_banks ${this.delete.name} --> ${this.className}`)
		}
	}
}
