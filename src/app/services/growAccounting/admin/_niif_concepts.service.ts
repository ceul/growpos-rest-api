import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { NiffConceptsModel } from 'src/app/models/growAccounting/_niif_concepts';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class NiffConceptsService extends BaseServiceService {

	className = 'NiffConceptsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: NiffConceptsModel) {
		try {
			return this.consumeAPI('/_niif_concepts', data);
		} catch (e) {
			console.log(`An error occurred saving _niif_concepts ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_niif_concepts/get');
		} catch (e) {
			console.log(`An error occurred getting _niif_concepts ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_niif_concepts/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _niif_concepts by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: NiffConceptsModel) {
		try {
			return this.consumeAPI('/_niif_concepts/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _niif_concepts ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_niif_concepts/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _niif_concepts ${this.delete.name} --> ${this.className}`)
		}
	}
}
