import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { AttributesAccountsModel } from 'src/app/models/growAccounting/_attributes_accounts';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AttributesAccountsService extends BaseServiceService {

	className = 'AttributesAccountsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: AttributesAccountsModel) {
		try {
			return this.consumeAPI('/_attributes_accounts', data);
		} catch (e) {
			console.log(`An error occurred saving _attributes_accounts ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_attributes_accounts/get');
		} catch (e) {
			console.log(`An error occurred getting _attributes_accounts ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_attributes_accounts/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _attributes_accounts by id ${this.getById.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_attributes_accounts/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _attributes_accounts ${this.delete.name} --> ${this.className}`)
		}
	}
}
