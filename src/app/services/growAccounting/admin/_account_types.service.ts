import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { AccountTypesModel } from 'src/app/models/growAccounting/_account_types.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AccountTypesService extends BaseServiceService {

	className = 'AccountTypesService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: AccountTypesModel) {
		try {
			return this.consumeAPI('/_account_types', data);
		} catch (e) {
			console.log(`An error occurred saving _account_types ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_account_types/get');
		} catch (e) {
			console.log(`An error occurred getting _account_types ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_account_types/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _account_types by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AccountTypesModel) {
		try {
			return this.consumeAPI('/_account_types/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _account_types ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_account_types/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _account_types ${this.delete.name} --> ${this.className}`)
		}
	}
}
