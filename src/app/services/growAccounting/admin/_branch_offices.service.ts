import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { BranchOfficesModel } from 'src/app/models/growAccounting/_branch-offices';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class BranchOfficesService extends BaseServiceService {

	className = 'BranchOfficesService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: BranchOfficesModel) {
		try {
			return this.consumeAPI('/_branch_office', data);
		} catch (e) {
			console.log(`An error occurred saving _branch_office ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_branch_office/get');
		} catch (e) {
			console.log(`An error occurred getting _branch_office ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_branch_office/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _branch_office by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: BranchOfficesModel) {
		try {
			return this.consumeAPI('/_branch_office/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _branch_office ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_branch_office/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _branch_office ${this.delete.name} --> ${this.className}`)
		}
	}
}
