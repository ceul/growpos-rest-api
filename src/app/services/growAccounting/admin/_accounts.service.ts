import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { AccountsModel } from 'src/app/models/growAccounting/_accounts';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AccountsService extends BaseServiceService {

	className = 'AccountsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: AccountsModel) {
		try {
			return this.consumeAPI('/_accounts', data);
		} catch (e) {
			console.log(`An error occurred saving _accounts ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_accounts/get');
		} catch (e) {
			console.log(`An error occurred getting _accounts ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_accounts/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _accounts by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AccountsModel) {
		try {
			return this.consumeAPI('/_accounts/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _accounts ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_accounts/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _accounts ${this.delete.name} --> ${this.className}`)
		}
	}
}
