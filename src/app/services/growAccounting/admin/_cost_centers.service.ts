import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { CostCentersModel } from 'src/app/models/growAccounting/_cost_centers';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class CostCentersService extends BaseServiceService {

	className = 'CostCentersService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: CostCentersModel) {
		try {
			return this.consumeAPI('/_cost_centers', data);
		} catch (e) {
			console.log(`An error occurred saving _cost_center ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_cost_centers/get');
		} catch (e) {
			console.log(`An error occurred getting _cost_center ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_cost_centers/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _cost_center by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: CostCentersModel) {
		try {
			return this.consumeAPI('/_cost_centers/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _cost_center ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_cost_centers/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _cost_center ${this.delete.name} --> ${this.className}`)
		}
	}
}
