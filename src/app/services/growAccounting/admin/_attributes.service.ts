import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { AttributesModel } from 'src/app/models/growAccounting/_attributes';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AttributesService extends BaseServiceService {

	className = 'AttributesService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: AttributesModel) {
		try {
			return this.consumeAPI('/_attributes', data);
		} catch (e) {
			console.log(`An error occurred saving _attributes ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_attributes/get');
		} catch (e) {
			console.log(`An error occurred getting _attributes ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_attributes/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _attributes by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AttributesModel) {
		try {
			return this.consumeAPI('/_attributes/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _attributes ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_attributes/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _attributes ${this.delete.name} --> ${this.className}`)
		}
	}
}
