import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { BanksModel } from 'src/app/models/growAccounting/_banks';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class BanksService extends BaseServiceService {

	className = 'BanksService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: BanksModel) {
		try {
			return this.consumeAPI('/_banks', data);
		} catch (e) {
			console.log(`An error occurred saving _banks ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_banks/get');
		} catch (e) {
			console.log(`An error occurred getting _banks ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_banks/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _banks by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: BanksModel) {
		try {
			return this.consumeAPI('/_banks/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _banks ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_banks/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _banks ${this.delete.name} --> ${this.className}`)
		}
	}
}
