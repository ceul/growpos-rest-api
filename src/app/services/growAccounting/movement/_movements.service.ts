import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { MovementsModel } from 'src/app/models/growAccounting/_movements';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class MovementsService extends BaseServiceService {

	className = 'MovementsService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: MovementsModel) {
		try {
			return this.consumeAPI('/_movements', data);
		} catch (e) {
			console.log(`An error occurred saving _movements ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_movements/get');
		} catch (e) {
			console.log(`An error occurred getting _movements ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_movements/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _movements by id ${this.getById.name} --> ${this.className}`)
		}
	}

	inactive(id: string) {
		try {
			return this.consumeAPI('/_movements/inactive', { id });
		} catch (e) {
			console.log(`An error occurred deleting _movements ${this.inactive.name} --> ${this.className}`)
		}
	}
}
