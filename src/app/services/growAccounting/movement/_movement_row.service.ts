import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { MovementsRowModel } from 'src/app/models/growAccounting/_movement_row';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class MovementsRowService extends BaseServiceService {

	className = 'MovementsRowService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: MovementsRowModel) {
		try {
			return this.consumeAPI('/_movement_row', data);
		} catch (e) {
			console.log(`An error occurred saving _movement_row ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_movement_row/get');
		} catch (e) {
			console.log(`An error occurred getting _movement_row ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_movement_row/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _movement_row by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: MovementsRowModel) {
		try {
			return this.consumeAPI('/_movement_row/update', data);
		} catch (e) {
			console.log(`An error occurred updatting _movement_row ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_movement_row/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _movement_row ${this.delete.name} --> ${this.className}`)
		}
	}
}
