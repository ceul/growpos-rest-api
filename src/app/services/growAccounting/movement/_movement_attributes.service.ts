import { Injectable } from '@angular/core';
import { BaseServiceService } from '../../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { MovementsAttributesModel } from 'src/app/models/growAccounting/_movement_attributes';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class MovementsAttributesService extends BaseServiceService {

	className = 'MovementsAttributesService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: MovementsAttributesModel) {
		try {
			return this.consumeAPI('/_movement_attributes', data);
		} catch (e) {
			console.log(`An error occurred saving _movement_attributes ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/_movement_attributes/get');
		} catch (e) {
			console.log(`An error occurred getting _movement_attributes ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/_movement_attributes/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting _movement_attributes by id ${this.getById.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/_movement_attributes/delete', { id });
		} catch (e) {
			console.log(`An error occurred deleting _movement_attributes ${this.delete.name} --> ${this.className}`)
		}
	}
}
