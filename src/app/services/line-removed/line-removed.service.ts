import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseServiceService } from '../base-services/base-service.service';
import LineRemovedModel from 'src/app/models/line-removed';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class LineRemovedService extends BaseServiceService {

	className = 'LineRemovedService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: LineRemovedModel) {
		try {
			return this.consumeAPI('/line-removed', data);
		} catch (e) {
			console.log(`An error occurred saving line-removed ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/line-removed/get');
		} catch (e) {
			console.log(`An error occurred getting line-removed ${this.get.name} --> ${this.className}`)
		}
	}

	getByUser(userName: string) {
		try {
			return this.consumeAPI('/line-removed/getByUser', { userName });
		} catch (e) {
			console.log(`An error occurred getting line-removed by user name ${this.getByUser.name} --> ${this.className}`)
		}
	}

	getByProduct(productId: string) {
		try {
			return this.consumeAPI('/line-removed/getByProduct', { productId });
		} catch (e) {
			console.log(`An error occurred getting line-removed by Product name ${this.getByProduct.name} --> ${this.className}`)
		}
	}
}