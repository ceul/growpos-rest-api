import { TestBed } from '@angular/core/testing';

import { LineRemovedService } from './line-removed.service';

describe('LineRemovedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LineRemovedService = TestBed.get(LineRemovedService);
    expect(service).toBeTruthy();
  });
});
