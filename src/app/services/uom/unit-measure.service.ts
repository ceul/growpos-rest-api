import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import UnitsOfMeasureModel from '../../models/units-measure.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class UnitMeasureService extends BaseServiceService {

	className = 'UnitMeasureService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: UnitsOfMeasureModel) {
		try {
			return this.consumeAPI('/unit-measure', data);
		} catch (e) {
			console.log(`An error occurred saving unit-measure ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/unit-measure/get');
		} catch (e) {
			console.log(`An error occurred getting units-measure ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/unit-measure/getById');
		} catch (e) {
			console.log(`An error occurred getting unit-measure by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: UnitsOfMeasureModel) {
		try {
			return this.consumeAPI('/unit-measure/update', data);
		} catch (e) {
			console.log(`An error occurred updatting unit-measure ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/unit-measure/delete', { id });
		} catch (e) {
			console.log(`An error occurred delleting unit-measure ${this.delete.name} --> ${this.className}`)
		}
	}
}
