import { TestBed } from '@angular/core/testing';

import { UnitMeasureService } from './unit-measure.service';

describe('UnitMeasureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnitMeasureService = TestBed.get(UnitMeasureService);
    expect(service).toBeTruthy();
  });
});
