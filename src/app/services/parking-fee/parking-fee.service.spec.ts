import { TestBed } from '@angular/core/testing';

import { ParkingFeeService } from './parking-fee.service';

describe('ParkingFeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParkingFeeService = TestBed.get(ParkingFeeService);
    expect(service).toBeTruthy();
  });
});
