import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import ParkingFeeModel from '../../models/parking-fee.model';
@Injectable({
  providedIn: 'root'
})
export class ParkingFeeService extends BaseServiceService {

	className = 'PlaceService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}


	save(data: ParkingFeeModel[]) {
		try {
			return this.consumeAPI('/parking-fee', data);
		} catch (e) {
			console.log(`An error occurred saving parking-fee ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/parking-fee/get');
		} catch (e) {
			console.log(`An error occurred getting parking-fee ${this.get.name} --> ${this.className}`)
		}
	}

	getGroupByType() {
		try {
			return this.consumeAPI('/parking-fee/getGroupByType');
		} catch (e) {
			console.log(`An error occurred getting parking-fee group by type ${this.getGroupByType.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/parking-fee/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting parking-fee by id ${this.getById.name} --> ${this.className}`)
		}
  }
  
  getByType(type: string) {
		try {
			return this.consumeAPI('/parking-fee/getByType', { type });
		} catch (e) {
			console.log(`An error occurred getting parking-fee by type ${this.getByType.name} --> ${this.className}`)
		}
	}

	update(data: ParkingFeeModel[]) {
		try {
			return this.consumeAPI('/parking-fee/update', data);
		} catch (e) {
			console.log(`An error occurred updatting parking-fee ${this.update.name} --> ${this.className}`)
		}
	}

	delete(type: string) {
		try {
			return this.consumeAPI('/parking-fee/delete', { type });
		} catch (e) {
			console.log(`An error occurred deleting parking-fee ${this.delete.name} --> ${this.className}`)
		}
	}
}