import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import SupplierModel from '../../models/supplier.model';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class SupplierService extends BaseServiceService {

	className = 'SupplierService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: SupplierModel) {
		try {
			return this.consumeAPI('/supplier', data);
		} catch (e) {
			console.log(`An error occurred saving supplier ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/supplier/get');
		} catch (e) {
			console.log(`An error occurred getting suppliers: ${this.get.name} --> ${this.className}`)
		}
	}

	getVisible() {
		try {
			return this.consumeAPI('/supplier/getVisible');
		} catch (e) {
			console.log(`An error occurred getting visible suppliers: ${this.getVisible.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/supplier/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting supplier by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: SupplierModel) {
		try {
			return this.consumeAPI('/supplier/update', data);
		} catch (e) {
			console.log(`An error occurred updatting supplier ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/supplier/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting supplier: ${this.delete.name} --> ${this.className}`)
		}
	}
}
