import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import TicketModel from 'src/app/models/ticket.model';
import { Storage } from '@ionic/storage';
import ReceiptFilterModel from 'src/app/models/receipt-filter.model';

@Injectable({
	providedIn: 'root'
})
export class TicketService extends BaseServiceService {

	className = 'TicketService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}
	save(ticket: TicketModel, location: string) {
		try {
			return this.consumeAPI('/ticket', { ticket, location });
		} catch (e) {
			console.log(`An error occurred saving ticket ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.consumeAPI('/ticket/get');
		} catch (e) {
			console.log(`An error occurred getting ticket ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/ticket/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting ticket by id ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: TicketModel) {
		try {
			return this.consumeAPI('/ticket/update', data);
		} catch (e) {
			console.log(`An error occurred updatting ticket ${this.update.name} --> ${this.className}`)
		}
	}

	getTotalSales() {
		try {
			return this.consumeAPI('/ticket/ts');
		} catch (e) {
			console.log(`An error occurred get total sales ${this.getTotalSales.name} --> ${this.className}`)
		}
	}

	getProfit() {
		try {
			return this.consumeAPI('/ticket/pr');
		} catch (e) {
			console.log(`An error occurred get profit ${this.getProfit.name} --> ${this.className}`)
		}
	}

	getNumTicket() {
		try {
			return this.consumeAPI('/ticket/nt');
		} catch (e) {
			console.log(`An error occurred get number tickets ${this.getNumTicket.name} --> ${this.className}`)
		}
	}

	getDailySales() {
		try {
			return this.consumeAPI('/ticket/ds');
		} catch (e) {
			console.log(`An error occurred get daily sales ${this.getDailySales.name} --> ${this.className}`)
		}
	}

	getTotalSalesByCloseCash(id: string) {
		try {
			return this.consumeAPI('/ticket/getTotalByCloseCash', { id });
		} catch (e) {
			console.log(`An error occurred getting total sales by close cash ${this.getTotalSalesByCloseCash.name} --> ${this.className}`)
		}
	}

	getReceiptFiltered(filter: ReceiptFilterModel) {
		try {
			return this.consumeAPI('/ticket/getReceiptFiltered', filter);
		} catch (e) {
			console.log(`An error occurred getting total sales by close cash ${this.getReceiptFiltered.name} --> ${this.className}`)
		}
	}

	getReceiptById(id: string) {
		try {
			return this.consumeAPI('/ticket/getReceiptById', {id});
		} catch (e) {
			console.log(`An error occurred getting total sales by close cash ${this.getReceiptById.name} --> ${this.className}`)
		}
	}

	
}
