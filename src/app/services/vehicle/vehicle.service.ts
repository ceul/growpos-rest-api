import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import VehicleModel from 'src/app/models/vehicle.model';
@Injectable({
  providedIn: 'root'
})
export class VehicleService extends BaseServiceService {

	className = 'ContractService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http, storage)
	}

	save(data: VehicleModel) {
		try {
			return this.consumeAPI('/vehicle', data);
		} catch (e) {
			console.log(`An error occurred saving vehicle ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/vehicle/get');
		} catch (e) {
			console.log(`An error occurred getting roles: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/vehicle/getById', { id });
		} catch (e) {
			console.log(`An error occurred getting vehicle by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getByPlate(plate: string) {
		try {
			return this.consumeAPI('/vehicle/getByPlate', { plate });
		} catch (e) {
			console.log(`An error occurred getting vehicle by plate: ${this.getByPlate.name} --> ${this.className}`)
		}
	}

	update(data: VehicleModel) {
		try {
			return this.consumeAPI('/vehicle/update', data);
		} catch (e) {
			console.log(`An error occurred updatting vehicle: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/vehicle/delete', { id });
		} catch (e) {
			console.log(`An error occurred deletting vehicle: ${this.delete.name} --> ${this.className}`)
		}
  }
}
