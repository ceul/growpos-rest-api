import { Injectable } from '@angular/core';
import { BaseServiceService } from '../base-services/base-service.service';
import { HttpClient } from '@angular/common/http';
import CustomerModel from '../../models/customer.model';
import { Storage } from '@ionic/storage';


@Injectable({
	providedIn: 'root'
})
export class CustomerService extends BaseServiceService {

	className = 'CustomerService'

	constructor(public http: HttpClient, public storage: Storage) {
		super(http,storage)
	}

	save(data: CustomerModel) {
		try {
			return this.consumeAPI('/customer',data);
		} catch (e) {
			console.log(`An error occurred saving customer ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.consumeAPI('/customer/get');
		} catch (e) {
			console.log(`An error occurred getting customers ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.consumeAPI('/customer/getById',{id});
		} catch (e) {
			console.log(`An error occurred getting customer by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getTransactions(id: string) {
		try {
			return this.consumeAPI('/customer/getTransactions',{id});
		} catch (e) {
			console.log(`An error occurred getting customer by id: ${this.getTransactions.name} --> ${this.className}`)
		}
	}

	getFiltered(data: CustomerModel) {
		try {
			return this.consumeAPI('/customer/getFiltered',data);
		} catch (e) {
			console.log(`An error occurred getting customer filtered ${this.getFiltered.name} --> ${this.className}`)
		}
	}

	update(data: CustomerModel) {
		try {
			return this.consumeAPI('/customer/update', data);
		} catch (e) {
			console.log(`An error occurred updating customer ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.consumeAPI('/customer/delete', {id});
		} catch (e) {
			console.log(`An error occurred deleting customer ${this.delete.name} --> ${this.className}`)
		}
	}
}
