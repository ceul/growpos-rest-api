import { TestBed, async, inject } from '@angular/core/testing';

import { CanActiveSalesGuard } from './can-active-sales.guard';

describe('CanActiveSalesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActiveSalesGuard]
    });
  });

  it('should ...', inject([CanActiveSalesGuard], (guard: CanActiveSalesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
