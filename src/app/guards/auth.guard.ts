import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthBusiness } from '../business/user/auth.business';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthBusiness, private router: Router) {
  }

  async canActivate(): Promise<boolean> {
    try {
      if (await this.auth.isValid()) {
        return true;
      } else {
        this.router.navigateByUrl('/login');
        return false;
      }
    } catch (error) {
      console.log('An error occurred in canActive:' + error.message)
      return false
    }
  }
}
