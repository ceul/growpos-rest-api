import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import ConfigModeEnum from '../models/config-mode.enum';

@Injectable({
  providedIn: 'root'
})
export class CanActiveSalesGuard implements CanActivate {

  constructor(private storage: Storage,
    private router: Router) {

  }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    let hostConfig = await this.storage.get('config')
    if (hostConfig.configMode == ConfigModeEnum.restaurant) {
      this.router.navigate(['/sales/restaurant/t']);
      return false;
    }
    return true
  }

}
