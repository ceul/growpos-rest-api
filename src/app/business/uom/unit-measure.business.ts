import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import UnitsOfMeasureModel from '../../models/units-measure.model';
import { UnitMeasureService } from 'src/app/services/uom/unit-measure.service';

@Injectable({
	providedIn: 'root'
})
export class UnitOfMeasureBusiness {

	className = 'UnitOfMeasureBusiness'

	constructor(private unitMeasureService: UnitMeasureService) { }

	save(data: UnitsOfMeasureModel) {
		try {
			return this.unitMeasureService.save(data);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.unitMeasureService.get();
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.unitMeasureService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	update(data: UnitsOfMeasureModel) {
		try {
			return this.unitMeasureService.update(data);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.unitMeasureService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}
}
