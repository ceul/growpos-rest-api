import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LineRemovedService } from 'src/app/services/line-removed/line-removed.service';
import LineRemovedModel from 'src/app/models/line-removed';

@Injectable({
	providedIn: 'root'
})
export class LineRemovedBusiness {

	className = 'LineRemovedBusiness'

	constructor(private drawerOpenedService: LineRemovedService) { }

	save(data: LineRemovedModel) {
		try {
			return this.drawerOpenedService.save(data);
		} catch (e) {
			console.log(`An error occurred saving line removed : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.drawerOpenedService.get();
		} catch (e) {
			console.log(`An error occurred getting line removed : ${this.get.name} --> ${this.className}`)
		}
    }

	getByUser(userName: string) {
		try {
			return this.drawerOpenedService.getByUser(userName);
		} catch (e) {
			console.log(`An error occurred getting line removed by user : ${this.getByUser.name} --> ${this.className}`)
		}
    }
    
    getByProduct(product: string) {
		try {
			return this.drawerOpenedService.getByProduct(product);
		} catch (e) {
			console.log(`An error occurred getting line removed by product : ${this.getByProduct.name} --> ${this.className}`)
		}
	}
}
