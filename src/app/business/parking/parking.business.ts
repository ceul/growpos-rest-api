import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParkingService } from 'src/app/services/parking/parking.service';
import ParkingMovModel from 'src/app/models/parking_mov.model';

@Injectable({
	providedIn: 'root'
})
export class ParkingBusiness {

	className = 'ParkingBusiness'

	constructor(private parkingService: ParkingService) { }

	save(data: ParkingMovModel) {
		try {
			return this.parkingService.save(data);
		} catch (e) {
			console.log(`An error occurred saving parking mov : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.parkingService.get();
		} catch (e) {
			console.log(`An error occurred getting parking mov: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.parkingService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting parking by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: ParkingMovModel) {
		try {
			return this.parkingService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating parking mov: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.parkingService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting parking mov: ${this.delete.name} --> ${this.className}`)
		}
	}
}
