import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParkingFeeService } from '../../services/parking-fee/parking-fee.service';
import ParkingFeeModel from '../../models/parking-fee.model';

@Injectable({
	providedIn: 'root'
})
export class ParkingFeeBusiness {

	className = 'ParkingFeeBusiness'

	constructor(private productService: ParkingFeeService) { }

	save(data: ParkingFeeModel[]) {
		try {
			return this.productService.save(data);
		} catch (e) {
			console.log(`An error occurred saving parking fee ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.productService.get();
		} catch (e) {
			console.log(`An error occurred getting products: ${this.get.name} --> ${this.className}`)
		}
	}

	getGroupByType() {
		try {
			return this.productService.getGroupByType();
		} catch (e) {
			console.log(`An error occurred getting products group by type: ${this.getGroupByType.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.productService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting parking fee by id: ${this.getById.name} --> ${this.className}`)
		}
	}

    getByType(type: string) {
		try {
			return this.productService.getByType(type);
		} catch (e) {
			console.log(`An error occurred getting parking fee by type: ${this.getByType.name} --> ${this.className}`)
		}
	}

	update(data: ParkingFeeModel[]) {
		try {
			return this.productService.update(data);
		} catch (e) {
			console.log(`An error occurred updating parking fee: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.productService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting parking fee: ${this.delete.name} --> ${this.className}`)
		}
	}
}