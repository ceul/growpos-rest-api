import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TaxCategoryService } from '../../services/tax-category/tax-category.service';
import TaxCategoryModel from '../../models/tax-category.model';

@Injectable({
	providedIn: 'root'
})
export class TaxCategoryBusiness {

	className = 'TaxCategoryBusiness'

	constructor(private taxCategoryService: TaxCategoryService) { }

	save(data: TaxCategoryModel) {
		try {
			return this.taxCategoryService.save(data);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.taxCategoryService.get();
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.taxCategoryService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	update(data: TaxCategoryModel) {
		try {
			return this.taxCategoryService.update(data);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.taxCategoryService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving tax category : ${this.save.name} --> ${this.className}`)
		}
	}

	getTotalTaxesByCloseCash(id: string) {
		try {
			return this.taxCategoryService.getTotalTaxesByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting total taxes by close cash ${this.getTotalTaxesByCloseCash.name} --> ${this.className}`)
		}
	}
}
