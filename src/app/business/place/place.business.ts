import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlaceService } from 'src/app/services/place/place.service';
import PlaceModel from 'src/app/models/place.model';

@Injectable({
	providedIn: 'root'
})
export class PlaceBusiness {

	className = 'PlaceBusiness'

	constructor(private placeService: PlaceService) { }

	save(data: PlaceModel) {
		try {
			return this.placeService.save(data);
		} catch (e) {
			console.log(`An error occurred saving place : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.placeService.get();
		} catch (e) {
			console.log(`An error occurred getting place : ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.placeService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting place by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: PlaceModel) {
		try {
			return this.placeService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating place : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.placeService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting place : ${this.delete.name} --> ${this.className}`)
		}
	}
}
