import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import ResourceModel from 'src/app/models/resource.model';
import { ResourceService } from 'src/app/services/resource/resource.service';

@Injectable({
	providedIn: 'root'
})
export class ResourceBusiness {

	className = 'ResourceBusiness'

	constructor(private resourceService: ResourceService) { }

	save(data: ResourceModel) {
		try {
			return this.resourceService.save(data);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.resourceService.get();
		} catch (e) {
			console.log(`An error occurred saving resource ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.resourceService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.getById.name} --> ${this.className}`)
		}
    }
    
    getByHost(name: string) {
		try {
			return this.resourceService.getByHost(name);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.getByHost.name} --> ${this.className}`)
		}
	}

	getCompanyLogo() {
		try {
			return this.resourceService.getCompanyLogo();
		} catch (e) {
			console.log(`An error occurred saving resource ${this.getCompanyLogo.name} --> ${this.className}`)
		}
	}

	update(data: ResourceModel) {
		try {
			return this.resourceService.update(data);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.update.name} --> ${this.className}`)
		}
	}

	uploadCompanyLogo(logo: File) {
		try {
			return this.resourceService.uploadCompanyLogo(logo);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.uploadCompanyLogo.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.resourceService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving resource ${this.delete.name} --> ${this.className}`)
		}
	}
}
