import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocationService } from '../../services/location/location.service';
import LocationModel from '../../models/location.model';

@Injectable({
	providedIn: 'root'
})
export class LocationBusiness {

	className = 'locationBusiness'

	constructor(private locationService: LocationService) { }

	save(data: LocationModel) {
		try {
			return this.locationService.save(data);
		} catch (e) {
			console.log(`An error occurred saving location ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.locationService.get();
		} catch (e) {
			console.log(`An error occurred getting locations: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.locationService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting location by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: LocationModel) {
		try {
			return this.locationService.update(data);
		} catch (e) {
			console.log(`An error occurred updatting location: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.locationService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting location: ${this.delete.name} --> ${this.className}`)
		}
	}
}
