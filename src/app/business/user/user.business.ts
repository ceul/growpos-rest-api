import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../../services/user/user.service';
import PeopleModel from '../../models/people.model';

@Injectable({
	providedIn: 'root'
})
export class UserBusiness {

	className = 'UserBusiness'

	constructor(private userService: UserService) { }

	save(data: PeopleModel) {
		try {
			return this.userService.save(data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.userService.get();
		} catch (e) {
			console.log(`An error occurred saving user ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.userService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving user ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: PeopleModel) {
		try {
			return this.userService.update(data);
		} catch (e) {
			console.log(`An error occurred saving user ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.userService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving user ${this.delete.name} --> ${this.className}`)
		}
	}

	getMenu(id: string, op?: any) {
		try {
			return this.userService.getMenu(id, op);
		} catch (e) {
			console.log(`An error occurred get menu ${this.getMenu.name} --> ${this.className}`)
		}
	}

	getSubMenu(id: string) {
		try {
			return this.userService.getSubMenu(id);
		} catch (e) {
			console.log(`An error occurred get sub menu${this.getSubMenu.name} --> ${this.className}`)
		}
	}
}
