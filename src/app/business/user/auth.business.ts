import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/user/auth.service';
import PeopleModel from '../../models/people.model';
import { map } from 'rxjs/operators';
//import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage/ngx';
import { Storage } from '@ionic/storage';
import { VirtualTimeScheduler } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthBusiness {

  userToken: string = '';
  user: PeopleModel
  className = 'AuthBusiness';

  constructor(private authService: AuthService,
    private router: Router,
    private storage: Storage) { }

  public val(id: PeopleModel) {
    try {
      //let result: any;
      let res = this.authService.val(id).then(value => {
        let otra: string = value['tokenReturn'];
        this.userToken = value['tokenReturn'].toString();
        this.saveToken(value['tokenReturn']);
        this.saveUser(value['user'])
        //result = value;
        return value;
      }).catch(err => {
        console.log('I did not think that this would happen: ', err);
        return null;
      });

      return res;
    } catch (e) {
      console.log(
        `An error occurred validating user ${this.val.name} --> ${
        this.className
        }`
      );
    }
  }

  logout() {
    try {
      this.router.navigateByUrl('/login');
      this.storage.clear()
    } catch (e) {
      throw new Error(`An error occurred login out => ${e.message}`)
    }
  }

  private saveToken(idToken: string) {
    try {
      this.userToken = idToken;
      this.storage.set('token', idToken);
      let today = new Date();
      today.setSeconds(3600);
      this.storage.set('expire', today.getTime().toString());
    } catch (error) {
      console.log('An error occurred saving the token')
    }
  }

  private saveUser(user: PeopleModel) {
    try {
      this.user = user;
      this.storage.set('user', user);
    } catch (error) {
      console.log('An error occurred saving the user')
    }
  }

  async readUser(): Promise<PeopleModel> {
    try {
      let val = await this.storage.get('user')
      if (val) {
        return val;
      } else {
        return null;
      }
    } catch (error) {
      console.log('An error occurred reading the user')
    }
  }

  async readToken(): Promise<string> {
    try {
      let val = await this.storage.get('token')
      if (val) {
        return val;
      } else {
        return '';
      }
    } catch (error) {
      console.log('An error occurred reading the token')
    }
  }

  async isValid(): Promise<boolean> {
    try {
      //if (this.userToken !== undefined) {
      let token = await this.readToken()
      if (token.length > 2) {
        return true;
      }
      else {
        return false;
      }
      /*} else {
        return false;
      }*/
      /*
          let expire: number;
          this.storage.get('expire').then((val) => {
            if (val) {
              expire = Number(val);
            } else {
              expire = 0;
            }
          });
          const expirationDate = new Date();
          expirationDate.setTime(expire);
      
          if (expirationDate > new Date()) {
            return true;
          } else {
            return false;
          }*/

    } catch (error) {
      return false
    }
  }
}
