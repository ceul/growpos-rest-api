import { Injectable } from '@angular/core';
import { CategoryService } from '../../services/category/category.service';
import CategoryModel from '../../models/category.model';

@Injectable({
	providedIn: 'root'
})
export class CategoryBusiness {

	className = 'CategoryBusiness'

	constructor(private categoryService: CategoryService) { }

	save(data: CategoryModel) {
		try {
			return this.categoryService.save(data);
		} catch (e) {
			console.log(`An error occurred saving category ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try { 
			return this.categoryService.get();
		} catch (e) {
			console.log(`An error occurred getting categories ${this.get.name} --> ${this.className}`)
		}
	}

	getParking() {
		try {
			return this.categoryService.getParking();
		} catch (e) {
			console.log(`An error occurred getting parking categories ${this.getParking.name} --> ${this.className}`)
		}
	}

	getRoot(){
		try {
			return this.categoryService.getRoot();
		} catch (e) {
			console.log(`An error occurred getting root categories: ${this.getRoot.name} --> ${this.className}`)
		}	
	}

	getChild(id: string){
		try {
			return this.categoryService.getChild(id);
		} catch (e) {
			console.log(`An error occurred getting child categories: ${this.getChild.name} --> ${this.className}`)
		}	
	}

	getById(id: string) {
		try {
			return this.categoryService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting category by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getSalesReport(datestart: Date, dateend: Date) {
		try {
			return this.categoryService.getSalesReport(datestart, dateend)
		} catch (e) {
			console.log(`An error occurred getting sales categories report ${this.getSalesReport.name} --> ${this.className}`)
		}
	}

	update(data: CategoryModel) {
		try {
			return this.categoryService.update(data);
		} catch (e) {
			console.log(`An error occurred updatting category ${this.update.name} --> ${this.className}`)
		}
	}

	uploadImage(img: File, id: string) {
		try {
			return this.categoryService.uploadImage(img, id);
		} catch (e) {
			console.log(`An error occurred uploading category: ${this.uploadImage.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.categoryService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting category ${this.delete.name} --> ${this.className}`)
		}
	}

	getBySales() {
		try {
			return this.categoryService.getBySales();
		} catch (e) {
			console.log(`An error occurred getting category by sales: --> ${this.className}`)
		}
	}
}
