import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CharacteristicsService } from 'src/app/services/growPos/admin/characteristic.service';
import { CharacteristicModel } from 'src/app/models/growPos/characteristic.model';

@Injectable({
	providedIn: 'root'
})
export class CharacteristicBusiness {

	className = 'CharacteristicBusiness'

	constructor(private CharacteristicService: CharacteristicsService) { }

	save(data: CharacteristicModel) {
		try {
			return this.CharacteristicService.save(data);
		} catch (e) {
			console.log(`An error occurred saving characteristic : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.CharacteristicService.get();
		} catch (e) {
			console.log(`An error occurred getting characteristic : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.CharacteristicService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting characteristic by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: CharacteristicModel) {
		try {
			return this.CharacteristicService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating characteristic : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.CharacteristicService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting characteristic : ${this.delete.name} --> ${this.className}`)
		}
	}
}