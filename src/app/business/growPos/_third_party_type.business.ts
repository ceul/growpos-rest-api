import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ThirdPartyTypesService } from 'src/app/services/growPos/admin/third_party_type.service';
import { ThirdPartyTypesModel } from 'src/app/models/growPos/_third_party_type.model';

@Injectable({
	providedIn: 'root'
})
export class ThirdPartyTypesBusiness {

	className = 'ThirdPartyTypesBusiness'

	constructor(private thirdPartyTypesService: ThirdPartyTypesService) { }

	save(data: ThirdPartyTypesModel) {
		try {
			return this.thirdPartyTypesService.save(data);
		} catch (e) {
			console.log(`An error occurred saving third_party_type : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.thirdPartyTypesService.get();
		} catch (e) {
			console.log(`An error occurred getting third_party_type : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.thirdPartyTypesService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting third_party_type by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: ThirdPartyTypesModel) {
		try {
			return this.thirdPartyTypesService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating third_party_type : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.thirdPartyTypesService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting third_party_type : ${this.delete.name} --> ${this.className}`)
		}
	}
}