import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductCharacteristicsService } from 'src/app/services/growPos/product-characteristic.service';
import { ProductCharacteristicModel } from 'src/app/models/growPos/product.characteristic.model';

@Injectable({
	providedIn: 'root'
})
export class ProductCharacteristicBusiness {

	className = 'ProductCharacteristicBusiness'

	constructor(private productCharacteristicsService: ProductCharacteristicsService) { }

	save(data: ProductCharacteristicModel) {
		try {
			return this.productCharacteristicsService.save(data);
		} catch (e) {
			console.log(`An error occurred saving product characteristic : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.productCharacteristicsService.get();
		} catch (e) {
			console.log(`An error occurred getting product characteristic : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id_p: string, id_c: string) {
		try {
			return this.productCharacteristicsService.getById(id_p, id_c);
		} catch (e) {
			console.log(`An error occurred getting product characteristic by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	getByProduct(id_p: string) {
		try {
			return this.productCharacteristicsService.getByProduct(id_p);
		} catch (e) {
			console.log(`An error occurred getting product characteristic by product : ${this.getByProduct.name} --> ${this.className}`)
		}
	}

	update(data: ProductCharacteristicModel) {
		try {
			return this.productCharacteristicsService.update(data);
		} catch (e) {
			console.log(`An error occurred updating product characteristic : ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id_p: string, id_c: string) {
		try {
			return this.productCharacteristicsService.delete(id_p, id_c);
		} catch (e) {
			console.log(`An error occurred deleting product characteristic : ${this.delete.name} --> ${this.className}`)
		}
	}
}