import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CloseCashService } from 'src/app/services/close-cash/close-cash.service';
import CloseCashModel from 'src/app/models/close-cash';
import PaymentModel from 'src/app/models/payment.model';

@Injectable({
	providedIn: 'root'
})
export class CloseCashBusiness {

	className = 'CloseCashBusiness'

	constructor(private closeCashService: CloseCashService) { }

	save(data: CloseCashModel) {
		try {
			return this.closeCashService.save(data);
		} catch (e) {
			console.log(`An error occurred saving close cash ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.closeCashService.get();
		} catch (e) {
			console.log(`An error occurred getting close cash ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.closeCashService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting close cash by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getLastByHost(id: string) {
		try {
			return this.closeCashService.getLastByHost(id);
		} catch (e) {
			console.log(`An error occurred getting close cash by host: ${this.getLastByHost.name} --> ${this.className}`)
		}
	}

	getByHostAndBySequence(host: string, sequence: number) {
		try {
			return this.closeCashService.getByHostAndBySequence(host,sequence);
		} catch (e) {
			console.log(`An error occurred getting close cash by host and by sequence: ${this.getLastByHost.name} --> ${this.className}`)
		}
	}

	getCloseCashListByDate(dateStart: Date, dateEnd: Date) {
		try {
			return this.closeCashService.getCloseCashListByDate(dateStart, dateEnd);
		} catch (e) {
			console.log(`An error occurred getting close-cash list by date ${this.getCloseCashListByDate.name} --> ${this.className}`)
		}
	}

	getCashFlowReport(dateStart: Date, dateEnd: Date) {
		try {
			return this.closeCashService.getCashFlowReport(dateStart, dateEnd);
		} catch (e) {
			console.log(`An error occurred getting cash flow report ${this.getCashFlowReport.name} --> ${this.className}`)
		}
	}

	update(data: CloseCashModel) {
		try {
			return this.closeCashService.update(data);
		} catch (e) {
			console.log(`An error occurred updating close cash ${this.update.name} --> ${this.className}`)
		}
	}

	isActive(id: string) {
		try {
			return this.closeCashService.isActive(id);
		} catch (e) {
			console.log(`An error occurred getting close cash by host: ${this.isActive.name} --> ${this.className}`)
		}
	}

	moveCash(closeCash: string, payment: PaymentModel) {
		try {
			return this.closeCashService.moveCash(closeCash, payment);
		} catch (e) {
			console.log(`An error occurred moving cash ${this.moveCash.name} --> ${this.className}`)
		}
	}
}
