import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import VehicleModel from 'src/app/models/vehicle.model';
@Injectable({
	providedIn: 'root'
})
export class VehicleBusiness {

	className = 'VehicleBusiness'

	constructor(private contractService: VehicleService) { }

	save(data: VehicleModel) {
		try {
			return this.contractService.save(data);
		} catch (e) {
			console.log(`An error occurred saving vehicle ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.contractService.get();
		} catch (e) {
			console.log(`An error occurred getting vehicle ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.contractService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting by id vehicle ${this.getById.name} --> ${this.className}`)
		}
	}

	getByPlate(plate: string) {
		try {
			return this.contractService.getByPlate(plate);
		} catch (e) {
			console.log(`An error occurred getting by plate vehicle ${this.getByPlate.name} --> ${this.className}`)
		}
	}

	update(data: VehicleModel) {
		try {
			return this.contractService.update(data);
		} catch (e) {
			console.log(`An error occurred updating vehicle ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.contractService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting vehicle ${this.delete.name} --> ${this.className}`)
		}
	}
}
