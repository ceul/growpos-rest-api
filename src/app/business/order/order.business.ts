import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderService } from '../../services/order/order.service';
import OrderModel from 'src/app/models/order.model';

@Injectable({
	providedIn: 'root'
})
export class OrderBusiness {

	className = 'OrderBusiness'

	constructor(private orderService: OrderService) { }

	save(data: OrderModel) {
		try {
			return this.orderService.save(data);
		} catch (e) {
			console.log(`An error occurred saving order ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.orderService.get();
		} catch (e) {
			console.log(`An error occurred saving order ${this.get.name} --> ${this.className}`)
		}
	}

	getProcess() {
		try {
			return this.orderService.getProcess();
		} catch (e) {
			console.log(`An error occurred getting process order ${this.getProcess.name} --> ${this.className}`)
		}
	}

	getReadyToDeliver() {
		try {
			return this.orderService.getReadyToDeliver();
		} catch (e) {
			console.log(`An error occurred getting process order ${this.getReadyToDeliver.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.orderService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving order ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: OrderModel) {
		try {
			return this.orderService.update(data);
		} catch (e) {
			console.log(`An error occurred saving order ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.orderService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving order ${this.delete.name} --> ${this.className}`)
		}
	}
}
