import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentService } from 'src/app/services/payment/payment.service';

@Injectable({
	providedIn: 'root'
})
export class PaymentBusiness {

	className = 'PaymentBusiness'

	constructor(private paymentService: PaymentService) { }


	getByCloseCash(id: string) {
		try {
			return this.paymentService.getByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting payment by close cash ${this.getByCloseCash.name} --> ${this.className}`)
		}
	}

	getTotalPaymentsByCloseCash(id: string) {
		try {
			return this.paymentService.getTotalPaymentsByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting totatl payments by close cash ${this.getTotalPaymentsByCloseCash.name} --> ${this.className}`)
		}
	}

	getTransactions(datestart: Date, dateend: Date) {
		try {
			return this.paymentService.getTransactions(datestart, dateend);
		} catch (e) {
			console.log(`An error occurred getting transactions ${this.getTransactions.name} --> ${this.className}`)
		}
	}

	getTransactionsByPayment(datestart: Date, dateend: Date) {
		try {
			return this.paymentService.getTransactionsByPayment(datestart, dateend);
		} catch (e) {
			console.log(`An error occurred getting transactions ${this.getTransactions.name} --> ${this.className}`)
		}
	}
}
