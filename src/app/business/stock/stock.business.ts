import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockService } from '../../services/stock/stock.service';
import StockDiaryModel from '../../models/stock-diary.model';

@Injectable({
	providedIn: 'root'
})
export class StockBusiness {

	className = 'StockBusiness'

	constructor(private stockService: StockService) { }

	saveStockDiary(data: StockDiaryModel[]) {
		try {
			return this.stockService.saveStockDiary(data);
		} catch (e) {
			console.log(`An error occurred saving stock diary: ${this.saveStockDiary.name} --> ${this.className}`)
		}
	}

	getStock() {
		try {
			return this.stockService.getStock();
		} catch (e) {
			console.log(`An error occurred getting current stock: ${this.getStock.name} --> ${this.className}`)
		}
	}

	getLowStock() {
		try {
			return this.stockService.getLowStock();
		} catch (e) {
			console.log(`An error occurred getting low stock: ${this.getLowStock.name} --> ${this.className}`)
		}
	}

	getLocationStock() {
		try {
			return this.stockService.getLocationStock();
		} catch (e) {
			console.log(`An error occurred getting location stock: ${this.getLocationStock.name} --> ${this.className}`)
		}
	}

	getProductList() {
		try {
			return this.stockService.getProductList();
		} catch (e) {
			console.log(`An error occurred getting location stock: ${this.getProductList.name} --> ${this.className}`)
		}
	}
	
}
