import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TaxService } from 'src/app/services/tax/tax.service';
import TaxModel from 'src/app/models/tax.model';

@Injectable({
	providedIn: 'root'
})
export class TaxBusiness {

	className = 'TaxBusiness'

	constructor(private taxService: TaxService) { }

	save(data: TaxModel) {
		try {
			return this.taxService.save(data);
		} catch (e) {
			console.log(`An error occurred saving tax : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.taxService.get();
		} catch (e) {
			console.log(`An error occurred getting tax : ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.taxService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting by id tax : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: TaxModel) {
		try {
			return this.taxService.update(data);
		} catch (e) {
			console.log(`An error occurred updating tax : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.taxService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting tax : ${this.delete.name} --> ${this.className}`)
		}
	}

	getTaxesByCategory(id: string) {
		try {
			return this.taxService.getTaxesByCategory(id);
		} catch (e) {
			console.log(`An error occurred getting total taxes by category ${this.getTaxesByCategory.name} --> ${this.className}`)
		}
	}

	getTotalTaxesByCloseCash(id: string) {
		try {
			return this.taxService.getTotalTaxesByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting total taxes by close cash ${this.getTotalTaxesByCloseCash.name} --> ${this.className}`)
		}
	}
}
