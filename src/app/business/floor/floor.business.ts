import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FloorService } from 'src/app/services/floor/floor.service';
import FloorModel from 'src/app/models/floor.model';

@Injectable({
	providedIn: 'root'
})
export class FloorBusiness {

	className = 'FloorBusiness'

	constructor(private floorService: FloorService) { }

	save(data: FloorModel) {
		try {
			return this.floorService.save(data);
		} catch (e) {
			console.log(`An error occurred saving floor : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.floorService.get();
		} catch (e) {
			console.log(`An error occurred getting floor : ${this.get.name} --> ${this.className}`)
		}
    }
    
    getWithPlace(type: string) {
		try {
			return this.floorService.getWithPlace(type);
		} catch (e) {
			console.log(`An error occurred getting floor with place : ${this.getWithPlace.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.floorService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting floor by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: FloorModel) {
		try {
			return this.floorService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating floor : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.floorService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting floor : ${this.delete.name} --> ${this.className}`)
		}
	}
}
