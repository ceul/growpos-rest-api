import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TicketService } from 'src/app/services/ticket/ticket.service';
import TicketModel from 'src/app/models/ticket.model';
import ReceiptFilterModel from 'src/app/models/receipt-filter.model';

@Injectable({
	providedIn: 'root'
})
export class TicketBusiness {

	className = 'TicketBusiness'

	constructor(private ticketService: TicketService) { }

	save(ticket: TicketModel, location: string) {
		try {
			return this.ticketService.save(ticket, location);
		} catch (e) {
			console.log(`An error occurred saving ticket : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.ticketService.get();
		} catch (e) {
			console.log(`An error occurred saving ticket : ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.ticketService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving ticket : ${this.save.name} --> ${this.className}`)
		}
	}

	update(data: TicketModel) {
		try {
			return this.ticketService.update(data);
		} catch (e) {
			console.log(`An error occurred saving ticket : ${this.save.name} --> ${this.className}`)
		}
	}

	getTotalSales() {
		try {
			return this.ticketService.getTotalSales();
		} catch (e) {
			console.log(`An error occurred getting totalsales : ${this.getTotalSales.name} --> ${this.className}`)
		}
	}

	getProfit() {
		try {
			return this.ticketService.getProfit();
		} catch (e) {
			console.log(`An error occurred getting profit : ${this.getProfit.name} --> ${this.className}`)
		}
	}

	getNumTicket() {
		try {
			return this.ticketService.getNumTicket();
		} catch (e) {
			console.log(`An error occurred getting number tickets : ${this.getNumTicket.name} --> ${this.className}`)
		}
	}

	getDailySales() {
		try {
			return this.ticketService.getDailySales();
		} catch (e) {
			console.log(`An error occurred getting daily sales : ${this.getDailySales.name} --> ${this.className}`)
		}
	}

	getTotalSalesByCloseCash(id: string) {
		try {
			return this.ticketService.getTotalSalesByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting total sales by close cash ${this.getTotalSalesByCloseCash.name} --> ${this.className}`)
		}
	}

	getReceiptFiltered(filter: ReceiptFilterModel) {
		try {
			return this.ticketService.getReceiptFiltered(filter);
		} catch (e) {
			console.log(`An error occurred getting Receipt filtered ${this.getReceiptFiltered.name} --> ${this.className}`)
		}
	}

	getReceiptById(id: string) {
		try {
			return this.ticketService.getReceiptById(id);
		} catch (e) {
			console.log(`An error occurred getting Receipt by id ${this.getReceiptById.name} --> ${this.className}`)
		}
	}

	
}
