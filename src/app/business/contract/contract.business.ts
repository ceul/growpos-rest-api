import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContractService } from 'src/app/services/contract/contract.service';
import ContractModel from 'src/app/models/contract.model';

@Injectable({
	providedIn: 'root'
})
export class ContractBusiness {

	className = 'ContractBusiness'

	constructor(private contractService: ContractService) { }

	save(data: ContractModel) {
		try {
			return this.contractService.save(data);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.contractService.get();
		} catch (e) {
			console.log(`An error occurred saving contract ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.contractService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.getById.name} --> ${this.className}`)
		}
	}

	getByPlate(plate: string) {
		try {
			return this.contractService.getByPlate(plate);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.getByPlate.name} --> ${this.className}`)
		}
	}

	update(data: ContractModel) {
		try {
			return this.contractService.update(data);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.contractService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving contract ${this.delete.name} --> ${this.className}`)
		}
	}
}
