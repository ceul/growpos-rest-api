import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AttributeService } from '../../services/attribute/attribute.service';
import AttributeModel from '../../models/attribute.model';

@Injectable({
	providedIn: 'root'
})
export class AttributeBusiness {

	className = 'AttributeBusiness'

	constructor(private attributeService: AttributeService) { }

	save(data: AttributeModel) {
		try {
			return this.attributeService.save(data);
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.attributeService.get();
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.attributeService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}
	}

	getByAttributeSet(id: string) {
		try {
			return this.attributeService.getByAttributeSet(id);
		} catch (e) {
			console.log(`An error occurred gatting attribute ${this.getByAttributeSet.name} --> ${this.className}`)
		}
	}

	update(data: AttributeModel) {
		try {
			return this.attributeService.update(data);
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.attributeService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving attribute ${this.save.name} --> ${this.className}`)
		}
	}
}
