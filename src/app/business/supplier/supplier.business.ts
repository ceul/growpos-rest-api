import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SupplierService } from '../../services/supplier/supplier.service';
import SupplierModel from '../../models/supplier.model';

@Injectable({
	providedIn: 'root'
})
export class SupplierBusiness {

	className = 'SupplierBusiness'

	constructor(private supplierService: SupplierService) { }

	save(data: SupplierModel) {
		try {
			return this.supplierService.save(data);
		} catch (e) {
			console.log(`An error occurred saving supplier ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.supplierService.get();
		} catch (e) {
			console.log(`An error occurred getting suppliers: ${this.get.name} --> ${this.className}`)
		}
	}

	getVisible() {
		try {
			return this.supplierService.getVisible();
		} catch (e) {
			console.log(`An error occurred getting visible suppliers: ${this.getVisible.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.supplierService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting supplier by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: SupplierModel) {
		try {
			return this.supplierService.update(data);
		} catch (e) {
			console.log(`An error occurred updatting supplier: ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.supplierService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting supplier: ${this.delete.name} --> ${this.className}`)
		}
	}
}
