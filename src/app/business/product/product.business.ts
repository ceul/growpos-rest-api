import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductService } from '../../services/product/product.service';
import ProductModel from '../../models/product.model';
import ProductFilterModel from '../../models/product-filter.model';
import PrintModel from 'src/app/models/print.model';

import { FormArray } from '@angular/forms';

@Injectable({
	providedIn: 'root'
})
export class ProductBusiness {

	className = 'ProductBusiness'

	constructor(private productService: ProductService) { }

	save(data: ProductModel) {
		try {
			return this.productService.save(data);
		} catch (e) {
			console.log(`An error occurred saving product ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.productService.get();
		} catch (e) {
			console.log(`An error occurred getting products: ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.productService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting product by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getTotalByCloseCash(id: string) {
		try {
			return this.productService.getTotalByCloseCash(id);
		} catch (e) {
			console.log(`An error occurred getting product by close cash: ${this.getTotalByCloseCash.name} --> ${this.className}`)
		}
	}

	getByBarCode(id: string) {
		try {
			return this.productService.getByBarCode(id);
		} catch (e) {
			console.log(`An error occurred getting product by id: ${this.getByBarCode.name} --> ${this.className}`)
		}
	}

	getByCategory(categoryId: string) {
		try {
			return this.productService.getByCategory(categoryId);
		} catch (e) {
			console.log(`An error occurred getting products by category: ${this.getByCategory.name} --> ${this.className}`)
		}
	}

	getConstCat() {
		try {
			return this.productService.getConstCat();
		} catch (e) {
			console.log(`An error occurred getting constant products in category: ${this.getConstCat.name} --> ${this.className}`)
		}
	}

	getFiltered(filter: ProductFilterModel) {
		try {
			return this.productService.getFiltered(filter);
		} catch (e) {
			console.log(`An error occurred getting product filtered: ${this.getFiltered.name} --> ${this.className}`)
		}
	}

	getProductSalesReport(datestart: Date, dateend: Date) {
		try {
			return this.productService.getProductSalesReport(datestart, dateend);
		} catch (e) {
			console.log(`An error occurred getting product sales report ${this.getProductSalesReport.name} --> ${this.className}`)
		}
	}

	getProfitSales(dateStart: Date, dateEnd: Date) {
		try {
			return this.productService.getProfitSales(dateStart, dateEnd);
		} catch (e) {
			console.log(`An error occurred getting product sales report ${this.getProfitSales.name} --> ${this.className}`)
		}
	}

	update(data: ProductModel) {
		try {
			return this.productService.update(data);
		} catch (e) {
			console.log(`An error occurred updating product: ${this.update.name} --> ${this.className}`)
		}
	}

	uploadImage(img: File, id: string) {
		try {
			return this.productService.uploadImage(img, id);
		} catch (e) {
			console.log(`An error occurred uploading product: ${this.uploadImage.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.productService.delete(id);
		} catch (e) {
			console.log(`An error occurred deletting product: ${this.delete.name} --> ${this.className}`)
		}
	}

	getProductBestSelling() {
		try {
			return this.productService.getProductBestSelling();
		} catch (e) {
			console.log(`An error occurred getting best selling: ${this.getConstCat.name} --> ${this.className}`)
		}
	}

	getWithLowerInventory() {
		try {
			return this.productService.getWithLowerInventory();
		} catch (e) {
			console.log(`An error occurred getting lower inventory: ${this.getConstCat.name} --> ${this.className}`)
		}
	}

	reportProduct() {
		try {
			model: PrintModel;
			result: JSON;
			this.productService.get().then(data => {
				console.log(data);
			});
		} catch (e) {
			console.log(`An error occurred getting lower inventory: ${this.getConstCat.name} --> ${this.className}`)
		}
	}
}