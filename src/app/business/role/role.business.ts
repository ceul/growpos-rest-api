import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RoleService } from '../../services/role/role.service';
import RoleModel from '../../models/roles.model';

@Injectable({
	providedIn: 'root'
})
export class RoleBusiness {

	className = 'RoleBusiness'

	constructor(private roleService: RoleService) { }

	save(data: RoleModel) {
		try {
			return this.roleService.save(data);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.roleService.get();
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.roleService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	update(data: RoleModel) {
		try {
			return this.roleService.update(data);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.roleService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}
}
