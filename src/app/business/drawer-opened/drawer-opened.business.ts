import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DrawerOpenedService } from 'src/app/services/drawer-opened/drawer-opened.service';
import DrawerOpenedModel from 'src/app/models/drawer-opened';


@Injectable({
	providedIn: 'root'
})
export class DrawerOpenedBusiness {

	className = 'DrawerOpenedBusiness'

	constructor(private drawerOpenedService: DrawerOpenedService) { }

	save(data: DrawerOpenedModel) {
		try {
			return this.drawerOpenedService.save(data);
		} catch (e) {
			console.log(`An error occurred saving drawer opened : ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.drawerOpenedService.get();
		} catch (e) {
			console.log(`An error occurred getting drawer opened : ${this.get.name} --> ${this.className}`)
		}
    }

	getByUser(userName: string) {
		try {
			return this.drawerOpenedService.getByUser(userName);
		} catch (e) {
			console.log(`An error occurred getting drawer opened by user : ${this.getByUser.name} --> ${this.className}`)
		}
    }
    
    getByTicket(product: string) {
		try {
			return this.drawerOpenedService.getByTicket(product);
		} catch (e) {
			console.log(`An error occurred getting drawer opened by product : ${this.getByTicket.name} --> ${this.className}`)
		}
	}
}
