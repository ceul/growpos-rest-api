import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedTicketService } from 'src/app/services/shared-ticket/shared-ticket.service';
import TicketModel from 'src/app/models/ticket.model';

@Injectable({
	providedIn: 'root'
})
export class SharedTicketBusiness {

	className = 'SharedTicketBusiness'

	constructor(private sharedTicketService: SharedTicketService) { }

	save(id:string,ticket: TicketModel) {
		try {
			return this.sharedTicketService.save(id,ticket);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.sharedTicketService.get();
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.sharedTicketService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	update(id:string, data: TicketModel) {
		try {
			return this.sharedTicketService.update(id, data);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.sharedTicketService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving role ${this.save.name} --> ${this.className}`)
		}
	}
}
