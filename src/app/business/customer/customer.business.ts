import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerService } from '../../services/customer/customer.service';
import CustomerModel from '../../models/customer.model';

@Injectable({
	providedIn: 'root'
})
export class CustomerBusiness {

	className = 'CustomerBusiness'

	constructor(private customerService: CustomerService) { }

	save(data: CustomerModel) {
		try {
			return this.customerService.save(data);
		} catch (e) {
			console.log(`An error occurred saving customer ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.customerService.get();
		} catch (e) {
			console.log(`An error occurred getting customers ${this.get.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.customerService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting customer by id: ${this.getById.name} --> ${this.className}`)
		}
	}

	getTransactions(id: string) {
		try {
			return this.customerService.getTransactions(id);
		} catch (e) {
			console.log(`An error occurred getting customer by id: ${this.getTransactions.name} --> ${this.className}`)
		}
	}

	getFiltered(data: CustomerModel) {
		try {
			return this.customerService.getFiltered(data);
		} catch (e) {
			console.log(`An error occurred getting customer filtered: ${this.getFiltered.name} --> ${this.className}`)
		}
	}

	update(data: CustomerModel) {
		try {
			return this.customerService.update(data);
		} catch (e) {
			console.log(`An error occurred updating customer ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.customerService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting customer ${this.delete.name} --> ${this.className}`)
		}
	}
}
