import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AttributeSetService } from '../../services/attribute-set/attribute-set.service';
import AttributeSetModel from '../../models/attribute-set.model';

@Injectable({
	providedIn: 'root'
})
export class AttributeSetBusiness {

	className = 'AttributeSetBusiness'

	constructor(private attributeSetService: AttributeSetService) { }

	save(data: AttributeSetModel) {
		try {
			return this.attributeSetService.save(data);
		} catch (e) {
			console.log(`An error occurred saving attribute set ${this.save.name} --> ${this.className}`)
		}

	}

	get() {
		try {
			return this.attributeSetService.get();
		} catch (e) {
			console.log(`An error occurred saving attribute set ${this.save.name} --> ${this.className}`)
		}
	}

	getById(id: string) {
		try {
			return this.attributeSetService.getById(id);
		} catch (e) {
			console.log(`An error occurred saving attribute set ${this.save.name} --> ${this.className}`)
		}
	}

	update(data: AttributeSetModel) {
		try {
			return this.attributeSetService.update(data);
		} catch (e) {
			console.log(`An error occurred saving attribute set ${this.save.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.attributeSetService.delete(id);
		} catch (e) {
			console.log(`An error occurred saving attribute set ${this.save.name} --> ${this.className}`)
		}
	}
}
