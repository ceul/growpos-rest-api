import { Injectable } from '@angular/core';
import { BranchOfficesService } from 'src/app/services/growAccounting/admin/_branch_offices.service';
import { BranchOfficesModel } from 'src/app/models/growAccounting/_branch-offices';

@Injectable({
	providedIn: 'root'
})
export class BranchOfficesBusiness {

	className = 'BranchOfficesBusiness'

	constructor(private branchOfficesService: BranchOfficesService) { }

	save(data: BranchOfficesModel) {
		try {
			return this.branchOfficesService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _branch_office : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.branchOfficesService.get();
		} catch (e) {
			console.log(`An error occurred getting _branch_office : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.branchOfficesService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _branch_office by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: BranchOfficesModel) {
		try {
			return this.branchOfficesService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _branch_office : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.branchOfficesService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _branch_office : ${this.delete.name} --> ${this.className}`)
		}
	}
}