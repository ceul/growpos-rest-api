import { Injectable } from '@angular/core';
import { AccountsService } from 'src/app/services/growAccounting/admin/_accounts.service';
import { AccountsModel } from 'src/app/models/growAccounting/_accounts';

@Injectable({
	providedIn: 'root'
})
export class AccountsBusiness {

	className = 'AccountsBusiness'

	constructor(private accountsService: AccountsService) { }

	save(data: AccountsModel) {
		try {
			return this.accountsService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _accounts : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.accountsService.get();
		} catch (e) {
			console.log(`An error occurred getting _accounts : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.accountsService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _accounts by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AccountsModel) {
		try {
			return this.accountsService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _accounts : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.accountsService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _accounts : ${this.delete.name} --> ${this.className}`)
		}
	}
}