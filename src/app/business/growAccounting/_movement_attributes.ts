import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovementsAttributesService } from 'src/app/services/growAccounting/movement/_movement_attributes.service';
import { MovementsAttributesModel } from 'src/app/models/growAccounting/_movement_attributes';

@Injectable({
	providedIn: 'root'
})
export class MovementsAttributesBusiness {

	className = 'MovementsAttributesBusiness'

	constructor(private movementsAttributesService: MovementsAttributesService) { }

	save(data: MovementsAttributesModel) {
		try {
			return this.movementsAttributesService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _movement_attributes : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.movementsAttributesService.get();
		} catch (e) {
			console.log(`An error occurred getting _movement_attributes : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.movementsAttributesService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _movement_attributes by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.movementsAttributesService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _movement_attributes : ${this.delete.name} --> ${this.className}`)
		}
	}
}