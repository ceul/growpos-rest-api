import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AttributesAccountsService } from 'src/app/services/growAccounting/admin/_attributes_accounts.service';
import { AttributesAccountsModel } from 'src/app/models/growAccounting/_attributes_accounts';

@Injectable({
	providedIn: 'root'
})
export class AttributesAccountsBusiness {

	className = 'AttributesAccountsBusiness'

	constructor(private attributesAccountsService: AttributesAccountsService) { }

	save(data: AttributesAccountsModel) {
		try {
			return this.attributesAccountsService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _attributes_accounts : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.attributesAccountsService.get();
		} catch (e) {
			console.log(`An error occurred getting _attributes_accounts : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.attributesAccountsService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _attributes_accounts by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.attributesAccountsService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _attributes_accounts : ${this.delete.name} --> ${this.className}`)
		}
	}
}