import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovementsService } from 'src/app/services/growAccounting/movement/_movements.service';
import { MovementsModel } from 'src/app/models/growAccounting/_movements';

@Injectable({
	providedIn: 'root'
})
export class MovementsBusiness {

	className = 'MovementsBusiness'

	constructor(private movementsService: MovementsService) { }

	save(data: MovementsModel) {
		try {
			return this.movementsService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _account_types : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.movementsService.get();
		} catch (e) {
			console.log(`An error occurred getting _account_types : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.movementsService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _account_types by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	inactive(id: string) {
		try {
			return this.movementsService.inactive(id);
		} catch (e) {
			console.log(`An error occurred inactive _account_types : ${this.inactive.name} --> ${this.className}`)
		}
	}
}