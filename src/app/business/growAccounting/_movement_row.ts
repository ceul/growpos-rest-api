import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovementsRowService } from 'src/app/services/growAccounting/movement/_movement_row.service';
import { MovementsRowModel } from 'src/app/models/growAccounting/_movement_row';

@Injectable({
	providedIn: 'root'
})
export class MovementsRowBusiness {

	className = 'MovementsRowBusiness'

	constructor(private movementsRowService: MovementsRowService) { }

	save(data: MovementsRowModel) {
		try {
			return this.movementsRowService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _movement_row : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.movementsRowService.get();
		} catch (e) {
			console.log(`An error occurred getting _movement_row : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.movementsRowService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _movement_row by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: MovementsRowModel) {
		try {
			return this.movementsRowService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _movement_row : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.movementsRowService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _movement_row : ${this.delete.name} --> ${this.className}`)
		}
	}
}