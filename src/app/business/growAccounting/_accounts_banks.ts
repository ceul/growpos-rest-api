import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountsBanksService } from 'src/app/services/growAccounting/admin/_accounts_banks.service';
import { AccountsBanksModel } from 'src/app/models/growAccounting/_accounts_banks';

@Injectable({
	providedIn: 'root'
})
export class AccountsBanksBusiness {

	className = 'AccountsBanksBusiness'

	constructor(private accountsBanksService: AccountsBanksService) { }

	save(data: AccountsBanksModel) {
		try {
			return this.accountsBanksService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _accounts_banks : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.accountsBanksService.get();
		} catch (e) {
			console.log(`An error occurred getting _accounts_banks : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.accountsBanksService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _accounts_banks by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.accountsBanksService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _accounts_banks : ${this.delete.name} --> ${this.className}`)
		}
	}
}