import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NiffConceptsService } from 'src/app/services/growAccounting/admin/_niif_concepts.service';
import { NiffConceptsModel } from 'src/app/models/growAccounting/_niif_concepts';

@Injectable({
	providedIn: 'root'
})
export class NiffConceptsBusiness {

	className = 'NiffConceptsBusiness'

	constructor(private niffConceptsService: NiffConceptsService) { }

	save(data: NiffConceptsModel) {
		try {
			return this.niffConceptsService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _niif_concepts : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.niffConceptsService.get();
		} catch (e) {
			console.log(`An error occurred getting _niif_concepts : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.niffConceptsService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _niif_concepts by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: NiffConceptsModel) {
		try {
			return this.niffConceptsService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _niif_concepts : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.niffConceptsService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _niif_concepts : ${this.delete.name} --> ${this.className}`)
		}
	}
}