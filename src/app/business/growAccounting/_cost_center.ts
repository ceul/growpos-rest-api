import { Injectable } from '@angular/core';
import { AccountTypesService } from 'src/app/services/growAccounting/admin/_account_types.service';
import { AccountTypesModel } from 'src/app/models/growAccounting/_account_types.model';

@Injectable({
	providedIn: 'root'
})
export class AccountTypesBusiness {

	className = 'AccountTypesBusiness'

	constructor(private accountTypesService: AccountTypesService) { }

	save(data: AccountTypesModel) {
		try {
			return this.accountTypesService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _account_types : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.accountTypesService.get();
		} catch (e) {
			console.log(`An error occurred getting _account_types : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.accountTypesService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _account_types by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AccountTypesModel) {
		try {
			return this.accountTypesService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _account_types : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.accountTypesService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _account_types : ${this.delete.name} --> ${this.className}`)
		}
	}
}