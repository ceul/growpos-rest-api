import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AttributesService } from 'src/app/services/growAccounting/admin/_attributes.service';
import { AttributesModel } from 'src/app/models/growAccounting/_attributes';

@Injectable({
	providedIn: 'root'
})
export class AttributesBusiness {

	className = 'AttributesBusiness'

	constructor(private attributesService: AttributesService) { }

	save(data: AttributesModel) {
		try {
			return this.attributesService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _attributes : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.attributesService.get();
		} catch (e) {
			console.log(`An error occurred getting _attributes : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.attributesService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _attributes by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: AttributesModel) {
		try {
			return this.attributesService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _attributes : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.attributesService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _attributes : ${this.delete.name} --> ${this.className}`)
		}
	}
}