import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BanksService } from 'src/app/services/growAccounting/admin/_banks.service';
import { BanksModel } from 'src/app/models/growAccounting/_banks';

@Injectable({
	providedIn: 'root'
})
export class BanksBusiness {

	className = 'BanksBusiness'

	constructor(private banksService: BanksService) { }

	save(data: BanksModel) {
		try {
			return this.banksService.save(data);
		} catch (e) {
			console.log(`An error occurred saving _account_types : ${this.save.name} --> ${this.className}`)
		}
	}

	get() {
		try {
			return this.banksService.get();
		} catch (e) {
			console.log(`An error occurred getting _account_types : ${this.get.name} --> ${this.className}`)
		}
    }

	getById(id: string) {
		try {
			return this.banksService.getById(id);
		} catch (e) {
			console.log(`An error occurred getting _account_types by id : ${this.getById.name} --> ${this.className}`)
		}
	}

	update(data: BanksModel) {
		try {
			return this.banksService.update(data);
		} catch (e) {
			console.log(`An error occurred udpating _account_types : ${this.update.name} --> ${this.className}`)
		}
	}

	delete(id: string) {
		try {
			return this.banksService.delete(id);
		} catch (e) {
			console.log(`An error occurred deleting _account_types : ${this.delete.name} --> ${this.className}`)
		}
	}
}