import TaxModel from './tax.model'
export default class TaxLineModel {
    public tax: TaxModel;
    public base: number;
    public amount: number;
}