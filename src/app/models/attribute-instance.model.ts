export default class AttributeInstanceModel{
    public id: string;
    public attributesetinstance_id: string;
    public attribute_id: string;
    public value: string;
}