import TaxModel from "./tax.model";
import ProductModel from './product.model'
import AttributeInstanceModel from './attribute-instance.model';

export default class TicketLineModel {
    public ticket: string;
    public line: number;
    public multiply: number;
    public price: number;
    public tax: TaxModel;
    public attributes: ProductModel;
    public productid: string;
    public attsetinstid: string;
    public updated: boolean;
    public attributeinstance: AttributeInstanceModel[];
}