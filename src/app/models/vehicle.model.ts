export default class VehicleModel{
    id: string;
	plate: string;
	model: string;
	color: string;
	description: string;
}