export default class LocationModel{
    public id: string;
    public name: string;
    public address: string;
}