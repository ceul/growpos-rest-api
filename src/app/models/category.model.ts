import ProductModel from './product.model';

export default class CategoryModel {

    public constructor(init?: Partial<CategoryModel >) {
        Object.assign(this, init);
    }

    public id: string;
    public name: string;
    public parentid: string;
    public image: string;
    public texttip: string;
    public catshowname: boolean;
    public catorder: string;
    public type: string;
    public products?: ProductModel[];
    public childCategories?: CategoryModel[];
}