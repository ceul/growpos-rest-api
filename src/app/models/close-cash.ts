export default class CloseCashModel {
    money: string;
    host: string;
    hostsequence: number;
    datestart: Date;
    dateend: Date;
    nosales: number;
}