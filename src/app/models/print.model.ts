export default class PrintModel {
    title: string;
    cols: Array<string>;
    data: JSON;
}
