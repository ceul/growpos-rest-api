import { AccountsModel } from '../growAccounting/_accounts';

export class MovementsRowModel{
    public id_movement_row: string;
    public id_movement: string;
    public id_account: AccountsModel;
    public sign: string;
    public state: string;
}