import { AttributesModel } from '../growAccounting/_attributes';

export class MovementsAttributesModel{
    public id_movement_attributes: string;
    public id_movement_row: string;
    public id_attribute: AttributesModel;
    public note: string;
}