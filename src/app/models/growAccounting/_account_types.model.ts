export class AccountTypesModel{

    public constructor(init?: Partial<AccountTypesModel >) {
        Object.assign(this, init);
    }
    public id_account_type: string;
    public description: string;
    public state: boolean;
}