import { BanksModel } from '../growAccounting/_banks';
import { AccountsModel } from '../growAccounting/_accounts';

export class AccountsBanksModel{
    public account_number: string;
    public id_bank: BanksModel;
    public id_account: AccountsModel;
    public note: string;
}