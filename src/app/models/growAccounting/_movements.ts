import CustomerModel from '../customer.model';

export class MovementsModel{
    public id_movement: string;
    public id_third_party: CustomerModel;
    public date: string;
    public description: string;
    public state: string;
}