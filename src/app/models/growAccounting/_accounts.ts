import { AccountTypesModel } from './_account_types.model';
import { BranchOfficesModel } from '../growAccounting/_branch-offices';
import { NiffConceptsModel } from '../growAccounting/_niif_concepts';
import { CostCentersModel } from '../growAccounting/_cost_centers';

export class AccountsModel{
    public id_account: string;
    public id_father: string;
    public id_account_type: AccountTypesModel;
    public id_branch_office: BranchOfficesModel;
    public id_niif_concept : NiffConceptsModel;
    public id_cost_center: CostCentersModel;
    public minimum_base: number;
    public allows_transact: string;
    public description: string;
    public sign: string;
    public state: string;
}