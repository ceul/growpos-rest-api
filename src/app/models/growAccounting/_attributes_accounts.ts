import { AccountsModel } from '../growAccounting/_accounts';
import { AttributesModel } from '../growAccounting/_attributes';

export class AttributesAccountsModel{
    public id_account: AccountsModel;
    public id_attribute: AttributesModel;
}