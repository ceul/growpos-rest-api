export default class DrawerOpenedModel {
    opendate: string;
    name: string;
    ticketid: string;
}