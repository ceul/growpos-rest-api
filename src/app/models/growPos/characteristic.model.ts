export class CharacteristicModel{

    public constructor(init?: Partial<CharacteristicModel >) {
        Object.assign(this, init);
    }
    public id_characteristic: string;
    public description: string;
    public state: boolean;
}