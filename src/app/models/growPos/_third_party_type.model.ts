export class ThirdPartyTypesModel{

    public constructor(init?: Partial<ThirdPartyTypesModel >) {
        Object.assign(this, init);
    }
    public id_third_party_type: string;
    public description: string;
    public state: boolean;
}