export class ProductCharacteristicModel{

    public constructor(init?: Partial<ProductCharacteristicModel >) {
        Object.assign(this, init);
    }
    public id_characteristic: string;
    public id_product: string;
    public val: string;
}