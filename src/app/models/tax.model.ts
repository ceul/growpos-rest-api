export default class TaxModel {
    public id: string;
    public name: string;
    public taxcategoryid: string; // Product
    public taxcustcategoryid: string; // Customer
    public parentid: string;
    public rate: number;
    public cascade: boolean;
    public order: number;
}