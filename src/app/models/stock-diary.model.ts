import ProductModel from './product.model';
import AttributeInstanceModel from './attribute-instance.model';

export default class StockDiaryModel{
    public id: string;
    public datenew: Date;
    public reason: number;
    public location: string;
    public product: string;
    public attributesetinstance_id: string;
    public attributeinstance: AttributeInstanceModel[];
    public units: number;
    public price: number;
    public appuser: string;
    public supplier: string;
    public supplierdoc: string;
    public productObject: ProductModel;
}