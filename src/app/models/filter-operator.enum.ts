enum FilterOperatorEnum {
    None = 1,
    Equals,
    Distinct,
    Greater,
    Less,
    GreaterOrEqual,
    LessOrEqual
}
export default FilterOperatorEnum;