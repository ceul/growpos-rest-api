import CategoryModel from './category.model';
import ProductModel from './product.model';

export default class ParkingFeeModel {
    public id: string;
    public type: CategoryModel;
	public rate: ProductModel;
	public starttime: Date;
	public endtime: Date;
	public graceperiod: number;
	public time: number;
	public isMonthly: boolean;
}