enum ConfigModeEnum {
    restaurant = 1,
    standard = 2,
    parking_lot = 3,
    accounting = 4
}
export default ConfigModeEnum;