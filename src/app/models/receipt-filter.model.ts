export default class ReceiptFilterModel{
    public ticket: string;
    public ticketType: string;
    public customer: string;
    public user: string;
    public dateStart: Date;
    public dateEnd: Date;
    public total: {
        filter: number,
        value: string
    };
}