export default class LogModel {
    id: number;
    datenew: Date;
	user: string;
	type: string;
	description: string; 
}