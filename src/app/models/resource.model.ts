export default class ResourceModel{
    public id: string;
    public name: string;
    public restype: number;
    public content: JSON;
}