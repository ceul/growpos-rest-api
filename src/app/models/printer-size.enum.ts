enum PrinterSizeEnum {
    '58mm' = 1,
    '80mm' = 2
}
export default PrinterSizeEnum;