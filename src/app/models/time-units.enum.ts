enum TimeUnitsEnum {
    MINUTE = 1,
    HOUR = 2,
    DAY = 3,
}
export default TimeUnitsEnum;