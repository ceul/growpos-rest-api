import ProductModel from './product.model';
import OrderTypeEnum from './order_type.model';
import OrderStateEnum from './order_state.enum';
import OrderProductsModel from './order-product.model';

export default class OrderModel{
    public id: number;
    public state: OrderStateEnum;
    public type: OrderTypeEnum;
	public start_date: Date;
	public end_date: Date;
	public user: string;
    public external_id: string;
    public products: OrderProductsModel[]
}