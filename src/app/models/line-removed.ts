export default class LineRemovedModel {
    removeddate: Date;
    name: string;
    ticketid: string;
    productid: string;
    productname: string;
    units: number;
}