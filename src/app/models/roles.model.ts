export default class RoleModel {
    public id: string;
    public name: string;
    public permissions: string;
}