import AttributeValueModel from './attribute-value.model';

export default class AttributeModel {
    id: string;
    name: string;
    attribute_values: AttributeValueModel[]
}