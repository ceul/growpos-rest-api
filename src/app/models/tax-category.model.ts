import TaxModel from './tax.model';

export default class TaxCategoryModel {
    id: string;
    name: string;
    taxes: TaxModel[];
}