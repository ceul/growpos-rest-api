export default class ProductStockModel {

	public constructor(init?: Partial<ProductStockModel>) {
		if (init) {
            this.id = init.id ;//|| this.id;
            this.location = init.location;
            this.units = init.units;
            this.minimum = init.minimum;
            this.maximum = init.maximum;
            this.pricebuy = init.pricebuy;
            this.pricesell = init.pricesell;
		} else {
			Object.assign(this, init)
		}
	}

    public id: string;
    public location: string;
    public units: number;
    public minimum: number;
    public maximum: number;
    public pricebuy: number;
    public pricesell: number;
}
