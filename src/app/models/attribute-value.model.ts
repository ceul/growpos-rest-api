export default class AttributeValueModel {
    id: string;
    value: string;
}