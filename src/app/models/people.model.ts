export default class PeopleModel {

    public constructor(init?: Partial<PeopleModel >) {
        Object.assign(this, init);
    }

    public id: string;
    public name: string;
    public apppassword: string;
    public card: string;
    public role: string;
    public visible: boolean;
    public image: string;
}