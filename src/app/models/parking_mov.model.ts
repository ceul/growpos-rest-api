
export default class ParkingMovModel{
    public id: string;
    public places: string;
    public plate: string;
    public receipt: number;
    public date_arrival: Date;
    public date_departure: Date;
    public type: string;
    public fee: string;
    public note: string;
    public state: string;
}