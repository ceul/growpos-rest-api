import ProductModel from './product.model';

export default class OrderProductsModel{
    public id: string;
    public order: string;
    public note: string;
    public quantity: number;
    public product: ProductModel
}