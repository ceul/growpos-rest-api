import VehicleModel from './vehicle.model';
import CustomerModel from './customer.model';

export default class ContractModel {
    id: string;
    customer: CustomerModel;
    datestart: Date;
    price: number;
    vehicle: VehicleModel;
    state: number;
}