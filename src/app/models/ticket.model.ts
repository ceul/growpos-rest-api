import PeopleModel from './people.model';
import CustomerModel from './customer.model';
import PaymentModel from './payment.model'
import TaxModel from './tax.model'
import TicketLineModel from './ticket-line.model';
import ProductModel from './product.model';
import TaxLineModel from './tax-line.model'
import { ToastService } from '../services/base-services/toast.service';
import ContractModel from './contract.model';

export default class TicketModel {

    public host: string;
    public id: string; // Es el identificador unico del ticket uui
    public ticketType: number;
    public ticketId: number; // Es el autoincremental del ticket
    public pickupId: number;
    public date: Date;
    // public Properties attributes;
    public user: PeopleModel;
    public multiply: number;
    public customer: CustomerModel;
    public activeCash: string;
    public lines: TicketLineModel[];
    public payments: PaymentModel[];
    public taxes: TaxLineModel[];
    public response: string;
    public loyaltyCardNumber: string;
    public oldTicket: boolean;
    public tip: boolean;
    public isProcessed: boolean;
    public locked: string;
    public nsum: number;
    public ticketstatus: number;
    public note: string;
    public isRefund: boolean;
    public contract: ContractModel;


    private toast: ToastService
    public total: number
    public subTotal: number
    public totalTaxes: number
    public discount = 0

    addProduct(product: ProductModel, taxes, toast: ToastService, qty = 1): number {
        try {
            let index = this.lines.findIndex(line => line.productid === product.id && line.attributeinstance === product.attributeinstance)
            if (index !== -1) {
                this.addQty(index, qty)
            } else {
                let ticketLine = new TicketLineModel()
                ticketLine.ticket = this.id
                ticketLine.attributes = product
                ticketLine.attributeinstance = product.attributeinstance
                ticketLine.productid = product.id
                ticketLine.line = this.lines.length
                ticketLine.updated = false
                ticketLine.tax = taxes.find(tax => tax.id === product.taxcat)
                if (ticketLine.tax === undefined) {
                    toast.showMessage('El impuesto del producto esta mal definido porfavor corrijalo', 'danger')
                } else {
                    ticketLine.price = (product.pricesell - (product.pricesell * (this.discount/100)))* (1 + ticketLine.tax.rate)
                    ticketLine.multiply = 0
                    this.lines.unshift(ticketLine)
                    this.lines[0]['subTotal'] = 0
                    this.addQty(0, qty)
                    this.lines = this.lines.slice()
                }
            }
            return index
        } catch (e) {
            console.log('An error occurred adding a product')
        }
    }

    addQty(index: number, qty = 1) {
        try {
            this.lines[index].multiply = this.lines[index].multiply + 1
            this.lines[index]['subTotal'] = this.lines[index]['subTotal'] + this.lines[index].price
            this.subTotal = this.subTotal + (this.lines[index].attributes.pricesell - (this.lines[index].attributes.pricesell * (this.discount/100)))
            this.addTax(this.lines[index].tax, this.lines[index].attributes)
            this.calculateTotal()
            if (qty > 1) {
                return this.addQty(index, --qty)
            }
            return index
        } catch (error) {
            console.log('An error occurred adding quantity')
        }
    }

    dismissQty(index: number, qty = 1): number {
        try {
            this.lines[index]['subTotal'] = this.lines[index]['subTotal'] - this.lines[index].price
            this.subTotal = this.subTotal - this.lines[index].attributes.pricesell
            this.dismissTax(this.lines[index].tax,this.lines[index].attributes)
            this.calculateTotal()
            if (this.lines[index].multiply > 1) {
                this.lines[index].multiply = this.lines[index].multiply - 1
            } else {
                this.lines.splice(index, 1)
                this.lines = this.lines.slice()
            }
            if (qty > 1) {
                this.dismissQty(index, --qty)
            }
            return index
        } catch (error) {
            console.log('An error occurred decreasing quantity')
        }
    }

    private addTax(tax: TaxModel, product: ProductModel): number {
        try {
            let index = this.taxes.findIndex(taxLine => taxLine.tax.id === tax.id)
            if (index !== -1) {
                this.taxes[index].amount = this.taxes[index].amount + (product.pricesell * (tax.rate))
                this.taxes[index].base = this.taxes[index].base + (product.pricesell * (1 + tax.rate))
                this.totalTaxes = this.totalTaxes + (product.pricesell * (tax.rate))
            } else {
                let taxLine = new TaxLineModel()
                taxLine.tax = tax
                taxLine.amount = (product.pricesell * (tax.rate))
                taxLine.base = (product.pricesell * (1 + tax.rate))
                this.taxes.unshift(taxLine)
                this.taxes = this.taxes.slice()
                this.totalTaxes = this.totalTaxes + taxLine.amount
            }
            return index
        } catch (e) {
            console.log('An error occurred adding a tax')
        }
    }

    private dismissTax(tax: TaxModel, product: ProductModel) {
        try {
            let index = this.taxes.findIndex(taxLine => taxLine.tax.id === tax.id)
            let taxBase = this.taxes[index].base - (product.pricesell * (1 + tax.rate))
            this.totalTaxes = this.totalTaxes - (product.pricesell * (tax.rate))
            if (taxBase <= 0) {
                this.taxes.splice(index, 1)
                this.taxes = this.taxes.slice()
            } else {
                this.taxes[index].amount = this.taxes[index].amount - (product.pricesell * (tax.rate))
                this.taxes[index].base = taxBase
            }
            return true
        } catch (error) {
            console.log('An error occurred decreasing the tax')
        }
    }

    calculateTotal() {
        try {
            this.total = this.subTotal + this.totalTaxes
        } catch (error) {
            console.log('An error occurred calculating total')
        }
    }
}