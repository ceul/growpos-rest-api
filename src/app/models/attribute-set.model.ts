import AttributeUseModel from './attribute-use.model';
import AttributeModel from './attribute.model';

export default class AttributeSetModel {
    id: string;
    name: string;
    attribute_use: AttributeUseModel[]
    attributes: AttributeModel[]
}