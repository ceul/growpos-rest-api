export default class ProductFilterModel{
    public constructor(init?: Partial<ProductFilterModel>) {
		if (init) {
           this.barcode = init.barcode
            this.category = init.category ;//|| this.category;
            this.name = {
                filter: parseFloat(init['name.filter']),
                value: init['name.value']
            } ;//|| this.name;
            this.pricebuy = {
                filter: parseFloat(init['pricebuy.filter']),
                value: parseFloat(init['pricebuy.value'])
            } ;//|| this.pricebuy;
			this.pricesell = {
                filter: parseFloat(init['pricesell.filter']),
                value: parseFloat(init['pricesell.value']) 
            }  ;//|| this.pricesell;
            
		} else {
			Object.assign(this, init)
		}
	}
    public barcode: string;
    public category: string;
    public name: {
        filter: number,
        value: string
    };
    public pricebuy: {
        filter: number,
        value: number
    }
    public pricesell: {
        filter: number,
        value: number
    }
}