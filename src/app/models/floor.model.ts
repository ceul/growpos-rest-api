import PlaceModel from './place.model';

export default class FloorModel{
    public id: string;
    public name: string;
    public image: string;
    public places: PlaceModel[];
}