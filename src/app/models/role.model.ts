export default class RoleModel {

    public constructor(init?: Partial<RoleModel >) {
        Object.assign(this, init);
    }

    public id: string;
    public name: string;
    public premissions: string;
}