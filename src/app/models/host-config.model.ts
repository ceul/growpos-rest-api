export default class HostConfigModel{
    hostName: string
    configMode: number
    companyName: string
    nit: string
    address: string
    line1: string
    line2: string
    line3: string
    activePrint: boolean
    activeTip: boolean
    prefix: string
    size: string
    tip: boolean
    tipRate: number
}