import AttributeModel from './attribute.model';

export default class AttributeUseModel {
    id: string;
    attribute: AttributeModel;
    line: number;
}