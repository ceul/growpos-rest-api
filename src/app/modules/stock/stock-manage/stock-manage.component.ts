import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import ProductModel from '../../../models/product.model';
import CategoryModel from '../../../models/category.model';
import { MatTableDataSource } from '@angular/material';
import { ToastService } from '../../../services/base-services/toast.service';
import { ProductBusiness } from '../../../business/product/product.business';
import { CategoryBusiness } from '../../../business/category/category.business';
import { SupplierBusiness } from '../../../business/supplier/supplier.business';
import LocationModel from '../../../models/location.model';
import { LocationBusiness } from '../../../business/location/location.business';
import SupplierModel from '../../../models/supplier.model';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import StockDiaryModel from '../../../models/stock-diary.model';
import { ModalDirective, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FindProductComponent } from '../../../components/find-product/find-product.component';
import MovementReasonEnum from '../../../models/movement-reason.enum';
import { StockBusiness } from '../../../business/stock/stock.business';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { take } from 'rxjs/operators';
import { SetAttributeValuesComponent } from 'src/app/components/set-attribute-values/set-attribute-values.component';


@Component({
  selector: 'app-stock-manage',
  templateUrl: './stock-manage.component.html',
  styleUrls: ['./stock-manage.component.scss'],
  animations: [],
})
export class StockManageComponent implements OnInit {

  @ViewChild(BsDatepickerDirective) datepicker: BsDatepickerDirective;
  @ViewChild('confirmDeletionOrder') public confirmDeletionOrder: ModalDirective;
  @ViewChild('editPriceBuyModal') public editPriceBuyModal: ModalDirective;

  @HostListener('window:scroll')
  onScrollEvent() {
    this.datepicker.hide();
  }

  combinedCode = null
  @HostListener('document:keypress', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.barcodeScaner(this.combinedCode)
      this.combinedCode = null
    } else {
      if (this.combinedCode === null) {
        this.combinedCode = event.key;
      } else {
        this.combinedCode = this.combinedCode + event.key;
      }
    }
  }

  activeLang = environment.activeLang
  displayedColumns = ['item', 'pricesell', 'units', 'value'];
  tableData: StockDiaryModel[]//new MatTableDataSource<{}>()
  products: ProductModel[]
  categories: CategoryModel[]
  constProducts: ProductModel[]
  suppliers: SupplierModel[]
  locations: LocationModel[]
  selectedItem: number;
  newPriceBuy: number;
  public movementReasons
  public movementKeys
  manageForm: FormGroup;
  findProductModal: BsModalRef;
  attributesModal: BsModalRef;
  submit: boolean

  constructor(private modalService: BsModalService,
    private productBusiness: ProductBusiness,
    private categoryBusiness: CategoryBusiness,
    private supplierBusiness: SupplierBusiness,
    private locationBusiness: LocationBusiness,
    private stockBusiness: StockBusiness,
    public translate: TranslateService,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.translate.setDefaultLang(this.activeLang);

    this.movementReasons = MovementReasonEnum
    this.movementKeys = Object.keys(this.movementReasons).filter(Number)
    let date = new Date()
    let hoursDiff = date.getHours() - date.getTimezoneOffset() / 60;
    date.setHours(hoursDiff);
    this.manageForm = new FormGroup({
      'date': new FormControl(date, Validators.required),
      'reason': new FormControl('', Validators.required),
      'location': new FormControl('', Validators.required),
      'supplier': new FormControl('', Validators.required),
      'document': new FormControl(''),
    })
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.submit = false
    this.newPriceBuy = 0
    this.tableData = []
    this.products = []
    this.categories = []
    this.constProducts = []
    this.locations = []
    this.suppliers = []

    promises.push(this.categoryBusiness.get().then(categories => {
      this.categories = Object.values(categories)
    }))

    promises.push(this.productBusiness.getConstCat().then(products => {
      this.constProducts = Object.values(products)
      this.products = this.constProducts
      this.products = this.products.slice()
    }))

    promises.push(this.locationBusiness.get().then(locations => {
      this.locations = Object.values(locations)
    }))

    promises.push(this.supplierBusiness.get().then(suppliers => {
      this.suppliers = Object.values(suppliers)
    }))

    Promise.all(promises).then(() => this.loadingService.hide())

  }

  async selectCategory(category: CategoryModel) {
    try {
      if (category.products == null) {
        let products = await this.productBusiness.getByCategory(category.id)
        category.products = Object.values(products)
        let filterConstProducts = this.constProducts.filter(product => product.category !== category.id)
        category.products = category.products.concat(filterConstProducts)
      }
      if (category.childCategories == null) {
        let categories = await this.categoryBusiness.getChild(category.id)
        category.childCategories = Object.values(categories)
      }
      let index = this.categories.map(category => category.id).indexOf(category.id)
      this.categories[index] = category
      this.products = category.products;
      this.products = this.products.slice()
    } catch (e) {
      console.log('An error occurred selecting a category')
    }
  }

  addProduct(product: ProductModel, qty = 1): number {
    try {
      let index = this.tableData.findIndex(line => line.product === product.id && line.attributeinstance === product.attributeinstance)
      if (index !== -1) {
        this.addQty(index, qty)
      } else {
        let productInline = new StockDiaryModel()
        productInline.product = product.id
        productInline.productObject = product
        productInline.units = 1
        productInline.price = product.pricebuy
        this.tableData.unshift(productInline)
        this.tableData = this.tableData.slice()
        this.selectedItem = 0
      }
      return index
    } catch (e) {
      console.log('An error occurred adding a product')
    }
  }

  addQty(index: number, qty = 1) {
    try {
      this.tableData[index].units = this.tableData[index].units + 1
      if (qty > 1) {
        return this.addQty(index, --qty)
      }
      return index
    } catch (error) {
      console.log('An error occurred adding quantity')
    }
  }

  dismissQty(index: number, qty = 1): number {
    try {
      if (this.tableData[index].units > 1) {
        this.tableData[index].units = this.tableData[index].units - 1
      } else {
        this.tableData.splice(index, 1)
        this.tableData = this.tableData.slice()
      }
      if (qty > 1) {
        this.dismissQty(index, --qty)
      }
      return index
    } catch (error) {
      console.log('An error occurred decreasing quantity')
    }
  }

  getBuyValue(units: number, price: number) {
    try {
      return units * price
    } catch (error) {
      console.log('An error occurred getting buy value')
    }
  }

  getRecord(row) {
    try {
      this.selectedItem = row
    } catch (error) {
      console.log('An error occurred getting the record')
    }
  }

  deleteRecord() {
    try {
      this.tableData.splice(this.selectedItem, 1)
      this.tableData = this.tableData.slice()
    } catch (error) {
      console.log('An error occurred deletting the record')
    }
  }

  deleteOrder() {
    try {
      this.tableData = []
      this.tableData = this.tableData.slice()
      this.confirmDeletionOrder.hide()
    } catch (error) {
      console.log('An error occurred deletting the order')
    }
  }

  async saveOrder() {
    try {
      this.submit = true
      if (this.manageForm.valid) {
        this.loadingService.show()
        this.tableData.forEach(row => {
          row.supplier = this.manageForm.controls['supplier'].value
          row.reason = parseInt(this.manageForm.controls['reason'].value)
          row.location = this.manageForm.controls['location'].value
          row.datenew = this.manageForm.controls['date'].value
          row.supplierdoc = this.manageForm.controls['document'].value
        })
        let save = await this.stockBusiness.saveStockDiary(this.tableData)
        this.toast.showMessage('La transacción ah sido almacenada con exito!!', 'success')
        this.clearForm()
        this.loadingService.hide()
      }

    } catch (error) {
      console.log('An error occurred saving the order')
    }
  }

  editPriceBuy() {
    try {
      this.tableData[this.selectedItem].price = this.newPriceBuy
      this.editPriceBuyModal.hide()
      this.newPriceBuy = 0
    } catch (error) {
      console.log('An error occurred editing the order')
    }
  }

  public async barcodeScaner(code) {
    try {
      let index = this.products.findIndex(item => item.code === code)
      if (index > -1) {
        this.addProduct(this.products[index])
      } else {
        let product = Object.values(await this.productBusiness.getByBarCode(code))[0]
        if (product !== null && product !== undefined) {
          this.addProduct(product)
        }
      }
    } catch (error) {
      console.log('An error occurred barcodeScaner')
    }
  }
  
  clearForm() {
    try {
      this.tableData = []
      let date = new Date()
      let hoursDiff = date.getHours() - date.getTimezoneOffset() / 60;
      date.setHours(hoursDiff);
      this.manageForm.reset()
      this.manageForm.controls['date'].setValue(date)
      this.manageForm.controls['reason'].setValue('')
      this.manageForm.controls['location'].setValue('')
      this.manageForm.controls['supplier'].setValue('')
      this.manageForm.controls['document'].setValue('')
      this.submit = false
    } catch (err) {
      console.log('An error ocurred clearing the form')
    }
  }

  openFindProductModal() {
    try {
      this.findProductModal = this.modalService.show(FindProductComponent, { class: 'modal-lg' });
      this.findProductModal.content.onClick.pipe(take(1)).subscribe(product => {
        this.addProduct(product)
      })
    } catch (error) {
      console.log('An error ocurred opening find product modal: ', error);
    }
  }

  openAttributesModal() {
    try {
      this.attributesModal = this.modalService.show(SetAttributeValuesComponent, {
        class: 'modal-lg',
        initialState: {
          attributeSetId: this.tableData[this.selectedItem].productObject.attributeset_id
        }
      });
      this.attributesModal.content.selectedAttributeValues.pipe(take(1)).subscribe(attributes => {
        this.tableData[this.selectedItem].attributeinstance = attributes
      })
    } catch (error) {
      console.log('An error ocurred opening attribute modal: ', error);
    }
  }

}
