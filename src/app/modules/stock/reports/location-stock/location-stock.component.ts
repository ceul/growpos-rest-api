import { Component, OnInit, ViewChild } from '@angular/core';
import { StockBusiness } from 'src/app/business/stock/stock.business';
import { BsLocaleService, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'growpos-location-stock',
  templateUrl: './location-stock.component.html',
  styleUrls: ['./location-stock.component.scss'],
})
export class LocationStockComponent implements OnInit {

  @ViewChild(BsDaterangepickerDirective) datepicker: BsDaterangepickerDirective;

  public minDate: Date;
  public maxDate: Date;
  public bsRangeValue: Date[];
  public activeLang = environment.activeLang;
  public displayedColumns = ['locationname', 'reference', 'productname', 'stocksecurity', 'stockmaximum', 'uom', 'units']
  public data: any
  public currencyFields: string[]
  constructor(private stockBusiness: StockBusiness,
    private loadingService: LoadingSpinnerService,
    private localeService: BsLocaleService) { }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.localeService.use(this.activeLang);
    this.minDate = new Date();
    this.maxDate = new Date();

    this.minDate.setDate(this.minDate.getDate() - 7);
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.minDate, this.maxDate];
    promises.push(this.stockBusiness.getLocationStock().then(res => {
      this.data = Object.values(res)
    }))
    this.currencyFields = []
    this.currencyFields.push('pricebuy')
    this.currencyFields.push('buyvalue')
    this.currencyFields.push('sellvalue')
    this.currencyFields.push('costvalue')
    this.currencyFields.push('pricesell')
    this.currencyFields.push('stockcost')
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  public execute() {
    try {
      this.loadingService.show()
      this.stockBusiness.getLocationStock().then(res => {
        this.data = Object.values(res)
        this.loadingService.hide()
      })
    } catch (error) {
      throw new Error('An error occurred executing CloseCashComponent => execute()')
    }
  }


}
