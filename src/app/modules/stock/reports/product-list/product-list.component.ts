import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDaterangepickerDirective, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { StockBusiness } from 'src/app/business/stock/stock.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'growpos-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {

  public activeLang = environment.activeLang;
  public displayedColumns = ['locationname', 'reference', 'productname', 'stocksecurity', 'stockmaximum', 'uom', 'units']
  public data: any
  public translateFields: string[]
  public dateFields: string[]
  public currencyFields: string[]
  constructor(private stockBusiness: StockBusiness,
    private loadingService: LoadingSpinnerService,
    private localeService: BsLocaleService) { }

  ngOnInit() {

    let promises = []
    this.loadingService.show()
    this.localeService.use(this.activeLang);
    promises.push(this.stockBusiness.getProductList().then(res => {
      this.data = Object.values(res)
    }))
    this.dateFields = []
    this.dateFields.push('dateend')
    this.dateFields.push('datestart')
    this.translateFields = []
    this.translateFields.push('payment')
    this.currencyFields = []
    this.currencyFields.push('pricebuy')
    this.currencyFields.push('pricesell')
    this.currencyFields.push('buyvalue')
    this.currencyFields.push('pricegross')
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  public execute() {
    try {
      this.loadingService.show()
      this.stockBusiness.getProductList().then(res => {
        this.data = Object.values(res)
        this.loadingService.hide()
      })
    } catch (error) {
      throw new Error('An error occurred executing ProductListComponent => execute()')
    }
  }


}
