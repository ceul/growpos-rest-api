import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockMenuComponent } from './stock-menu/stock-menu.component';
import { ProductManageComponent } from './product-manage/product-manage.component';
import { CategoryManageComponent } from './category-manage/category-manage.component';
import { StockManageComponent } from './stock-manage/stock-manage.component';
import { ProductReportComponent } from './product-report/product-report.component';
import { CurrentStockComponent } from './reports/current-stock/current-stock.component';
import { LowStockComponent } from './reports/low-stock/low-stock.component';
import { LocationStockComponent } from './reports/location-stock/location-stock.component';
import { ProductListComponent } from './reports/product-list/product-list.component';
import { AttributesManageComponent } from './attributes-manage/attributes-manage.component';
import { AttributeSetManageComponent } from './attribute-set-manage/attribute-set-manage.component';
import { CharacteristicManageComponent } from './characteristic-manage/characteristic-manage.component';

// Print Web
//import { PrintGenericComponent } from '../../modules/print-generic/print-generic.component'
//import { PrintComponent } from '../../modules/print-generic/print/print.component'


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Stock'
    },
    children: [
      {
        path: '',
        component: StockMenuComponent,
        data: {
          title: 'Stock menu'
        }
      },
      {
        path: 'product-manage',
        component: ProductManageComponent,
        data: {
          title: 'Product Manage'
        }
      },
      {
        path: 'category-manage',
        component: CategoryManageComponent,
        data: {
          title: 'Category Manage'
        }
      },
      {
        path: 'characteristic-manage',
        component: CharacteristicManageComponent,
        data: {
          title: 'Characteristic Manage'
        }
      },
      {
        path: 'stock-manage',
        component: StockManageComponent,
        data: {
          title: 'Stock Manage'
        }
      },
      {
        path: 'attribute-manage',
        component: AttributesManageComponent,
        data: {
          title: 'Inventario'
        }
      },
      {
        path: 'attribute-set-manage',
        component: AttributeSetManageComponent,
        data: {
          title: 'Inventario'
        }
      },
      {
        path: 'product-report',
        component: ProductReportComponent,
        data: {
          title: 'Product Report'
        }
      },
      {
        path: 'report',
        data: {
          title: 'Inventario'
        },
        children: [
          {
            path: 'current-stock',
            component: CurrentStockComponent,
            data: {
              title: 'Inventario'
            }
          },
          {
            path: 'low-stock',
            component: LowStockComponent,
            data: {
              title: 'Inventario'
            }
          },
          {
            path: 'location-stock',
            component: LocationStockComponent,
            data: {
              title: 'Inventario'
            }
          },
          {
            path: 'product-list',
            component: ProductListComponent,
            data: {
              title: 'Inventario'
            }
          },
          
          
        ]
      }/*,
      { // No funciona
        path: 'print',
        outlet: 'print',
        component: PrintGenericComponent
      }*/
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class StockRoutingModule { }
