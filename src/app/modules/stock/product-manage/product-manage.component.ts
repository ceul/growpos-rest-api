// Base
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ModalDirective, BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';

// Component
import { CreateSupplierComponent } from '../../../components/create-supplier/create-supplier.component';
import { FindSupplierComponent } from '../../../components/find-supplier/find-supplier.component';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';

// Model
import ProductModel from '../../../models/product.model';
import CategoryModel from '../../../models/category.model';
import { CharacteristicModel } from '../../../models/growPos/characteristic.model';
import TaxCategoryModel from '../../../models/tax-category.model';
import UnitsOfMeasureModel from '../../../models/units-measure.model';
import AttributeSetModel from '../../../models/attribute-set.model';
import SupplierModel from '../../../models/supplier.model';
import { ProductCharacteristicModel } from '../../../models/growPos/product.characteristic.model';

// Business
import { ProductBusiness } from '../../../business/product/product.business';
import { CategoryBusiness } from '../../../business/category/category.business';
import { CharacteristicBusiness } from '../../../business/growPos/characteristic.business';
import { ProductCharacteristicBusiness } from '../../../business/growPos/product_characteristic.business';
import { TaxCategoryBusiness } from '../../../business/tax-category/tax-category.business';
import { UnitOfMeasureBusiness } from '../../../business/uom/unit-measure.business';
import { AttributeSetBusiness } from '../../../business/attribute-set/attribute-set.business';
import { SupplierBusiness } from '../../../business/supplier/supplier.business';
import { TaxBusiness } from 'src/app/business/tax/tax.business';

// Services base
import { ToastService } from '../../../services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'app-product-manage',
  templateUrl: './product-manage.component.html',
  styleUrls: ['./product-manage.component.scss']
})
export class ProductManageComponent implements OnInit, OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  product: ProductModel
  characteristic: CharacteristicModel;
  products: ProductModel[] = []
  categories: CategoryModel[] = []
  characteristics: CharacteristicModel[] = [];
  attributes: AttributeSetModel[] = []
  taxCategories: TaxCategoryModel[] = []
  uoms: UnitsOfMeasureModel[] = []
  suppliers: SupplierModel[] = []
  displayedColumns = ['location', 'current', 'minimum', 'maximun'];
  tableData = [];
  subscription: Subscription
  filterFields: any[]
  productForm: FormGroup;
  createSupplierModal: BsModalRef;
  canCalculate: boolean
  canActive: boolean
  priceselllock: boolean
  selectedFile: File
  public url
  public isTooLarge: boolean;
  public pendingFeatures: boolean

  public characteristicsForm: FormGroup;
  public isBeingEdited: boolean

  constructor(private modalService: BsModalService,
    private productBusiness: ProductBusiness,
    private categoryBusiness: CategoryBusiness,
    private characteristicBusiness: CharacteristicBusiness,
    private taxCategoryBusiness: TaxCategoryBusiness,
    private taxBusiness: TaxBusiness,
    private unitMeasureBusiness: UnitOfMeasureBusiness,
    private attributeSetBusiness: AttributeSetBusiness,
    private loadingService: LoadingSpinnerService,
    private supplierBusiness: SupplierBusiness,
    private dialogo: MatDialog,
    private toast: ToastService,
    private productCharacteristicBusiness: ProductCharacteristicBusiness,
    private _fb: FormBuilder) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;

    this.productForm = new FormGroup({
      'id': new FormControl(''),
      'reference': new FormControl('', Validators.required),
      'code': new FormControl('', Validators.required),
      'codetype': new FormControl(0, Validators.required),
      'name': new FormControl('', Validators.required),
      'pricebuy': new FormControl('', [Validators.required, Validators.min(0)]),
      'pricesell': new FormControl('', [Validators.required, Validators.min(0)]),
      'category': new FormControl('', Validators.required),
      'taxcat': new FormControl('', Validators.required),
      'attributeset_id': new FormControl(''),
      'stockcost': new FormControl(0),
      'stockvolume': new FormControl(0),
      'image': new FormControl(''),
      'iscom': new FormControl(false),
      'isscale': new FormControl(false),
      'isconstant': new FormControl(false),
      'printkb': new FormControl(false),
      'sendstatus': new FormControl(false),
      'isservice': new FormControl(false),
      'attributes': new FormControl(''),
      'display': new FormControl(''),
      'isvprice': new FormControl(false),
      'isverpatrib': new FormControl(false),
      'texttip': new FormControl(''),
      'warranty': new FormControl(false),
      'stockunits': new FormControl(0),
      'printto': new FormControl('null'),
      'supplier': new FormControl('null'),
      'uom': new FormControl('', Validators.required),
      'flag': new FormControl(false),
      'description': new FormControl(''),
      'margin': new FormControl(''),
      'grossProfit': new FormControl({ value: '', disabled: true }, Validators.min(0)),
      'pricesell1': new FormControl(0, Validators.min(0)),
      'weigth': new FormControl(0, Validators.min(0)),
      'width': new FormControl(0, Validators.min(0)),
      'height': new FormControl(0, Validators.min(0)),
      'length': new FormControl(0, Validators.min(0)),
      'catorder': new FormControl(0, Validators.min(0)),
      'max': new FormControl(0),
      'min': new FormControl(0),
      'inCat': new FormControl(true),
    })
  }

  ngOnInit() {
    this.subscription = new Subscription()
    this.url = `../../../assets/img/product.svg`
    this.isTooLarge = false
    this.selectedFile = null
    let promises = []
    this.loadingService.show()
    this.products = []
    this.categories = []
    this.characteristics = []
    this.canCalculate = false
    this.canActive = true
    this.priceselllock = false
    this.filterFields = ['name', 'category', 'reference', 'code']

    promises.push(this.productBusiness.get().then((products) => {
      this.products = Object.values(products)
    }))

    promises.push(this.categoryBusiness.get().then(categories => {
      this.categories = Object.values(categories)
    }));

    promises.push(this.characteristicBusiness.get().then(characteristics => {
      this.characteristics = Object.values(characteristics)
    }));

    promises.push(this.taxCategoryBusiness.get().then(taxCat => {
      this.taxCategories = Object.values(taxCat)
    }))

    promises.push(this.supplierBusiness.get().then(supplier => {
      this.suppliers = Object.values(supplier)
    }))

    promises.push(this.attributeSetBusiness.get().then(attributeSet => {
      this.attributes = Object.values(attributeSet)
    }))

    promises.push(this.unitMeasureBusiness.get().then(uom => {
      this.uoms = Object.values(uom)
    }))

    this.subscription.add(this.productForm.valueChanges.subscribe(() => {
      if (this.productForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    );

    this.subscription.add(this.productForm.get('taxcat').valueChanges.subscribe(async val => {
      if (this.canActive) {
        this.priceNoTaxSellManager()
      }
    }));

    this.subscription.add(this.productForm.get('pricesell').valueChanges.subscribe(async val => {
      if (this.canActive) {
        this.priceNoTaxSellManager()
      }
    }));

    this.subscription.add(this.productForm.get('pricesell1').valueChanges.subscribe(async val => {
      if (this.canActive) {
        this.priceTaxSellManager()
      }
    }));

    this.subscription.add(this.productForm.get('pricebuy').valueChanges.subscribe(async val => {
      if (this.canActive) {
        this.priceNoTaxSellManager()
      }
    }));

    this.subscription.add(this.productForm.get('margin').valueChanges.subscribe(async val => {
      if (this.canActive) {
        this.marginManager()
      }
    }));

    Promise.all(promises).then(() => this.loadingService.hide())

    this.characteristicsForm = this._fb.group({
      Rows: this._fb.array([])
    });

    this.isBeingEdited = false;
    this.pendingFeatures = false;

  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public openCreateSupplierModal() {
    try {
      this.createSupplierModal = this.modalService.show(CreateSupplierComponent, { class: 'modal-lg' });
      this.createSupplierModal.content.createdSupplier.pipe(take(1)).subscribe(supplier => {
        this.suppliers.push(supplier)
      })
    } catch (error) {
      console.log('An error ocurred opening create Supplier modal: ', error);
    }
  }

  public calculateMargin() {
    try {
      if (!this.canCalculate) {
        this.canCalculate = true
        let dPriceBuy = this.productForm.controls['pricebuy'].value;
        let dPriceSell = this.productForm.controls['pricesell'].value;

        if (dPriceBuy == null || dPriceSell == null) {
          this.productForm.controls['margin'].setValue(null);
        } else {
          this.productForm.controls['margin'].setValue((dPriceSell / dPriceBuy - 1.0) * 100)
        }
        this.canCalculate = false
      }
    } catch (e) {
      console.log('An error occurred calculating the margin')
    }
  }

  public async calculatePriceSellTax() {
    if (!this.canCalculate) {
      this.canCalculate = true
      let dPriceSell = this.productForm.controls['pricesell'].value;

      if (dPriceSell == null) {
        this.productForm.controls['pricesell1'].setValue(null)
      } else {
        let dTaxRate = await this.getTaxRate()
        this.productForm.controls['pricesell1'].setValue(dPriceSell * (1.0 + dTaxRate))
      }
      this.canCalculate = false
    }
  }

  public calculateGP() {
    if (!this.canCalculate) {
      this.canCalculate = true
      let dPriceBuy = this.productForm.controls['pricebuy'].value;
      let dPriceSell = this.productForm.controls['pricesell'].value;

      if (dPriceBuy == null || dPriceSell == null) {
        this.productForm.controls['grossProfit'].setValue(null)
      } else {
        this.productForm.controls['grossProfit'].setValue(((dPriceSell - dPriceBuy) / dPriceSell) * 100);
      }
      this.canCalculate = false
    }
  }

  public calculatePriceSellfromMargin() {
    if (!this.canCalculate) {
      this.canCalculate = true
      let dPriceBuy = this.productForm.controls['pricebuy'].value;
      let dMargin = this.productForm.controls['margin'].value / 100;

      if (dMargin == null || dPriceBuy == null) {
        this.productForm.controls['pricesell'].setValue(null)
      } else {
        let a = dPriceBuy * (1.0 + dMargin)
        this.productForm.controls['pricesell'].setValue(dPriceBuy * (1.0 + dMargin));
      }
      this.canCalculate = false
    }
  }

  public async calculatePriceSellfromPST() {
    if (!this.canCalculate) {
      this.canCalculate = true
      let dPriceSellTax = this.productForm.controls['pricesell1'].value;

      if (dPriceSellTax == null) {
        this.productForm.controls['pricesell'].setValue(null)
      } else {
        let dTaxRate = await this.getTaxRate()
        this.productForm.controls['pricesell'].setValue(dPriceSellTax / (1.0 + dTaxRate));
      }
      this.canCalculate = false
    }
  }

  private async getTaxRate() {
    try {
      let index = this.taxCategories.findIndex(cat => cat.id === this.productForm.get('taxcat').value)
      if (this.taxCategories[index].taxes === undefined) {
        this.taxCategories[index].taxes = Object.values(await this.taxBusiness.getTaxesByCategory(this.productForm.get('taxcat').value))
      }
      let taxCat = this.taxCategories.find(cat => cat.id === this.productForm.get('taxcat').value)//get tax;
      let rate = 0
      taxCat.taxes.forEach(tax => {
        rate = rate + tax.rate
      });
      return rate
    } catch (e) {
      this.toast.showMessage('Categoria de impuestos no seleccionada', 'danger')
      console.log('An error occurred getting the tax rate')
    }
  }

  public priceNoTaxSellManager() {
    this.calculateMargin()
    this.calculatePriceSellTax()
    this.calculateGP()
  }

  public priceBuyManager() {
    this.calculateMargin()
    this.calculatePriceSellTax()
    this.calculateGP()
  }
  public priceTaxSellManager() {
    this.calculatePriceSellfromPST()
    this.calculateMargin()
    this.calculateGP()
  }
  public marginManager() {
    this.calculatePriceSellfromMargin();
    this.calculatePriceSellTax();
    this.calculateGP();
  }

  // --------------------------------------- Tool bar logic --------------------------------------//

  public selectProduct(product) {
    if (this.product !== product && product !== null) {

      // clean characteristics
      this.characteristicsForm = this._fb.group({
        Rows: this._fb.array([])
      });

      this.canActive = false
      product.codetype = parseInt(product.codetype)
      this.product = product
      this.productForm.reset()
      this.productForm.patchValue(this.product)
      this.canActive = true // This flag is used because when the product is patched the subscription catch the change faster than all product is patche and it creates conflict during the calculation
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
      this.url = `${environment.url}:${environment.port}/product/${product.id}/getProductImage`
      this.priceNoTaxSellManager()
      this.getProductCharacteristic(this.product.id);
    }
    if (product === null) {
      this.url = `../../../assets/img/product.svg`
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveProduct() {
    try {
      if (this.productForm.valid) {
        this.loadingService.show()
        this.product = new ProductModel(this.productForm.value)
        Object.keys(this.product).map((key, index) => {
          this.product[key] = this.product[key] === '' || this.product[key] === 'null' ? null : this.product[key]
        })
        if (this.newItem) {
          let response: any;
          if (this.pendingFeatures && this.formArr.value.length > 0) {
            console.log("Guardando con características");
           
            let  productCharacteristicModel: ProductCharacteristicModel = new ProductCharacteristicModel();
            this.product.productCharacteristic = []
            for (let index = 0; index < this.formArr.value.length; index++) {
              productCharacteristicModel.id_characteristic = this.formArr.value[index].id;
              productCharacteristicModel.id_product = this.product.id;
              productCharacteristicModel.val = this.formArr.value[index].val
              this.product.productCharacteristic.push( productCharacteristicModel);
            }
            response = await this.productBusiness.save(this.product);
          }
          else {
            console.log("Guardando SIN características");
            response = await this.productBusiness.save(this.product);
          }
          this.product = <ProductModel>response
          await this.onUploadFile()
          this.products.unshift(Object(response))
          this.products = this.products.slice()
          this.toast.showMessage('El producto ah sido añadido con exito!!', 'success')
          this.edit = false
        } else {
          let promises = []
          promises.push(this.productBusiness.update(this.product))
          promises.push(this.onUploadFile())
          await Promise.all(promises)
          let index = this.products.map(product => product.id).indexOf(this.product.id)
          this.products[index] = this.product
          this.products = this.products.slice()
          this.toast.showMessage('El producto ah sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (err) {
      this.toast.showMessage(`Upss! Ocurrio algo al añadir el producto`, 'danger')
      console.log(`An error occurred saving product: ${this.saveProduct.name} --> ${ProductManageComponent.name}`)
    }
  }

  public async deleteProduct() {
    try {
      this.loadingService.show()
      let response = await this.productBusiness.delete(this.product.id)
      this.toast.showMessage('El producto ah sido eliminado con exito!!', 'success')
      let index = this.products.map(product => product.id).indexOf(this.product.id)
      this.products.splice(index, 1)
      this.products = this.products.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting the form: ${this.deleteProduct.name} --> ${ProductManageComponent.name}`)
    }
  }

  public clearForm() {
    try {
      let product = new ProductModel()
      product.stockcost = 0
      product.stockvolume = 0
      product.iscom = false
      product.isscale = false
      product.isconstant = false
      product.printkb = false
      product.sendstatus = false
      product.isservice = false
      product.isvprice = false
      product.isverpatrib = false
      product.warranty = false
      product.stockunits = 0
      product.printto = 'null'
      product.supplier = 'null'
      product.flag = false
      product.weigth = 0
      product.width = 0
      product.height = 0
      product.length = 0
      product.catorder = 0
      product.inCat = true
      product.codetype = '0'
      this.newItem = true
      this.product = product
      this.productForm.reset()
      this.productForm.patchValue(this.product)
      this.trash = true
      this.selectedFile = null
      this.pendingFeatures = false;
      this.characteristicsForm = this._fb.group({
        Rows: this._fb.array([])
      });

    } catch (e) {
      console.log(`An error occurred cleaning the form: ${this.clearForm.name} --> ${ProductManageComponent.name}`)
    }
  }

  onFileSelected(event) {
    try {
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) !== null && event.target.files[0].size < 1000001) {
        this.selectedFile = <File>event.target.files[0]
        this.isTooLarge = false
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (_event) => {
          this.url = reader.result;
        }
      } else {
        this.isTooLarge = true
      }
    } catch (error) {
      console.log(`An error occurred onfileselected => ProductManageComponent `)
    }
  }

  async onUploadFile() {
    try {
      if (this.selectedFile !== null) {
        await this.productBusiness.uploadImage(this.selectedFile, this.product.id)
      } else {
        return new Promise((resolve, reject) => {
          resolve()
        })
      }
    } catch (error) {
      console.log(`An error occurred onUploadFile => ConfigurationComponent `)
    }
  }

  setDefaultPic() {
    try {
      this.url = "../../../assets/img/product.svg";
    } catch (error) {
      console.log(`An error occurred setDefaultPic => ConfigurationComponent `)
    }
  }

  // Product - characteristics --------------------------------------------------
  // ----------------------------------------------------------------------------

  get formArr() {
    return this.characteristicsForm.get("Rows") as FormArray;
  }

  getControls() {
    return (this.characteristicsForm.get('controlName') as FormArray).controls;
  }

  findRows(search): boolean {
    let howManyTimesAreYou: number = 0;
    if (this.formArr.length >= 1) {
      this.formArr.value.map(res => {
        if (res['description'] === search) {
          howManyTimesAreYou++;
        }
      });
      return ((howManyTimesAreYou > 0) ? true : false);
    }
    return false;
  }

  initRows(id_characteristic?, characteristic_description?, val?, edit = false ){
    return this._fb.group({
      id: id_characteristic,
      description: characteristic_description,
      val: [{ value: val, disabled: false }, Validators.required],
      edit: [edit]
    });
  }

  addNewRow() {
    let res: boolean = false;
    if (this.formArr.controls.length > 0) {
      res = this.findRows(this.characteristic.description);
    }
    if (res === false) {
      if (this.isBeingEdited === false) {
        if (this.characteristic !== null && !!this.characteristic) {
          this.formArr.push(this.initRows(this.characteristic.id_characteristic, this.characteristic.description));
          this.isBeingEdited = true;
        }
        else {
          this.toast.showMessage('Deberás seleccionar una característica', 'danger')
        }
      }
      else {
        this.toast.showMessage('Deberás guardar primero la caracteristica que está en edición para agregar una nueva', 'danger')
      }
    } else {
      this.toast.showMessage('La característica ya está asignada, deberás seleccionar otra', 'danger')
    }
  }

  async deleteRow(index: number) {
    try {
      let res = await new Promise((resolve, rejects) => {
        this.dialogo.open(ConfirmDialogComponent, {
          data: `¿Deseas eliminar la característica? No podrás arrepentirte!`
        }).afterClosed().pipe(take(1)).subscribe((confirmado: Boolean) => {
          if (confirmado) {

            let obj: ProductCharacteristicModel = new ProductCharacteristicModel();
            obj.id_characteristic = this.formArr.controls[index].get('id').value;
            obj.id_product = this.product.id;
            obj.val = this.formArr.controls[index].get('val').value;

            this.productCharacteristicBusiness.getById(obj.id_product, obj.id_characteristic).then(async res => {
              if (Object.values(res).length > 0) {
                let response = await this.productCharacteristicBusiness.delete(obj.id_product, obj.id_characteristic);
                this.toast.showMessage('Eliminado correctamente', 'success')
              }
           /*   else {
                let response = await this.productCharacteristicBusiness.save(obj);
                this.toast.showMessage('Agregado correctamente', 'success')
              }*/
            });

            resolve(true);
            this.formArr.removeAt(index);
          } else {
            resolve(false);
          }
        });
      });
    } catch (error) {
      this.toast.showMessage('Algo pasó!', 'danger')
      console.log(`An error occurred deleting product - category ` + error)
    }
  }

  editRow(index: number) {
    if (this.characteristicsForm.invalid) {
      return;
    }
    this.formArr.controls[index].get('val').enable();
    this.formArr.controls[index].get('edit').setValue(false);
    this.isBeingEdited = true;
  }

  saveRow(index: number) {
    try {
      if (this.characteristicsForm.invalid) {
        return;
      }
      //console.log(this.product);

      if (!this.newItem) {
        if (this.product !== null && !!this.product) {
          let obj: ProductCharacteristicModel = new ProductCharacteristicModel();
          obj.id_characteristic = this.formArr.controls[index].get('id').value;
          obj.id_product = this.product.id;
          obj.val = this.formArr.controls[index].get('val').value;
          this.productCharacteristicBusiness.getById(obj.id_product, obj.id_characteristic).then(async res => {
            if (Object.values(res).length > 0) {
              let response = await this.productCharacteristicBusiness.update(obj);
              this.toast.showMessage('Modificado correctamente', 'success')
            }
            else {
              let response = await this.productCharacteristicBusiness.save(obj);
              this.toast.showMessage('Agregado correctamente', 'success')
            }
          });
        }
        else {
          this.toast.showMessage('Deberás seleccionar un producto', 'danger')
        }
      }
      this.formArr.controls[index].get('edit').setValue(true);
      this.isBeingEdited = false;
      this.pendingFeatures = true;


    } catch (error) {
      this.toast.showMessage('Algo pasó!', 'danger')
      console.log(`An error occurred saving product - category ` + error)
    }
  }

  getProductCharacteristic(idProduct: string) {
    try {
      this.productCharacteristicBusiness.getByProduct(idProduct).then(async res => {
        if (Object.values(res).length > 0) {

          // Clean Form Characteristics
          this.pendingFeatures = false;
          this.characteristicsForm = this._fb.group({
            Rows: this._fb.array([])
          });
          Object.values(res).map(element => {
            this.formArr.push(this.initRows(element['id_characteristic'], element['description'], element['val'], true));
          });
          this.toast.showMessage('Se cargaron las caracteristicas', 'success')
        }
        else {
          this.toast.showMessage('No hay caracteristicas', 'success')
        }
      });
      //this.formArr.controls[index].get('edit').setValue(true);
      this.isBeingEdited = false;
    } catch (error) {
      this.toast.showMessage('Algo pasó!', 'danger')
      console.log(`An error occurred saving product - category ` + error)
    }
  }

  public selectCharacteristic(characteristic) {
    if (this.characteristic !== characteristic && characteristic !== null) {
      this.characteristic = characteristic
    }
  }

  onSubmit() {
    if (this.characteristicsForm.invalid) {
      return;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.characteristicsForm.controls; }

}
