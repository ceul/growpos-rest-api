import { Component, OnInit, OnDestroy } from '@angular/core';
import CategoryModel from '../../../models/category.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryBusiness } from '../../../business/category/category.business';
import { ToastService } from '../../../services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-category-manage',
  templateUrl: './category-manage.component.html',
  styleUrls: ['./category-manage.component.scss']
})
export class CategoryManageComponent implements OnInit,OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  category: CategoryModel
  categories: CategoryModel[] = []
  categoryForm: FormGroup;
  subscription: Subscription;
  selectedFile: File
  public url
  public isTooLarge: boolean

  constructor(private categoryBusiness: CategoryBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.categoryForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'parentid': new FormControl(''),
      'image': new FormControl(''),
      'texttip': new FormControl(''),
      'catshowname': new FormControl(true),
      'catorder': new FormControl('', Validators.min(0)),
    })
  }

  async ngOnInit() {
    this.loadingService.show()
    this.categories = []
    let categories = await this.categoryBusiness.get()
    this.categories = Object.values(categories)
    this.subscription = this.categoryForm.valueChanges.subscribe(() => {
      if (this.categoryForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    this.loadingService.hide()
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public selectCategory(category) {
    if (this.category !== category && category !== null) {
      this.category = category
      this.categoryForm.reset()
      this.categoryForm.patchValue(this.category)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
      this.url = `${environment.url}:${environment.port}/category/${category.id}/getCategoryImage`
    }
    if (category === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveCategory() {
    if (this.categoryForm.valid) {
      this.loadingService.show()
      this.category = new CategoryModel(this.categoryForm.value)
      if (this.category.parentid === '') {
        this.category.parentid = null
      }
      if (this.newItem) {
        let response = await this.categoryBusiness.save(this.category)
        await this.onUploadFile()
        this.categories.unshift(Object(response))
        this.categories = this.categories.slice()
        this.toast.showMessage('La categoria ah sido añadida con exito!!', 'success')
        this.edit = false
      } else {
        //let response = await this.categoryBusiness.update(this.category)
        let promises = []
        promises.push(this.categoryBusiness.update(this.category))
        promises.push(this.onUploadFile())
        await Promise.all(promises)
        let index = this.categories.map(category => category.id).indexOf(this.category.id)
        this.categories[index] = this.category
        this.categories = this.categories.slice()
        this.toast.showMessage('La categoria ah sido actualizada con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async deleteCategory() {
    try {
      this.loadingService.show()
      let response = await this.categoryBusiness.delete(this.category.id)
      this.toast.showMessage('La categoria ah sido eliminada con exito!!', 'success')
      let index = this.categories.map(category => category.id).indexOf(this.category.id)
      this.categories.splice(index, 1)
      this.categories = this.categories.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {

    }
  }

  public clearForm() {
    try {
      let category = new CategoryModel()
      category.catshowname = true
      category.parentid = ''
      this.category = category
      this.categoryForm.reset()
      this.categoryForm.patchValue(this.category)
      this.trash = true
    } catch (e) {

    }
  }

  onFileSelected(event) {
    try {
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) !== null && event.target.files[0].size < 1000001) {
        this.selectedFile = <File>event.target.files[0]
        this.isTooLarge = false
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (_event) => {
          this.url = reader.result;
        }
      } else {
        this.isTooLarge = true
      }
    } catch (error) {
      console.log(`An error occurred onfileselected => ProductManageComponent `)
    }
  }

  async onUploadFile(){
    try{
      if (this.selectedFile !== null) {
        await this.categoryBusiness.uploadImage(this.selectedFile, this.category.id)
      } else {
        return new Promise((resolve, reject) => {
          resolve()
        })
      }
    } catch(error){
      console.log(`An error occurred onUploadFile => ConfigurationComponent `)
    }
  }
  

  setDefaultPic() {
    try {
      this.url = "../../../assets/img/product.svg";
    } catch (error) {
      console.log(`An error occurred setDefaultPic => ConfigurationComponent `)
    }
  }

}
