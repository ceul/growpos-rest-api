import { Component, OnInit } from '@angular/core';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';
import { PrintService } from 'src/app/services/print-service/print.service';
import {Router} from '@angular/router';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'growpos-product-report',
  templateUrl: './product-report.component.html',
  styleUrls: ['./product-report.component.scss'],
})
export class ProductReportComponent implements OnInit {

  constructor(private printer: Printer,
    private printService: PrintService,
    private loadingService: LoadingSpinnerService,
    private router: Router) { }

  ngOnInit() {
    
  }

  onPrintInvoice() {
    setTimeout(() => {
      window.print();
      //this.isPrinting = false;
      this.router.navigate([{ outlets: { print: null }}]);
    });
    //const invoiceIds = ['101', '102'];
    //this.printService
    //  .printDocument('product', invoiceIds);
  }
  
  public printAndriod (){
    this.printer.isAvailable().then((onsuccess: any) => {

      let options: PrintOptions = {
          name: 'MyDocument',
          printerId: 'printer007',
          duplex: true,
          landscape: true,
          grayscale: true
      };
     this.printer.print('<h1>aaa</h1>',options).then((value: any) => {
              console.log('value:', value);
          }, (error) => {
              console.log('error:', error);
          });

  }, (err) => {
      console.log('err:', err)
  });
  }

}
