import { Component, OnInit } from '@angular/core';
import AttributeSetModel from 'src/app/models/attribute-set.model';
import AttributeModel from 'src/app/models/attribute.model';
import { AttributeSetBusiness } from 'src/app/business/attribute-set/attribute-set.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AttributeBusiness } from 'src/app/business/attribute/attribute.business';
import { CreateAttributeComponent } from 'src/app/components/create-attribute/create-attribute.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'growpos-attribute-set-manage',
  templateUrl: './attribute-set-manage.component.html',
  styleUrls: ['./attribute-set-manage.component.scss'],
})
export class AttributeSetManageComponent implements OnInit {

  //-------------------- Modals ----------------------//

  createAttributeModal: BsModalRef;

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  public attributeSet: AttributeSetModel
  public attributeSets: AttributeSetModel[]
  public attributes: AttributeModel[]
  attributeSetForm: FormGroup;
  subscription: Subscription

  constructor(private attributeSetBussines: AttributeSetBusiness,
    private attributeBussines: AttributeBusiness,
    private modalService: BsModalService,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.attributeSetForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'attributes': new FormControl([], Validators.required),
    })
  }

  ngOnInit() {
    try {
      let promises = []
      this.attributeSets = []
      this.attributes = []
      this.loadingService.show()

      promises.push(this.attributeSetBussines.get().then((attributes) => {
        this.attributeSets = Object.values(attributes)
      }))

      promises.push(this.attributeBussines.get().then((attributes) => {
        this.attributes = Object.values(attributes)
      }))

      this.subscription = this.attributeSetForm.valueChanges.subscribe(() => {
        if (this.attributeSetForm.dirty) {
          if (!this.newItem) {
            this.edit = true
          }
        }
      })

      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (e) {
      console.log('An error occurred in ngOninit')
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public async selectAttributeSet(attribute) {
    try {
      if (this.attributeSet !== attribute && attribute !== null) {
        this.attributeSet = attribute
        this.attributeSetForm.reset()
        this.attributeSetForm.patchValue(this.attributeSet)
        this.attributeSetForm.get('attributes').setValue(Object.values(await this.attributeBussines.getByAttributeSet(attribute.id)))
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (attribute === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a attribute`)
    }
  }

  public async saveAttributeSet() {
    try {
      if (this.attributeSetForm.valid) {
        this.loadingService.show()
        let attribute = new AttributeSetModel()
        attribute = this.attributeSetForm.value
        this.attributeSet = attribute
        if (this.newItem) {
          let response = await this.attributeSetBussines.save(this.attributeSet)
          this.attributeSets.unshift(Object(response))
          this.attributeSets = this.attributeSets.slice()
          this.toast.showMessage('El conjunto de atributos ha sido añadido con exito!!', 'success')
          this.edit = false
          this.selectAttributeSet(response)
        } else {
          let response = await this.attributeSetBussines.update(this.attributeSet)
          let index = this.attributeSets.map(attribute => attribute.id).indexOf(this.attributeSet.id)
          this.attributeSets[index] = this.attributeSet
          this.attributeSets = this.attributeSets.slice()
          this.toast.showMessage('El conjunto de atributos ha sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a attribute`)
    }
  }

  public async deleteAttributeSet() {
    try {
      this.loadingService.show()
      let response = await this.attributeSetBussines.delete(this.attributeSet.id)
      this.toast.showMessage('El conjunto de atributos ha sido eliminada con exito!!', 'success')
      let index = this.attributeSets.map(attribute => attribute.id).indexOf(this.attributeSet.id)
      this.attributeSets.splice(index, 1)
      this.attributeSets = this.attributeSets.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a attribute`)
    }
  }

  public clearForm() {
    try {
      let attribute = new AttributeSetModel()
      attribute.name = ''
      attribute.attributes = []
      this.attributeSet = attribute
      this.attributeSetForm.reset()
      this.attributeSetForm.patchValue(this.attributeSet)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }

  public openCreateAttributeModal() {
    try {
      this.createAttributeModal = this.modalService.show(CreateAttributeComponent, { class: 'modal-lg' });
      this.createAttributeModal.content.createdAttribute.pipe(take(1)).subscribe(attribute => {
        this.attributes.push(attribute)
        let selectedAttributes = this.attributeSetForm.get('attributes').value
        selectedAttributes.push(attribute)
        this.attributeSetForm.get('attributes').setValue(selectedAttributes)
      })
    } catch (error) {
      console.log('An error ocurred opening create attribute modal: ', error);
    }
  }



}
