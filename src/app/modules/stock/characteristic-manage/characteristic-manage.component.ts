import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CharacteristicModel } from '../../../models/growPos/characteristic.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { CharacteristicBusiness } from '../../../business/growPos/characteristic.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from '../../../components/confirm-dialog/confirm-dialog.component';
import { Nav, NavData } from '../../../_nav';

import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { promise } from 'protractor';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-account-types',
  templateUrl: './characteristic-manage.component.html',
  styleUrls: ['./characteristic-manage.component.scss']
})
export class CharacteristicManageComponent implements OnInit, OnDestroy {
  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  characteristic: CharacteristicModel
  characteristics: CharacteristicModel[] = []
  subscription: Subscription
  dataForm: FormGroup;

  constructor(private characteristicBusiness: CharacteristicBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService,
    private headerBodyService: HeaderBodyService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogo: MatDialog) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.dataForm = new FormGroup({
      'id_characteristic': new FormControl(''),
      'description': new FormControl('', Validators.required),
      'state': new FormControl(true, Validators.required),
    });

    // Configure the navigation bar
    headerBodyService.navbarFunction();
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.characteristics = []

    promises.push(this.characteristicBusiness.get().then(res => {
      this.characteristics = Object.values(res)
    }));

    this.subscription = this.dataForm.valueChanges.subscribe(() => {
      if (this.dataForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide());

  }

  async canDeactivate() {
    if (this.dataForm.dirty) {
      let res = await new Promise((resolve, rejects) => {
        this.dialogo.open(ConfirmDialogComponent, {
          data: `¿Deseas salir? Faltan datos por guardar!`
        }).afterClosed().pipe(take (1)).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            resolve (true);
          } else {
            resolve (false);
          }
        });
      });
      return res;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public select(characteristic) {
    if (this.characteristic !== characteristic && characteristic !== null) {
      this.characteristic = characteristic
      this.dataForm.reset()
      this.dataForm.patchValue(this.characteristic)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
    }
    if (characteristic === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveReg() {
    if (this.dataForm.valid) {
      this.loadingService.show()
      this.characteristic = new CharacteristicModel(this.dataForm.value)
      if (this.newItem) {
        let response = await this.characteristicBusiness.save(this.characteristic)
        this.characteristics.unshift(Object(this.characteristic))
        this.characteristics = this.characteristics.slice()
        this.toast.showMessage('La caracteristica ha sido añadida con exito!!', 'success')
        this.edit = false
      } else {
        let response = await this.characteristicBusiness.update(this.characteristic)
        let index = this.characteristics.map(characteristic => characteristic.id_characteristic).indexOf(this.characteristic.id_characteristic)
        this.characteristics[index] = this.characteristic
        this.characteristics = this.characteristics.slice()
        this.toast.showMessage('La caracteristica ha sido actualizado con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async delete() {
    try {
      this.loadingService.show()
      let response = await this.characteristicBusiness.delete(this.characteristic.id_characteristic)
      this.toast.showMessage('La caracteristica ha sido eliminado con exito!!', 'success')
      let index = this.characteristics.map(characteristic => characteristic.id_characteristic).indexOf(this.characteristic.id_characteristic)
      this.characteristics.splice(index, 1)
      this.characteristics = this.characteristics.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
    }
  }

  public clearForm() {
    try {
      let characteristic = new CharacteristicModel()
      this.characteristic = characteristic
      this.dataForm.reset()
      this.dataForm.patchValue(this.characteristic)
      this.trash = true
    } catch (e) {
    }
  }
}
