import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockRoutingModule } from './stock-routing.module';
import { StockMenuComponent } from './stock-menu/stock-menu.component';
import { ProductManageComponent } from './product-manage/product-manage.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ComponentsModule } from '../../components/components.module';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { CreateSupplierComponent } from '../../components/create-supplier/create-supplier.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { CategoryManageComponent } from './category-manage/category-manage.component';
import { StockManageComponent } from './stock-manage/stock-manage.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FindProductComponent } from '../../components/find-product/find-product.component';
import { TranslateModule } from '@ngx-translate/core';
import { ProductReportComponent } from './product-report/product-report.component';
import { CharacteristicManageComponent } from './characteristic-manage/characteristic-manage.component';

// Print Native Android - IOS
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';

//Print WEB
// Print
//import { PrintGenericComponent } from '../../modules/print-generic/print-generic.component';
import {PrintService} from '../../services/print-service/print.service';
import { CurrentStockComponent } from './reports/current-stock/current-stock.component';
import { LowStockComponent } from './reports/low-stock/low-stock.component';
import { LocationStockComponent } from './reports/location-stock/location-stock.component';
import { ProductListComponent } from './reports/product-list/product-list.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { AttributesManageComponent } from './attributes-manage/attributes-manage.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AttributeSetManageComponent } from './attribute-set-manage/attribute-set-manage.component';
import { CreateAttributeComponent } from 'src/app/components/create-attribute/create-attribute.component';
import { SetAttributeValuesComponent } from 'src/app/components/set-attribute-values/set-attribute-values.component';
//import { PrintComponent } from '../print-generic/print/print.component';

@NgModule({
  declarations: [
    StockMenuComponent,
    ProductManageComponent,
    CategoryManageComponent,
    CharacteristicManageComponent,
    StockManageComponent,
    ProductReportComponent,
    CurrentStockComponent,
    LowStockComponent,
    LocationStockComponent,
    ProductListComponent,
    AttributesManageComponent,
    AttributeSetManageComponent
    //PrintGenericComponent,
    //PrintComponent
  ],
  imports: [
    CommonModule,
    StockRoutingModule,
    ModalModule.forRoot(),
    TabsModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    BsDatepickerModule.forRoot(),
    TranslateModule,
    NgxCurrencyModule,
    NgSelectModule
    
  ],
  providers: [
    BsModalService,
    Printer, // Android - IOS
    PrintService // Web
  ],
  bootstrap: [
    CreateSupplierComponent,
    CreateAttributeComponent,
    FindProductComponent,
    SetAttributeValuesComponent
  ]
})

export class StockModule { }
