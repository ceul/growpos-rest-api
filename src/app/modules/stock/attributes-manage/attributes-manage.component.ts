import { Component, OnInit } from '@angular/core';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import AttributeValueModel from 'src/app/models/attribute-value.model';
import AttributeModel from 'src/app/models/attribute.model';
import { AttributeBusiness } from 'src/app/business/attribute/attribute.business';
import * as uuid from "uuid";

@Component({
  selector: 'growpos-attributes-manage',
  templateUrl: './attributes-manage.component.html',
  styleUrls: ['./attributes-manage.component.scss'],
})
export class AttributesManageComponent implements OnInit {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  public attribute: AttributeModel
  public attributes: AttributeModel[]
  attributeForm: FormGroup;
  subscription: Subscription

  constructor(private attributeBussines: AttributeBusiness,
    private modalService: BsModalService,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.attributeForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'attribute_values': new FormControl([], Validators.required),
    })
  }

  ngOnInit() {
    try {
      let promises = []
      this.attributes = []
      this.loadingService.show()

      promises.push(this.attributeBussines.get().then((values) => {
        this.attributes = Object.values(values)
      }))

      this.subscription = this.attributeForm.valueChanges.subscribe(() => {
        if (this.attributeForm.dirty) {
          if (!this.newItem) {
            this.edit = true
          }
        }
      })

      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (e) {
      console.log('An error occurred in ngOninit')
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public async selectAttribute(attribute) {
    try {
      if (this.attribute !== attribute && attribute !== null) {
        this.attribute = attribute
        this.attributeForm.reset()
        this.attributeForm.patchValue(this.attribute)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (attribute === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a attribute`)
    }
  }

  public async saveAttribute() {
    try {
      if (this.attributeForm.valid) {
        this.loadingService.show()
        let attribute = new AttributeModel()
        attribute = this.attributeForm.value
        this.attribute = attribute
        if (this.newItem) {
          let response = await this.attributeBussines.save(this.attribute)
          this.attributes.unshift(Object(response))
          this.attributes = this.attributes.slice()
          this.toast.showMessage('El conjunto de atributos ha sido añadido con exito!!', 'success')
          this.selectAttribute(response)
          this.edit = false
        } else {
          let response = await this.attributeBussines.update(this.attribute)
          let index = this.attributes.map(attribute => attribute.id).indexOf(this.attribute.id)
          this.attributes[index] = this.attribute
          this.attributes = this.attributes.slice()
          this.toast.showMessage('El conjunto de atributos ha sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a attribute`)
    }
  }

  public async deleteAttribute() {
    try {
      this.loadingService.show()
      let response = await this.attributeBussines.delete(this.attribute.id)
      this.toast.showMessage('El conjunto de atributos ha sido eliminada con exito!!', 'success')
      let index = this.attributes.map(attribute => attribute.id).indexOf(this.attribute.id)
      this.attributes.splice(index, 1)
      this.attributes = this.attributes.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a attribute`)
    }
  }

  public clearForm() {
    try {
      let attribute = new AttributeModel()
      attribute.name = ''
      attribute.attribute_values = []
      this.attribute = attribute
      this.attributeForm.reset()
      this.attributeForm.patchValue(this.attribute)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }

  public addTagFn(value) {
    try{
      return { id: uuid.v4(), value: value };
    } catch(e){
      console.log(`An error occurred adding tag`)
    }
  }


}
