import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-stock-menu',
  templateUrl: './stock-menu.component.html',
  styleUrls: ['./stock-menu.component.scss']
})
export class StockMenuComponent implements OnInit {

  public activeLang = environment.activeLang;

  module: {
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };

  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);
  }

  ngOnInit() {
    this.module = {
      name: 'inventory',
      manage: 'manage',
      reports: 'reports',
      manageItems: [],
      reportsItems: []
    };
    this.module.manageItems.push({
      name: 'products',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/product-manage'
    });
    this.module.manageItems.push({
      name: 'categories',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/category-manage'
    });
    this.module.manageItems.push({
      name: 'characteristic',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/characteristic-manage'
    });
    this.module.manageItems.push({
      name: 'stock_managment',
      icon: 'fa fa-shipping-fast fa-lg',
      url: '/stock/stock-manage'
    });
    this.module.manageItems.push({
      name: 'attribute_managment',
      icon: 'fa fa-shipping-fast fa-lg',
      url: '/stock/attribute-manage'
    });
    this.module.manageItems.push({
      name: 'attribute_set_managment',
      icon: 'fa fa-shipping-fast fa-lg',
      url: '/stock/attribute-set-manage'
    });
    this.module.reportsItems.push({
      name: 'current-stock',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/report/current-stock'
    });
    this.module.reportsItems.push({
      name: 'low-stock',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/report/low-stock'
    });
    this.module.reportsItems.push({
      name: 'location-stock',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/report/location-stock'
    });
    this.module.reportsItems.push({
      name: 'product-list',
      icon: 'fa fa-shopping-cart fa-lg',
      url: '/stock/report/product-list'
    });
  }

}
