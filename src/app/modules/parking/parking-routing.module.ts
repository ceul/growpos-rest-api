import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlacesComponent } from 'src/app/components/places/places.component';
import { ParkingFeeComponent } from './parking-fee/parking-fee.component';
import { ParkingMovComponent } from './parking-mov/parking-mov.component';
import { MonthlyPaymentPanelComponent } from './monthly-payment-panel/monthly-payment-panel.component';
import { VehicleComponent } from './vehicle/vehicle.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Parqueadero'
    },
    children: [
      {
        path: 'places/:type',
        component: PlacesComponent,
        data: {
          title: 'Parqueadero'
        }
      },
      {
        path: 'mov',
        component: ParkingMovComponent,
        data: {
          title: 'Movimiento'
        }
      },
      {
        path: 'configuration',
        component: ParkingFeeComponent,
        data: {
          title: 'Configuracion'
        }
      },
      {
        path: 'monthly-payment-panel',
        component: MonthlyPaymentPanelComponent,
        data: {
          title: 'Monthly Payment Panel'
        }
      },
      {
        path: 'vehicle-panel',
        component: VehicleComponent,
        data: {
          title: 'Vehicle Panel'
        }
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ParkingRoutingModule { }
