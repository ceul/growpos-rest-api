import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import ContractModel from 'src/app/models/contract.model';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ContractBusiness } from 'src/app/business/contract/contract.business';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CreateCustomerComponent } from 'src/app/components/create-customer/create-customer.component';
import { take } from 'rxjs/operators';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import CustomerModel from 'src/app/models/customer.model';
import { CreateVehicleComponent } from 'src/app/components/create-vehicle/create-vehicle.component';
import { FindVehicleComponent } from 'src/app/components/find-vehicle/find-vehicle.component';
import VehicleModel from 'src/app/models/vehicle.model';

@Component({
  selector: 'growpos-monthly-payment-panel',
  templateUrl: './monthly-payment-panel.component.html',
  styleUrls: ['./monthly-payment-panel.component.scss'],
})
export class MonthlyPaymentPanelComponent implements OnInit, OnDestroy {

  findCustomerModal: BsModalRef;
  createCustomerModal: BsModalRef;
  customer: CustomerModel
  vehicle: VehicleModel
  createVehicleModal: BsModalRef
  findVehicleModal: BsModalRef

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  contract: ContractModel
  contracts: ContractModel[] = []

  contractForm: FormGroup;
  subscription: Subscription

  constructor(private contractBusiness: ContractBusiness,
    private modalService: BsModalService,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.contractForm = new FormGroup({
      'id': new FormControl(''),
      'customer': new FormControl('', Validators.required),
      'datestart': new FormControl('', Validators.required),
      'price': new FormControl('', Validators.required),
      'vehicle': new FormControl('', Validators.required),
      'state': new FormControl(true),
    })
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.contracts = []
    this.customer = new CustomerModel()
    promises.push(this.contractBusiness.get().then(contracts => {
      this.contracts = Object.values(contracts)
    }))

    this.subscription = this.contractForm.valueChanges.subscribe(() => {
      if (this.contractForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public selectContract(contract) {
    try {
      if (this.contract !== contract && contract !== null) {
        this.contract = contract
        this.contractForm.reset()
        this.vehicle = this.contract.vehicle
        this.customer = this.contract.customer
        this.contractForm.patchValue({
          id: this.contract.id,
          customer: this.contract.customer.name,
          datestart: new Date(this.contract.datestart),
          price: this.contract.price,
          vehicle: this.contract.vehicle.plate,
          state: this.contract.state
        })
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (contract === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a contract`)
    }
  }

  public async saveContract() {
    try {
      if (this.contractForm.valid) {
        this.loadingService.show()
        this.contract = this.contractForm.value
        let values = this.contractForm.value
        values.customer = this.customer.id
        values.vehicle = this.vehicle.id
        if (this.newItem) {
          let response = await this.contractBusiness.save(values)
          response["customer"] = this.customer
          response["vehicle"] = this.vehicle
          this.contracts.unshift(Object(response))
          this.contracts = this.contracts.slice()
          this.toast.showMessage('El contrato ha sido añadido con exito!!', 'success')
          this.edit = false
        } else {
          await this.contractBusiness.update(values)
          let index = this.contracts.map(contract => contract.id).indexOf(this.contract.id)
          this.contract.customer = this.customer
          this.contract.vehicle = this.vehicle
          this.contracts[index] = this.contract
          this.contracts = this.contracts.slice()
          this.toast.showMessage('El contrato ha sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a contract`)
    }
  }

  public async deleteContract() {
    try {
      this.loadingService.show()
      let response = await this.contractBusiness.delete(this.contract.id)
      this.toast.showMessage('El contrato ha sido eliminado con exito!!', 'success')
      let index = this.contracts.map(contract => contract.id).indexOf(this.contract.id)
      this.contracts.splice(index, 1)
      this.contracts = this.contracts.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a contract`)
    }
  }

  public clearForm() {
    try {
      let contract = new ContractModel()
      this.contract = contract
      this.contractForm.reset()
      this.contractForm.patchValue(this.contract)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }


  openFindCustomerModal() {
    try {
      this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
      this.findCustomerModal.content.selectedCustomer.pipe(take(1)).subscribe(customer => {
        this.customer = customer
        this.contractForm.controls['customer'].setValue(this.customer.name)
      })
    } catch (error) {
      console.log('An error ocurred opening find customer modal: ', error);

    }
  }

  openCreateCustomerModal() {
    try {
      this.createCustomerModal = this.modalService.show(CreateCustomerComponent, { class: 'modal-lg' });
      this.createCustomerModal.content.createdCustomer.pipe(take(1)).subscribe(customer => {
        this.customer = customer
        this.contractForm.controls['customer'].setValue(this.customer.name)
      })
    } catch (error) {
      console.log('An error ocurred opening create customer modal: ', error);
    }
  }

  openFindVehicleModal() {
    try {
      this.findVehicleModal = this.modalService.show(FindVehicleComponent, { class: 'modal-lg' });
      this.findVehicleModal.content.selectedVehicle.pipe(take(1)).subscribe(vehicle => {
        this.vehicle = vehicle
        this.contractForm.controls['vehicle'].setValue(this.vehicle.plate)
      })
    } catch (error) {
      console.log('An error ocurred opening find vehicle modal: ', error);

    }
  }

  openCreateVehicleModal() {
    try {
      this.createVehicleModal = this.modalService.show(CreateVehicleComponent, { class: 'modal-lg' });
      this.createVehicleModal.content.createdVehicle.pipe(take(1)).subscribe(vehicle => {
        this.vehicle = vehicle
        this.contractForm.controls['vehicle'].setValue(this.vehicle.plate)
      })
    } catch (error) {
      console.log('An error ocurred opening create vehicle modal: ', error);
    }
  }
}