import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import VehicleModel from 'src/app/models/vehicle.model';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { VehicleBusiness } from 'src/app/business/vehicle/vehicle.business';
import { ParkingFeeBusiness } from 'src/app/business/parking-fee/parking-fee.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'growpos-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss'],
})
export class VehicleComponent implements OnInit {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  vehicle: VehicleModel
  vehicles: VehicleModel[] = []

  subscription: Subscription
  parkingFees: any
  vehicleForm: FormGroup;

  constructor(private loadingService: LoadingSpinnerService,
    private vehicleBusiness: VehicleBusiness,
    private parkingFeeBusiness: ParkingFeeBusiness,
    private toast: ToastService) {
    this.vehicleForm = new FormGroup({
      'plate': new FormControl('', Validators.required),
      'type': new FormControl('', Validators.required),
      'model': new FormControl(''),
      'color': new FormControl(''),
      'description': new FormControl(''),
    })

  }

  ngOnInit() {
    try {
      this.loadingService.show()
      let promises = []
      this.vehicle = new VehicleModel()
      promises.push(this.parkingFeeBusiness.getGroupByType().then(parkingFees => {
        this.parkingFees = Object.values(parkingFees)
      }))

      promises.push(this.vehicleBusiness.get().then(vehicles => {
        this.vehicles = Object.values(vehicles)
      }))

      this.subscription = this.vehicleForm.valueChanges.subscribe(() => {
        if (this.vehicleForm.dirty) {
          if (!this.newItem) {
            this.edit = true
          }
        }
      })

      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (error) {
      console.log(`An error occurred on ngOnInit => VehicleComponent`)
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public selectVehicle(vehicle) {
    try {
      if (this.vehicle !== vehicle && vehicle !== null) {
        this.vehicle = vehicle
        this.vehicleForm.reset()
        this.vehicleForm.patchValue(this.vehicle)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (vehicle === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a vehicle`)
    }
  }

  public async saveVehicle() {
    try {
      if (this.vehicleForm.valid) {
        this.loadingService.show()
        let vehicle = new VehicleModel()
        vehicle = this.vehicleForm.value
        this.vehicle = vehicle
        if (this.newItem) {
          let response = await this.vehicleBusiness.save(this.vehicle)
          this.vehicles.unshift(Object(this.vehicle))
          this.vehicles = this.vehicles.slice()
          this.toast.showMessage('El vehiculo ha sido añadido con exito!!', 'success')
          this.edit = false
        } else {
          let response = await this.vehicleBusiness.update(this.vehicle)
          let index = this.vehicles.map(vehicle => vehicle.id).indexOf(this.vehicle.id)
          this.vehicles[index] = this.vehicle
          this.vehicles = this.vehicles.slice()
          this.toast.showMessage('El vehiculo ha sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a vehicle`)
    }
  }

  onKeypress(event) {
    try {
      if (typeof this.vehicleForm.controls['plate'] !== 'undefined') {
        if (this.vehicleForm.controls['plate'].value.length === 3) {
          this.vehicleForm.controls['plate'].setValue(this.vehicleForm.controls['plate'].value.toUpperCase() + "-");
        }
      }
    } catch (e) {
      console.log(`An error occurred on key press => VehicleComponent`)
    }
  }

  public async deleteVehicle() {
    try {
      this.loadingService.show()
      let response = await this.vehicleBusiness.delete(this.vehicle.id)
      this.toast.showMessage('La planta ah sido eliminada con exito!!', 'success')
      let index = this.vehicles.map(vehicle => vehicle.id).indexOf(this.vehicle.id)
      this.vehicles.splice(index, 1)
      this.vehicles = this.vehicles.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a vehicle`)
    }
  }

  public clearForm() {
    try {
      let vehicle = new VehicleModel()
      vehicle.color = ''
      vehicle.description = ''
      vehicle.model = ''
      vehicle.plate = ''
      this.vehicle = vehicle
      this.vehicleForm.reset()
      this.vehicleForm.patchValue(this.vehicle)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }

}