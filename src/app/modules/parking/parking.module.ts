import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParkingRoutingModule } from './parking-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { NgxCurrencyModule } from 'ngx-currency';
import { CdkTableModule } from '@angular/cdk/table';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ParkingFeeComponent } from './parking-fee/parking-fee.component';
import { TimepickerModule, TimepickerConfig, TimepickerActions } from 'ngx-bootstrap/timepicker';
import { ParkingMovComponent } from './parking-mov/parking-mov.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { LoadingSpinnerComponent } from 'src/app/components/loading-spinner/loading-spinner.component';
import { ChangeComponent } from 'src/app/components/change/change.component';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { MonthlyPaymentPanelComponent } from './monthly-payment-panel/monthly-payment-panel.component';
import { CreateCustomerComponent } from 'src/app/components/create-customer/create-customer.component';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { CreateVehicleComponent } from 'src/app/components/create-vehicle/create-vehicle.component';
import { FindVehicleComponent } from 'src/app/components/find-vehicle/find-vehicle.component';
import { VehicleComponent } from './vehicle/vehicle.component';


@NgModule({
  declarations: [
    ParkingFeeComponent,
    ParkingMovComponent,
    MonthlyPaymentPanelComponent,
    VehicleComponent
  ],
  imports: [
    ParkingRoutingModule,
    CommonModule,
    MatTableModule,
    FormsModule,
    TranslateModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    MatPaginatorModule,
    ModalModule,
    ComponentsModule,
    CdkTableModule,
    NgxCurrencyModule,
    TimepickerModule,
    PipeModule.forRoot(),
  ],
  providers: [
    BsModalService,
    TimepickerConfig,
    TimepickerActions
  ],
  bootstrap: [
    PaymentsComponent,
    ChangeComponent,
    LoadingSpinnerComponent,
    FindCustomerComponent,
    CreateCustomerComponent,
    CreateVehicleComponent,
    FindVehicleComponent
  ]
})
export class ParkingModule { }
