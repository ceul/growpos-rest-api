import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ParkingFeeBusiness } from 'src/app/business/parking-fee/parking-fee.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import ParkingFeeModel from 'src/app/models/parking-fee.model';
import CategoryModel from 'src/app/models/category.model';
import { CategoryBusiness } from 'src/app/business/category/category.business';
import TaxCategoryModel from 'src/app/models/tax-category.model';
import { TaxCategoryBusiness } from 'src/app/business/tax-category/tax-category.business';
import UnitsOfMeasureModel from 'src/app/models/units-measure.model';
import { UnitOfMeasureBusiness } from 'src/app/business/uom/unit-measure.business';
import ProductModel from 'src/app/models/product.model';
import * as uuid from "uuid";

@Component({
  selector: 'growpos-parking-fee',
  templateUrl: './parking-fee.component.html',
  styleUrls: ['./parking-fee.component.scss'],
})
export class ParkingFeeComponent implements OnInit {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  parkingFee: ParkingFeeModel[]
  parkingFeesList: CategoryModel[] = []
  taxCategories: TaxCategoryModel[] = []
  uoms: UnitsOfMeasureModel[] = []

  public parkingFeeForm: FormGroup;
  subscription: Subscription
  t: any


  constructor(private parkingFeeBusiness: ParkingFeeBusiness,
    private categoryBusiness: CategoryBusiness,
    private loadingService: LoadingSpinnerService,
    private formBuilder: FormBuilder,
    private taxCategoryBusiness: TaxCategoryBusiness,
    private unitMeasureBusiness: UnitOfMeasureBusiness,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.parkingFeeForm = new FormGroup({
      'id': new FormControl(''),
      'type': new FormControl('', Validators.required),
      'rates': new FormArray([])
    })
  }

  ngOnInit() {
    this.t = this.parkingFeeForm.controls['rates'] as FormArray
    this.addRate()
    let promises = []
    this.loadingService.show()
    this.parkingFee = []
    this.parkingFeesList = []
    promises.push(this.categoryBusiness.getParking().then(parkingFeesList => {
      this.parkingFeesList = Object.values(parkingFeesList)
    }))
    promises.push(this.taxCategoryBusiness.get().then(taxCat => {
      this.taxCategories = Object.values(taxCat)
    }))
    promises.push(this.unitMeasureBusiness.get().then(uom => {
      this.uoms = Object.values(uom)
    }))

    this.subscription = this.parkingFeeForm.valueChanges.subscribe(() => {
      if (this.parkingFeeForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public addRate() {
    try {
      this.t.push(this.formBuilder.group({
        id: [''],
        rete: [''],
        starttime: [new Date(), Validators.required],
        endtime: [new Date(), Validators.required],
        time: ['', Validators.required],
        graceperiod: [0],
        units: ['', Validators.required],
        ismonthly:[false, Validators.required],
        price: [0, Validators.required],
        name: ['', Validators.required],
        tax: ['', Validators.required],
      }));
      
    } catch (error) {
      console.log(`An error occurred adding a rate => parkingFee`)
    }
  }

  public deleteRate(index) {
    try {
      
      this.t.controls.splice(index, 1);
      this.parkingFeeForm.value['rates'].splice(index, 1);
      this.parkingFeeForm.value['rates'] = this.parkingFeeForm.value['rates']
    } catch (error) {
      console.log(`An error occurred deletting a rate => parkingFee`)
    }
  }

  public async selectParkingFee(type) {
    try {
      if ((this.parkingFee.length > 0 ? this.parkingFee[0].type !== type && type !== null : type !== null) ) {
        let parkingFee = Object.values(await this.parkingFeeBusiness.getByType(type.id))
        this.parkingFee = parkingFee
        this.parkingFeeForm.reset()
        
        let parkingForm = {
          id: this.parkingFee[0].type.id,
          type: type.name,
          rates: []
        }
        this.parkingFee.map(rate => {
          parkingForm.rates.push({
            id: rate.id,
            rate: rate.rate.id,
            type: rate.type.id,
            endtime: new Date('1969-01-01T' + rate.endtime + 'Z'),
            starttime: new Date('1970-01-01T' + rate.starttime + 'Z'),
            time: rate.time,
            graceperiod: rate.graceperiod,
            units: rate.rate.uom,
            ismonthly: rate.isMonthly,
            price: rate.rate.pricesell,
            name: rate.rate.name,
            tax: rate.rate.taxcat
          })
        })
        this.t.controls = []
        parkingForm.rates.map(rate => {
          this.t.push(this.formBuilder.group({
            id: [rate.id],
            rate: [rate.rate],
            type: [rate.type],
            starttime: [rate.starttime, Validators.required],
            endtime: [rate.endtime, Validators.required],
            time: [rate.time, Validators.required],
            graceperiod: [rate.graceperiod, Validators.required],
            units: [rate.units, Validators.required],
            ismonthly: [rate.ismonthly, Validators.required],
            price: [rate.pricesell, Validators.required],
            name: [rate.name,Validators.required],
            tax: [rate.tax, Validators.required]
          }));
          
        })
        
        this.parkingFeeForm.patchValue(parkingForm)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (type === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a parkingFee`)
    }
  }

  public async saveParkingFee() {
    try {
      if (this.parkingFeeForm.valid) {
        
        this.loadingService.show()
        this.parkingFee = []
        this.parkingFeeForm.value['rates'].map(rate => {
          let parkingFee = new ParkingFeeModel()
          parkingFee.id = rate.id
          parkingFee.starttime = rate.starttime
          parkingFee.endtime = rate.endtime
          parkingFee.time = rate.time
          parkingFee.isMonthly = rate.ismonthly
          parkingFee.graceperiod = rate.graceperiod
          let product = new ProductModel()
          
          product.id = rate.rate
          product.category = rate.type
          product.name = rate.name
          product.isservice = true
          product.pricebuy = 0
          product.pricesell = rate.price
          product.code = uuid.v4()
          product.reference = product.code
          product.taxcat = rate.tax
          product.uom = rate.units
          product.stockcost = 0
          product.stockvolume = 0
          product.codetype = '0'
          product.stockcost = 0
          product.stockvolume = 0
          product.iscom = false
          product.isscale = false
          product.isconstant = false
          product.printkb = false
          product.sendstatus = false
          product.isservice = false
          product.isvprice = false
          product.isverpatrib = false
          product.warranty = false
          product.stockunits = 0
          product.flag = false
          product.weigth = 0
          product.width = 0
          product.height = 0
          product.length = 0
          product.catorder = 0
          product.inCat = true
          parkingFee.rate = product
          let category = new CategoryModel()
          category.id = this.parkingFeeForm.value['id']
          category.name = this.parkingFeeForm.value['type']
          category.type = 'p'
          parkingFee.type = category
          this.parkingFee.push(parkingFee)
        })


        if (this.newItem) {
          let response = await this.parkingFeeBusiness.save(this.parkingFee)
          this.parkingFeesList.unshift(Object(response[0].type))
          this.parkingFeesList = this.parkingFeesList.slice()
          this.toast.showMessage('La tarifa ha sido añadida con exito!!', 'success')
          this.edit = false
        } else {
          let response = await this.parkingFeeBusiness.update(this.parkingFee)
          let index = this.parkingFeesList.map(parkingFee => parkingFee.id).indexOf(this.parkingFee[0].type.id)
          this.parkingFeesList[index] = this.parkingFee[0].type
          this.parkingFeesList = this.parkingFeesList.slice()
          this.toast.showMessage('La tarifa ha sido actualizada con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a parkingFee`)
    }
  }

  public async deleteParkingFee() {
    try {
      this.loadingService.show()
      let response = await this.parkingFeeBusiness.delete(this.parkingFee[0].type.id)
      this.toast.showMessage('La tarifa ha sido eliminada con exito!!', 'success')
      let index = this.parkingFeesList.map(parkingFee => parkingFee.id).indexOf(this.parkingFee[0].type.id)
      this.parkingFeesList.splice(index, 1)
      this.parkingFeesList = this.parkingFeesList.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a parkingFee`)
    }
  }

  public clearForm() {
    try {
      this.parkingFeeForm.reset()
      this.parkingFee = []
      this.t.controls = []
      this.addRate()
      
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }
}
