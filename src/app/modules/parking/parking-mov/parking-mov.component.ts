import { Component, OnInit, ViewChild } from '@angular/core';
import ParkingMovModel from 'src/app/models/parking_mov.model';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ParkingBusiness } from 'src/app/business/parking/parking.business';
import { Time } from '@angular/common';
import { PrintService } from 'src/app/services/print-service/print.service';
import { ParkingFeeBusiness } from 'src/app/business/parking-fee/parking-fee.business';
import ParkingFeeModel from 'src/app/models/parking-fee.model';
import TimeUnitsEnum from 'src/app/models/time-units.enum';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { take } from 'rxjs/operators';
import TicketModel from 'src/app/models/ticket.model';
import * as uuid from "uuid";
import TicketEnum from 'src/app/models/ticket.enum';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import PeopleModel from 'src/app/models/people.model';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import TaxModel from 'src/app/models/tax.model';
import { TaxBusiness } from 'src/app/business/tax/tax.business';
import { ContractBusiness } from 'src/app/business/contract/contract.business';
import { flattenStyles } from '@angular/platform-browser/src/dom/dom_renderer';
import { CustomerBusiness } from 'src/app/business/customer/customer.business';


@Component({
  selector: 'growpos-parking-mov',
  templateUrl: './parking-mov.component.html',
  styleUrls: ['./parking-mov.component.scss'],
})
export class ParkingMovComponent implements OnInit {

  //------------------- Modals -----------------------
  paymentsModal: BsModalRef;

  private ticket: TicketModel
  private hostConfig: HostConfigModel
  private activeCash: string
  private user: PeopleModel
  private taxes: TaxModel[]
  parkingMovModel: ParkingMovModel
  parkingFees: any
  fees
  parked_cars: {}[]
  displayedColumnsParked = ['name', 'total', 'notes'];
  clockElinnerHTML: string;
  tableHeight = '250px'
  dialLines: {}[]
  plate: string;
  id: string;
  date: Date;
  clockHandle;
  show: boolean = false
  isMonthly: boolean = false
  disabled: boolean = true;
  msg: string = "Registrar Entrada";
  date_arrival_get: Date
  state: string
  total: number = 1000;
  toUpdate: boolean = false;
  public submit: boolean = false
  public contractEnd: Date

  public parkingMovForm: FormGroup;

  @ViewChild('reminder') public reminder: ModalDirective;

  constructor(private loadingService: LoadingSpinnerService,
    public printService: PrintService,
    private toast: ToastService,
    private parkingBusiness: ParkingBusiness,
    private contractBusiness: ContractBusiness,
    private modalService: BsModalService,
    private storage: Storage,
    private closeCashBusiness: CloseCashBusiness,
    public authBusiness: AuthBusiness,
    private taxBusiness: TaxBusiness,
    private parkingFeeBusiness: ParkingFeeBusiness,
    private customerBusiness: CustomerBusiness
  ) {

    this.parkingMovForm = new FormGroup({
      'id': new FormControl(''),
      'type': new FormControl('', Validators.required),
      'plate': new FormControl('', Validators.required),
      'date_arrival': new FormControl(new Date()),
      'date_departure': new FormControl(''),
      'places': new FormControl(''),
      'receipt': new FormControl(''),
      'fee': new FormControl('', Validators.required),
      'note': new FormControl(''),
      'total': new FormControl(''),
      'state': new FormControl(''),
      'date_arrival_get': new FormControl(new Date()),
      'print': new FormControl(true),
    })
  }

  async ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.contractEnd = new Date()
    await this.resetComponet()
    this.clockHandle = setInterval(() => {
      if (!this.toUpdate) {
        this.parkingMovForm.controls['date_arrival'].setValue(new Date())
      }
    }, 1000);

    this.fees = []
    this.date_arrival_get = new Date()

    promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
      this.activeCash = Object.values(closeCash)[0].money
    }))

    promises.push(this.parkingFeeBusiness.getGroupByType().then(parkingFees => {
      this.parkingFees = Object.values(parkingFees)
    }))

    promises.push(this.parkingBusiness.get().then(parked_cars => {
      this.parked_cars = Object.values(parked_cars)
    }))

    promises.push(this.authBusiness.readUser().then(user => {
      this.user = user
    }))

    promises.push(this.taxBusiness.get().then(taxes => {
      this.taxes = Object.values(taxes)
    }))

    Promise.all(promises).then(() => this.loadingService.hide())
  }

  public async register() {
    this.submit = true
    if (this.parkingMovForm.valid) {
      if (!this.toUpdate) {
        this.registerArrival();
      } else if (!this.isMonthly) {
        this.pay()
      } else {
        this.registerDeparture(-2)
      }
    }
  }

  public async registerDeparture(receiptId: number) {
    try {
      this.loadingService.show()
      this.parkingMovForm.controls['date_departure'].setValue(new Date())
      this.parkingMovForm.controls['receipt'].setValue(receiptId)
      let newParkingBusiness = await this.parkingBusiness.update(this.parkingMovForm.value)
      this.toast.showMessage('Salida registrada con éxito', 'success')
      let index = this.parked_cars.map(item => item['plate']).indexOf(this.parkingMovForm.controls['plate'].value)
      this.parked_cars.splice(index, 1)
      this.parked_cars = this.parked_cars.slice()
      this.submit = false
      this.clean()
      this.loadingService.hide()
    } catch (error) {
      this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar la salida del vehículo', 'danger')
      console.log(`An error occurred registering the exit`)
    }
  }

  public async registerArrival() {
    try {
      this.loadingService.show()
      this.parkingMovForm.controls['date_arrival'].setValue(new Date())
      this.parkingMovForm.controls['plate'].setValue(this.parkingMovForm.controls['plate'].value.toUpperCase())
      let newParkingBusiness = await this.parkingBusiness.save(this.parkingMovForm.value)
      this.toast.showMessage('Ingreso éxito', 'success')
      this.parked_cars.unshift(this.parkingMovForm.value)
      this.parked_cars = this.parked_cars.slice()
      this.printIncome()
      this.loadingService.hide()
      this.submit = false
      this.clean()
    } catch (error) {
      this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el ingreso del vehículo', 'danger')
      console.log(`An error occurred registering the vehicle`)
    }
  }

  public async getPlate(plate: string) {
    try {
      this.loadingService.show()
      let promises = []
      this.parkingMovForm.controls['date_departure'].setValue(new Date())
      let parking_mov
      let contract
      promises.push(this.parkingBusiness.getById(plate).then(value => {
        parking_mov = Object.values(value)
      }));
      promises.push(this.contractBusiness.getByPlate(plate).then(value => {
        contract = Object.values(value)
      }))
      await Promise.all(promises).then(() => this.loadingService.hide())
      return { parking_mov, contract };
    } catch (error) {
      this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar la consulta del vehículo', 'danger')
      console.log(`An error occurred getPlate() `)
    }
  }

  esLetra = (caracter) => {
    let ascii = caracter.toUpperCase().charCodeAt(0);
    return ascii > 64 && ascii < 91;
  };

  onKeypress(event) {
    try {
      if (typeof this.parkingMovForm.controls['plate'] !== 'undefined') {
        if (this.parkingMovForm.controls['plate'].value.length === 3) {
          this.parkingMovForm.controls['plate'].setValue(this.parkingMovForm.controls['plate'].value.toUpperCase() + "-");
        }
      }
    } catch (error) {
      console.log(`An error occurred filtering the plate`)
    }
  }

  public async onChange(event) {
    if (typeof this.parkingMovForm.controls['plate'] !== 'undefined') {
      this.processPlate(this.parkingMovForm.controls['plate'].value);
    }
  }

  public async processPlate(plate: string) {
    try {
      let res = await this.getPlate(plate);
      this.isMonthly = false
      if (res.contract.length > 0) {
        if (res.contract[0].state) {
          this.isMonthly = true
        }
      }
      if (res.parking_mov.length === 0) {
        this.show = false;
        this.msg = "Registrar Entrada";
        this.toUpdate = false;
        this.disabled = false;
        this.parkingMovForm.controls['note'].setValue('');
        this.resetComponet()
        this.ticket.contract = res.contract.length === 0 ? null : res.contract[0]
        if (this.isMonthly) {
          this.ticket.customer = Object.values(await this.customerBusiness.getById(res.contract[0].customer))[0]    
                
          this.parkingMovForm.controls['type'].setValue(res.contract[0].vehicle.type);
          this.selectedType()
          let fee = this.fees['parkingFees'].find(item => item.isMonthly === true)
          this.parkingMovForm.controls['fee'].setValue(fee.id);
          this.parkingMovForm.controls['plate'].setValue(res.contract[0].vehicle.plate);
          this.calculateFee();

          // monthly payment is calculated
          this.contractEnd = new Date(res.contract[0].datestart)
          let today = new Date()
          if (this.contractEnd.getDate() <= today.getDate()) {
            this.contractEnd.setMonth(today.getMonth() + 1)
          } else {
            this.contractEnd.setMonth(today.getMonth())
          }
          let substraction = (this.contractEnd.getTime() - today.getTime());
          let diff = (substraction / 86400000);
          if (diff <= 5) {
            this.reminder.show()
          }
        }
      }
      else {
        this.resetComponet()
        if (this.isMonthly) {
          this.ticket.customer = Object.values(await this.customerBusiness.getById(res.contract[0].customer))[0]    
        }
        this.ticket.contract = res.contract.length === 0 ? null : res.contract[0]
        this.show = true;
        this.toUpdate = true;
        this.msg = "Registrar Salida";
        this.date_arrival_get = res.parking_mov[0].date_arrival;
        // Set information for update register
        this.parkingMovForm.controls['id'].setValue(res.parking_mov[0].id);
        this.parkingMovForm.controls['date_arrival'].setValue(res.parking_mov[0].date_arrival);
        this.parkingMovForm.controls['date_arrival_get'].setValue(res.parking_mov[0].date_arrival);
        this.parkingMovForm.controls['date_departure'].setValue(new Date())
        this.parkingMovForm.controls['places'].setValue(res.parking_mov[0].places);
        this.parkingMovForm.controls['type'].setValue(res.parking_mov[0].type);
        this.parkingMovForm.controls['fee'].setValue(res.parking_mov[0].fee);
        this.parkingMovForm.controls['note'].setValue(res.parking_mov[0].note);
        this.parkingMovForm.controls['plate'].setValue(res.parking_mov[0].plate);
        this.parkingMovForm.controls['state'].setValue("1");
        this.parkingMovForm.controls['receipt'].setValue(-2); // Pendiente
        this.selectedType()
        this.disabled = false;
        this.calculateFee();
      }

    } catch (error) {
      console.log('An error ocurred in ParkingMovComponent => onChange')
    }
  }

  public printIncome() {
    try {
      if (this.parkingMovForm.controls['print'].value) {
        let fee = (this.fees['parkingFees'].find(item => item.id === this.parkingMovForm.controls['fee'].value))
        let price = fee.rate.pricesell
        fee = fee.rate.name
        let print = {
          date_arrival: this.parkingMovForm.controls['date_arrival'].value,
          plate: this.parkingMovForm.controls['plate'].value,
          type: this.fees['name'],
          fee,
          note: this.parkingMovForm.controls['note'].value,
          price
        }
        this.printService.printDocument('parking-income', print)
      }
    } catch (e) {
      console.log('An error occurred printing parking income')
    }
  }

  private calculateFee() {
    try {
      let date_start = new Date(this.parkingMovForm.controls['date_arrival'].value)
      let date_end = this.parkingMovForm.controls['date_departure'].value
      let res = (date_end.getTime() - date_start.getTime());
      let diff = 0
      let gracePeriod = 0
      let fee = this.fees['parkingFees'].find(item => item.id === this.parkingMovForm.controls['fee'].value)
      if (TimeUnitsEnum.MINUTE === parseInt(fee.rate.uom)) {
        diff = Math.round(res / 60000); // minutes
      } else if (TimeUnitsEnum.HOUR === parseInt(fee.rate.uom)) {
        diff = res / 3600000
        gracePeriod = (fee.graceperiod) / 60
        if (diff % 1 > gracePeriod) {
          diff = Math.ceil(diff); // days
        } else {
          diff = Math.floor(diff);
        }
      } else if (TimeUnitsEnum.DAY === parseInt(fee.rate.uom)) {
        diff = (res / 86400000);
        gracePeriod = (fee.graceperiod) / 1440
        if (diff % 1 > gracePeriod) {
          diff = Math.ceil(diff);
        } else {
          diff = Math.floor(diff);
        }
      }
      diff = Math.ceil(diff / fee.time)
      if (this.isMonthly) {
        fee.rate.pricesell = this.ticket.contract.price
      }
      this.ticket.addProduct(fee.rate, this.taxes, this.toast, diff)
      this.parkingMovForm.controls['total'].setValue(this.ticket.total)
    } catch (e) {
      console.log('An error ocurred calculating fee' + e)
    }
  }

  clean() {
    try {
      this.isMonthly = false
      this.fees = []
      this.disabled = true;
      this.show = false
      this.msg = 'Registrar Entrada'
      this.toUpdate = false
      this.date_arrival_get = new Date()
      this.parkingMovForm.reset()
      this.parkingMovForm.controls['date_departure'].setValue(new Date())
      this.parkingMovForm.controls['plate'].setValue("");
      this.parkingMovForm.controls['places'].setValue("");
      this.parkingMovForm.controls['receipt'].setValue(-1);
      this.parkingMovForm.controls['type'].setValue("");
      this.parkingMovForm.controls['fee'].setValue("");
      this.parkingMovForm.controls['note'].setValue("");
      this.parkingMovForm.controls['state'].setValue("");
      this.resetComponet()
    } catch (e) {
      console.log(`An error occurred cleaning the form`)
    }
  }

  public selectedType() {
    try {
      this.fees = this.parkingFees.find(item => item.id === this.parkingMovForm.controls['type'].value)
    } catch (e) {
      console.log('An error ocurred selecting type')
    }
  }

  async openPaymentsModal() {
    try {
      this.paymentsModal = this.modalService.show(PaymentsComponent, {
        class: 'modal-lg',
        initialState: {
          origin: 'sales',
          ticket: this.ticket,
          print: this.parkingMovForm.controls['print'].value
        }
      });
      this.paymentsModal.content.completed.pipe(take(1)).subscribe(async completed => {
        if (completed.isCompleted) {
          this.registerDeparture(completed.ticketId);
        } else {
          this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el pago', 'danger')
        }
      })
    } catch (error) {
      console.log('An error ocurred opening payments modal: ', error);
    }
  }

  async resetComponet() {
    try {
      this.ticket = new TicketModel()
      this.ticket.id = uuid.v4()
      this.ticket.pickupId = 0
      this.ticket.ticketId = 0
      this.ticket.ticketType = TicketEnum.RECEIPT_NORMAL
      this.ticket.ticketstatus = 0
      this.ticket.user = this.user ? this.user : null
      this.ticket.lines = []
      this.ticket.taxes = []
      this.ticket.date = new Date()
      let hoursDiff = this.ticket.date.getHours() - this.ticket.date.getTimezoneOffset() / 60;
      this.ticket.date.setHours(hoursDiff);
      this.ticket.total = 0
      this.ticket.subTotal = 0
      this.ticket.totalTaxes = 0
      this.ticket.activeCash = this.activeCash
      this.ticket.contract = null
      this.hostConfig = await this.storage.get('config')
      this.ticket.host = this.hostConfig.hostName
    } catch (e) {
      console.log('An error ocurred resetting tickets modal: ', e);
    }
  }

  async pay() {
    try {
      //this.calculateFee()
      this.openPaymentsModal()
    } catch (e) {
      console.log('An error occurred paying')
    }
  }

  public getRecord(event) {
    if (typeof event.plate !== 'undefined') {
      this.processPlate(event.plate);
    }
  }
}
