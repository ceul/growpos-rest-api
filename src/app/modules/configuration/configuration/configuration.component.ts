import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import ResourceModel from 'src/app/models/resource.model';
import HostConfigModel from 'src/app/models/host-config.model';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ResourceBusiness } from 'src/app/business/resource/resource.business';
import { Router } from '@angular/router';
import ConfigModeEnum from 'src/app/models/config-mode.enum';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { environment } from 'src/environments/environment';
import PrinterSizeEnum from 'src/app/models/printer-size.enum';

import { PrintService } from './../../../services/print-service/print.service';
@Component({
  selector: 'growpos-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent implements OnInit {

  configForm: FormGroup;
  submit: boolean;
  newResource: ResourceModel
  configModel: HostConfigModel
  file: File
  public activeLang = environment.activeLang;
  public url
  public modes
  public sizes
  public modesKeys
  public sizesKeys
  public printerList
  public isTooLarge: boolean

  private selectedPrinter: any = [];

  constructor(private toast: ToastService,
    private resourceBusiness: ResourceBusiness,
    private router: Router,
    private loadingService: LoadingSpinnerService,
    private translate: TranslateService,
    private storage: Storage,
    private printService: PrintService) {
    this.translate.setDefaultLang(this.activeLang);
    this.configForm = new FormGroup({
      'hostName': new FormControl('', Validators.required),
      'configMode': new FormControl('', Validators.required),
      'companyName': new FormControl(''),
      'nit': new FormControl(''),
      'address': new FormControl(''),
      'line1': new FormControl(''),
      'line2': new FormControl(''),
      'line3': new FormControl(''),
      'activePrint': new FormControl(true),
      'activeTip': new FormControl(false),
      'prefix': new FormControl(''),
      'size': new FormControl('')
    })
  }

  async ngOnInit() {
    this.file = null
    this.isTooLarge = false
    this.url = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.loadingService.show()
    this.modes = ConfigModeEnum
    this.sizes = PrinterSizeEnum
    this.sizesKeys = Object.keys(this.sizes).filter(Number)
    this.modesKeys = Object.keys(this.modes).filter(Number)
    this.submit = false;
    let fingerprint = this.getFingerPrint()
    this.newResource = Object.values(await this.resourceBusiness.getByHost(fingerprint))[0]
    this.configModel = await this.storage.get('config')
    this.listBTDevice()
    this.configForm.controls['hostName'].setValue(this.configModel.hostName)
    this.configForm.controls['configMode'].setValue(this.configModel.configMode)
    this.configForm.controls['companyName'].setValue(this.configModel.companyName)
    this.configForm.controls['nit'].setValue(this.configModel.nit)
    this.configForm.controls['address'].setValue(this.configModel.address)
    this.configForm.controls['line1'].setValue(this.configModel.line1)
    this.configForm.controls['line2'].setValue(this.configModel.line2)
    this.configForm.controls['line3'].setValue(this.configModel.line3)
    this.configForm.controls['activePrint'].setValue(this.configModel.activePrint)
    this.configForm.controls['activeTip'].setValue(this.configModel.activeTip)
    this.configForm.controls['prefix'].setValue(this.configModel.prefix)
    this.configForm.controls['size'].setValue(this.configModel.size)
    this.loadingService.hide()
    let image = <File>await this.resourceBusiness.getCompanyLogo()
  }

  async submitForm() {
    try {
      if (this.configForm.valid) {
        this.loadingService.show()
        this.configModel.hostName = this.configForm.controls['hostName'].value
        this.configModel.configMode = this.configForm.controls['configMode'].value
        this.configModel.companyName = this.configForm.controls['companyName'].value
        this.configModel.nit = this.configForm.controls['nit'].value
        this.configModel.address = this.configForm.controls['address'].value
        this.configModel.line1 = this.configForm.controls['line1'].value
        this.configModel.line2 = this.configForm.controls['line2'].value
        this.configModel.line3 = this.configForm.controls['line3'].value
        this.configModel.activePrint = this.configForm.controls['activePrint'].value
        this.configModel.activeTip = this.configForm.controls['activeTip'].value
        this.configModel.prefix = this.configForm.controls['prefix'].value
        this.configModel.size = this.configForm.controls['size'].value
        this.newResource.content = JSON.parse(JSON.stringify(this.configModel))
        await this.resourceBusiness.update(this.newResource)
        this.storage.set('config', this.configModel);
        await this.onUploadFile()
        this.loadingService.hide()
        this.toast.showMessage('La configuración ha sido actualizada con exito. Los cambios han sido aplicados', 'success')
      } else {
        this.submit = true
      }
    } catch (error) {
      console.log('An error ocurred in submitForm => first-config')
    }
  }

  listBTDevice() {
    this.printService.searchBt().then(datalist => {
      this.printerList = datalist

    }, err => {
      console.log("ERROR", err);
    })

  }

  testConnectPrinter() {
    var id = this.selectedPrinter.id;
    if (id == null || id == "" || id == undefined) {
      //nothing happens, you can put an alert here saying no printer selected
    }
    else {
      let foo = this.printService.connectBT(id).subscribe(data => {
        console.log("CONNECT SUCCESSFUL", data);

      }, err => {
        console.log("Not able to connect", err);

      });
    }
  }

  testPrinter() {
    var id = this.selectedPrinter.id;
    if (id == null || id == "" || id == undefined) {
      //nothing happens, you can put an alert here saying no printer selected
    }
    else {
      let foo = this.printService.testPrint(id);
    }
  }

  onFileSelected(event) {
    try {
      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) !== null && event.target.files[0].size < 1000001) {
        this.file = <File>event.target.files[0]
        this.isTooLarge = false
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (_event) => {
          this.url = reader.result;
        }
      } else {
        this.isTooLarge = true
      }
    } catch (error) {
      console.log(`An error occurred onfileselected => ConfigurationComponent `)
    }
  }

  async onUploadFile() {
    try {
      if (this.file !== null) {
      await this.resourceBusiness.uploadCompanyLogo(this.file)
      } else {
        return new Promise((resolve, reject) => {
          resolve()
        })
      }
    } catch (error) {
      console.log(`An error occurred onUploadFile => ConfigurationComponent `)
    }
  }

  setDefaultPic() {
    try {
      this.url = "../../../assets/img/brand/logo-grow.svg";
    } catch (error) {
      console.log(`An error occurred setDefaultPic => ConfigurationComponent `)
    }
  }

  private getFingerPrint() {
    try {
      // @ts-ignore
      let client = new ClientJS()
      let ua = client.getBrowserData().ua;
      //let canvasPrint = client.getCanvasPrint();
      let os = client.getOS();
      let cpu = client.getCPU();
      let timeZone = client.getTimeZone();
      let lenguage = client.getLanguage();
      let engine = client.getEngine();
      let device = client.getDevice();
      let deviceType = client.getDeviceType();
      let deviceVendor = client.getDeviceVendor();
      let fingerprint = client.getCustomFingerprint(ua, os, cpu, timeZone, lenguage, engine, device, deviceType, deviceVendor);
      return fingerprint
    } catch (error) {
      console.log(`An error occurred on getFingerPrint => ConfigurationComponent`)
    }
  }
}
