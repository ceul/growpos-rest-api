import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationComponent } from './configuration/configuration.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Configuración'
    },
    children: [
      {
        path: '',
        component: ConfigurationComponent,
        data: {
          title: 'Configuración'
        }
      },
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class ConfigurationRoutingModule { }
