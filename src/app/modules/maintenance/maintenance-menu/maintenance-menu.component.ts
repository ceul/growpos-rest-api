import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-menu',
  templateUrl: './maintenance-menu.component.html',
  styleUrls: ['./maintenance-menu.component.scss']
})
export class MaintenanceMenuComponent implements OnInit {

  module: {
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };

  constructor() {
    this.module = {
      name: 'Mantenimiento',
      manage: 'Gestión',
      reports: 'Reportes',
      manageItems: [],
      reportsItems: []
    };
    this.module.manageItems.push({
      name: `Usuarios`,
      icon: 'fa fa-users fa-lg',
      url: '/maintenance/user-manage'
    });
    this.module.manageItems.push({
      name: `Plantas`,
      icon: 'fa fa-building fa-lg',
      url: '/maintenance/floors'
    });
    this.module.manageItems.push({
      name: `Mesas`,
      icon: 'fa fa-table fa-lg',
      url: '/maintenance/places'
    });
  }
  ngOnInit() {
  }

}
