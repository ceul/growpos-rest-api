import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceMenuComponent } from './maintenance-menu/maintenance-menu.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { PlacesComponent } from './places/places.component';
import { FloorsComponent } from './floors/floors.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Maintenance'
    },
    children: [
      {
        path: '',
        component: MaintenanceMenuComponent,
        data: {
          title: 'Menu de Mantenimiento'
        }
      },
      {
        path: 'user-manage',
        component: UserManageComponent,
        data: {
          title: 'Administración de usuarios'
        }
      },
      {
        path: 'places',
        component: PlacesComponent,
        data: {
          title: 'Mesas'
        }
      },
      {
        path: 'floors',
        component: FloorsComponent,
        data: {
          title: 'Plantas'
        }
      }
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
