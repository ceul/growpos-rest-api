import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceMenuComponent } from './maintenance-menu/maintenance-menu.component';
import { MaintenanceRoutingModule } from './maintenance-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { UserManageComponent } from './user-manage/user-manage.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlacesComponent } from './places/places.component';
import { FloorsComponent } from './floors/floors.component';

@NgModule({
  declarations: [
    MaintenanceMenuComponent,
    UserManageComponent,
    PlacesComponent,
    FloorsComponent],
  imports: [
    CommonModule,
    MaintenanceRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class MaintenanceModule { }
