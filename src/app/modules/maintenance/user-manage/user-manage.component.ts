import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import PeopleModel from '../../../models/people.model';
import RoleModel from '../../../models/roles.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { UserBusiness } from '../../../business/user/user.business';
import { RoleBusiness } from '../../../business/role/role.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit, OnDestroy {
  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  user: PeopleModel
  users: PeopleModel[] = []
  roles: RoleModel[] = []
  subscription: Subscription
  userForm: FormGroup;

  constructor(private userBusiness: UserBusiness, 
    private loadingService: LoadingSpinnerService,
    private roleBusiness: RoleBusiness, 
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.userForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'apppassword': new FormControl(''),
      'card': new FormControl(''),
      'role': new FormControl('', Validators.required),
      'visible': new FormControl(true, Validators.required),
      'image': new FormControl(''),
    })
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.users = []
    this.roles = []
    promises.push(this.userBusiness.get().then(users => {
      this.users = Object.values(users)
    }))

    promises.push(this.roleBusiness.get().then(roles => {
      this.roles = Object.values(roles)
    }))

    this.subscription = this.userForm.valueChanges.subscribe(() => {
      if (this.userForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })

    Promise.all(promises).then(() => this.loadingService.hide())
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public selectUser(user) {
    if (this.user !== user && user !== null) {
      this.user = user
      this.userForm.reset()
      this.userForm.patchValue(this.user)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
    }
    if (user === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveUser() {
    if (this.userForm.valid) {
      this.loadingService.show()
      this.user = new PeopleModel(this.userForm.value)
      if (this.newItem) {
        let response = await this.userBusiness.save(this.user)
        this.users.unshift(Object(this.user))
        this.users = this.users.slice()
        this.toast.showMessage('El usuario ha sido añadido con exito!!', 'success')
        this.edit = false
      } else {
        let response = await this.userBusiness.update(this.user)
        let index = this.users.map(user => user.id).indexOf(this.user.id)
        this.users[index] = this.user
        this.users = this.users.slice()
        this.toast.showMessage('El usuario ha sido actualizado con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async deleteUser() {
    try {
      this.loadingService.show()
      let response = await this.userBusiness.delete(this.user.id)
      this.toast.showMessage('El usuario ha sido eliminado con exito!!', 'success')
      let index = this.users.map(user => user.id).indexOf(this.user.id)
      this.users.splice(index, 1)
      this.users = this.users.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {

    }
  }

  public clearForm() {
    try {
      let user = new PeopleModel()
      user.visible = true
      this.user = user
      this.userForm.reset()
      this.userForm.patchValue(this.user)
      this.trash = true
    } catch (e) {

    }
  }
}
