import { Component, OnInit, OnDestroy } from '@angular/core';
import FloorModel from 'src/app/models/floor.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FloorBusiness } from 'src/app/business/floor/floor.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-floors',
  templateUrl: './floors.component.html',
  styleUrls: ['./floors.component.scss'],
})
export class FloorsComponent implements OnInit,OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  floor: FloorModel
  floors: FloorModel[] = []

  floorForm: FormGroup;
  subscription: Subscription

  constructor(private floorBusiness: FloorBusiness, 
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.floorForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
    })
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.floors = []
    promises.push(this.floorBusiness.get().then(floors => {
      this.floors = Object.values(floors)
    }))

    this.subscription = this.floorForm.valueChanges.subscribe(() => {
      if (this.floorForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(()=> this.loadingService.hide())
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }
  public selectFloor(floor) {
    try {
      if (this.floor !== floor && floor !== null) {
        this.floor = floor
        this.floorForm.reset()
        this.floorForm.patchValue(this.floor)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (floor === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a floor`)
    }
  }

  public async saveFloor() {
    try {
      if (this.floorForm.valid) {
        this.loadingService.show()
        let floor = new FloorModel()
        floor = this.floorForm.value
        this.floor = floor
        if (this.newItem) {
          let response = await this.floorBusiness.save(this.floor)
          this.floors.unshift(Object(this.floor))
          this.floors = this.floors.slice()
          this.toast.showMessage('La planta ah sido añadida con exito!!', 'success')
          this.edit = false
        } else {
          let response = await this.floorBusiness.update(this.floor)
          let index = this.floors.map(floor => floor.id).indexOf(this.floor.id)
          this.floors[index] = this.floor
          this.floors = this.floors.slice()
          this.toast.showMessage('La planta ah sido actualizada con exito!!', 'success')
          this.edit = false
          console.log(response)
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a floor`)
    }
  }

  public async deleteFloor() {
    try {
      this.loadingService.show()
      let response = await this.floorBusiness.delete(this.floor.id)
      this.toast.showMessage('La planta ah sido eliminada con exito!!', 'success')
      let index = this.floors.map(floor => floor.id).indexOf(this.floor.id)
      this.floors.splice(index, 1)
      this.floors = this.floors.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a floor`)
    }
  }

  public clearForm() {
    try {
      let floor = new FloorModel()
      floor.name = ''
      this.floor = floor
      this.floorForm.reset()
      this.floorForm.patchValue(this.floor)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }
}
