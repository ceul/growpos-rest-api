import { Component, OnInit, OnDestroy } from '@angular/core';
import PlaceModel from 'src/app/models/place.model';
import FloorModel from 'src/app/models/floor.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlaceBusiness } from 'src/app/business/place/place.business';
import { FloorBusiness } from 'src/app/business/floor/floor.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.scss'],
})
export class PlacesComponent implements OnInit, OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  place: PlaceModel
  places: PlaceModel[] = []
  floors: FloorModel[] = []
  subscription: Subscription
  placeForm: FormGroup;

  constructor(private placeBusiness: PlaceBusiness,
    private loadingService: LoadingSpinnerService,
    private floorBusiness: FloorBusiness,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.placeForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'floor': new FormControl(''),
      'seats': new FormControl(''),
      'x': new FormControl(''),
    })
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.places = []
    this.floors = []
    promises.push(this.placeBusiness.get().then(places => {
      this.places = Object.values(places)
    }))

    promises.push(this.floorBusiness.get().then(floors => {
      this.floors = Object.values(floors)
    }))

    this.subscription = this.placeForm.valueChanges.subscribe(() => {
      if (this.placeForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public selectPlace(place) {
    try {
      if (this.place !== place && place !== null) {
        this.place = place
        this.placeForm.reset()
        this.placeForm.patchValue(this.place)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (place === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (error) {
      console.log(`An error occurred selecting a place`)
    }
  }

  public async savePlace() {
    try {
      if (this.placeForm.valid) {
        this.loadingService.show()
        let place = new PlaceModel()
        place = this.placeForm.value
        this.place = place
        if (this.newItem) {
          let response = await this.placeBusiness.save(this.place)
          this.places.unshift(Object(this.place))
          this.places = this.places.slice()
          this.toast.showMessage('La mesa ah sido añadida con exito!!', 'success')
          this.edit = false
        } else {
          let response = await this.placeBusiness.update(this.place)
          let index = this.places.map(place => place.id).indexOf(this.place.id)
          this.places[index] = this.place
          this.places = this.places.slice()
          this.toast.showMessage('La mesa ah sido actualizada con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (error) {
      console.log(`An error occurred saving a place`)
    }
  }

  public async deletePlace() {
    try {
      this.loadingService.show()
      let response = await this.placeBusiness.delete(this.place.id)
      this.toast.showMessage('La mesa ah sido eliminada con exito!!', 'success')
      let index = this.places.map(place => place.id).indexOf(this.place.id)
      this.places.splice(index, 1)
      this.places = this.places.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred deleting a place`)
    }
  }

  public clearForm() {
    try {
      let place = new PlaceModel()
      place.name = ''
      place.floor = ''
      place.seats = ''
      this.place = place
      this.placeForm.reset()
      this.placeForm.patchValue(this.place)
      this.trash = true
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }

}
