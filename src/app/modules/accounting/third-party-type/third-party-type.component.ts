import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ThirdPartyTypesModel } from '../../../models/growPos/_third_party_type.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { ThirdPartyTypesBusiness } from '../../../business/growPos/_third_party_type.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from '../../../components/confirm-dialog/confirm-dialog.component';
import { Nav, NavData } from '../../../_nav';

import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { promise } from 'protractor';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { take } from 'rxjs/operators';

@Component({
  selector: 'growpos-third-party-type',
  templateUrl: './third-party-type.component.html',
  styleUrls: ['./third-party-type.component.scss'],
})
export class ThirdPartyTypeComponent implements OnInit, OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  thirdPartyType: ThirdPartyTypesModel
  thirdPartyTypes: ThirdPartyTypesModel[] = []
  subscription: Subscription
  dataForm: FormGroup;

  constructor(private thirdPartyTypesBusiness: ThirdPartyTypesBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService,
    private headerBodyService: HeaderBodyService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogo: MatDialog) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.dataForm = new FormGroup({
      'id_account_type': new FormControl(''),
      'description': new FormControl('', Validators.required),
      'state': new FormControl(true, Validators.required),
    });

    // Configure the navigation bar
    headerBodyService.navbarFunction();
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.thirdPartyTypes = []

    promises.push(this.thirdPartyTypesBusiness.get().then(third_party_type => {
      this.thirdPartyTypes = Object.values(third_party_type)
    }));

    this.subscription = this.dataForm.valueChanges.subscribe(() => {
      if (this.dataForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide());

  }

  async canDeactivate() {
    if (this.dataForm.dirty) {
      let res = await new Promise((resolve, rejects) => {
        this.dialogo.open(ConfirmDialogComponent, {
          data: `¿Deseas salir? Faltan datos por guardar!`
        }).afterClosed().pipe(take (1)).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            resolve (true);
          } else {
            resolve (false);
          }
        });
      });
      return res;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public select(thirdPartyType) {
    if (this.thirdPartyType !== thirdPartyType && thirdPartyType !== null) {
      this.thirdPartyType = thirdPartyType
      this.dataForm.reset()
      this.dataForm.patchValue(this.thirdPartyType)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
    }
    if (thirdPartyType === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveReg() {
    if (this.dataForm.valid) {
      this.loadingService.show()
      this.thirdPartyType = new ThirdPartyTypesModel(this.dataForm.value)
      if (this.newItem) {
        let response = await this.thirdPartyTypesBusiness.save(this.thirdPartyType)
        this.thirdPartyTypes.unshift(Object(this.thirdPartyType))
        this.thirdPartyTypes = this.thirdPartyTypes.slice()
        this.toast.showMessage('El tipo de tercero ha sido añadida con exito!!', 'success')
        this.edit = false
      } else {
        let response = await this.thirdPartyTypesBusiness.update(this.thirdPartyType)
        let index = this.thirdPartyTypes.map(accountType => accountType.id_third_party_type).indexOf(this.thirdPartyType.id_third_party_type)
        this.thirdPartyTypes[index] = this.thirdPartyType
        this.thirdPartyTypes = this.thirdPartyTypes.slice()
        this.toast.showMessage('El tipo de tercero ha sido actualizado con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async delete() {
    try {
      this.loadingService.show()
      let response = await this.thirdPartyTypesBusiness.delete(this.thirdPartyType.id_third_party_type)
      this.toast.showMessage('El tipo de cuenta ha sido eliminado con exito!!', 'success')
      let index = this.thirdPartyTypes.map(accountType => accountType.id_third_party_type).indexOf(this.thirdPartyType.id_third_party_type)
      this.thirdPartyTypes.splice(index, 1)
      this.thirdPartyTypes = this.thirdPartyTypes.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
    }
  }

  public clearForm() {
    try {
      let accountType = new ThirdPartyTypesModel()
      this.thirdPartyType = accountType
      this.dataForm.reset()
      this.dataForm.patchValue(this.thirdPartyType)
      this.trash = true
    } catch (e) {
    }
  }

}
