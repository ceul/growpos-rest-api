import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';

import { AccountTypesComponent } from './account-types/account-types.component';
import { ThirdPartyTypeComponent } from './third-party-type/third-party-type.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Accounting'
    },
    children: [
      {
        path: 'ac_types',
        component: AccountTypesComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'Tipo de Cuenta'
        }
      },
      {
        path: 'third_party_type',
        component: ThirdPartyTypeComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'Tipo de Tercero'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }
