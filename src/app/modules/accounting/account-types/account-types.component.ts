import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountTypesModel } from '../../../models/growAccounting/_account_types.model';
import { ToastService } from '../../../services/base-services/toast.service';
import { AccountTypesBusiness } from '../../../business/growAccounting/_account_types.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from '../../../components/confirm-dialog/confirm-dialog.component';
import { Nav, NavData } from '../../../_nav';

import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { promise } from 'protractor';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-account-types',
  templateUrl: './account-types.component.html',
  styleUrls: ['./account-types.component.scss']
})
export class AccountTypesComponent implements OnInit, OnDestroy {
  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  accountType: AccountTypesModel
  accountTypes: AccountTypesModel[] = []
  subscription: Subscription
  dataForm: FormGroup;

  constructor(private accountTypesBusiness: AccountTypesBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService,
    private headerBodyService: HeaderBodyService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogo: MatDialog) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.dataForm = new FormGroup({
      'id_account_type': new FormControl(''),
      'description': new FormControl('', Validators.required),
      'state': new FormControl(true, Validators.required),
    });

    // Configure the navigation bar
    headerBodyService.navbarFunction();
  }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.accountTypes = []

    promises.push(this.accountTypesBusiness.get().then(account_types => {
      this.accountTypes = Object.values(account_types)
    }));

    this.subscription = this.dataForm.valueChanges.subscribe(() => {
      if (this.dataForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    Promise.all(promises).then(() => this.loadingService.hide());

  }

  async canDeactivate() {
    if (this.dataForm.dirty) {
      let res = await new Promise((resolve, rejects) => {
        this.dialogo.open(ConfirmDialogComponent, {
          data: `¿Deseas salir? Faltan datos por guardar!`
        }).afterClosed().pipe(take (1)).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            resolve (true);
          } else {
            resolve (false);
          }
        });
      });
      return res;
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  public select(accountType) {
    if (this.accountType !== accountType && accountType !== null) {
      this.accountType = accountType
      this.dataForm.reset()
      this.dataForm.patchValue(this.accountType)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
    }
    if (accountType === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveReg() {
    if (this.dataForm.valid) {
      this.loadingService.show()
      this.accountType = new AccountTypesModel(this.dataForm.value)
      if (this.newItem) {
        let response = await this.accountTypesBusiness.save(this.accountType)
        this.accountTypes.unshift(Object(this.accountType))
        this.accountTypes = this.accountTypes.slice()
        this.toast.showMessage('El tipo de cuenta ha sido añadida con exito!!', 'success')
        this.edit = false
      } else {
        let response = await this.accountTypesBusiness.update(this.accountType)
        let index = this.accountTypes.map(accountType => accountType.id_account_type).indexOf(this.accountType.id_account_type)
        this.accountTypes[index] = this.accountType
        this.accountTypes = this.accountTypes.slice()
        this.toast.showMessage('El tipo de cuenta ha sido actualizado con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async delete() {
    try {
      this.loadingService.show()
      let response = await this.accountTypesBusiness.delete(this.accountType.id_account_type)
      this.toast.showMessage('El tipo de cuenta ha sido eliminado con exito!!', 'success')
      let index = this.accountTypes.map(accountType => accountType.id_account_type).indexOf(this.accountType.id_account_type)
      this.accountTypes.splice(index, 1)
      this.accountTypes = this.accountTypes.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
    }
  }

  public clearForm() {
    try {
      let accountType = new AccountTypesModel()
      this.accountType = accountType
      this.dataForm.reset()
      this.dataForm.patchValue(this.accountType)
      this.trash = true
    } catch (e) {
    }
  }
}
