import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingRoutingModule } from './accounting-routing.module';

import { ComponentsModule } from '../../components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';

import { AccountTypesComponent } from './account-types/account-types.component';
import { ThirdPartyTypeComponent } from './third-party-type/third-party-type.component';

@NgModule({
  declarations: [
    AccountTypesComponent,
    ThirdPartyTypeComponent],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    CanDeactivateGuard
  ],
})
export class AccountingModule { }
