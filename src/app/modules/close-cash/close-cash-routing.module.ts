import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CloseCashPanelComponent } from './close-cash-panel/close-cash-panel.component';
import { CashMovementComponent } from './cash-movement/cash-movement.component';
import { RecepitsComponent } from './recepits/recepits.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Close Cash'
    },
    children: [
      {
        path: '',
        component: CloseCashPanelComponent,
        data: {
          title: 'Cierre de caja'
        }
      },
      {
        path: 'cash-movement',
        component: CashMovementComponent,
        data: {
          title: 'Movimiento de caja'
        }
      },
      {
        path: 'receipts',
        component: RecepitsComponent,
        data: {
          title: 'Facturas'
        }
      },
      
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class CloseCashRoutingModule { }
