import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CloseCashPanelComponent } from './close-cash-panel/close-cash-panel.component';
import { CloseCashRoutingModule } from './close-cash-routing.module';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { CashMovementComponent } from './cash-movement/cash-movement.component';
import { RecepitsComponent } from './recepits/recepits.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { CdkTableModule } from '@angular/cdk/table';
import { ChangeComponent } from 'src/app/components/change/change.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  declarations: [
    CloseCashPanelComponent,
    CashMovementComponent,
    RecepitsComponent
  ],
  imports: [
    CommonModule,
    CloseCashRoutingModule,
    MatTableModule,
    FormsModule,
    TranslateModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    MatPaginatorModule,
    ModalModule,
    ComponentsModule,
    CdkTableModule,
    NgxCurrencyModule
  ],
  providers: [
    BsModalService
  ],
  bootstrap: [
    FindCustomerComponent,
    PaymentsComponent,
    ChangeComponent,
  ]
})
export class CloseCashModule { }
