import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import PaymentModel from 'src/app/models/payment.model';
import { Storage } from '@ionic/storage';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'app-cash-movement',
  templateUrl: './cash-movement.component.html',
  styleUrls: ['./cash-movement.component.scss'],
})
export class CashMovementComponent implements OnInit {

  cashMovementForm: FormGroup;
  submit: boolean;
  movementReasons: { name: string }[] = []
  activeCash: string
  

  constructor(private closeCashBusiness: CloseCashBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService,
    private storage: Storage) {
    this.cashMovementForm = new FormGroup({
      'reason': new FormControl('', Validators.required),
      'total': new FormControl('', [Validators.required, Validators.min(0)]),
      'note': new FormControl(''),
    })
  }

  async ngOnInit() {
    this.submit = false
    this.movementReasons.push({ name: 'cashin' })
    this.movementReasons.push({ name: 'cashout' })
    let conf = await this.storage.get('config')
    this.closeCashBusiness.getLastByHost(conf.hostName).then(closeCash => {
      this.activeCash = Object.values(closeCash)[0].money
    })
  }

  async saveMovement() {
    try {
      this.submit = true
      if (this.cashMovementForm.valid) {
        this.loadingService.show()
        let payment: PaymentModel = new PaymentModel()
        payment.payment = this.cashMovementForm.value['reason']
        payment.total = payment.payment === 'cashout' ? this.cashMovementForm.value['total'] * -1 : this.cashMovementForm.value['total']
        payment.notes = this.cashMovementForm.value['note']
        await this.closeCashBusiness.moveCash(this.activeCash, payment)
        this.resetForm()
        this.loadingService.hide()
        this.toast.showMessage('El movimiento de caja se realizo con exito!!', 'success')
      }
    } catch (error) {
      this.toast.showMessage(`Upss! Ocurrio algo al realizar el movimiento de caja`, 'danger')
      console.log(`An error occurred in ${CashMovementComponent.name} => ${this.saveMovement.name}`)
    }
  }

  resetForm() {
    try {
      this.submit = false
      let cashMovement = {
        reason: '',
        total: '',
        note: ''
      }
      this.cashMovementForm.reset()
      this.cashMovementForm.patchValue(cashMovement)
    } catch (error) {
      console.log(`An error occurred in ${CashMovementComponent.name} => ${this.resetForm.name}`)
    }
  }

}
