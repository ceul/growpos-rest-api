import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentBusiness } from 'src/app/business/payment/payment.business';
import { TaxBusiness } from 'src/app/business/tax/tax.business';
import { TaxCategoryBusiness } from 'src/app/business/tax-category/tax-category.business';
import { TicketBusiness } from 'src/app/business/ticket/ticket.business';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import CloseCashModel from 'src/app/models/close-cash';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { environment } from '../../../../environments/environment';
import { Storage } from '@ionic/storage';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ProductBusiness } from 'src/app/business/product/product.business';
import { PrintService } from 'src/app/services/print-service/print.service';

@Component({
  selector: 'app-close-cash-panel',
  templateUrl: './close-cash-panel.component.html',
  styleUrls: ['./close-cash-panel.component.scss'],
})
export class CloseCashPanelComponent implements OnInit {

  public activeLang = environment.activeLang;
  tableHeight: string
  payments: {}[]
  taxes: {}[]
  products: {}[]
  transactions: number
  numberSales: number
  netSales: number
  money: number
  totalTaxes: number
  activeCash: CloseCashModel
  public searchCloseCashSequence: number
  public isActiveCash: boolean
  hostConfig

  displayedColumnsProducts = ['name', 'units', 'price', 'total'];
  displayedColumnsPayments = ['name', 'total', 'notes'];
  displayedColumnsTaxes = ['name', 'amount', 'base'];

  @ViewChild('confirmCloseCash') public confirmCloseCash: ModalDirective;
  @ViewChild('searchCloseCash') public searchCloseCash: ModalDirective;

  constructor(private closeCashBusiness: CloseCashBusiness,
    private paymentBusiness: PaymentBusiness,
    private productBusiness: ProductBusiness,
    private taxBusiness: TaxBusiness,
    private taxCategoryBusiness: TaxCategoryBusiness,
    private ticketBusiness: TicketBusiness,
    public translate: TranslateService,
    private toast: ToastService,
    private loadingService: LoadingSpinnerService,
    public printService: PrintService,
    private storage: Storage) {
    this.translate.setDefaultLang(this.activeLang);
  }

  async ngOnInit() {
    try {
      this.loadingService.show()
      this.tableHeight = "300px"
      this.resetCloseCash()
      this.activeCash = new CloseCashModel()
      this.hostConfig = await this.storage.get('config')
      this.returnActualCloseCash()
    } catch (error) {
      console.log(`An error occuerred on: CloseCashPanelComponent -> ngOnInit`)
    }
  }

  public async closeCash() {
    try {
      this.loadingService.show()
      this.activeCash.dateend = new Date()
      let newCloseCash = await this.closeCashBusiness.update(this.activeCash)
      this.toast.showMessage('El cierre de caja se ha realizado con exito', 'success')
      this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closecash => {
        this.activeCash = Object.values(closecash)[0]
      })
      this.print()
      this.resetCloseCash()
      this.confirmCloseCash.hide()
      this.loadingService.hide()
    } catch (error) {
      this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el cierre de caja', 'danger')
      console.log(`An error occurred closing the cash`)
    }
  }

  public print() {
    try {
      this.printService.printDocument('close-cash', {
        activeCash: this.activeCash,
        payments: this.payments,
        taxes: this.taxes,
        products: this.products,
        transactions: this.transactions,
        numberSales: this.numberSales,
        netSales: this.netSales,
        money: this.money,
        totalTaxes: this.totalTaxes
      })
    } catch (error) {
      this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de imprimir el pre cierre de caja', 'danger')
      console.log(`An error occurred printing closing the cash`)
    }
  }

  private resetCloseCash() {
    try {
      this.payments = []
      this.taxes = []
      this.products = []
      this.transactions = 0
      this.numberSales = 0
      this.netSales = 0
      this.money = 0
      this.totalTaxes = 0
    } catch (error) {
      console.log(`An error occurred restting close cash`)
    }
  }

  public async searchCloseCashFn() {
    try {
      this.loadingService.show()
      this.activeCash = Object.values(await this.closeCashBusiness.getByHostAndBySequence(this.hostConfig.hostName, this.searchCloseCashSequence))[0]
      await this.getCloseCash()
      this.isActiveCash = false
      this.searchCloseCash.hide()
      this.loadingService.hide()
    } catch (error) {
      console.log(`An error occurred searching close cash`)
    }
  }

  public async getCloseCash() {
    try {
      let promises = []
      promises.push(this.paymentBusiness.getTotalPaymentsByCloseCash(this.activeCash.money).then(payment => {
        this.transactions = Object.values(payment)[0].transactions
        this.money = Object.values(payment)[0].money
      }))
      promises.push(this.paymentBusiness.getByCloseCash(this.activeCash.money).then(payments => {
        this.payments = Object.values(payments)
      }))
      promises.push(this.taxBusiness.getTotalTaxesByCloseCash(this.activeCash.money).then(totalTax => {
        this.totalTaxes = Object.values(totalTax)[0].amount
      }))
      promises.push(this.taxCategoryBusiness.getTotalTaxesByCloseCash(this.activeCash.money).then(catTaxes => {
        this.taxes = Object.values(catTaxes)
      }))
      promises.push(this.ticketBusiness.getTotalSalesByCloseCash(this.activeCash.money).then(sale => {
        this.numberSales = Object.values(sale)[0].sales
        this.netSales = Object.values(sale)[0].total
      }))
      promises.push(this.productBusiness.getTotalByCloseCash(this.activeCash.money).then(sale => {
        this.products = Object.values(sale)
      }))

      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (error) {
      console.log(`An error occurred getting close cash`)
    }
  }

  public async returnActualCloseCash() {
    try {
      this.loadingService.show()
      let closeCash = await this.closeCashBusiness.getLastByHost(this.hostConfig.hostName)
      this.activeCash = Object.values(closeCash)[0]
      await this.getCloseCash()
      this.isActiveCash = true
      this.loadingService.hide()
    } catch (error) {
      console.log(`An error occurred returning to actual close cash`)
    }
  }
}
