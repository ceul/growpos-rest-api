import { Component, OnInit, ViewChild } from '@angular/core';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { BsLocaleService, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { environment } from 'src/environments/environment';
import { TicketBusiness } from 'src/app/business/ticket/ticket.business';
import ReceiptFilterModel from 'src/app/models/receipt-filter.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import PeopleModel from 'src/app/models/people.model';
import { UserBusiness } from 'src/app/business/user/user.business';
import { take } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MasterDetailTableDataSource } from 'src/app/components/master-detail-table/master-detail-table-datasource';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import TicketModel from 'src/app/models/ticket.model';
import { PrintService } from 'src/app/services/print-service/print.service';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { ToastService } from 'src/app/services/base-services/toast.service';
import TicketEnum from 'src/app/models/ticket.enum';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import * as uuid from "uuid";

@Component({
  selector: 'growpos-recepits',
  templateUrl: './recepits.component.html',
  styleUrls: ['./recepits.component.scss'],
})
export class RecepitsComponent implements OnInit {

  @ViewChild(BsDaterangepickerDirective) datepicker: BsDaterangepickerDirective;

  paymentsModal: BsModalRef;
  public filter: ReceiptFilterModel;
  public bsRangeValue: Date[];
  public activeLang = environment.activeLang;
  private findCustomerModal: BsModalRef;
  public pageSizef: number = 25
  public receipts//: MatTableDataSource<any>;
  public translateFields: string[]
  public dateFields: string[]
  public ticketTypes: { name: string, value: string }[] = []
  public employees: PeopleModel[] = []
  public receiptFilterForm: FormGroup;
  public displayedColumns = ['tickettype', 'ticketid', 'employee', 'date', 'total'];
  public totalFilters
  public hostConfig: HostConfigModel
  public receipt: TicketModel

  user: PeopleModel
  activeCash: string
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private ticketBusiness: TicketBusiness,
    private loadingService: LoadingSpinnerService,
    private peopleBusiness: UserBusiness,
    private modalService: BsModalService,
    private storage: Storage,
    public printService: PrintService,
    public authBusiness: AuthBusiness,
    private translate: TranslateService,
    private closeCashBusiness: CloseCashBusiness,
    private toast: ToastService, ) {
    this.translate.setDefaultLang(this.activeLang);
    this.receiptFilterForm = new FormGroup({
      'ticket': new FormControl(''),
      'tickettype': new FormControl(''),
      'customerName': new FormControl(''),
      'customerId': new FormControl(''),
      'employee': new FormControl(''),
      'total': new FormControl(''),
      'totalFilter': new FormControl('')
    })
  }

  async ngOnInit() {

    let promises = []
    this.ticketTypes.push({ name: 'sale', value: '0' })
    this.ticketTypes.push({ name: 'refund', value: '1' })
    this.ticketTypes.push({ name: 'payment', value: '2' })
    this.ticketTypes.push({ name: 'all', value: '' })
    this.filter = new ReceiptFilterModel()
    this.loadingService.show()
    this.filter.dateStart = new Date();
    this.filter.dateEnd = new Date();

    this.filter.dateStart.setDate(this.filter.dateStart.getDate() - 7);
    this.filter.dateEnd.setDate(this.filter.dateEnd.getDate());
    this.bsRangeValue = [this.filter.dateStart, this.filter.dateEnd];
    this.hostConfig = await this.storage.get('config')
    promises.push(this.peopleBusiness.get().then(res => {
      this.employees = Object.values(res)
    }))

    promises.push(this.ticketBusiness.getReceiptFiltered(this.filter).then(res => {
      this.receipts = Object.values(res)
    }))

    promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
      this.activeCash = Object.values(closeCash)[0].money
    }))

    promises.push(this.authBusiness.readUser().then(user => {
      this.user = user
    }))
    this.dateFields = []
    this.dateFields.push('date')
    this.translateFields = []
    this.translateFields.push('payment')
    Promise.all(promises).then(() => this.loadingService.hide())
    
  }

  public execute() {
    try {
      this.loadingService.show()
      this.filter.dateStart = this.bsRangeValue[0]
      this.filter.dateEnd = this.bsRangeValue[1]
      this.filter.customer = this.receiptFilterForm.controls['customerId'].value
      this.filter.ticket = this.receiptFilterForm.controls['ticket'].value
      this.filter.ticketType = this.receiptFilterForm.controls['tickettype'].value
      this.filter.user = this.receiptFilterForm.controls['employee'].value
      this.filter.total = {
        value: this.receiptFilterForm.controls['total'].value,
        filter: this.receiptFilterForm.controls['totalFilter'].value
      }
      this.ticketBusiness.getReceiptFiltered(this.filter).then(res => {
        this.receipts = Object.values(res)
        this.loadingService.hide()
      })
    } catch (error) {
      throw new Error('An error occurred in execute() => ReceiptsComponent')
    }
  }

  public refund() {
    try {
      let refund : TicketModel = this.receipt
      refund.activeCash = this.activeCash
      refund.host = this.hostConfig.hostName
      refund.ticketType = TicketEnum.RECEIPT_REFUND
      refund.ticketstatus = this.receipt.ticketId
      refund.user = this.user
      refund.date = new Date()
      refund.payments = []
      refund.ticketId = 0
      //refund.id = uuid.v4()
      let hoursDiff = refund.date.getHours() - refund.date.getTimezoneOffset() / 60;
      refund.date.setHours(hoursDiff);
      this.paymentsModal = this.modalService.show(PaymentsComponent, {
        class: 'modal-lg',
        initialState: {
          origin: 'refund',
          ticket: refund
        }
      });
      this.paymentsModal.content.completed.pipe(take(1)).subscribe(async completed => {
        if (completed.isCompleted) {
          this.toast.showMessage('La factura se ha devuelto con exito!!', 'success')
        } else {
          this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar la devolución', 'danger')
        }
      })
    } catch (error) {
      throw new Error('An error occurred in refund() => ReceiptsComponent')
    }
  }

  clearForm() {
    try {
      let form = {
        'ticket': '',
        'tickettype': '',
        'customerName': '',
        'customerId': '',
        'employee': '',
        'total': '',
        'totalFilter': '',
      }
      this.filter.dateStart.setDate(this.filter.dateStart.getDate() - 7);
      this.filter.dateEnd.setDate(this.filter.dateEnd.getDate());
      this.bsRangeValue = [this.filter.dateStart, this.filter.dateEnd];
      this.receiptFilterForm.patchValue(form)
    } catch (error) {
      console.log('An error occurred clearing the form')
    }
  }

  openFindCustomerModal() {
    try {
      this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
      this.findCustomerModal.content.selectedCustomer.pipe(take(1)).subscribe(customer => {
        this.receiptFilterForm.controls['customerName'].patchValue(customer.name)
        this.receiptFilterForm.controls['customerId'].patchValue(customer.id)
      })
    } catch (error) {
      throw new Error('An error ocurred opening find customer modal');
    }
  }

  getRecord(index) {
    try {
      let id = this.receipts[index].id
      this.ticketBusiness.getReceiptById(id).then(ticket => {
        this.receipt = Object.values(ticket)[0]
      })
    } catch (error) {
      console.log(`An error occurred getting the record`);
    }
  }

  rePrint() {
    try {
      this.printService.printDocument('invoice', this.receipt)
    } catch (error) {
      console.log(`An error occurred getting the record`);
    }
  }

}
