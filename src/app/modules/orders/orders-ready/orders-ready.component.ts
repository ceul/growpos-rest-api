import { Component, OnInit, OnDestroy } from '@angular/core';
import OrderModel from 'src/app/models/order.model';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { OrderBusiness } from 'src/app/business/order/order.business';
import OrderStateEnum from 'src/app/models/order_state.enum';
import TicketModel from 'src/app/models/ticket.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import TicketEnum from 'src/app/models/ticket.enum';
import PeopleModel from 'src/app/models/people.model';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { take } from 'rxjs/operators';
import * as uuid from "uuid";
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { Storage } from '@ionic/storage';
import HostConfigModel from 'src/app/models/host-config.model';
import { TaxBusiness } from 'src/app/business/tax/tax.business';
import TaxModel from 'src/app/models/tax.model';
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/services/socket/socket.service';

@Component({
  selector: 'growpos-orders-ready',
  templateUrl: './orders-ready.component.html',
  styleUrls: ['./orders-ready.component.scss'],
})
export class OrdersReadyComponent implements OnInit, OnDestroy {

  //------------------- Modals -----------------------
  paymentsModal: BsModalRef;
  
  public orders: OrderModel[]
  public time: Date[]
  displayedColumns = ['product', 'multiply'];
  private clockHandle
  private ticket: TicketModel
  private user: PeopleModel
  private activeCash: string
  private hostConfig: HostConfigModel
  private selectedOrder: number
  private taxes: TaxModel[]
  private subscription: Subscription
  
  constructor(private orderBusiness: OrderBusiness,
    private loadingService: LoadingSpinnerService,
    private modalService: BsModalService,
    public authBusiness: AuthBusiness,
    private storage: Storage,
    private closeCashBusiness: CloseCashBusiness,
    private taxBusiness: TaxBusiness,
    private socketService: SocketService,
    private toast: ToastService) {

  }

  async ngOnInit() {
    try {
      let promises = []
      this.orders = []
      this.selectedOrder = 0
      this.time = []
      this.loadingService.show()
      await this.resetComponet()
      this.subscription = this.socketService.listen('order').subscribe((newOrder)=> {
        let order = <OrderModel> newOrder
        if(order.state === OrderStateEnum.ON_DELIVERY || order.state === OrderStateEnum.READY){
          let index = this.orders.findIndex(item => item.id === order.id)
          if(index !== -1){
            this.orders[index] = order
          } else {
            this.orders.push(order)
          }
        } else if(order.state === OrderStateEnum.FINISH){
          let index = this.orders.findIndex(item => item.id === order.id)
          if(index !== -1){
            this.orders.splice(index,1)
          }
        }
      })
      promises.push(this.authBusiness.readUser().then(user => {
        this.user = user
      }))
      promises.push(await this.orderBusiness.getReadyToDeliver().then(orders => {
        this.orders = Object.values(orders)
        this.orders.forEach(item => {
          let time = this.getTime(item.start_date)
          this.time.push(time)
        })
      }))
      promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
        this.activeCash = Object.values(closeCash)[0].money
      }))
  
      promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
        this.activeCash = Object.values(closeCash)[0].money
      }))

      promises.push(this.taxBusiness.get().then(taxes => {
        this.taxes = Object.values(taxes)
      }))

      await Promise.all(promises)
      this.loadingService.hide()
      this.clockHandle = setInterval(() => {
        this.changeTime()
      }, 1000);
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => OrdersReadyComponent`)
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  changeTime() {
    this.time.forEach((item, index) => {
      item.setSeconds(item.getSeconds() + 1)
      this.time[index] = item

    })
    this.time = this.time.slice()
  }

  async orderOnDelivery(index: number) {
    try {
      this.orders[index].state = OrderStateEnum.ON_DELIVERY
      await this.orderBusiness.update(this.orders[index])
      this.orders.splice(index,1)
    } catch (error) {
      console.log(`An error occurred on orderReady => OrderDisplayComponent`)
    }
  }

  async payOrder(index: number) {
    try {
      this.selectedOrder = index
      this.orders[index].state = OrderStateEnum.FINISH
      this.orders[index].end_date = new Date()
      this.resetComponet()
      this.orders[index].products.forEach(product => {
        this.ticket.addProduct(product.product, this.taxes, this.toast)
      })
      this.openPaymentsModal()
    } catch (error) {
      console.log(`An error occurred on orderReady => OrderDisplayComponent`)
    }
  }

  getTime(time: Date) {
    try {
      let tableTime = new Date(time).getTime()
      let date = new Date()
      let hoursDiff = date.getHours() - date.getTimezoneOffset() / 60;
      date.setHours(hoursDiff);
      let res = (date.getTime() - tableTime);
      var diffMins = Math.round(res / 60000); // minutes
      var diffSegs = Math.round((res % 60000) / 1000); // minutes
      date.setMinutes(diffMins)
      date.setSeconds(diffSegs)
      return date;
    } catch (error) {
      console.log(`An error occurred getting time`)
    }
  }

  async openPaymentsModal() {
    try {
      this.paymentsModal = this.modalService.show(PaymentsComponent, {
        class: 'modal-lg',
        initialState: {
          origin: 'sales',
          ticket: this.ticket
        }
      });
      this.paymentsModal.content.completed.pipe(take(1)).subscribe(async completed => {
        if (completed.isCompleted) {
          await this.orderBusiness.update(this.orders[this.selectedOrder])
          this.orders.splice(this.selectedOrder, 1)
          this.toast.showMessage('La Orden ha sido paga satisfactoriamente', 'success')
        } else {
          this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el pago', 'danger')
        }
      })
    } catch (error) {
      console.log('An error ocurred opening payments modal: ', error);
    }
  }

  async resetComponet() {
    try {
      this.ticket = new TicketModel()
      this.ticket.id = uuid.v4()
      this.ticket.pickupId = 0
      this.ticket.ticketId = 0
      this.ticket.ticketType = TicketEnum.RECEIPT_NORMAL
      this.ticket.ticketstatus = 0
      this.ticket.user = this.user ? this.user : null
      this.ticket.lines = []
      this.ticket.taxes = []
      this.ticket.date = new Date()
      let hoursDiff = this.ticket.date.getHours() - this.ticket.date.getTimezoneOffset() / 60;
      this.ticket.date.setHours(hoursDiff);
      this.ticket.total = 0
      this.ticket.subTotal = 0
      this.ticket.totalTaxes = 0
      this.ticket.activeCash = this.activeCash
      this.ticket.contract = null
      this.hostConfig = await this.storage.get('config')
      this.ticket.host = this.hostConfig.hostName
    } catch (e) {
      console.log('An error ocurred resetting tickets modal: ', e);
    }
  }

}
