import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';
import { CanActiveSalesGuard } from 'src/app/guards/can-active-sales.guard';
import { OrderDisplayComponent } from './order-display/order-display.component';
import { OrdersReadyComponent } from './orders-ready/orders-ready.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ordenes'
    },
    children: [
      {
        path: '',
        component: OrderDisplayComponent,
        data: {
          title: 'Display de ordenes'
        }
      },
      {
        path: 'list-ready',
        component: OrdersReadyComponent,
        data: {
          title: 'Lista de ordenes listas'
        }
      }
      
    ]
  }


];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
