import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { OrderBusiness } from 'src/app/business/order/order.business';
import OrderModel from 'src/app/models/order.model';
import OrderStateEnum from 'src/app/models/order_state.enum';
import { SocketService } from 'src/app/services/socket/socket.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'growpos-order-display',
  templateUrl: './order-display.component.html',
  styleUrls: ['./order-display.component.scss'],
})
export class OrderDisplayComponent implements OnInit, OnDestroy{

  public orders: OrderModel[]
  public time: Date[]
  displayedColumns = ['product', 'multiply'];
  private clockHandle
  private subscription: Subscription
  constructor(private orderBusiness: OrderBusiness,
    private loadingService: LoadingSpinnerService,
    private socketService: SocketService,
    private toast: ToastService) {

  }

  async ngOnInit() {
    try {
      this.orders = []
      this.time = []
      this.loadingService.show()
      this.subscription = this.socketService.listen('order').subscribe((newOrder)=> {
        let order = <OrderModel> newOrder
        if(order.state === OrderStateEnum.PROCESS){
          this.orders.push(order)
        }
      })
      this.orders = Object.values(await this.orderBusiness.getProcess())
      this.orders.forEach(item => {
        let time = this.getTime(item.start_date)
        this.time.push(time)
      })
      this.loadingService.hide()
      this.clockHandle = setInterval(() => {
        this.changeTime()
      }, 1000);
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => OrderDisplayComponent`)
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  changeTime(){
    this.time.forEach((item,index) => {
      item.setSeconds(item.getSeconds()+1)
      this.time[index]= item

    })
    this.time = this.time.slice()
  }

  async orderReady(index: number){
    try{
      this.orders[index].state = OrderStateEnum.READY
      this.orders[index].end_date = new Date()
      await this.orderBusiness.update(this.orders[index])
      this.orders.splice(index, 1)
    } catch(error){
      console.log(`An error occurred on orderReady => OrderDisplayComponent`)
    }
  }

  getTime(time: Date) {
    try {
      let tableTime = new Date(time).getTime()
      let date = new Date()
      let hoursDiff = date.getHours() - date.getTimezoneOffset() / 60;
      date.setHours(hoursDiff);
      let res = (date.getTime() - tableTime);
      var diffMins = Math.round(res / 60000); // minutes
      var diffSegs = Math.round((res % 60000)/1000); // minutes
      date.setMinutes(diffMins)
      date.setSeconds(diffSegs)
      return date;
    } catch (error) {
      console.log(`An error occurred getting time`)
    }
  }



}
