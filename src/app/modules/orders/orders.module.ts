import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderDisplayComponent } from './order-display/order-display.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';
import { CanActiveSalesGuard } from 'src/app/guards/can-active-sales.guard';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { LoadingSpinnerComponent } from 'src/app/components/loading-spinner/loading-spinner.component';
import { ChangeComponent } from 'src/app/components/change/change.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { ComponentsModule } from 'src/app/components/components.module';
import { AlertModule } from 'ngx-bootstrap/alert';
import { OrdersRoutingModule } from './orders-routing.module';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTableModule } from '@angular/material';
import { OrdersReadyComponent } from './orders-ready/orders-ready.component';

@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    AlertModule.forRoot(),
    ComponentsModule,
    NgxCurrencyModule,
    CdkTableModule,
    MatTableModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
  ],
  declarations: [
    OrderDisplayComponent,
    OrdersReadyComponent
  ],
  providers: [
    BsModalService,
  ],
  bootstrap: [
    PaymentsComponent,
    ChangeComponent,
    LoadingSpinnerComponent,
  ]
})
export class OrdersModule { }
