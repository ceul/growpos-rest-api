import { Component, OnInit } from '@angular/core';
import { PrintService } from 'src/app/services/print-service/print.service';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'growpos-parking-income',
  templateUrl: './parking-income.component.html',
  styleUrls: ['./parking-income.component.scss'],
})
export class ParkingIncomeComponent implements OnInit {

  
  data: any
  public url
  hostConfig: HostConfigModel
  constructor(private printService: PrintService,
    private storage: Storage) { }

  async ngOnInit() {
    this.hostConfig = await this.storage.get('config')
    this.url = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.data = history.state.data
    this.printService.onDataReady()
  }
}
