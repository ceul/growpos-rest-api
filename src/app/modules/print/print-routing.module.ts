import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceComponent } from './invoice/invoice.component';
import { PreInvoiceComponent } from './pre-invoice/pre-invoice.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { CustomerPaidComponent } from './customer-paid/customer-paid.component';
import { CloseCashPrintComponent } from './close-cash-print/close-cash-print.component';
import { ParkingIncomeComponent } from './parking-income/parking-income.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PrintLayoutComponent,
    children: [
      {
        path: 'invoice',
        component: InvoiceComponent,
      },
      {
        path: 'customer-paid',
        component: CustomerPaidComponent,
      },
      {
        path: 'pre-invoice',
        component: PreInvoiceComponent,
      },
      {
        path: 'close-cash',
        component: CloseCashPrintComponent,
      },
      {
        path: 'parking-income',
        component: ParkingIncomeComponent,
      }
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PrintRoutingModule { }
