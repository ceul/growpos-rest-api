import { Component, OnInit } from '@angular/core';
import HostConfigModel from 'src/app/models/host-config.model';
import { PrintService } from 'src/app/services/print-service/print.service';
import { Storage } from '@ionic/storage';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import PeopleModel from 'src/app/models/people.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'growpos-close-cash-print',
  templateUrl: './close-cash-print.component.html',
  styleUrls: ['./close-cash-print.component.scss'],
})
export class CloseCashPrintComponent implements OnInit {

  data: any
  hostConfig: HostConfigModel
  user: PeopleModel
  public url

  constructor(private printService: PrintService,
    public authBusiness: AuthBusiness,
    private storage: Storage) { }

  async ngOnInit() {
    this.user = Object.values(await this.authBusiness.readUser())[0]
    this.hostConfig = await this.storage.get('config')
    this.data = history.state.data
    this.url = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.printService.onDataReady()
  }
}
