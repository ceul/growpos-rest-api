import { Component, OnInit } from '@angular/core';
import { PrintService } from 'src/app/services/print-service/print.service';
import HostConfigModel from 'src/app/models/host-config.model';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'growpos-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit {

  data: any
  public url
  hostConfig: HostConfigModel

  constructor(private printService: PrintService,
    private storage: Storage) { }

  async ngOnInit() {
    this.hostConfig = await this.storage.get('config')
    this.url = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.data = history.state.data
    this.printService.onDataReady()
  }

}
