import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintRoutingModule } from './print-routing.module';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { InvoiceComponent } from './invoice/invoice.component';

@NgModule({
  declarations: [
    /*PrintLayoutComponent,
    InvoiceComponent*/
  ],
  imports: [
    CommonModule
    //PrintRoutingModule
  ]
})
export class PrintModule { }
