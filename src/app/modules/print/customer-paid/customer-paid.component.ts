import { Component, OnInit } from '@angular/core';
import { PrintService } from 'src/app/services/print-service/print.service';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';
import TicketModel from 'src/app/models/ticket.model';

@Component({
  selector: 'growpos-customer-paid',
  templateUrl: './customer-paid.component.html',
  styleUrls: ['./customer-paid.component.scss'],
})
export class CustomerPaidComponent implements OnInit {

  data: any
  public url
  hostConfig: HostConfigModel

  constructor(private printService: PrintService,
    private storage: Storage) { }

  async ngOnInit() {
    this.url = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.hostConfig = await this.storage.get('config')
    this.data = history.state.data
    this.printService.onDataReady()
  }

}
