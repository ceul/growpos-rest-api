import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierRoutingModule } from './supplier-routing.module';
import { SupplierMenuComponent } from './supplier-menu/supplier-menu.component';
import { SupplierManageComponent } from './supplier-manage/supplier-manage.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TranslateModule } from '@ngx-translate/core';
import { MatPaginatorModule, MatTableModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [
    SupplierMenuComponent,
    SupplierManageComponent
  ],
  imports: [
    CommonModule,
    SupplierRoutingModule,
    ModalModule.forRoot(),
    TabsModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    BsDatepickerModule.forRoot(),
    TranslateModule
    
  ]
})
export class SupplierModule { }
