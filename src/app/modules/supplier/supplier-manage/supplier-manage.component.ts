import { Component, OnInit, OnDestroy } from '@angular/core';
import SupplierModel from 'src/app/models/supplier.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SupplierBusiness } from 'src/app/business/supplier/supplier.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'growpos-supplier-manage',
  templateUrl: './supplier-manage.component.html',
  styleUrls: ['./supplier-manage.component.scss'],
})
export class SupplierManageComponent implements OnInit, OnDestroy {

  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  supplier: SupplierModel
  suppliers: SupplierModel[] = []
  transactions: {}[] = []
  displayedColumns = ['ticketid', 'date', 'product', 'quantity', 'total'];
  subscription: Subscription
  supplierForm: FormGroup;

  constructor(private supplierBusiness: SupplierBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.supplierForm = new FormGroup({
      'id': new FormControl(''),
      'searchkey': new FormControl('', Validators.required),
      'taxid': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'maxdebt': new FormControl('', Validators.min(0)),
      'address': new FormControl(''),
      'address2': new FormControl(''),
      'postal': new FormControl(''),
      'city': new FormControl(''),
      'region': new FormControl(''),
      'country': new FormControl(''),
      'firstname': new FormControl(''),
      'lastname': new FormControl(''),
      'email': new FormControl('', Validators.email),
      'phone': new FormControl(''),
      'phone2': new FormControl(''),
      'fax': new FormControl(''),
      'notes': new FormControl(''),
      'visible': new FormControl(true),
      'curdate': new FormControl({ value: '', disabled: true }),
      'curdebt': new FormControl({ value: '', disabled: true }, Validators.min(0)),
      'vatid': new FormControl(''),
    })
  }

  async ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.suppliers = []
    let suppliers = await this.supplierBusiness.get()
    this.suppliers = Object.values(suppliers)
    this.subscription = this.supplierForm.valueChanges.subscribe(() => {
      if (this.supplierForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    this.loadingService.hide()
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public selectSupplier(supplier) {
    try {
      if (this.supplier !== supplier && supplier !== null) {
        this.supplier = supplier
        this.supplierForm.reset()
        this.supplierForm.patchValue(this.supplier)
        this.submit = false
        this.newItem = false
        this.trash = false
        this.edit = false
      }
      if (supplier === null) {
        this.clearForm()
        this.submit = false;
        this.newItem = true
        this.trash = true
        this.edit = false
      }
    } catch (e) {
      this.toast.showMessage('Upss! Algo ha ocurrido al seleccionar el proveedor', 'danger')
    }
  }

  public async saveSupplier() {
    try {
      if (this.supplierForm.valid) {
        this.loadingService.show()
        this.supplier = new SupplierModel(this.supplierForm.value)
        if (this.newItem) {
          let response = await this.supplierBusiness.save(this.supplier)
          this.suppliers.unshift(Object(this.supplier))
          this.suppliers = this.suppliers.slice()
          this.toast.showMessage('El proveedor ah sido añadido con exito!!', 'success')
          this.edit = false
        } else {
          let response = await this.supplierBusiness.update(this.supplier)
          let index = this.suppliers.map(supplier => supplier.id).indexOf(this.supplier.id)
          this.suppliers[index] = this.supplier
          this.suppliers = this.suppliers.slice()
          this.toast.showMessage('El proveedor ah sido actualizado con exito!!', 'success')
          this.edit = false
        }
        this.loadingService.hide()
      }
      this.submit = true
    } catch (e) {
      this.toast.showMessage('Upss! Algo ha ocurrido con la transacción.', 'danger')
    }
  }

  public async deleteSupplier() {
    try {
      this.loadingService.show()
      let response = await this.supplierBusiness.delete(this.supplier.id)
      this.toast.showMessage('El proveedor ah sido eliminado con exito!!', 'success')
      let index = this.suppliers.map(supplier => supplier.id).indexOf(this.supplier.id)
      this.suppliers.splice(index, 1)
      this.suppliers = this.suppliers.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {
      this.toast.showMessage('Upss! Algo ha ocurrido al eliminar el proveedor.', 'danger')
    }
  }

  public clearForm() {
    try {
      let supplier = new SupplierModel()
      supplier.visible = true
      supplier.maxdebt = 0
      this.supplier = supplier
      this.supplierForm.reset()
      this.supplierForm.patchValue(this.supplier)
      this.trash = true
    } catch (e) {
      this.toast.showMessage('Upss! Algo ha ocurrido al reestablecer el formulario', 'danger')
    }
  }


}
