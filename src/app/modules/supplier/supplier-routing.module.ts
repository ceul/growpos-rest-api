import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierMenuComponent } from './supplier-menu/supplier-menu.component';
import { SupplierManageComponent } from './supplier-manage/supplier-manage.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Supplier'
    },
    children: [
      {
        path: '',
        component: SupplierMenuComponent,
        data: {
          title: 'Menu de Proveedores'
        }
      },{
        path: 'supplier-manage',
        component: SupplierManageComponent,
        data: {
          title: 'Gestion de proveedores'
        }
      },
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class SupplierRoutingModule { }
