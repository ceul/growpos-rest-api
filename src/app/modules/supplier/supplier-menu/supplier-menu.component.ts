import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'growpos-supplier-menu',
  templateUrl: './supplier-menu.component.html',
  styleUrls: ['./supplier-menu.component.scss'],
})
export class SupplierMenuComponent implements OnInit {

  module:{
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };
  constructor() {
    this.module = {
      name: 'Proveedores',
      manage: 'Gestión',
      reports: 'Reportes',
      manageItems: [],
      reportsItems: []
    };
    this.module.manageItems.push({
      name: `Proveedores`,
      icon: 'fa fa-address-card-o fa-lg',
      url: '/supplier/supplier-manage'
    });
   }

  ngOnInit() {
  }

}
