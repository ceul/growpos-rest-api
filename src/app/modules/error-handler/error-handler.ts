import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from '../../services/base-services/toast.service';
import { LogBusiness } from '../../business/log/log.business';
import LogModel from 'src/app/models/log.model';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
@Injectable()
export class ErrorsHandler implements ErrorHandler {

    constructor(private logBusiness: LogBusiness, 
        private toast: ToastService, 
        private loadingService: LoadingSpinnerService,
        private auth : AuthBusiness) { }

    handleError(error: Error | HttpErrorResponse) {
        if (error instanceof HttpErrorResponse) {
            // Server or connection error happened
            if (!navigator.onLine) {
                // Handle offline error
                this.toast.showMessage('Se ha perdido la conexion a internet', 'danger')
            } else {
                // Handle Http Error (error.status === 403, 404...)
                if (error.status === 0) {
                    this.toast.showMessage('Ups! Algo ha pasado con el servidor, Porfavor informa al administrador', 'danger')
                } else if(error.status === 401){
                    this.auth.logout()
                    this.toast.showMessage('Tu sesion a expirado, porfavor inicia sesion nuevamente', 'danger')
                } else if(error.status === 403){
                    this.toast.showMessage(error.error.message, 'danger')
                }else {
                    this.toast.showMessage('Ups! Algo ha pasado', 'danger')
                }
            }
        } else {
            // Handle Client Error (Angular Error, ReferenceError...)   
            let log = new LogModel()
            log.description = error.stack
            log.type = '2'
            this.logBusiness.save(log)
            this.loadingService.hide()
        }
        this.loadingService.hide()
    }
}