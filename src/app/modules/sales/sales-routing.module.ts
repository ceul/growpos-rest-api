import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesPanelComponent } from './sales-panel/sales-panel.component';
import { PlacesComponent } from '../../components/places/places.component';
import { ReservationsComponent } from './reservations/reservations.component';
import { SalesMenuComponent } from './sales-menu/sales-menu.component';
import { CloseCashComponent } from './reports/close-cash/close-cash.component';
import { ProductSalesProductComponent } from './reports/product-sales-product/product-sales-product.component';
import { CategorySalesComponent } from './reports/category-sales/category-sales.component';
import { TransactionsComponent } from './reports/transactions/transactions.component';
import { CashFlowComponent } from './reports/cash-flow/cash-flow.component';
import { ProductSalesProfitComponent } from './reports/product-sales-profit/product-sales-profit.component';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';
import { CanActiveSalesGuard } from 'src/app/guards/can-active-sales.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ventas'
    },
    children: [
      {
        path: '',
        component: SalesPanelComponent,
        canDeactivate: [CanDeactivateGuard],
        canActivate: [CanActiveSalesGuard],
        data: {
          title: 'Panel de ventas'
        }
      },
      {
        path: 'table/:id',
        component: SalesPanelComponent,
        canDeactivate: [CanDeactivateGuard],
        data: {
          title: 'Panel de ventas'
        }
      },
      {
        path: 'restaurant/:type',
        //path: '',
        component: PlacesComponent,
        data: {
          title: 'Mesas'
        }
      },
      {
        path: 'reservation',
        component: ReservationsComponent,
        data: {
          title: 'Reservaciones'
        }
      },
      {
        path: 'menu',
        component: SalesMenuComponent,
        data: {
          title: 'Menu'
        }
      },
      {
        path: 'report',
        data: {
          title: 'Ventas'
        },
        children: [
          {
            path: 'close-cash',
            component: CloseCashComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
          {
            path: 'product-sales',
            component: ProductSalesProductComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
          {
            path: 'category-sales',
            component: CategorySalesComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
          {
            path: 'transactions',
            component: TransactionsComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
          {
            path: 'cash-flow',
            component: CashFlowComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
          {
            path: 'product-sales-profit',
            component: ProductSalesProfitComponent,
            data: {
              title: 'Panel de ventas'
            }
          },
        ]
      }
    ]
  }


];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
