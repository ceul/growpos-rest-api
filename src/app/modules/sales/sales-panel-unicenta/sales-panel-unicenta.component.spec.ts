import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPanelUnicentaComponent } from './sales-panel-unicenta.component';

describe('SalesPanelUnicentaComponent', () => {
  let component: SalesPanelUnicentaComponent;
  let fixture: ComponentFixture<SalesPanelUnicentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPanelUnicentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPanelUnicentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
