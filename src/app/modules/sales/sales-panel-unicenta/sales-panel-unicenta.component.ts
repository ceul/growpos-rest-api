import { Component, OnInit, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { ModalDirective, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NumericKeyboardComponent } from '../../../components/numeric-keyboard/numeric-keyboard.component'
import { PaymentsComponent } from '../../../components/payments/payments.component'
import { CreateCustomerComponent } from '../../../components/create-customer/create-customer.component';
import { ProductListComponent } from '../../../components/product-list/product-list.component';
import { ActivatedRoute } from '@angular/router';
import { FindCustomerComponent } from '../../../components/find-customer/find-customer.component';
import ProductModel from '../../../models/product.model';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-sales-panel--unicenta',
  templateUrl: './sales-panel-unicenta.component.html',
  styleUrls: ['./sales-panel-unicenta.component.scss']
})
export class SalesPanelUnicentaComponent implements OnInit {

  paymentsModal: BsModalRef;
  findCustomerModal: BsModalRef;
  createCustomerModal: BsModalRef;
  productListModal: BsModalRef;

  @ViewChild('editLine') public editLine: ModalDirective;
  @ViewChild('confirmDeletionLine') public confirmDeletionLine: ModalDirective;
  @ViewChild('noAttributesInfo') public noAttributesInfo: ModalDirective;
  @ViewChild('productInfo') public productInfo: ModalDirective;
  @ViewChild('splitReceipt') public splitReceipt: ModalDirective;
  @ViewChild('numberGuest') public numberGuest: ModalDirective;
  @ViewChild('confirmDeletionReceipt') public confirmDeletionReceipt: ModalDirective;


  displayedColumns = ['name', 'pricesell', 'units', 'taxes', 'value', 'printer'];
  tableData = [];

  categories: {
    catId: string,
    catName: string,
    catImg: string
    products: {
      id: string,
      img: string,
      item: string,
      price: number,
      units: number,
      taxes: number,
      value: number,
      printer: number,
    }[]
  }[] = []

  products:ProductModel[]= []


  constructor(private modalService: BsModalService, private _activatedRoute: ActivatedRoute,
    private changeDetectorRefs: ChangeDetectorRef) {
    let catName
    let catId
    let products
    for (let l = 0; l < 8; l++) {
      products = []
      catId = `${l}`
      catName = `Category ${l}`
      for (let t = 0; t < 24; t++) {
        let product = new ProductModel()
        product.id = `${l}${t}`
        product.image = 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png'//'https://www.w3schools.com/bootstrap4/paris.jpg'
        product.name= `Producto ${t}`
        product.pricesell= t
        products.push(product)
      }
      this.categories.push({
        catId,
        catName,
        catImg: 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png',
        products
      })
    }

    /*for (let i = 0; i < 8; i++) {
      this.tableData.push({
        item: `Cafe ${i}`,
        price: i,
        units: i,
        taxes: `${i} %`,
        value: i * i,
        printer: i
      });
    }*/
  }


  ngOnInit() {

  }

  getRecord(row) {
    console.log(row);
  }

  selectCategory(category) {
    try {
      this.products = category.products;
    } catch (e) {
      console.log('An error ocurred selecting a category')
    }
  }

  addProduct(product) {
    try {
      this.tableData = this.tableData.concat(product)
      this.changeDetectorRefs.detectChanges();
    } catch (e) {
      console.log('An error ocurred adding a product')
    }
  }

  selectCustomer(customer){
    try{
      console.log(customer)
    } catch(e){
      console.log('An error occurred selecting a customer')
    }
  }

  deleteTicket(){
    try{
      this.tableData=[]
      this.confirmDeletionReceipt.hide()
    }catch(e){
      console.log('An error ocurred deleting the ticket')
    }
  }

  //---------------------------- Modals ---------------------------------------------------------//

  openPaymentsModal() {
    try {
      this.paymentsModal = this.modalService.show(PaymentsComponent, { class: 'modal-lg' });
    } catch (error) {
      console.log('An error ocurred opening payments modal: ', error);

    }
  }

  openFindCustomerModal() {
    try {
      this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
    } catch (error) {
      console.log('An error ocurred opening find customer modal: ', error);

    }
  }

  openCreateCustomerModal() {
    try {
      this.createCustomerModal = this.modalService.show(CreateCustomerComponent, { class: 'modal-lg' });
      this.createCustomerModal.content.createdCustomer.pipe(take(1)).subscribe(customer => {
        console.log('Customer Created: ',customer)
      })
    } catch (error) {
      console.log('An error ocurred opening create customer modal: ', error);
    }
  }

  openProductListModal() {
    try {
      this.productListModal = this.modalService.show(ProductListComponent, { class: 'modal-lg' });
    } catch (error) {
      console.log('An error ocurred opening product list modal: ', error);
    }
  }

}