import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FindCustomerComponent } from '../../../components/find-customer/find-customer.component';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit {

  findCustomerModal: BsModalRef;
  
  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  openFindCustomerModal() {
    try{
      this.findCustomerModal = this.modalService.show(FindCustomerComponent,{class: 'modal-lg'});
    } catch(error){
      console.log('An error ocurred opening find customer modal: ', error);
      
    }
  }

}
