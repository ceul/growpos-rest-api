import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import ProductModel from 'src/app/models/product.model';
import CategoryModel from 'src/app/models/category.model';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ProductBusiness } from 'src/app/business/product/product.business';
import { CategoryBusiness } from 'src/app/business/category/category.business';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { ProductListComponent } from 'src/app/components/product-list/product-list.component';
import { CreateCustomerComponent } from 'src/app/components/create-customer/create-customer.component';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import TaxModel from 'src/app/models/tax.model';
import { TaxBusiness } from 'src/app/business/tax/tax.business';
import TicketModel from 'src/app/models/ticket.model';
import { SharedTicketBusiness } from 'src/app/business/shared-ticket/shared-ticket.business';
import * as uuid from "uuid";
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { PlaceBusiness } from 'src/app/business/place/place.business';
import PlaceModel from 'src/app/models/place.model';
import { LineRemovedBusiness } from 'src/app/business/line-removed/line-removed.business';
import LineRemovedModel from 'src/app/models/line-removed';
import * as _ from "lodash"
import { AuthBusiness } from 'src/app/business/user/auth.business';
import PeopleModel from 'src/app/models/people.model';
import { MoveTableService } from 'src/app/services/move-table/move-table.service';
import { Storage } from '@ionic/storage';
import ConfigModeEnum from 'src/app/models/config-mode.enum';
import HostConfigModel from 'src/app/models/host-config.model';
import TicketEnum from 'src/app/models/ticket.enum';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { TipComponent } from 'src/app/components/tip/tip.component';
import { async } from '@angular/core/testing';
import { VariablePriceComponent } from 'src/app/components/variable-price/variable-price.component';
import { CurrencyPipe } from '@angular/common';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
import { SetAttributeValuesComponent } from 'src/app/components/set-attribute-values/set-attribute-values.component';
import { CustomerBusiness } from 'src/app/business/customer/customer.business';
import { PrintService } from 'src/app/services/print-service/print.service';

@Component({
  selector: 'app-sales-panel',
  templateUrl: './sales-panel.component.html',
  styleUrls: ['./sales-panel.component.scss'],
})
export class SalesPanelComponent implements OnInit {

  combinedCode = null
  @HostListener('document:keypress', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.barcodeScaner(this.combinedCode)
      this.combinedCode = null
    } else {
      if (this.combinedCode === null) {
        this.combinedCode = event.key;
      } else {
        this.combinedCode = this.combinedCode + event.key;
      }
    }
  }

  //------------------- Modals -----------------------
  paymentsModal: BsModalRef;
  tipModal: BsModalRef;
  findCustomerModal: BsModalRef;
  createCustomerModal: BsModalRef;
  productListModal: BsModalRef;
  variablePriceModal: BsModalRef;

  //--------------------- Needed variables
  displayedColumns = ['item', 'multiply', 'pricesell', 'value'];
  products: ProductModel[]
  categories: CategoryModel[]
  constProducts: ProductModel[]
  taxes: TaxModel[]
  sharedTickets: any[]
  sharedTicketId: string
  ticket: TicketModel
  splitTicket: TicketModel
  selectedItem: { index: number, origin: string };
  catalogHeight: string
  categoriesHeight: string
  tableHeight: string
  place: PlaceModel;
  guestNumber: number;
  restaurantMode: number;
  paymentOrigin: string;
  user: PeopleModel
  isInMove: boolean
  hostConfig: HostConfigModel
  navOrigin: number
  configModes
  attributesModal: BsModalRef;
  scanConfig = { scanTimeout: 100, scannerEndsWith: 'Enter' }

  //---------------------- Cash Information -------------------

  activeCash: string


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.catalogHeight = (window.innerHeight - 101).toString() + 'px';
    this.categoriesHeight = (window.innerHeight - 65).toString() + 'px';
    if (window.innerWidth < 768) {
      this.tableHeight = (window.innerHeight - 70).toString() + 'px';
    } else {
      this.tableHeight = (window.innerHeight - (65 + 197)).toString() + 'px';
    }
  }

  @ViewChild('confirmDeletionLine') public confirmDeletionLine: ModalDirective;
  @ViewChild('splitReceipt') public splitReceipt: ModalDirective;
  @ViewChild('numberGuest') public numberGuest: ModalDirective;
  @ViewChild('confirmDeletionReceipt') public confirmDeletionReceipt: ModalDirective;
  @ViewChild('sharedTicketList') public sharedTicketListModal: ModalDirective;

  constructor(
    private modalService: BsModalService,
    private moveTableService: MoveTableService,
    private productBusiness: ProductBusiness,
    private categoryBusiness: CategoryBusiness,
    private taxBusiness: TaxBusiness,
    private sharedTicketBusiness: SharedTicketBusiness,
    private closeCashBusiness: CloseCashBusiness,
    private placeBusiness: PlaceBusiness,
    private lineRemovedBusiness: LineRemovedBusiness,
    private toast: ToastService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public authBusiness: AuthBusiness,
    private loadingService: LoadingSpinnerService,
    private headerBodyService: HeaderBodyService,
    private customerBusiness: CustomerBusiness,
    public printService: PrintService,
    private storage: Storage) {

  }

  async ngOnInit() {
    try {
      let promises = []
      this.loadingService.show()
      // Temporales ------------------------------------
      this.navOrigin = 0
      this.configModes = ConfigModeEnum
      this.isInMove = false
      this.paymentOrigin = 'sales'
      this.products = []
      this.categories = []
      this.constProducts = []
      await this.resetComponent()

      this.catalogHeight = (window.innerHeight - 101).toString() + 'px';
      this.categoriesHeight = (window.innerHeight - 65).toString() + 'px';
      this.tableHeight = (window.innerHeight - (65 + 197)).toString() + 'px';

      this.selectedItem = { index: 0, origin: 'ticket' }
      if (this.restaurantMode == ConfigModeEnum.restaurant) {
        this.activatedRoute.params.pipe(take(1)).subscribe(async params => {
          this.sharedTicketId = params['id'];
          this.place = Object.values(await this.placeBusiness.getById(this.sharedTicketId))[0]
          if (this.place.ticketid !== '' && this.place.ticketid !== null && this.place.ticketid !== undefined) {
            this.ticket.customer = Object.values(await this.customerBusiness.getById(this.place.ticketid))[0]
            this.headerBodyService.customerFunction(this.ticket.customer.name)
          }
          this.headerBodyService.tableFunction(this.place.name)
          if (this.place.occupied === null) {
            this.place.occupied = new Date()
            let hoursDiff = this.place.occupied.getHours() - this.place.occupied.getTimezoneOffset() / 60;
            this.place.occupied.setHours(hoursDiff);
          }
          this.guestNumber = this.place.guests
          let sharedTicket = Object.values(await this.sharedTicketBusiness.getById(this.sharedTicketId))
          if (sharedTicket.length > 0) {
            this.sharedTicketToTicket(sharedTicket[0])
          } else {
            this.sharedTicketBusiness.save(this.sharedTicketId, this.ticket)
          }
          //------------------ if a table is moving ----------------------------------------------//
          let table = this.moveTableService.getTable()
          if (table !== null) {
            let sharedTicket = Object.values(await this.sharedTicketBusiness.getById(table.id))
            if (sharedTicket.length > 0) {
              this.mergeTicket(sharedTicket[0])
            }
          }
        });

      }

      promises.push(this.categoryBusiness.get().then(categories => {
        this.categories = Object.values(categories)
      }))

      promises.push(this.productBusiness.getConstCat().then(products => {
        this.constProducts = Object.values(products)
        this.products = this.constProducts
        this.products = this.products.slice()
      }))

      promises.push(this.taxBusiness.get().then(taxes => {
        this.taxes = Object.values(taxes)
      }))

      promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
        this.activeCash = Object.values(closeCash)[0].money
        this.ticket.activeCash = this.activeCash
      }))

      promises.push(this.authBusiness.readUser().then(user => {
        this.user = user
        this.ticket.user = this.user
      }))
      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (error) {
      console.log('An error ocurred on ngoninit ', error)
    }
  }


  async selectCategory(category: CategoryModel) {
    try {
      if (category.products == null) {
        let products = await this.productBusiness.getByCategory(category.id)
        category.products = Object.values(products)
        let filterConstProducts = this.constProducts.filter(product => product.category !== category.id)
        category.products = category.products.concat(filterConstProducts)
      }
      if (category.childCategories == null) {
        let categories = await this.categoryBusiness.getChild(category.id)
        category.childCategories = Object.values(categories)
      }
      let index = this.categories.map(category => category.id).indexOf(category.id)
      this.categories[index] = category
      this.products = category.products;
      this.products = this.products.slice()
    } catch (e) {
      console.log('An error occurred selecting a category')
    }
  }

  sharedTicketToTicket(sharedTicket) {
    try {
      this.sharedTicketId = sharedTicket.id
      this.ticket.lines = sharedTicket.products
      if (this.ticket.lines.length > 0) {
        this.ticket.id = this.ticket.lines[0].ticket
      }
      sharedTicket.products.forEach(product => {
        this.ticket.subTotal += product.subTotal
        this.ticket.total = this.ticket.subTotal + this.ticket.totalTaxes
      });
    } catch (error) {
      console.log('An error occurred conveting shared ticket to ticket')
    }
  }

  async mergeTicket(sharedTicket) {
    try {
      sharedTicket.products.forEach(line => {
        line.ticket = this.ticket.id
        this.ticket.lines.unshift(line)
        this.ticket.subTotal += line.subTotal
        this.ticket.total = this.ticket.subTotal + this.ticket.totalTaxes
      })
      this.ticket.lines = this.ticket.lines.slice()
      this.sharedTicketBusiness.delete(sharedTicket.id)
      let place = Object.values(await this.placeBusiness.getById(sharedTicket.id))[0]
      place.occupied = null
      place.waiter = null
      place.customer = null
      place.guests = 0
      this.placeBusiness.update(place)
      this.moveTableService.setTable(null)
    } catch (error) {
      console.log('An error occurred merging the tickets')
    }
  }

  selectSharedTicket(sharedTicket) {
    try {
      this.sharedTicketToTicket(sharedTicket)
      this.sharedTicketId = sharedTicket.id
      this.sharedTicketListModal.hide()
    } catch (error) {
      console.log('An error occurred selecting a shared ticket')
    }
  }
  async addProduct(product: ProductModel) {
    try {

      let modifiedProduct: ProductModel = new ProductModel()
      Object.assign(modifiedProduct, product)
      if (product.isvprice) {
        let index = this.ticket.lines.findIndex(line => line.productid === product.id)
        if (index === -1) {
          modifiedProduct = await this.openVariablePriceModal(modifiedProduct)
        }
      }
      if (product.attributeset_id !== '' && product.attributeset_id !== null && product.attributeset_id !== undefined) {
        modifiedProduct = await this.openAttributesModal(modifiedProduct)
      }
      let index = this.ticket.addProduct(modifiedProduct, this.taxes, this.toast)

    } catch (e) {
      console.log('An error occurred adding a product')
    }
  }

  async dismissQty(index: number, qty = 1) {
    try {
      if (this.ticket.lines[index].multiply < 1) {
        //confirmDeletionLine
        let lineRemoved = new LineRemovedModel();
        lineRemoved.name = this.ticket.user.name
        lineRemoved.ticketid = 'Void'
        lineRemoved.productid = this.ticket.lines[index].productid
        lineRemoved.productname = this.ticket.lines[index].attributes.name
        lineRemoved.units = 1
        await this.lineRemovedBusiness.save(lineRemoved)
        this.ticket.lines.splice(index, 1)
        this.ticket.lines = this.ticket.lines.slice()
      }
      this.ticket.dismissQty(index, qty)
      return true
    } catch (error) {
      console.log('An error occurred decreasing quantity')
    }
  }

  getRecord(index, origin: string) {
    try {
      this.selectedItem = { index, origin }
    } catch (error) {
      console.log(`An error occurred getting the record`);
    }
  }

  async updateSharedTicket() {
    try {
      await this.sharedTicketBusiness.update(this.sharedTicketId, this.ticket)
    } catch (error) {
      console.log('An error occurred updating shared ticket')
    }
  }

  async getSharedTicketList() {
    try {
      let sharedTickets = await this.sharedTicketBusiness.get()
      console.log(sharedTickets)
    } catch (error) {
      console.log('An error occurred getting shared ticket')
    }
  }

  async deleteSharedTicket() {
    try {
      this.closeTable()
      if (this.restaurantMode != ConfigModeEnum.restaurant) {
        this.resetComponent()
        this.sharedTicketId = uuid.v4()
        this.confirmDeletionReceipt.hide()
        if (this.sharedTickets.length > 0) {
          this.sharedTicketListModal.show()
        } else {
          this.sharedTicketId = uuid.v4()
          this.sharedTicketBusiness.save(this.sharedTicketId, this.ticket)
        }
      }
    } catch (error) {
      console.log('An error occurred gatting shared ticket')
    }
  }

  async resetComponent() {
    try {
      this.guestNumber = 0
      this.sharedTicketId = ''
      this.sharedTickets = []
      this.ticket = new TicketModel()
      this.ticket.id = uuid.v4()
      this.ticket.pickupId = 0
      this.ticket.ticketId = 0
      this.ticket.lines = []
      this.ticket.taxes = []
      this.ticket.ticketType = TicketEnum.RECEIPT_NORMAL
      this.ticket.ticketstatus = 0
      this.ticket.user = this.user ? this.user : null
      this.ticket.date = new Date()
      let hoursDiff = this.ticket.date.getHours() - this.ticket.date.getTimezoneOffset() / 60;
      //let minutesDiff = (this.ticket.date.getHours() - this.ticket.date.getTimezoneOffset()) % 60;
      this.ticket.date.setHours(hoursDiff);
      //this.ticket.date.setMinutes(minutesDiff);
      this.ticket.total = 0
      this.ticket.subTotal = 0
      this.ticket.totalTaxes = 0
      this.ticket.activeCash = this.activeCash
      this.splitTicket = _.cloneDeep(this.ticket)
      this.hostConfig = await this.storage.get('config')
      this.ticket.host = this.hostConfig.hostName
      this.restaurantMode = this.hostConfig.configMode
      if (this.restaurantMode != ConfigModeEnum.restaurant) {
        this.sharedTicketBusiness.get().then(sharedTicketList => {
          this.sharedTickets = Object.values(sharedTicketList)
          if (this.sharedTickets.length > 0) {
            this.sharedTicketListModal.show()
          } else {
            this.sharedTicketId = uuid.v4()
            this.sharedTicketBusiness.save(this.sharedTicketId, this.ticket)
          }
        })
      }
    } catch (error) {
      console.log('An error occurred resetting the component')
    }
  }

  async goTables() {
    try {
      if (!this.isInMove) {
        let promises = []
        if (this.ticket.customer !== undefined) {
          this.place.customer = this.ticket.customer.name
          this.place.ticketid = this.ticket.customer.id
        }
        this.place.waiter = this.ticket.user.name
        this.place.guests = this.guestNumber
        promises.push(this.placeBusiness.update(this.place))
        promises.push(this.updateSharedTicket())
        await Promise.all(promises)
      }
      this.navigateToTables()
    } catch (error) {
      console.log('An error occurred navigating to tables')
    }
  }

  async closeTable() {
    try {
      await this.sharedTicketBusiness.delete(this.sharedTicketId)
      if (this.restaurantMode == ConfigModeEnum.restaurant) {
        this.place.occupied = null
        this.place.waiter = null
        this.place.customer = null
        this.place.guests = 0
        await this.placeBusiness.update(this.place)
        this.navigateToTables()
      } else {
        this.resetComponent()
      }
    } catch (error) {
      console.log(`An error occurred closing table`)
    }
  }

  navigateToTables() {
    try {
      this.navOrigin = 1
      this.router.navigate(['/sales/restaurant/t'])
    } catch (error) {
      console.log('An error occurred navigating to tables')
    }
  }

  setGuestNumber(guestNumber: string) {
    try {
      this.guestNumber = parseInt(guestNumber)
    } catch (error) {
      console.log(`An error occurred settig the number of guest`)
    }
  }

  moveItems(action: string) {
    try {
      switch (action) {
        case 'right':
          this.splitTicket.addProduct(this.ticket.lines[this.selectedItem.index].attributes, this.taxes, this.toast)
          this.dismissQty(this.selectedItem.index)
          break;
        case 'doubleRight':
          this.splitTicket.addProduct(this.ticket.lines[this.selectedItem.index].attributes, this.taxes, this.toast, this.ticket.lines[this.selectedItem.index].multiply)
          this.dismissQty(this.selectedItem.index, this.ticket.lines[this.selectedItem.index].multiply)
          break;
        case 'left':
          this.ticket.addProduct(this.splitTicket.lines[this.selectedItem.index].attributes, this.taxes, this.toast)
          this.splitTicket.dismissQty(this.selectedItem.index)
          break;
        case 'doubleLeft':
          this.ticket.addProduct(this.splitTicket.lines[this.selectedItem.index].attributes, this.taxes, this.toast, this.splitTicket.lines[this.selectedItem.index].multiply)
          this.splitTicket.dismissQty(this.selectedItem.index, this.splitTicket.lines[this.selectedItem.index].multiply)
          break;

      }
    } catch (error) {
      console.log(`An error occurred moving th items from ticket to split ticket`)
    }
  }

  moveTable() {
    try {
      this.isInMove = true
      this.moveTableService.setTable(this.place)
      this.goTables()
    } catch (error) {
      console.log('An error occurred moving the table')
    }
  }

  async canDeactivate() {
    try {
      if (this.hostConfig.configMode == ConfigModeEnum.restaurant) {
        if (this.navOrigin === 0) {
          let promises = []
          if (this.ticket.customer !== undefined) {
            this.place.customer = this.ticket.customer.name
            this.place.ticketid = this.ticket.customer.id
          }
          this.place.waiter = this.ticket.user.name
          this.place.guests = this.guestNumber
          promises.push(this.placeBusiness.update(this.place))
          promises.push(this.updateSharedTicket())
          await Promise.all(promises)
        }
      } else {
        if (this.ticket.lines.length > 0) {
          await this.sharedTicketBusiness.update(this.sharedTicketId, this.ticket)
        } else {
          await this.sharedTicketBusiness.delete(this.sharedTicketId)
        }
      }
      this.headerBodyService.tableFunction('')
      this.headerBodyService.customerFunction('')
      return true;
    } catch (error) {
      this.toast.showMessage('Upss algo ha ocurrido', 'danger')
      return false
    }
  }

  public printPreInvoice() {
    try {
      this.printService.printDocument('pre-invoice', this.ticket)
    } catch (e) {
      console.log('An error occurred printing pre invoice')
    }
  }

  public async barcodeScaner(code) {
    try {
      let index = this.products.findIndex(item => item.code === code)
      if (index > -1) {
        this.addProduct(this.products[index])
      } else {
        let product = Object.values(await this.productBusiness.getByBarCode(code))[0]
        if (product !== null && product !== undefined) {
          this.addProduct(product)
        }
      }
    } catch (error) {
      console.log('An error occurred barcodeScaner')
    }
  }
  //---------------------------- Modals ---------------------------------------------------------//

  closeSharedTicketListModal() {
    try {
      if (this.sharedTicketId === '') {
        this.sharedTicketId = uuid.v4()
        this.sharedTicketBusiness.save(this.sharedTicketId, this.ticket)
      }
      this.sharedTicketListModal.hide()
    } catch (error) {
      console.log(`an error occurred closing the shared ticket list modal`)
    }

  }

  closeSplitTicketModal() {
    try {
      if (this.splitTicket.lines.length > 0) {
        this.splitTicket.lines.forEach(line => {
          this.ticket.addProduct(line.attributes, this.taxes, this.toast, line.multiply)
        })
      }
      this.splitReceipt.hide()
    } catch (error) {
      console.log(`an error occurred closing the shared ticket list modal`)
    }

  }

  openSplitReceiptModal() {
    try {
      this.splitTicket = _.cloneDeep(this.ticket)
      this.splitTicket.total = 0
      this.splitTicket.subTotal = 0
      this.splitTicket.totalTaxes = 0
      this.splitTicket.lines = []
      this.selectedItem = { index: 0, origin: 'sales' }
      this.splitReceipt.show()
    } catch (error) {
      console.log(`An error occurred opening the split receipt modal`)
    }
  }
  async openTipModal() {
    try {
      this.tipModal = this.modalService.show(TipComponent, {
        class: 'modal-lg',
        initialState: {
          subTotal: this.ticket.total
        }
      })
      return new Promise((resolve, reject) => {
        try {
          this.tipModal.content.tip.pipe(take(1)).subscribe(async (tip: ProductModel) => {
            if (tip.pricesell > 0) {
              this.addProduct(tip)
            }
            resolve()
          })
        } catch (e) {
          console.log('An error occurred in opentipModal => salesPanelComponent')
        }
      })

    } catch (e) {
      console.log('An error occurred in opentipModal => salesPanelComponent')
    }
  }
  async openPaymentsModal(origin: string) {
    try {
      if (this.hostConfig.activeTip) {
        await this.openTipModal()
        this.tipModal.hide()
      }

      this.paymentOrigin = origin
      this.paymentsModal = this.modalService.show(PaymentsComponent, {
        class: 'modal-lg',
        initialState: {
          origin: 'sales',
          ticket: origin === 'sales' ? this.ticket : this.splitTicket
        }
      });
      this.paymentsModal.content.completed.pipe(take(1)).subscribe(async completed => {
        if (completed.isCompleted) {
          if (this.paymentOrigin === 'sales') {
            this.closeTable()
          } else {
            this.ticket.id = uuid.v4();
            this.ticket.lines.forEach(line => {
              line.ticket = this.ticket.id
            })
            this.splitReceipt.hide()
            await this.sharedTicketBusiness.update(this.sharedTicketId, this.ticket)
          }

        } else {
          this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el pago', 'danger')
          if (this.paymentOrigin === 'split') {
            this.closeSplitTicketModal()
          }
        }
      })
    } catch (error) {
      console.log('An error ocurred opening payments modal: ', error);
    }
  }

  async openVariablePriceModal(product: ProductModel) {
    try {
      this.variablePriceModal = this.modalService.show(VariablePriceComponent, {
        class: 'modal-lg',
        initialState: {
          originalPrice: product.pricesell
        }
      });
      return new Promise<ProductModel>(async (resolve, reject) => {
        this.variablePriceModal.content.finalPrice.pipe(take(1)).subscribe(finalPrice => {
          product.pricesell = finalPrice
          resolve(product)
        })
      })
    } catch (error) {
      console.log('An error ocurred opening variable price modal: ', error);
    }
  }

  openFindCustomerModal() {
    try {
      this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
      this.findCustomerModal.content.selectedCustomer.pipe(take(1)).subscribe(customer => {
        this.ticket.customer = customer
        if(customer.isvip){
          this.ticket.discount = customer.discount
          this.headerBodyService.discountFunction(customer.discount)
        }
        this.headerBodyService.customerFunction(customer.name)
      })
    } catch (error) {
      console.log('An error ocurred opening find customer modal: ', error);

    }
  }

  openCreateCustomerModal() {
    try {
      this.createCustomerModal = this.modalService.show(CreateCustomerComponent, { class: 'modal-lg' });
      this.createCustomerModal.content.createdCustomer.pipe(take(1)).subscribe(customer => {
        this.ticket.customer = customer
        this.headerBodyService.customerFunction(customer.name)
        if(customer.isvip){
          this.ticket.discount = customer.discount
          this.headerBodyService.discountFunction(customer.discount)
        }
      })
    } catch (error) {
      console.log('An error ocurred opening create customer modal: ', error);
    }
  }

  openProductListModal() {
    try {
      this.productListModal = this.modalService.show(ProductListComponent, { class: 'modal-lg' });
    } catch (error) {
      console.log('An error ocurred opening product list modal: ', error);
    }
  }

  openAttributesModal(product: ProductModel): Promise<ProductModel> {
    try {
      this.attributesModal = this.modalService.show(SetAttributeValuesComponent, {
        class: 'modal-lg',
        initialState: {
          attributeSetId: product.attributeset_id
        }
      });
      return new Promise(async (resolve, reject) => {
        try {
          this.attributesModal.content.selectedAttributeValues.pipe(take(1)).subscribe(attributes => {
            product.attributeinstance = attributes
            resolve(product)
          })
        } catch (err) {
          console.log('An error occurred consumming the api: ', err);
        }
      })
    } catch (error) {
      console.log('An error ocurred opening attribute modal: ', error);
    }
  }
}
