import { NgModule, Component } from '@angular/core';
import { SalesRoutingModule } from './sales-routing.module'
import { SalesPanelComponent } from './sales-panel/sales-panel.component';
import { BsDropdownModule, BsDropdownToggleDirective } from 'ngx-bootstrap/dropdown';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkTableModule } from '@angular/cdk/table';
import {
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule
} from "@angular/material";
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { NumericKeyboardComponent } from '../../components/numeric-keyboard/numeric-keyboard.component'
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AlertModule } from 'ngx-bootstrap/alert';
import { PaymentsComponent } from '../../components/payments/payments.component'
import { FindCustomerComponent } from '../../components/find-customer/find-customer.component'
import { CreateCustomerComponent } from '../../components/create-customer/create-customer.component'
import { ProductListComponent } from '../../components/product-list/product-list.component';
import { CommonModule } from '@angular/common';
import { ReservationsComponent } from './reservations/reservations.component';
import { ComponentsModule } from '../../components/components.module';
import { SalesPanelUnicentaComponent } from './sales-panel-unicenta/sales-panel-unicenta.component';
import { FormsModule } from '@angular/forms';
import { ChangeComponent } from 'src/app/components/change/change.component';
import { SalesMenuComponent } from './sales-menu/sales-menu.component';
import { CloseCashComponent } from './reports/close-cash/close-cash.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { deLocale } from 'ngx-bootstrap/locale';
import { environment } from '../../../environments/environment';
import { ProductSalesProductComponent } from './reports/product-sales-product/product-sales-product.component';
import { CategorySalesComponent } from './reports/category-sales/category-sales.component';
import { TransactionsComponent } from './reports/transactions/transactions.component';
import { CashFlowComponent } from './reports/cash-flow/cash-flow.component';
import { ProductSalesProfitComponent } from './reports/product-sales-profit/product-sales-profit.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { CanDeactivateGuard } from 'src/app/guards/can-deactivate.guard';
import { CanActiveSalesGuard } from 'src/app/guards/can-active-sales.guard';
import { LoadingSpinnerComponent } from 'src/app/components/loading-spinner/loading-spinner.component';
import { TipComponent } from 'src/app/components/tip/tip.component';
import { VariablePriceComponent } from 'src/app/components/variable-price/variable-price.component';
import { NgxCurrencyModule } from "ngx-currency";
import { PlacesComponent } from '../maintenance/places/places.component';
import { SetAttributeValuesComponent } from 'src/app/components/set-attribute-values/set-attribute-values.component';

defineLocale(environment.activeLang, deLocale); 
@NgModule({
  imports: [
    CommonModule,
    SalesRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ScrollingModule,
    CdkTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    ModalModule,
    TabsModule,
    AlertModule.forRoot(),
    ComponentsModule,
    BsDatepickerModule.forRoot(),
    NgxCurrencyModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
  ],
  declarations: [
    SalesMenuComponent,
    SalesPanelComponent,
    SalesPanelUnicentaComponent,
    ReservationsComponent,
    CloseCashComponent,
    ProductSalesProductComponent,
    CategorySalesComponent,
    TransactionsComponent,
    CashFlowComponent,
    ProductSalesProfitComponent
  ],
  providers: [
    BsModalService,
    CanDeactivateGuard,
    CanActiveSalesGuard
  ],
  bootstrap: [
    PaymentsComponent,
    ChangeComponent,
    FindCustomerComponent,
    CreateCustomerComponent,
    ProductListComponent,
    LoadingSpinnerComponent,
    TipComponent,
    VariablePriceComponent,
    SetAttributeValuesComponent
  ]
})
export class SalesModule { }
