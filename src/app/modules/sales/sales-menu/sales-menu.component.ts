import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'growpos-sales-menu',
  templateUrl: './sales-menu.component.html',
  styleUrls: ['./sales-menu.component.scss'],
})
export class SalesMenuComponent implements OnInit {

  public activeLang = environment.activeLang;

  module: {
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };

  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);

  }

  ngOnInit() {

    this.module = {
      name: this.translate.instant('sales'),
      manage: this.translate.instant('informs'),
      reports: this.translate.instant('graphics'),
      manageItems: [],
      reportsItems: []
    };
    this.module.manageItems.push({
      name: this.translate.instant('cash_closures'),
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/close-cash'
    });
    this.module.manageItems.push({
      name: this.translate.instant('sales_product'),
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/product-sales'
    });
    this.module.manageItems.push({
      name: this.translate.instant('sales_category'),
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/category-sales'
    });
    this.module.manageItems.push({
      name: `Transacciones`,
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/transactions'
    });
    this.module.manageItems.push({
      name: `Movimientos de caja`,
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/cash-flow'
    });
    this.module.manageItems.push({
      name: `Ganacia por producto`,
      icon: 'fa fa-calculator fa-lg',
      url: '/sales/report/product-sales-profit'
    });

  }
}
