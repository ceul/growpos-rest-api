import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BsDaterangepickerDirective, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'growpos-cash-flow',
  templateUrl: './cash-flow.component.html',
  styleUrls: ['./cash-flow.component.scss'],
})
export class CashFlowComponent implements OnInit {

  @ViewChild(BsDaterangepickerDirective) datepicker: BsDaterangepickerDirective;

  public minDate: Date;
  public maxDate: Date;
  public bsRangeValue: Date[];
  public activeLang = environment.activeLang;

  public data: any
  public translateFields: string[]
  public dateFields: string[]
  constructor(private closeCashBusiness: CloseCashBusiness,
    private loadingService: LoadingSpinnerService,
    private localeService: BsLocaleService) { }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.localeService.use(this.activeLang);
    this.minDate = new Date();
    this.maxDate = new Date();

    this.minDate.setDate(this.minDate.getDate() - 7);
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.minDate, this.maxDate];
    promises.push(this.closeCashBusiness.getCashFlowReport(this.minDate, this.maxDate).then(res => {
      this.data = Object.values(res)
    }))
    this.dateFields = []
    this.translateFields = []
    this.translateFields.push('payment')
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  public execute() {
    try {
      this.loadingService.show()
      this.closeCashBusiness.getCashFlowReport(this.bsRangeValue[0], this.bsRangeValue[1]).then(res => {
        this.data = Object.values(res)
        this.loadingService.hide()
      })
    } catch (error) {

    }
  }

}
