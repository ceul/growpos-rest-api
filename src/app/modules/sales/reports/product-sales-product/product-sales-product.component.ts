import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDaterangepickerDirective, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { environment } from 'src/environments/environment';
import { ProductBusiness } from 'src/app/business/product/product.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'growpos-product-sales-product',
  templateUrl: './product-sales-product.component.html',
  styleUrls: ['./product-sales-product.component.scss'],
})
export class ProductSalesProductComponent implements OnInit {

  @ViewChild(BsDaterangepickerDirective) datepicker: BsDaterangepickerDirective;

  public minDate: Date;
  public maxDate: Date;
  public bsRangeValue: Date[];
  public activeLang = environment.activeLang;

  public data: any
  public translateFields: string[]
  public dateFields: string[]
  constructor(private productBusiness: ProductBusiness,
    private loadingService: LoadingSpinnerService,
    private localeService: BsLocaleService) { }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.localeService.use(this.activeLang);
    this.minDate = new Date();
    this.maxDate = new Date();

    this.minDate.setDate(this.minDate.getDate() - 7);
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.minDate, this.maxDate];
    promises.push(this.productBusiness.getProductSalesReport(this.minDate, this.maxDate).then(res => {
      this.data = Object.values(res)
    }))
    this.dateFields = []
    this.dateFields.push('dateend')
    this.translateFields = []
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  public execute() {
    try {
      this.loadingService.show()
      this.productBusiness.getProductSalesReport(this.bsRangeValue[0], this.bsRangeValue[1]).then(res => {
        this.data = Object.values(res)
        this.loadingService.hide()
      })
    } catch (error) {

    }
  }

}
