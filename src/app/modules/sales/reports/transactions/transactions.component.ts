import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDaterangepickerDirective, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { environment } from 'src/environments/environment';
import { PaymentBusiness } from 'src/app/business/payment/payment.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { TranslateService } from '@ngx-translate/core';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'growpos-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {

  @ViewChild(BsDaterangepickerDirective) datepicker: BsDaterangepickerDirective;

  public minDate: Date;
  public maxDate: Date;
  public bsRangeValue: Date[];
  public activeLang = environment.activeLang;
  public tableHeight: string;
  public total: number;
  public countTicket: number;

  public data: any
  public data_payment: any
  public translateFields: string[]
  public dateFields: string[]
  public showDetail: boolean;

  displayedColumns = ['ticket_no', 'payment', 'total'];

  constructor(private paymentBusiness: PaymentBusiness,
    private loadingService: LoadingSpinnerService,
    private localeService: BsLocaleService,
    private translate: TranslateService) { 
      this.translate.setDefaultLang(this.activeLang);
    }

  ngOnInit() {
    let promises = []
    this.loadingService.show()
    this.localeService.use(this.activeLang);
    this.minDate = new Date();
    this.maxDate = new Date();
    this.tableHeight = "305px"

    this.minDate.setDate(this.minDate.getDate() - 7);
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.minDate, this.maxDate];
    promises.push(this.paymentBusiness.getTransactions(this.minDate, this.maxDate).then(res => {
      this.data = Object.values(res)
    }))
    this.dateFields = []
    this.dateFields.push('date')
    this.translateFields = []
    this.translateFields.push('payment')
    Promise.all(promises).then(() => this.loadingService.hide())
    this.showDetail = false;
  }

  public execute() {
    try {
      this.loadingService.show()
      this.paymentBusiness.getTransactions(this.bsRangeValue[0], this.bsRangeValue[1]).then(res => {
        this.data = Object.values(res);
        this.countTicket = this.data.length;
        this.loadingService.hide()
      });
      this.total = 0;
      this.countTicket = 0;
      
      this.paymentBusiness.getTransactionsByPayment(this.bsRangeValue[0], this.bsRangeValue[1]).then(res => {
        this.data_payment= Object.values(res);
        this.data_payment.map(values => {
          
          let element = Object.values(values);
          this.total = this.total + parseInt(values.total);
          
        });
        
        this.loadingService.hide();
      });
    } catch (error) {
      throw new Error('An error occurred in execute() => TransactionsComponent')
    }
  }

}
