import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-menu',
  templateUrl: './customer-menu.component.html',
  styleUrls: ['./customer-menu.component.scss']
})
export class CustomerMenuComponent implements OnInit {

  module:{
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };
  constructor() {
    this.module = {
      name: 'Clientes',
      manage: 'Gestion',
      reports: 'Reportes',
      manageItems: [],
      reportsItems: []
    };
    this.module.manageItems.push({
      name: `Clientes`,
      icon: 'fa fa-address-card-o fa-lg',
      url: '/customer/customer-manage'
    });
   }

  ngOnInit() {
  }

}
