import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import CustomerModel from 'src/app/models/customer.model';
import PaymentModel from 'src/app/models/payment.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import TicketModel from 'src/app/models/ticket.model';
import { take } from 'rxjs/operators';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { CloseCashBusiness } from 'src/app/business/close-cash/close-cash.business';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import HostConfigModel from 'src/app/models/host-config.model';
import PeopleModel from 'src/app/models/people.model';
import * as uuid from "uuid";
import { Storage } from '@ionic/storage';
import TicketEnum from 'src/app/models/ticket.enum';
import { CustomerBusiness } from 'src/app/business/customer/customer.business';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'growpos-customer-pays',
  templateUrl: './customer-pays.component.html',
  styleUrls: ['./customer-pays.component.scss'],
})
export class CustomerPaysComponent implements OnInit {

  //------------------- Modals -----------------------
  paymentsModal: BsModalRef;
  findCustomerModal: BsModalRef;

  public transactions: any
  public translateFields: string[]
  public dateFields: string[]

  customer: CustomerModel
  payment: PaymentModel
  ticket: TicketModel
  activeCash: string
  hostConfig: HostConfigModel
  user: PeopleModel
  note: string
  debt: number;
  prePayAmount: number

  constructor(
    private modalService: BsModalService,
    private toast: ToastService,
    private closeCashBusiness: CloseCashBusiness,
    private authBusiness: AuthBusiness,
    private customerBusiness: CustomerBusiness,
    private loadingService: LoadingSpinnerService,
    private storage: Storage
  ) { }

  async ngOnInit() {
    let promises = []
    this.loadingService.show()
    await this.resetComponent()
    promises.push(this.closeCashBusiness.getLastByHost(this.hostConfig.hostName).then(closeCash => {
      this.activeCash = Object.values(closeCash)[0].money
      this.ticket.activeCash = this.activeCash
    }))

    promises.push(this.authBusiness.readUser().then(user => {
      this.user = user
      this.ticket.user = this.user
    }))

    this.dateFields = []
    this.dateFields.push('date')
    this.translateFields = []
    Promise.all(promises).then(() => this.loadingService.hide())
  }

  pay() {
    try {
      this.ticket.customer.curdebt = 0
      this.openPaymentsModal()
    } catch (error) {
      console.log('An error occurred in pay')
    }
  }

  prePay() {
    try {
      this.ticket.total = this.prePayAmount
      this.ticket.customer.curdebt = this.ticket.customer.curdebt - this.prePayAmount
      this.openPaymentsModal()
    } catch (error) {
      console.log('An error occurred in prepay')
    }
  }

  async resetComponent() {
    try {
      //this.customer = new CustomerModel()
      this.ticket = new TicketModel()
      this.ticket.customer = new CustomerModel()
      this.ticket.customer.curdebt = 0
      this.ticket.id = uuid.v4()
      this.ticket.pickupId = 0
      this.ticket.ticketId = 0
      this.ticket.lines = []
      this.ticket.taxes = []
      this.ticket.ticketType = TicketEnum.RECEIPT_PAYMENT
      this.ticket.ticketstatus = 0
      this.ticket.user = this.user ? this.user : null
      this.ticket.date = new Date()
      let hoursDiff = this.ticket.date.getHours() - this.ticket.date.getTimezoneOffset() / 60;
      this.ticket.date.setHours(hoursDiff);
      this.ticket.total = 0
      this.ticket.subTotal = 0
      this.ticket.totalTaxes = 0
      this.ticket.activeCash = this.activeCash
      this.prePayAmount = 0
      this.hostConfig = await this.storage.get('config')
      this.ticket.host = this.hostConfig.hostName
    } catch (error) {
      console.log('An error occurred resetting the component')
    }
  }

  openPaymentsModal() {
    try {
      this.paymentsModal = this.modalService.show(PaymentsComponent, {
        class: 'modal-lg',
        initialState: {
          origin: 'customer_pay',
          ticket: this.ticket,
          note: this.note
        }
      });
      this.paymentsModal.content.completed.pipe(take(1)).subscribe(async completed => {
        if (completed.isCompleted) {
          //this.resetComponent()
          let customer = new CustomerModel(await this.customerBusiness.getById(this.ticket.customer.id));
          this.ticket.customer = customer;
          this.ticket.total = customer.curdebt
          this.debt = customer.curdebt
          this.prePayAmount = 0
          this.toast.showMessage('El pago se ha realizado con exito!!', 'success')
        } else {
          this.ticket.customer.curdebt = this.debt
          this.toast.showMessage('Upss!! Algo ha ocurrido a la hora de realizar el pago', 'danger')
        }
      })
    } catch (error) {
      console.log('An error ocurred opening payments modal: ', error);
    }
  }

  openFindCustomerModal() {
    try {
      this.findCustomerModal = this.modalService.show(FindCustomerComponent, { class: 'modal-lg' });
      this.findCustomerModal.content.selectedCustomer.pipe(take(1)).subscribe(async customer => {
        this.ticket.customer = customer
        this.ticket.total = customer.curdebt
        this.debt = customer.curdebt
        this.transactions = Object.values(await this.customerBusiness.getTransactions(this.ticket.customer.id))
      })
    } catch (error) {
      console.log('An error ocurred opening find customer modal: ', error);
    }
  }

}
