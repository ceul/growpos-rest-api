import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerMenuComponent } from './customer-menu/customer-menu.component';
import { CustomerManageComponent } from './customer-manage/customer-manage.component';
import { CustomerPaysComponent } from './customer-pays/customer-pays.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Customers'
    },
    children: [
      {
        path: '',
        component: CustomerMenuComponent,
        data: {
          title: 'Menu de clientes'
        }
      },
      {
        path: 'customer-manage',
        component: CustomerManageComponent,
        data: {
          title: 'Gestion de clientes'
        }
      },
      {
        path: 'customer-payments',
        component: CustomerPaysComponent,
        data: {
          title: 'Pago de clientes'
        }
      }
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class CustomerRoutingModule { }
