import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import CustomerModel from '../../../models/customer.model';
import { CustomerService } from '../../../services/customer/customer.service';
import { CustomerBusiness } from '../../../business/customer/customer.business';
import { CustomerModule } from '../customer.module';
import { ToastService } from '../../../services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-customer-manage',
  templateUrl: './customer-manage.component.html',
  styleUrls: ['./customer-manage.component.scss'],
  providers: [
    CustomerBusiness
  ]
})
export class CustomerManageComponent implements OnInit, OnDestroy {
  //------------ tool bar button state ---------------//

  trash: boolean
  save: boolean
  add: boolean
  edit: boolean
  newItem: boolean

  //------------------ needed variables -----------------
  submit: boolean;
  customer: CustomerModel
  customers: CustomerModel[] = []
  transactions: {}[] = []
  displayedColumns = ['ticketid', 'date', 'product', 'quantity', 'total'];
  subscription: Subscription
  customerForm: FormGroup;

  constructor(private customerBusiness: CustomerBusiness,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.trash = true
    this.save = false
    this.add = false
    this.edit = false
    this.submit = false;
    this.newItem = true;
    this.customerForm = new FormGroup({
      'id': new FormControl(''),
      'searchkey': new FormControl('', Validators.required),
      'taxid': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'taxcategory': new FormControl(''),
      'card': new FormControl(''),
      'maxdebt': new FormControl(0, Validators.min(0)),
      'address': new FormControl(''),
      'address2': new FormControl(''),
      'postal': new FormControl(''),
      'city': new FormControl(''),
      'region': new FormControl(''),
      'country': new FormControl(''),
      'firstname': new FormControl(''),
      'lastname': new FormControl(''),
      'email': new FormControl('', Validators.email),
      'phone': new FormControl(''),
      'phone2': new FormControl(''),
      'fax': new FormControl(''),
      'notes': new FormControl(''),
      'visible': new FormControl(true),
      'curdate': new FormControl({ value: '', disabled: true }),
      'curdebt': new FormControl({ value: '', disabled: true }, Validators.min(0)),
      'image': new FormControl(''),
      'isvip': new FormControl(false),
      'discount': new FormControl('', Validators.min(0)),
      'id_third_party_type': new FormControl('1')
    })
  }

  async ngOnInit() {
    this.loadingService.show()
    this.customers = []
    let customers = await this.customerBusiness.get()
    this.customers = Object.values(customers)
    this.subscription = this.customerForm.valueChanges.subscribe(() => {
      if (this.customerForm.dirty) {
        if (!this.newItem) {
          this.edit = true
        }
      }
    })
    this.loadingService.hide()
  }

  ngOnDestroy(){
    this.subscription.unsubscribe()
  }

  public selectCustomer(customer) {
    if (this.customer !== customer && customer !== null) {
      this.customer = customer
      this.customerForm.reset()
      this.customerForm.patchValue(this.customer)
      this.submit = false
      this.newItem = false
      this.trash = false
      this.edit = false
    }
    if (customer === null) {
      this.clearForm()
      this.submit = false;
      this.newItem = true
      this.trash = true
      this.edit = false
    }
  }

  public async saveCustomer() {

    if (this.customerForm.valid) {
      this.loadingService.show()
      this.customer = new CustomerModel(this.customerForm.value)
      if (this.newItem) {
        let response = await this.customerBusiness.save(this.customer)
        this.customers.unshift(Object(this.customer))
        this.customers = this.customers.slice()
        this.toast.showMessage('El cliente ha sido añadido con exito!!', 'success')
        this.edit = false
      } else {
        let response = await this.customerBusiness.update(this.customer)
        let index = this.customers.map(customer => customer.id).indexOf(this.customer.id)
        this.customers[index] = this.customer
        this.customers = this.customers.slice()
        this.toast.showMessage('El cliente ha sido actualizado con exito!!', 'success')
        this.edit = false
      }
      this.loadingService.hide()
    }
    this.submit = true
  }

  public async deleteCustomer() {
    try {
      this.loadingService.show()
      let response = await this.customerBusiness.delete(this.customer.id)
      this.toast.showMessage('El cliente ha sido eliminado con exito!!', 'success')
      let index = this.customers.map(customer => customer.id).indexOf(this.customer.id)
      this.customers.splice(index, 1)
      this.customers = this.customers.slice()
      this.clearForm()
      this.edit = false
      this.loadingService.hide()
    } catch (e) {

    }
  }

  public clearForm() {
    try {
      let customer = new CustomerModel()
      customer.visible = true
      customer.isvip = false
      customer.id_third_party_type = '1'
      customer.maxdebt = 0
      this.customer = customer
      this.customerForm.reset()
      this.customerForm.patchValue(this.customer)
      this.trash = true
    } catch (e) {

    }
  }

}
