import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerMenuComponent } from './customer-menu/customer-menu.component';
import { CustomerManageComponent } from './customer-manage/customer-manage.component';
import { StockMenuComponent } from '../stock/stock-menu/stock-menu.component';
import { ComponentsModule } from '../../components/components.module';
import { CustomerRoutingModule } from './customer-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { MatInputModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { CustomerPaysComponent } from './customer-pays/customer-pays.component';
import { FindCustomerComponent } from 'src/app/components/find-customer/find-customer.component';
import { PaymentsComponent } from 'src/app/components/payments/payments.component';
import { ChangeComponent } from 'src/app/components/change/change.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  declarations: [
    CustomerMenuComponent, 
    CustomerManageComponent,
    CustomerPaysComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    CustomerRoutingModule,
    TabsModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    NgxCurrencyModule
  ],
  bootstrap: [
    FindCustomerComponent,
    PaymentsComponent,
    ChangeComponent
  ]
})
export class CustomerModule { }
