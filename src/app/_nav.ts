import { Storage } from '@ionic/storage';
import ConfigModeEnum from './models/config-mode.enum';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

@Injectable({
  providedIn: 'root'
})
export class Nav {

  className = 'Nav'
  restaurantMode

  constructor(private storage: Storage) { }


  async navItems(permissions?: JSON): Promise<NavData[]> {
    var result = [];
    for (var i in permissions)
      result.push(permissions[i]);
    return result;
  }
  /*
    async navItems(): Promise<NavData[]> {
      await this.configMode()
      return [
        {
          name: 'Escritorio',
          url: '/dashboard',
          icon: 'icon-speedometer'
  
        },
        {
        name: 'Principal',
        icon: 'icon-basket-loaded',
        children: [
            {
              name: 'Ventas',
              url: this.restaurantMode,
              icon: 'icon-basket'
            },
            {
              name: 'Pagos de clientes',
              url: '/customer/customer-payments',
              icon: 'icon-user-follow'
            },
            {
              name: 'Movimientos de caja',
              url: '/close-cash/cash-movement',
              icon: 'icon-handbag'
            },
            {
              name: 'Cierre de caja',
              url: '/close-cash',
              icon: 'icon-calculator'
            }
          ]
        },
        {
          name: 'Administracion',
          icon: 'icon-settings',
          children: [
            {
              name: 'Clientes',
              url: '/customer',
              icon: 'icon-user'
            },
            {
              name: 'Inventario',
              url: '/stock',
              icon: 'icon-home'
            },
            {
              name: 'Reportes de Ventas',
              url: '/sales/menu',
              icon: 'icon-basket'
            },
            {
              name: 'Mantenimiento',
              url: '/maintenance',
              icon: 'icon-wrench'
            },
            {
              name: 'Configuración',
              url: '/configuration',
              icon: 'icon-settings'
            },
          ]
        },
      ];
    }
  */

}
