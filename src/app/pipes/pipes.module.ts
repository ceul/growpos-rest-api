import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe } from '@angular/common';
import { MasterDetailPipe } from './master-detail.pipe';
import { ReversePipe } from './reverse.pipe';
import { VehicleTypePipe } from './vehicle-type.pipe';
import { ItemListPropertiesPipe } from './item-list-properties.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MasterDetailPipe, ReversePipe, VehicleTypePipe, ItemListPropertiesPipe],
  exports: [MasterDetailPipe, ReversePipe, VehicleTypePipe, ItemListPropertiesPipe],
  providers: [
    DatePipe,
    CurrencyPipe
  ]
})
export class PipeModule {

  static forRoot() {
    return {
      ngModule: PipeModule,
      providers: [],
    };
  }
} 