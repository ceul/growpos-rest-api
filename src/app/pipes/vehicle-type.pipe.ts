import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vehicleType'
})
export class VehicleTypePipe implements PipeTransform {

  transform(type: string, types: any[]): any {
    try {
      let transformType = types.find(item => item.id === type)
      return transformType.name
    } catch (e) {
      console.log('An error occurred on pipe vehicle type')
    }
  }
}
