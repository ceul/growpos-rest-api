import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'masterDetail'
})
export class MasterDetailPipe implements PipeTransform {

  public activeLang = environment.activeLang;

  constructor(private datePipe: DatePipe, 
    private translate: TranslateService,
    private currencyPipe: CurrencyPipe
  ) {
    this.translate.setDefaultLang(this.activeLang);
  }

  transform(element: string, fieldName: string, translateFields: string[] = [], datesFields: string[] = [], currencyFields: string[] = []): any {

    if (translateFields.length === 0 && datesFields.length === 0 && currencyFields.length === 0) {
      return element
    }

    if (translateFields.length > 0) {
      if (translateFields.findIndex(item => item === fieldName) !== -1) {
        return this.translate.instant(element)
      }
    }

    if (currencyFields.length > 0) {
      if (currencyFields.findIndex(item => item === fieldName) !== -1) {
        return this.currencyPipe.transform(element,'USD')
      }
    }

    if (datesFields.length > 0) {
      if (datesFields.findIndex(item => item === fieldName) !== -1) {
        return this.datePipe.transform(element, 'short')
      }
    }
    return element;
  }

}
