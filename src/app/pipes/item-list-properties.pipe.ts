import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'itemListProperties'
})
export class ItemListPropertiesPipe implements PipeTransform {

  transform(item: string, itemName: string): any {
    try {
      let index = itemName.indexOf('.')
      if (index !== -1) {
        return this.transform(item[itemName.substring(0, index)], itemName.slice(index+1))
      } else {
        return item[itemName]
      }
    } catch (e) {
      console.log('An error occurred on pipe item list properties')
    }
  }

  getName(item: string, itemName: string): any {

  }
}
