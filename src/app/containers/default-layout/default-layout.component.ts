import { Component, OnDestroy, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Nav, NavData } from '../../_nav';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { UserBusiness } from 'src/app/business/user/user.business';
import { Storage } from '@ionic/storage';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
import { Subscription } from 'rxjs';
import PeopleModel from 'src/app/models/people.model';
import { SocketService } from 'src/app/services/socket/socket.service';
import OrderModel from 'src/app/models/order.model';
import OrderStateEnum from 'src/app/models/order_state.enum';

import { filter } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})

export class DefaultLayoutComponent implements OnInit, OnDestroy {
  public navItems: NavData[]
  public navBarItems: NavData[]
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  private activeLang: string = environment.activeLang
  private subscriptions: Subscription
  public customer: string
  public discount: number
  public user: PeopleModel
  public table: string
  public faviconUrl: string
  public notifications: any[]
  public notificationsNumber: number
  public Ifoundit: boolean

  public breadcrumb: Array<NavData>

  constructor(private auth: AuthBusiness,
    private nav: Nav,
    private router: Router,
    private translate: TranslateService,
    private userBusiness: UserBusiness,
    private headerBodyService: HeaderBodyService,
    private socketService: SocketService,
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    @Inject(DOCUMENT) _document?: any) {
    this.translate.setDefaultLang(this.activeLang);
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
    this.breadcrumb = new Array<NavData>();
  }

  async ngOnInit() {

    // OJO Storage
    this.subscriptions = new Subscription()
    this.customer = ''
    this.discount = 0
    this.notificationsNumber = 0
    this.notifications = []
    this.faviconUrl = `${environment.url}:${environment.port}/resource/getCompanyLogo`
    this.user = await this.auth.readUser()
    this.table = ''
    let r = await this.storage.get('r')
    this.navItems = <NavData[]>await this.userBusiness.getMenu(r);
    this.navBarItems = <NavData[]>await this.userBusiness.getMenu(r, "navBar");

    this.subscriptions.add(this.headerBodyService.customer.subscribe(customer => {
      this.customer = customer
    }))
    this.subscriptions.add(this.headerBodyService.discount.subscribe(discount => {
      this.discount = discount
    }))
    this.subscriptions.add(this.headerBodyService.user.subscribe(user => {
      this.user = user
    }))
    this.subscriptions.add(this.headerBodyService.table.subscribe(table => {
      this.table = table
    }))
    this.subscriptions.add(this.socketService.listen('order').subscribe(newOrder => {
      let order = <OrderModel>newOrder
      let description = ''
      let route = ''
      if (order.state === OrderStateEnum.PROCESS) {
        description = `Nueva Orden`
        route = '/orders'
      } else if (order.state === OrderStateEnum.READY) {
        description = `Orden ${order.id} Lista`
        route = '/orders/list-ready'
      } else if (order.state === OrderStateEnum.ON_DELIVERY) {
        description = `Orden ${order.id} Despachada`
        route = '/orders/list-ready'
      }
      if (order.state !== OrderStateEnum.FINISH) {
        this.notificationsNumber++
        this.notifications.unshift({ description, route })
      }
    }))

    this.subscriptions.add(this.headerBodyService.navBar.subscribe(breadcrumb => {
      this.createBreadcrumbs();
    }))
  }

  private createBreadcrumbs() {
    this.breadcrumb = new Array<NavData>();
    this.Ifoundit = false;
    if (!isNullOrUndefined(this.navItems)) {
      this.findURL(this.router.url, this.navBarItems);
      this.breadcrumb.reverse()
    }
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
    this.subscriptions.unsubscribe();
  }

  notificationsOnClick() {
    try {
      this.notificationsNumber = 0
    } catch (error) {
      console.log(`An error occurred on notificationOnClick => DefaultLayoutComponent`)
    }
  }

  notificationOnClick(notification) {
    try {
      this.router.navigateByUrl(notification.route);
    } catch (error) {
      console.log(`An error occurred on notificationOnClick => DefaultLayoutComponent`)
    }
  }

  logout() {
    this.auth.logout();
  }

  dashboard() {
    this.router.navigateByUrl('/dashboard');
  }

  sales() {
    this.router.navigateByUrl('/sales/restaurant')
  }

  setDefaultPic() {
    try {
      this.faviconUrl = "../../../assets/img/brand/logo-grow.svg";
    } catch (error) {
      console.log(`An error occurred setDefaultPic => ConfigurationComponent `)
    }
  }

  private findURL(url: string, root: NavData[]): boolean {
    if (!isNullOrUndefined(root)) {
      for (let i = 0; i < root.length; i++) {
        if (this.Ifoundit === false) {
          if (url === root[i].url) {
            this.breadcrumb.push(root[i]);
            this.Ifoundit = true;
            return true;
          }
          else {
            if (!isNullOrUndefined(root[i].children) && root[i].children.length > 0) {
              let res = this.findURL(url, root[i].children);
              if (res) {
                this.breadcrumb.push(root[i]);
                break;
              }
            }
          }
        }
      }
    }
  }
}
