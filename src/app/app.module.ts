import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button'

//import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// Storage
import { IonicStorageModule } from '@ionic/storage';

//Translation
import { TranslateLoader, TranslateModule, TranslateCompiler } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

// Bluetooth
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

//import { AppComponent } from './app.component';

// Print
//import { PrintGenericComponent } from './modules/print-generic/print-generic.component';


const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { ToastMessagesComponent } from './components/toast-messages/toast-messages.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component'
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { RegisterComponent } from './components/register/register.component';
import { AppComponent } from './app.component';
import { ErrorsModule } from './modules/error-handler/errors.module';
import { AuthGuard } from './guards/auth.guard';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PipeModule } from './pipes/pipes.module';
import { FirstConfigComponent } from './components/first-config/first-config.component';
import { PrintLayoutComponent } from './modules/print/print-layout/print-layout.component';
import { InvoiceComponent } from './modules/print/invoice/invoice.component';
import { CustomerPaidComponent } from './modules/print/customer-paid/customer-paid.component';
import { LoadingSpinnerService } from './services/loading-spinner/loading-spinner.service';
import { ComponentsModule } from './components/components.module';
import { PreInvoiceComponent } from './modules/print/pre-invoice/pre-invoice.component';
import { CloseCashPrintComponent } from './modules/print/close-cash-print/close-cash-print.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ParkingIncomeComponent } from './modules/print/parking-income/parking-income.component';
import { SocketService } from './services/socket/socket.service';

@NgModule({
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    MenuComponent,
    FirstConfigComponent,
    RegisterComponent,
    ToastMessagesComponent,
    ConfirmDialogComponent,
    PrintLayoutComponent,
    InvoiceComponent,
    CustomerPaidComponent,
    PreInvoiceComponent,
    ParkingIncomeComponent,
    CloseCashPrintComponent
    //    PrintGenericComponent,
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    ErrorsModule,
    AlertModule.forRoot(),
    BrowserAnimationsModule,
    MatDialogModule, // <--- Aquí
    MatButtonModule, // <--- Aquí
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    PipeModule.forRoot(),
    ComponentsModule,
    TimepickerModule.forRoot()
  ],
  providers: [
    AuthGuard,
    BluetoothSerial,
    LoadingSpinnerService,
    SocketService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
