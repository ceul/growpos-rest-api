import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { FirstConfigComponent } from './components/first-config/first-config.component';
import { PrintLayoutComponent } from './modules/print/print-layout/print-layout.component';
import { InvoiceComponent } from './modules/print/invoice/invoice.component';
import { CustomerPaidComponent } from './modules/print/customer-paid/customer-paid.component';
import { PreInvoiceComponent } from './modules/print/pre-invoice/pre-invoice.component';
import { CloseCashPrintComponent } from './modules/print/close-cash-print/close-cash-print.component';
import { ParkingIncomeComponent } from './modules/print/parking-income/parking-income.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'print',
    outlet: 'print',
    component: PrintLayoutComponent,
    children: [
      {
        path: 'invoice',
        component: InvoiceComponent,
      },
      {
        path: 'customer-paid',
        component: CustomerPaidComponent,
      },
      {
        path: 'pre-invoice',
        component: PreInvoiceComponent,
      },
      {
        path: 'parking-income',
        component: ParkingIncomeComponent,
      },
      {
        path: 'close-cash',
        component: CloseCashPrintComponent,
      },
    ]
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'first-config',
    component: FirstConfigComponent,
    data: {
      title: 'Config Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate : [ AuthGuard ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'accounting',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/accounting/accounting.module#AccountingModule'
      },
      {
        path: 'base',
        canActivate : [ AuthGuard ],
        loadChildren: './views/base/base.module#BaseModule'
      },
      {
        path: 'sales',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/sales/sales.module#SalesModule'
      },
      {
        path: 'orders',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/orders/orders.module#OrdersModule'
      },
      {
        path: 'parking',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/parking/parking.module#ParkingModule'
      },
      {
        path: 'stock',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/stock/stock.module#StockModule'
      },
      {
        path: 'customer',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/customer/customer.module#CustomerModule'
      },
      {
        path: 'supplier',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/supplier/supplier.module#SupplierModule'
      },
      {
        path: 'close-cash',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/close-cash/close-cash.module#CloseCashModule'
      },
      {
        path: 'maintenance',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/maintenance/maintenance.module#MaintenanceModule'
      },
      {
        path: 'menu/:id',
        canActivate : [ AuthGuard ],
        component: MenuComponent,
        data: {
          title: 'Menu'
        }
      },
      {
        path: 'configuration',
        canActivate : [ AuthGuard ],
        loadChildren: './modules/configuration/configuration.module#ConfigurationModule'
      },
      {
        path: 'buttons',
        canActivate : [ AuthGuard ],
        loadChildren: './views/buttons/buttons.module#ButtonsModule'
      },
      {
        path: 'charts',
        canActivate : [ AuthGuard ],
        loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'dashboard',
        canActivate : [ AuthGuard ],
        loadChildren: './components/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'icons',
        canActivate : [ AuthGuard ],
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'notifications',
        canActivate : [ AuthGuard ],
        loadChildren: './views/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'theme',
        canActivate : [ AuthGuard ],
        loadChildren: './views/theme/theme.module#ThemeModule'
      },
      {
        path: 'widgets',
        canActivate : [ AuthGuard ],
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      }
    ]
    
  },
  { path: '**', component: P404Component }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
