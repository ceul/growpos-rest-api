import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PrintService } from './services/print-service/print.service';
import { SocketService } from './services/socket/socket.service';

@Component({
  selector: 'body',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  loading = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public printService: PrintService,
    private socketService: SocketService
  ) {
    this.initializeApp();
  }

  ngOnInit(){
    this.socketService.setupSocketConnection();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
