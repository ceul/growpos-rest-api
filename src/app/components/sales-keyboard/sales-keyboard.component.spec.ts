import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesKeyboardComponent } from './sales-keyboard.component';

describe('SalesKeyboardComponent', () => {
  let component: SalesKeyboardComponent;
  let fixture: ComponentFixture<SalesKeyboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesKeyboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesKeyboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
