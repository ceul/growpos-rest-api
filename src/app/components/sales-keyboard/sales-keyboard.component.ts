import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sales-keyboard',
  templateUrl: './sales-keyboard.component.html',
  styleUrls: ['./sales-keyboard.component.scss']
})
export class SalesKeyboardComponent implements OnInit {

  @Output() btnEqual: EventEmitter<null> = new EventEmitter();
  barcode: string = ""

  constructor() { }

  ngOnInit() {
  }

  numClicked(num: number) {
    try {
      console.log(num)
      this.barcode = this.barcode.concat(num.toString())
    } catch (e) {
      console.log(`An error occurred in ${this.numClicked.name}`)
    }
  }

  equalClicked() {
    try {
      this.btnEqual.emit()
    } catch (e) {
      console.log(`An error occurred in ${this.equalClicked.name}`)
    }
  }

}
