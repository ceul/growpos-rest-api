import { Component, OnInit } from '@angular/core';
import PeopleModel from '../../models/people.model';
import { NgForm } from '@angular/forms';
import { AuthBusiness } from '../../business/user/auth.business';
import { ToastService } from '../../services/base-services/toast.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import 'clientjs';
import { ResourceBusiness } from 'src/app/business/resource/resource.business';
import { Storage } from '@ionic/storage';
import { RoleBusiness } from 'src/app/business/role/role.business';
import { Nav } from 'src/app/_nav';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {
  people: PeopleModel;
  public activeLang = environment.activeLang;

  constructor(private authBusiness: AuthBusiness,
    private resourceBusiness: ResourceBusiness,
    private toast: ToastService,
    private router: Router,
    private loadingService: LoadingSpinnerService,
    private storage: Storage,
    private headerBodyService: HeaderBodyService,
    private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);
  }

  ngOnInit() {
    this.people = new PeopleModel();
    this.people.id = '';
    this.people.apppassword = '';
    this.people.card = '';
    this.people.image = '';
    this.people.name = '';
    this.people.role = '';
    this.people.visible = true;
    this.storage.clear()
  }

  async onSubmit(form: NgForm) {
    try {
      if (form.invalid) {
        return;
      }
      this.loadingService.show()
      let value = await this.authBusiness.val(this.people)
      if (value == null) {
        this.loadingService.hide()
        this.toast.showMessage('Al parecer no te encuentras registrado!!', 'success');
      } else {
        this.toast.showMessage('Disfruta!!', 'success');
        let fingerprint = this.getFingerPrint()
        let conf = Object.values(await this.resourceBusiness.getByHost(fingerprint))[0]
        this.storage.set('r', value['user'].id);
        this.headerBodyService.customerFunction(value['user'].name)
        if (conf == null) {
          this.router.navigateByUrl('/first-config');
          return
        }
        this.storage.set('config', conf['content']);
        this.router.navigateByUrl('/dashboard');
      }
    } catch (error) {
      this.loadingService.hide()
      this.toast.showMessage('Al parecer no te encuentras registrado!!', 'error');
    }
  }

  private getFingerPrint() {
    try {
      // @ts-ignore
      let client = new ClientJS()
      let ua = client.getBrowserData().ua;
      //let canvasPrint = client.getCanvasPrint();
      let os = client.getOS();
      let cpu = client.getCPU();
      let timeZone = client.getTimeZone();
      let lenguage = client.getLanguage();
      let engine = client.getEngine();
      let device = client.getDevice();
      let deviceType = client.getDeviceType();
      let deviceVendor = client.getDeviceVendor();
      let fingerprint = client.getCustomFingerprint(ua, os, cpu, timeZone, lenguage, engine, device, deviceType, deviceVendor);
      return fingerprint
    } catch (error) {
      console.log(`An error occurred on getFingerPrint => LoginComponent`)
    }
  }

}