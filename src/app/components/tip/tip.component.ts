import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import ProductModel from 'src/app/models/product.model';
import { PrintService } from 'src/app/services/print-service/print.service';

@Component({
  selector: 'growpos-tip',
  templateUrl: './tip.component.html',
  styleUrls: ['./tip.component.scss'],
})
export class TipComponent implements OnInit {

  @Input() subTotal: number
  @Output() tip: EventEmitter<ProductModel> = new EventEmitter();
  public tipAmount: number
  public total: number
  private tipProduct: ProductModel

  constructor(public tipModal: BsModalRef,
    private modalService: BsModalService,
    public printService: PrintService,
    private loadingService: LoadingSpinnerService) { }

  ngOnInit() {
    try {
      this.tipAmount = 0
      this.total = this.subTotal
      this.tipProduct = new ProductModel()
      this.tipProduct.id = 'xxx998_998xxx_x8x8x8'
      this.tipProduct.reference = 'xxx998'
      this.tipProduct.code ='xxx998'
      this.tipProduct.name = 'Propina'
      this.tipProduct.category = '000'
      this.tipProduct.taxcat = '000'
      this.tipProduct.isservice = true
    } catch (e) {
      console.log('An error occurred on ngOnInit -> TipComponent')
    }
  }

  calculateTotal(rate?: number) {
    try {
      if (rate > 0) {
        this.tipAmount = this.subTotal * rate
      }
      this.total = this.subTotal + this.tipAmount
    } catch (e) {
      console.log('An error occurred on calculateTotal -> TipComponent')
    }
  }

  pay() {
    try {
      this.tipProduct.pricesell = this.tipAmount
      this.tip.emit(this.tipProduct)
    } catch (e) {
      console.log('An error occurred on pay -> TipComponent')
    }
  }

}
