import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { ToastService } from 'src/app/services/base-services/toast.service';
import { CustomerBusiness } from 'src/app/business/customer/customer.business';
import CustomerModel from 'src/app/models/customer.model';
import TaxCategoryModel from 'src/app/models/tax-category.model';
@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {

  customer: CustomerModel
  //taxCategories: TaxCategoryModel[] = []

  customerForm: FormGroup;

  @Output() createdCustomer: EventEmitter<any> = new EventEmitter();

  submit: boolean = false

  constructor(public createCustomer: BsModalRef,
    private customerBusiness: CustomerBusiness,
    private toast: ToastService) {
    this.customerForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'searchKey': new FormControl('', Validators.required),
     // 'taxcategory': new FormControl(''),
      'maxdebt': new FormControl(0, Validators.pattern("^[0-9]*$")),
      'isvip': new FormControl(false),
      'discount': new FormControl(0, [Validators.pattern("^[0-9]+$")]),
      'firstName': new FormControl(''),
      'lastName': new FormControl(''),
      'email': new FormControl('', [Validators.email]),
      'phone': new FormControl(''),
      'phone2': new FormControl(''),
      'id_third_party_type': new FormControl('1')
    })
    
  }

  ngOnInit() {
    this.customer = new CustomerModel()
  }

  async saveCustomer() {
    console.log(this.customerForm.valid)
    if (this.customerForm.valid) {
      this.customer = this.customerForm.value
      let response = await this.customerBusiness.save(this.customer)
      this.createdCustomer.emit(response)
      this.toast.showMessage('El cliente ha sido añadido con exito!!', 'success')
      this.submit = false
      this.customerForm.reset(
        {
          name: "",
          keySearch: "",
          accountId: "",
          //taxCat: "",
          creditLimit: 0,
          vip: false,
          discount: 0,
          firstName: "",
          lastName: "",
          email: "",
          telephone: "",
          cellPhone: "",
          id_third_party_type: "1",
        })

      this.createCustomer.hide()
    } else {
      this.submit = true
    }
  }

}
