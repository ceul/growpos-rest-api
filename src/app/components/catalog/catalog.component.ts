import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {

  className = 'CatalogComponent'

  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;

  @Input() items: any[];

  @Input() listHeight: string;

  @Input() itemName: string;

  @Input() itemImage: string;

  @Input() haveFilter: boolean;

  @Output() onClick: EventEmitter<null> = new EventEmitter();

  public itemsTmp: any[]
  public imgUrl: string

  constructor() { }

  ngOnInit() {
    this.imgUrl = `${environment.url}:${environment.port}`
  }

  ngOnChanges(changes) {
    try {
      if (changes.items) {

        this.items = changes.items.currentValue
        this.itemsTmp = this.items
      }
    } catch (e) {
      console.log('An error occurred changing the catalog list e', e)
    }
  }

  clickItem(item) {
    try {
      this.onClick.emit(item);
    } catch (e) {
      console.log('An error ocurred when clicked the item')
    }
  }

  onSearch($event) {
    this.itemsTmp = $event.items
  }

}
