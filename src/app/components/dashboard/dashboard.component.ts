import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { PlaceBusiness } from '../../business/place/place.business';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';

import { CategoryBusiness } from '../../business/category/category.business';
import { ProductBusiness } from 'src/app/business/product/product.business';
import { TicketBusiness } from 'src/app/business/ticket/ticket.business';
import ProductModel from 'src/app/models/product.model';
import any from 'src/app/models/ticket.model';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import ConfigModeEnum from 'src/app/models/config-mode.enum';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  places: any;
  catSales: any;
  restaurantMode: number;
  hostConfig: HostConfigModel;
  configModes

  public productBestSelling: any;
  public withLowerInventory: any;
  public totalSales: any;
  profit: any;
  numTicket: any;
  dailySales: any[];

  public activeLang = environment.activeLang;
  radioModel: string = 'Day';


  // mainChart

  public mainChartElements = 24;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Facturas'
    }/*,
    {
      data: this.mainChartData2,
      label: 'total'
    },
    {
      data: this.mainChartData3,
      label: 'units'
    }*/
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips,
      intersect: true,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        labelColor: function (tooltipItem, chart) {
          return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
      }],
      yAxes: []
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';


  public constructor(private placesBussines: PlaceBusiness,
    private translate: TranslateService,
    private categoryBusiness: CategoryBusiness,
    private ProductBusiness: ProductBusiness,
    private ticketBusiness: TicketBusiness,
    private loadingService: LoadingSpinnerService,
    private storage: Storage
  ) {
    this.translate.setDefaultLang(this.activeLang);
  }

  async ngOnInit() {
    try {
      let promises = []
      this.loadingService.show()
      this.configModes = ConfigModeEnum
      this.totalSales = new any;
      this.profit = new any;
      this.numTicket = new any;
      this.dailySales = [];
      this.productBestSelling = new ProductModel();
      this.withLowerInventory = new ProductModel();
      this.hostConfig = await this.storage.get('config')
      this.restaurantMode = this.hostConfig.configMode

      promises.push(this.placesBussines.get().then(data => {
        if (Object.values(data).length > 0) {
          this.places = Object.values(data);
        }
      }));

      promises.push(this.categoryBusiness.getBySales().then(res => {
        if (Object.values(res).length > 0) {
          this.catSales = Object.values(res)
        }
      }));

      // Statistics

      promises.push(this.ticketBusiness.getTotalSales().then(res => {
        if (Object.values(res).length > 0) {
          this.totalSales = Object.values(res)[0];
        }
      }));

      promises.push(this.ticketBusiness.getProfit().then(res => {
        if (Object.values(res).length > 0) {
          this.profit = Object.values(res)[0];
        }
      }));

      promises.push(this.ticketBusiness.getNumTicket().then(res => {
        if (Object.values(res).length > 0) {
          this.numTicket = Object.values(res)[0];
        }
      }));

      promises.push(this.ProductBusiness.getProductBestSelling().then(res => {
        if (Object.values(res).length > 0) {
          this.productBestSelling = Object.values(res)[0];
        }
      }));

      promises.push(this.ProductBusiness.getWithLowerInventory().then(res => {
        if (Object.values(res).length > 0) {
          this.withLowerInventory = Object.values(res)[0];
        }
      }));


      promises.push(this.ticketBusiness.getDailySales().then(res => {
        let max = 1
        this.dailySales = Object.values(res);
        for (let i = 0; i < 24; i++) {
          this.mainChartData1[i] = 0;
        }
        this.dailySales.forEach(sale => {
          if (sale.receipts > max) {
            max = sale.receipts
          }
          this.mainChartData1[sale.hour] = sale.receipts
        })
        this.mainChartOptions.scales.yAxes[0] = {
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 10,
            stepSize: Math.ceil(max / 5),
            max: max
          }
        }


        this.mainChartOptions.scales.xAxes[0] = {

          ticks: {
            beginAtZero: true,
            maxTicksLimit: 24,
            stepSize: 1,
            max: 23,
            min: 0
          }
        }
        this.mainChartOptions = Object.assign(this.mainChartOptions, this.mainChartOptions)
        this.mainChartData = this.mainChartData.slice()
      }));
      Promise.all(promises).then(()=> this.loadingService.hide())
    } catch (error) {
      console.log(`An error occuerred on ngoninit`)

    }
  }

  logout() {
  }
}
