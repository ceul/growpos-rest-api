import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-find-supplier',
  templateUrl: './find-supplier.component.html',
  styleUrls: ['./find-supplier.component.scss']
})
export class FindSupplierComponent implements OnInit {

  emit: boolean = false
  suppliers: {}[];

  constructor(public findSupplier: BsModalRef) { }

  ngOnInit() {
    this.suppliers = []
  }

  selectSupplier(supplier) {
    try {
      console.log(supplier)
    } catch (e) {
      console.log('An error ocurred selecting a supplier')
    }
  }

  execute() {
    try {
      for (let t = 0; t < 10; t++) {
        this.suppliers.push({
          id: `${t}`,
          name: `Proveedor ${t}`,
          img: 'https://www.halliecrawford.com/wp-content/uploads/2015/05/contrast-list.png',
        })
      }
    } catch (e) {
      console.log('An error occurred executting the form')
    }
  }

  reset() {
    try {
      this.suppliers = []
    } catch (e) {
      console.log('An error occurred resetting the form')
    }
  }

}
