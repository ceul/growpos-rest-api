import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import VehicleModel from 'src/app/models/vehicle.model';
import { VehicleBusiness } from 'src/app/business/vehicle/vehicle.business';

@Component({
  selector: 'growpos-find-vehicle',
  templateUrl: './find-vehicle.component.html',
  styleUrls: ['./find-vehicle.component.scss'],
})
export class FindVehicleComponent implements OnInit {

  @Output() public selectedVehicle: EventEmitter<VehicleModel> = new EventEmitter();

  emit: boolean = false;
  vehicles: VehicleModel[];
  filterForm: FormGroup;
  vehicle: VehicleModel

  constructor(public findVehicleModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private vehicleBusiness: VehicleBusiness,
    private toast: ToastService) {
    this.filterForm = new FormGroup({
      'plate': new FormControl(''),
    })
  }

  ngOnInit() {
    try{
      this.vehicles = []      
    } catch(error){
      console.log(`An error occurred on ngOnInit`)
    }
  }

  okBtn() {
    try {
      this.selectedVehicle.emit(this.vehicle)
      this.findVehicleModal.hide()
    } catch (error) {
      console.log('An error occurred in ok button')
    }
  }

  selectVehicle(vehicle) {
    try {
      this.vehicle = vehicle
    } catch (e) {
      console.log('An error ocurred selecting a vehicle')
    }
  }

  reset() {
    try {
      this.vehicles = []
    } catch (e) {
      console.log('An error occurred resetting the form')
    }
  }

  clearForm() {
    try {
      let form = {
        'plate': '',
      }
      this.filterForm.patchValue(form)
    } catch (error) {
      console.log('An error occurred clearing the form')
    }
  }

  async getVehicles() {
    try {
      this.loadingService.show()
      let vehicles
      if (this.filterForm.controls['plate'].value !== '') {
        vehicles = await this.vehicleBusiness.getByPlate(this.filterForm.controls['plate'].value)
      } else{
        vehicles = await this.vehicleBusiness.get()
      }
      this.vehicles = Object.values(vehicles)
      this.loadingService.hide()
    } catch (error) {
      console.log('An error occurred getting the vehicles: ', error)
    }
  }

  onKeypress(event) {
    try {
      if (typeof this.filterForm.controls['plate'] !== 'undefined') {
        if (this.filterForm.controls['plate'].value.length === 3) {
          this.filterForm.controls['plate'].setValue(this.filterForm.controls['plate'].value.toUpperCase() + "-");
        }
      }
    } catch (error) {
      console.log(`An error occurred filtering the plate`)
    }
  }

}
