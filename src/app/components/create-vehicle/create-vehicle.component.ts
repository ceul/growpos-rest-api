import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import VehicleModel from 'src/app/models/vehicle.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { VehicleBusiness } from 'src/app/business/vehicle/vehicle.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ParkingFeeBusiness } from 'src/app/business/parking-fee/parking-fee.business';

@Component({
  selector: 'growpos-create-vehicle',
  templateUrl: './create-vehicle.component.html',
  styleUrls: ['./create-vehicle.component.scss'],
})
export class CreateVehicleComponent implements OnInit {

  vehicle: VehicleModel
  parkingFees: any

  vehicleForm: FormGroup;

  @Output() createdVehicle: EventEmitter<any> = new EventEmitter();

  submit: boolean = false

  constructor(public createVehicle: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private vehicleBusiness: VehicleBusiness,
    private parkingFeeBusiness: ParkingFeeBusiness,
    private toast: ToastService) {
    this.vehicleForm = new FormGroup({
      'plate': new FormControl('', Validators.required),
      'type': new FormControl('', Validators.required),
      'model': new FormControl(''),
      'color': new FormControl(''),
      'description': new FormControl(''),
    })

  }

  ngOnInit() {
    try {
      this.loadingService.show()
      let promises = []
      this.vehicle = new VehicleModel()
      promises.push(this.parkingFeeBusiness.getGroupByType().then(parkingFees => {
        this.parkingFees = Object.values(parkingFees)
      }))
      Promise.all(promises).then(() => this.loadingService.hide())
    } catch (error) {
      console.log(`An error occurred on ngOnInit => CreateVehicleComponent`)
    }
  }

  async saveVehicle() {
    try {
      if (this.vehicleForm.valid) {
        this.vehicle = this.vehicleForm.value
        let response = await this.vehicleBusiness.save(this.vehicle)
        this.createdVehicle.emit(response)
        this.toast.showMessage('El vehiculo ha sido añadido con exito!!', 'success')
        this.submit = false
        this.vehicleForm.reset(
          {
            id: "",
            plate: "",
            type: "",
            model: "",
            color: "",
            description: "",
          })

        this.createVehicle.hide()
      } else {
        this.submit = true
      }
    } catch (error) {
      console.log(`An error occurred on saveVehicle => CreateVehicleComponent`)
    }
  }

  onKeypress(event) {
    try {
      if (typeof this.vehicleForm.controls['plate'] !== 'undefined') {
        if (this.vehicleForm.controls['plate'].value.length === 3) {
          this.vehicleForm.controls['plate'].setValue(this.vehicleForm.controls['plate'].value.toUpperCase() + "-");
        }
      }
    } catch (e) {
      console.log(`An error occurred on key press => CreateVehicleComponent`)
    }
  }

}
