import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { ResourceBusiness } from 'src/app/business/resource/resource.business';
import ResourceModel from 'src/app/models/resource.model';
import HostConfigModel from 'src/app/models/host-config.model';
import 'clientjs';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import ConfigModeEnum from 'src/app/models/config-mode.enum';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
@Component({
  selector: 'growpos-first-config',
  templateUrl: './first-config.component.html',
  styleUrls: ['./first-config.component.scss'],
})
export class FirstConfigComponent implements OnInit {

  configForm: FormGroup;
  submit: boolean;
  newResource: ResourceModel
  configModel: HostConfigModel
  public modes
  public modesKeys

  constructor(private toast: ToastService,
    private resourceBusiness: ResourceBusiness,
    private loadingService: LoadingSpinnerService,
    private router: Router,
    private storage: Storage) {
    this.configForm = new FormGroup({
      'hostName': new FormControl('',Validators.required),
      'configMode': new FormControl('',Validators.required),
    })
  }

  ngOnInit() {
    this.loadingService.show()
    this.modes = ConfigModeEnum
    this.modesKeys = Object.keys(this.modes).filter(Number)
    this.submit = false;
    let fingerprint = this.getFingerPrint()
    this.newResource = new ResourceModel()
    this.newResource.name = fingerprint
    this.configModel = new HostConfigModel()
    this.loadingService.hide()
  }

  submitForm(){
    try{
      if(this.configForm.valid){
        this.configModel.hostName = this.configForm.controls['hostName'].value
        this.configModel.configMode = this.configForm.controls['configMode'].value
        this.newResource.content = JSON.parse(JSON.stringify(this.configModel))
        this.resourceBusiness.save(this.newResource).then(value => {
          this.storage.set('config', value['content']);
          this.router.navigateByUrl('/dashboard');
        })
      }else{
        this.submit = true
      }
    } catch (error){
      console.log('An error ocurred in submitForm => first-config')
    }
  }

  private getFingerPrint() {
    try {
      // @ts-ignore
      let client = new ClientJS()
      let ua = client.getBrowserData().ua;
      //let canvasPrint = client.getCanvasPrint();
      let os = client.getOS();
      let cpu = client.getCPU();
      let timeZone = client.getTimeZone();
      let lenguage = client.getLanguage();
      let engine = client.getEngine();
      let device = client.getDevice();
      let deviceType = client.getDeviceType();
      let deviceVendor = client.getDeviceVendor();
      let fingerprint = client.getCustomFingerprint(ua, os, cpu, timeZone, lenguage, engine, device, deviceType, deviceVendor);
      return fingerprint
    } catch (error) {
      console.log(`An error occurred on getFingerPrint => LoginComponent`)
    }
  }

}
