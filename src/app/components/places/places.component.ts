import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import FloorModel from 'src/app/models/floor.model';
import PeopleModel from 'src/app/models/people.model';
import { AuthBusiness } from 'src/app/business/user/auth.business';
import { FloorBusiness } from 'src/app/business/floor/floor.business';
import PlaceModel from 'src/app/models/place.model';
import { MoveTableService } from 'src/app/services/move-table/move-table.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { take } from 'rxjs/operators';
import { rejects } from 'assert';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.scss']
})
export class PlacesComponent implements OnInit {

  public tableMove: PlaceModel
  public activeLang = environment.activeLang;

  floors: FloorModel[]
  user: PeopleModel
  type: any;

  constructor(private _router: Router,
    private floorBusiness: FloorBusiness,
    private moveTableService: MoveTableService,
    private loadingService: LoadingSpinnerService,
    public translate: TranslateService,
    public authBusiness: AuthBusiness,
    public activatedRoute: ActivatedRoute) {
    this.translate.setDefaultLang(this.activeLang);
  }

  async ngOnInit() {
    let promise = []
    this.loadingService.show()
    this.type =  await new Promise((resolve, reject) => {
      this.activatedRoute.params.pipe(take(1)).subscribe(params => {   
          
          let type:string = params['type']
          resolve (type) 
        } )
    });
    this.user = await this.authBusiness.readUser()
    promise.push(this.getPlaces())
    this.tableMove = this.moveTableService.getTable()
    Promise.all(promise).then(() => this.loadingService.hide())
  }

  getPlaces(): Promise<any> {
    try {
      this.loadingService.show()
      return this.floorBusiness.getWithPlace(this.type).then(floors => {
        this.floors = Object.values(floors)
        this.loadingService.hide()
      })
    } catch (error) {
      console.log('An error occurred getting places')
    }
  }

  getTime(time: string) {
    try {
      let tableTime = new Date(time).getTime()
      let date = new Date()
      let hoursDiff = date.getHours() - date.getTimezoneOffset() / 60;
      date.setHours(hoursDiff);
      let res = (date.getTime() - tableTime);
      var diffDays = Math.floor(res / 86400000); // days
      var diffHrs = Math.floor((res % 86400000) / 3600000); // hours
      var diffMins = Math.round(((res % 86400000) % 3600000) / 60000); // minutes
      return (diffDays + "d:" + diffHrs + "h:" + diffMins + "m");
    } catch (error) {
      console.log(`An error occurred getting time`)
    }
  }

  navigateSales(id) {
    try {
      if (this.type === 'p'){
        this._router.navigate(['/parking/mov'])
      }
      else{
        this._router.navigate(['/sales/table', id])
      }
      
    } catch (e) {
      console.log('An error ocurred navigating to the sales panel')
    }
  }

}
