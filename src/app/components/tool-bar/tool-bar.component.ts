import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { ToolBarItemListService } from '../../services/tool-bar-item-list/tool-bar-item-list.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss']
})
export class ToolBarComponent implements OnInit, OnDestroy {

  angleLeftSubscribe: Subscription
  leftSubscribe: Subscription
  downSubscribe: Subscription
  rightSubscribe: Subscription
  angleRightSubscribe: Subscription
  itemSelectedSubscribe: Subscription

  @Input() add: boolean
  @Input() trash: boolean
  @Input() save: boolean
  @Input() edit: boolean

  @Output() btnAdd: EventEmitter<null> = new EventEmitter();
  @Output() btnTrash: EventEmitter<null> = new EventEmitter();
  @Output() btnSave: EventEmitter<null> = new EventEmitter();

  @ViewChild('confirmDeletion') public confirmDeletion: ModalDirective;

  angleLeft: boolean;
  left: boolean;
  down: boolean;
  right: boolean;
  angleRight: boolean;

  itemSelected: string

  constructor(private toolBarItemListService: ToolBarItemListService) {
    this.edit = false
  }

  ngOnInit() {

    this.angleLeftSubscribe = this.toolBarItemListService.angleLeft.subscribe((state) => {
      this.angleLeft = state
    })

    this.leftSubscribe = this.toolBarItemListService.left.subscribe((state) => {
      this.left = state
    })

    this.downSubscribe = this.toolBarItemListService.down.subscribe((state) => {
      this.down = state
    })

    this.rightSubscribe = this.toolBarItemListService.right.subscribe((state) => {
      this.right = state
    })

    this.angleRightSubscribe = this.toolBarItemListService.angleRight.subscribe((state) => {
      this.angleRight = state
    })

    this.itemSelectedSubscribe = this.toolBarItemListService.itemSelected.subscribe((item) => {
      this.itemSelected = item
    })
  }

  ngOnDestroy() {
    this.angleLeftSubscribe.unsubscribe()
    this.leftSubscribe.unsubscribe()
    this.downSubscribe.unsubscribe()
    this.rightSubscribe.unsubscribe()
    this.angleRightSubscribe.unsubscribe()
    this.itemSelectedSubscribe.unsubscribe()
  }

  btnAngleLeftOnClick() {
    try {
      this.toolBarItemListService.btnAngleLeftOnClick()
    } catch (e) {
      console.log('An error occured clicking button AngleLeft')
    }
  }

  btnLeftOnClick() {
    try {
      this.toolBarItemListService.btnLeftOnClick()
    } catch (e) {
      console.log('An error occured clicking button Left')
    }
  }

  btnDownOnClick() {
    try {
      this.toolBarItemListService.btnDownOnClick()
    } catch (e) {
      console.log('An error occured clicking button Down')
    }
  }

  btnRightOnClick() {
    try {
      this.toolBarItemListService.btnRightOnClick()
    } catch (e) {
      console.log('An error occured clicking button Right')
    }
  }

  btnAngleRightOnClick() {
    try {
      this.toolBarItemListService.btnAngleRightOnClick()
    } catch (e) {
      console.log('An error occured clicking button AngleRight')
    }
  }

  btnRepeatOnClick() {
    try {
      this.toolBarItemListService.btnRepeatOnClick()
    } catch (e) {
      console.log('An error occured clicking button Repeat')
    }
  }

  btnSearchOnClick() {
    try {
      this.toolBarItemListService.btnSearchOnClick()
    } catch (e) {
      console.log('An error occured clicking button Search')
    }
  }

  btnSortOnClick() {
    try {
      this.toolBarItemListService.btnSortOnClick()
    } catch (e) {
      console.log('An error occured clicking button Sort')
    }
  }

  btnAddOnClick() {
    try {
      this.btnAdd.emit()
    } catch (e) {
      console.log('An error occured clicking button Add')
    }
  }

  btnTrashOnClick() {
    try {
      this.confirmDeletion.hide()
      this.btnTrash.emit()
    } catch (e) {
      console.log('An error occured clicking button Trash')
    }
  }

  btnSaveOnClick() {
    try {
      this.btnSave.emit()
    } catch (e) {
      console.log('An error occured clicking button Save')
    }
  }

}
