import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import SupplierModel from 'src/app/models/supplier.model';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { SupplierBusiness } from 'src/app/business/supplier/supplier.business';

@Component({
  selector: 'app-create-supplier',
  templateUrl: './create-supplier.component.html',
  styleUrls: ['./create-supplier.component.scss']
})
export class CreateSupplierComponent implements OnInit {

  supplier: SupplierModel
  supplierForm: FormGroup;
  submit: boolean = false

  @Output() createdSupplier: EventEmitter<any> = new EventEmitter();

  constructor(public createSupplier: BsModalRef,
    private supplierBusiness: SupplierBusiness,
    private toast: ToastService) {
    this.supplierForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'searchkey': new FormControl('', Validators.required),
      'taxid': new FormControl(''),
      'firstname': new FormControl(''),
      'lastname': new FormControl(''),
      'email': new FormControl('', [Validators.email]),
      'phone': new FormControl('')
    })
  }

  ngOnInit() {
    this.supplier = new SupplierModel()
  }

  async saveSupplier() {
    if (this.supplierForm.valid) {
      this.supplier = this.supplierForm.value
      let response = await this.supplierBusiness.save(this.supplier)
      this.createdSupplier.emit(response)
      this.toast.showMessage('El proveedor ha sido añadido con exito!!', 'success')
      this.submit = false
      this.supplierForm.reset(
        {
          name: "",
          keySearch: "",
          taxid: "",
          taxCat: "",
          creditLimit: 0,
          vip: false,
          discount: 0,
          firstName: "",
          lastName: "",
          email: "",
          telephone: "",
          cellPhone: ""
        }
      )

      this.createSupplier.hide()
    } else {
      this.submit = true
    }
  }
}
