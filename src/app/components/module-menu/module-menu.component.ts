import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-module-menu',
  templateUrl: './module-menu.component.html',
  styleUrls: ['./module-menu.component.scss']
})
export class ModuleMenuComponent implements OnInit {
  
  @Input() module:{
    name: String,
    manage: String,
    reports: String,
    manageItems: [{}],
    reportsItems: [{}]
  };

  public activeLang = environment.activeLang;
  
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);
  }

  ngOnInit() {
  }

}
