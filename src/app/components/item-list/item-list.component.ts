import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild, SimpleChange, OnDestroy } from '@angular/core';
import { ToolBarItemListService } from '../../services/tool-bar-item-list/tool-bar-item-list.service';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, OnDestroy {

  className = 'ItemListComponent'

  btnAngleLeftSubscribe: Subscription
  btnLeftSubscribe: Subscription
  btnDownSubscribe: Subscription
  btnRightSubscribe: Subscription
  btnAngleRightSubscribe: Subscription
  btnRepeatSubscribe: Subscription
  btnSearchSubscribe: Subscription
  btnSortSubscribe: Subscription

  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;

  @Input() items: any[];

  @Input() listHeight: string;

  @Input() itemName: string;

  @Input() itemImage: string;

  @Input() newItem: boolean;

  @Input() emit: boolean = true;

  @Input() filterFields: any[];

  @Input() haveFilter: boolean;

  @Output() onClick: EventEmitter<null> = new EventEmitter();

  selectedItem: any;

  public itemsTmp: any[]

  constructor(private toolBarItemListService: ToolBarItemListService) { }

  ngOnInit() {
    try {
      if (this.emit) {
        if (this.items) {
          this.itemsTmp = this.items
          this.toolBarItemListService.itemSelectedFunction(`*/${this.itemsTmp.length}`)
          if (this.itemsTmp.length < 1) {
            this.toolBarItemListService.angleRightState(true)
            this.toolBarItemListService.rightState(true)
            this.toolBarItemListService.downState(true)
            this.toolBarItemListService.leftState(true)
            this.toolBarItemListService.angleLeftState(true)
          } else {
            this.toolBarItemListService.angleRightState(true)
            this.toolBarItemListService.rightState(true)
            this.toolBarItemListService.downState(false)
            this.toolBarItemListService.leftState(true)
            this.toolBarItemListService.angleLeftState(true)
          }
        } else {
          this.toolBarItemListService.angleRightState(true)
          this.toolBarItemListService.rightState(true)
          this.toolBarItemListService.downState(false)
          this.toolBarItemListService.leftState(true)
          this.toolBarItemListService.angleLeftState(true)
        }
      }

      this.btnAngleLeftSubscribe = this.toolBarItemListService.btnAngleLeft.subscribe(() => {
        this.btnAngleLeft()
      })
      this.btnLeftSubscribe = this.toolBarItemListService.btnLeft.subscribe(() => {
        this.btnLeft()
      })
      this.btnDownSubscribe = this.toolBarItemListService.btnDown.subscribe(() => {
        this.btnDown()
      })
      this.btnRightSubscribe = this.toolBarItemListService.btnRight.subscribe(() => {
        this.btnRight()
      })
      this.btnAngleRightSubscribe = this.toolBarItemListService.btnAngleRight.subscribe(() => {
        this.btnAngleRight()
      })
      this.btnRepeatSubscribe = this.toolBarItemListService.btnRepeat.subscribe(() => {
        this.btnRepeat()
      })
      this.btnSearchSubscribe = this.toolBarItemListService.btnSearch.subscribe(() => {
        this.btnSearch()
      })
      this.btnSortSubscribe = this.toolBarItemListService.btnSort.subscribe(() => {
        this.btnSort()
      })
    } catch{
      console.log(`An error occurred on ngOnInit`)
    }
  }

  ngOnDestroy() {
    this.btnAngleLeftSubscribe.unsubscribe()
    this.btnLeftSubscribe.unsubscribe()
    this.btnDownSubscribe.unsubscribe()
    this.btnRightSubscribe.unsubscribe()
    this.btnAngleRightSubscribe.unsubscribe()
    this.btnRepeatSubscribe.unsubscribe()
    this.btnSearchSubscribe.unsubscribe()
    this.btnSortSubscribe.unsubscribe()
  }

  ngOnChanges(changes) {
    try {
      if (changes.filterFields) {
        if (changes.filterFields.currentValue) {
          this.filterFields = changes.filterFields.currentValue
        }
      }
      if (changes.newItem) {
        if (changes.newItem.currentValue) {
          this.itemSelected(null)
        }
      }
      if (changes.items) {
        this.items = changes.items.currentValue
        this.itemsTmp = this.items
        if (this.itemsTmp && this.emit) {
          this.toolBarItemListService.itemSelectedFunction(`*/${this.itemsTmp.length}`)
          if (this.itemsTmp.length < 1) {
            this.toolBarItemListService.angleRightState(true)
            this.toolBarItemListService.rightState(true)
            this.toolBarItemListService.downState(true)
            this.toolBarItemListService.leftState(true)
            this.toolBarItemListService.angleLeftState(true)
          } else {
            this.toolBarItemListService.angleRightState(true)
            this.toolBarItemListService.rightState(true)
            this.toolBarItemListService.downState(false)
            this.toolBarItemListService.leftState(true)
            this.toolBarItemListService.angleLeftState(true)
          }
        } else {
          this.toolBarItemListService.angleRightState(true)
          this.toolBarItemListService.rightState(true)
          this.toolBarItemListService.downState(false)
          this.toolBarItemListService.leftState(true)
          this.toolBarItemListService.angleLeftState(true)
        }

      }
    } catch (e) {
      console.log('An error occurred changing the list e', e)
    }
  }

  customSearchFn(term: string, item) {
    try {
      let flag = false
      this.filterFields.forEach(elem => {
        if (item[elem.val].toLowerCase().indexOf(term) > -1) {
          flag = true
          return true
        }

      })
      return flag
    } catch (error) {
      console.log('An error occurred filtering the items')
    }
  }

  onSearch($event) {
    this.itemsTmp = $event.items
  }

  arrowsState() {
    try {
      let index = this.itemsTmp.indexOf(this.selectedItem)
      if (index === 0) {
        this.toolBarItemListService.angleRightState(false)
        this.toolBarItemListService.rightState(false)
        this.toolBarItemListService.downState(false)
        this.toolBarItemListService.leftState(true)
        this.toolBarItemListService.angleLeftState(true)
      } else if (index > 0 && index < this.itemsTmp.length - 1) {
        this.toolBarItemListService.angleRightState(false)
        this.toolBarItemListService.rightState(false)
        this.toolBarItemListService.downState(false)
        this.toolBarItemListService.leftState(false)
        this.toolBarItemListService.angleLeftState(false)
      } else {
        this.toolBarItemListService.angleRightState(true)
        this.toolBarItemListService.rightState(true)
        this.toolBarItemListService.downState(false)
        this.toolBarItemListService.leftState(false)
        this.toolBarItemListService.angleLeftState(false)
      }
    } catch (e) {
      console.log('An error occurred selecting the arrows state')
    }
  }

  btnAngleLeft() {
    try {
      this.selectedItem = this.itemsTmp[0]
      this.itemSelected(this.selectedItem)
    } catch (e) {
      console.log(`An error occurred on ${this.btnAngleLeft.name}`)
    }
  }

  btnLeft() {
    try {
      let index = this.itemsTmp.indexOf(this.selectedItem)
      if (index > 0) {
        this.selectedItem = this.itemsTmp[index - 1]
        this.itemSelected(this.selectedItem)
      }
    } catch (e) {
      console.log(`An error occurred on ${this.btnLeft.name}`)
    }
  }

  btnDown() {
    try {
      this.selectedItem = this.itemsTmp[0]
      this.itemSelected(this.selectedItem)
    } catch (e) {
      console.log(`An error occurred on ${this.btnDown.name}`)
    }
  }

  btnRight() {
    try {
      let index = this.itemsTmp.indexOf(this.selectedItem)
      if (index < this.itemsTmp.length - 1) {
        this.selectedItem = this.itemsTmp[index + 1]
        this.itemSelected(this.selectedItem)
      }
    } catch (e) {
      console.log(`An error occurred on ${this.btnRight.name}`)
    }
  }

  btnAngleRight() {
    try {
      this.selectedItem = this.itemsTmp[this.itemsTmp.length - 1]
      this.itemSelected(this.selectedItem)
    } catch (e) {
      console.log(`An error occurred on ${this.btnAngleRight.name}`)
    }
  }

  btnRepeat() {
    try {
      this.selectedItem = this.itemsTmp[0]
      this.itemSelected(this.selectedItem)
    } catch (e) {
      console.log(`An error occurred on ${this.btnRepeat.name}`)
    }
  }

  btnSearch() {
    try {

    } catch (e) {
      console.log(`An error occurred on ${this.btnSearch.name}`)
    }
  }

  btnSort() {
    try {

    } catch (e) {
      console.log(`An error occurred on ${this.btnSort.name}`)
    }
  }


  clickItem(item) {
    try {
      this.selectedItem = item;
      this.itemSelected(item)
    } catch (e) {
      console.log('An error ocurred when clicked the item')
    }
  }

  itemSelected(item) {
    try {
      if (this.emit) {
        if (item === null) {
          this.toolBarItemListService.itemSelectedFunction(`*/${this.itemsTmp.length}`)
        } else {
          let index = this.itemsTmp.indexOf(item) + 1
          this.viewPort.scrollToIndex(index - 1, 'smooth');
          this.toolBarItemListService.itemSelectedFunction(`${index}/${this.itemsTmp.length}`)
        }
      }
      this.onClick.emit(item);
      this.arrowsState()
      this.selectedItem = item

    } catch (e) {
      console.log(`An error occurred ${this.itemSelected.name} -> ${this.className}`)
    }
  }


}
