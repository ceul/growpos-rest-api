import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import AttributeModel from 'src/app/models/attribute.model';
import { AttributeBusiness } from 'src/app/business/attribute/attribute.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as uuid from "uuid";
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'growpos-create-attribute',
  templateUrl: './create-attribute.component.html',
  styleUrls: ['./create-attribute.component.scss'],
})
export class CreateAttributeComponent implements OnInit {


  //------------------ needed variables -----------------
  submit: boolean;
  public attribute: AttributeModel
  attributeForm: FormGroup;
  @Output() createdAttribute: EventEmitter<any> = new EventEmitter();

  constructor(private attributeBussines: AttributeBusiness,
    public createAttribute: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService) {
    this.attributeForm = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'attribute_values': new FormControl([], Validators.required),
    })
  }

  ngOnInit() {
    try {
      this.attribute =  new AttributeModel()
    } catch (e) {
      console.log('An error occurred in ngOninit')
    }
  }

  public async saveAttribute() {
    try {
      if (this.attributeForm.valid) {
        this.loadingService.show()
        let attribute = new AttributeModel()
        attribute = this.attributeForm.value
        this.attribute = attribute
        let response = await this.attributeBussines.save(this.attribute)
        this.createdAttribute.emit(response)
        this.toast.showMessage('El atributo ha sido añadido con exito!!', 'success')
        this.clearForm()
        this.createAttribute.hide()
        this.loadingService.hide()
      } else {
        this.submit = true
      }
    } catch (error) {
      console.log(`An error occurred saving a attribute`)
    }
  }

  public clearForm() {
    try {
      let attribute = new AttributeModel()
      attribute.name = ''
      attribute.attribute_values = []
      this.attribute = attribute
      this.attributeForm.reset()
      this.attributeForm.patchValue(this.attribute)
    } catch (e) {
      console.log(`An error occurred clearing the form`)
    }
  }

  public addTagFn(value) {
    try {
      return { id: uuid.v4(), value: value };
    } catch (e) {
      console.log(`An error occurred adding tag`)
    }
  }

}
