import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { ToastService } from 'src/app/services/base-services/toast.service';
import CustomerModel from 'src/app/models/customer.model';
import { AttributeBusiness } from 'src/app/business/attribute/attribute.business';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import AttributeInstanceModel from 'src/app/models/attribute-instance.model';
import AttributeModel from 'src/app/models/attribute.model';
import AttributeValueModel from 'src/app/models/attribute-value.model';
@Component({
  selector: 'growpos-set-attribute-values',
  templateUrl: './set-attribute-values.component.html',
  styleUrls: ['./set-attribute-values.component.scss'],
})
export class SetAttributeValuesComponent implements OnInit {

  @Input() attributeSetId: string
  @Output() public selectedAttributeValues: EventEmitter<AttributeInstanceModel[]> = new EventEmitter();

  setAttributeValuesForm: FormGroup;
  attributes: any[]
  submit: boolean

  constructor(public setAttributeValuesModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private toast: ToastService,
    private attributeBusinesss: AttributeBusiness
  ) { }

  async ngOnInit() {
    try {
      this.loadingService.show()
      this.setAttributeValuesForm = new FormGroup({})
      this.submit = false
      let questions = {}
      this.attributes = Object.values(await this.attributeBusinesss.getByAttributeSet(this.attributeSetId))

      this.attributes.forEach(attribute => {
        questions[attribute.name] = new FormControl('', Validators.required)
      });
      this.setAttributeValuesForm = new FormGroup(questions)
      this.loadingService.hide()
    } catch (e) {
      console.log(`An error occurred in ngOnInit() => SetAttributeValuesComponent`)
    }
  }


  okBtn() {
    try {
      if (this.setAttributeValuesForm.valid) {
        let attributeInstances: AttributeInstanceModel[]
        attributeInstances = []
        let attributes = Object.values(this.setAttributeValuesForm.value)
        attributes.forEach((attribute: AttributeValueModel,index) => {
          let attributeInstance = new AttributeInstanceModel()
          attributeInstance.value = attribute.value
          attributeInstance.attribute_id = this.attributes[index].id
          attributeInstance.attributesetinstance_id = this.attributeSetId
          attributeInstances.push(attributeInstance)
        });
        this.selectedAttributeValues.emit(attributeInstances)
        this.setAttributeValuesModal.hide()
      }
      this.submit = true
    } catch (error) {
      console.log('An error occurred in ok button')
    }
  }

}


