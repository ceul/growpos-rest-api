
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MasterDetailTableDataSource } from './master-detail-table-datasource';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'growpos-master-detail-table',
  templateUrl: './master-detail-table.component.html',
  styleUrls: ['./master-detail-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', visibility: 'hidden' })
      ),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})

export class MasterDetailTableComponent {

  public activeLang = environment.activeLang;
  public dateFieldsf: string[]
  public currencyFieldsf: string[]
  public translateFieldsf: string[]
  public pageSizef: number = 25

  @Input()
  set pageSize(_pageSize: number) {
    if (_pageSize) {
      this.pageSizef = _pageSize
    }
  }

  @Input()
  set dateFields(_dateFields: string[]) {
    if (_dateFields) {
      this.dateFieldsf = _dateFields
    }
  }

  @Input()
  set currencyFields(_currencyFields: string[]) {
    if (_currencyFields) {
      this.currencyFieldsf = _currencyFields
    }
  }

  @Input()
  set translateFields(_translateFields: string[]) {
    if (_translateFields) {
      this.translateFieldsf = _translateFields
    }
  };

  @Input()
  set displayedColumns(_displayedColumns: string[]) {
    if (_displayedColumns) {
      this.displayedColumnsI = _displayedColumns
    }
  };

  @Input()
  set data(_data: any[]) {
    if (_data !== undefined) {
      if (_data.length > 0) {
        this.dataSource = new MasterDetailTableDataSource(
          this.paginator,
          _data,
          this.sort
        );
        if (!this.displayedColumnsI) {
          this.displayedColumnsI = Object.keys(_data[0]).filter(
            key => key !== 'details'
          );
        }
      }
    }
  }

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MasterDetailTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumnsI: Array<string>;
  expandedElement: Array<string>;
  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty('detailRow');

  public constructor(private translate: TranslateService
  ) {
    this.translate.setDefaultLang(this.activeLang);
  }
  /**
   * expand collapse a row
   * @param row
   */
  toggleRow(row) {
    if (this.expandedElement === row) {
      this.expandedElement = null;
    } else {
      this.expandedElement = row;
    }
    let index = this.dataSource.data.findIndex(element => element === row)
    this.onClick.emit(index);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue;
  }

  transform(fieldName: string, element: string): any {
    if (this.translateFieldsf.length === 0 && this.dateFieldsf.length === 0 && this.currencyFieldsf.length === 0) {
      return element
    }

    if (this.translateFieldsf.length > 0) {
      if (this.translateFieldsf.findIndex(item => item === fieldName) !== -1) {
        return this.translate.instant(element)
      }
    }

    /*if (this.dateFieldsf.length > 0) {
      if (this.dateFieldsf.findIndex(item => item === fieldName) !== -1) {
        return this.datePipe.transform(element, 'short')
      }
    }*/
    return element;
  }
}
