import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-numeric-keyboard',
  templateUrl: './numeric-keyboard.component.html',
  styleUrls: ['./numeric-keyboard.component.scss']
})
export class NumericKeyboardComponent implements OnInit {

  @Output() valueEmt: EventEmitter<string> = new EventEmitter();
  value: string = ""

  constructor() { }

  ngOnInit() {
  }

  numClicked(num: number) {
    try {
      this.value = this.value.concat(num.toString())
      console.log(this.value)
      this.valueEmt.emit(this.value)
    } catch (e) {
      console.log(`An error occurred in ${this.numClicked.name}`)
    }
  }

  ceBtn() {
    try {
      this.value = ''
      this.valueEmt.emit(this.value)
    } catch (e) {
      console.log(`An error occurred in ${this.ceBtn.name}`)
    }
  }

}
