import { Component, OnInit } from '@angular/core';
import PeopleModel from '../../models/people.model';
import { NgForm } from '@angular/forms';
import { UserBusiness } from '../../business/user/user.business';
import { ToastService } from '../../services/base-services/toast.service';
import * as uuid from 'uuid'
@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit {

  people: PeopleModel;


  constructor(private userBusiness: UserBusiness, private toast: ToastService) { }

  ngOnInit() {
    this.people = new PeopleModel();

    this.people.id = uuid.v4();
    this.people.apppassword = '';
    this.people.card = '';
    this.people.image = '';
    this.people.name = '';
    this.people.role = '1';
    this.people.visible = true;
  }


  onSubmit(form: NgForm) {

    if (form.invalid) {
      return;
    } else {
      this.savePeople(this.people);
    }
  }

  private async savePeople(data: PeopleModel) {

    this.toast.showMessage('Apparently you are already registered!!', 'success');
    /*let response = Object.values(await this.userBusiness.getById(this.people.id))
    if (response.length === 0 ) {
      let response = await this.userBusiness.save(data);
      console.log('Respuesta' + response);
      this.toast.showMessage('Now you are a new user!!', 'success');
    } else {
      this.toast.showMessage('Apparently you are already registered!!', 'success');
    }*/
  }

}
