import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PrintService } from 'src/app/services/print-service/print.service';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss'],
})
export class ChangeComponent implements OnInit {

  @Input() change: number

  constructor(public changeModal: BsModalRef,
    public printService: PrintService) { }

  ngOnInit() {}

}
