import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import ProductFilterModel from '../../models/product-filter.model';
import { CategoryBusiness } from '../../business/category/category.business';
import CategoryModel from '../../models/category.model';
import { ProductBusiness } from '../../business/product/product.business';
import ProductModel from '../../models/product.model';
import FilterOperatorEnum from '../../models/filter-operator.enum';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'app-find-product',
  templateUrl: './find-product.component.html',
  styleUrls: ['./find-product.component.scss']
})
export class FindProductComponent implements OnInit {

  @Output() public onClick: EventEmitter<ProductModel> = new EventEmitter();
  //public selectedProduct: Subject<ProductModel>

  emit:boolean = false
  private filterOperators
  public keys
  filterForm: FormGroup;
  private filter: ProductFilterModel;

  categories: CategoryModel[]
  products: ProductModel[]
  selectedProduct: ProductModel

  constructor(public findProduct: BsModalRef,
    private categoryBusiness: CategoryBusiness,
    private loadingService: LoadingSpinnerService,
    private productBusiness: ProductBusiness) {
    this.filterOperators = FilterOperatorEnum
    this.keys = Object.keys(this.filterOperators).filter(Number)
    this.filter = new ProductFilterModel();
    this.filterForm = new FormGroup({
      'barcode': new FormControl(''),
      'name.filter': new FormControl(2),
      'name.value': new FormControl(''),
      'pricebuy.filter': new FormControl(1),
      'pricebuy.value': new FormControl(''),
      'pricesell.filter': new FormControl(1),
      'pricesell.value': new FormControl(''),
      'category': new FormControl(''),
    })
  }

  ngOnInit() {
    //this.selectedProduct = new Subject();
    this.categories = []
    this.products = []
    this.categoryBusiness.get().then(categories => {
      this.categories = Object.values(categories)
    })
  }

  okBtn() {
    try {
      this.onClick.emit(this.selectedProduct)
      this.findProduct.hide()
    } catch (error) {
      console.log('An error occurred in ok button')
    }
  }

  selectProduct(product) {
    try {
      this.selectedProduct = product
    } catch (error) {
      console.log('An error occurred selecting the product')
    }
  }

  clearForm() {
    try {
      let form = {
        'barcode' : '',
        'name.filter' : 2,
        'name.value' : '',
        'pricebuy.filter' : 1,
        'pricebuy.value' : null,
        'pricesell.filter' : 1,
        'pricesell.value' : null,
        'category' : ''
      }
      this.filterForm.patchValue(form)
    } catch (error) {
      console.log('An error occurred clearing the form')
    }
  }

  async getProducts() {
    try {
      this.loadingService.show()
      this.filter = new ProductFilterModel(this.filterForm.value)
      if(this.filter.name.value === ''){
        this.filter.name.filter = 1
      }
      let products = await this.productBusiness.getFiltered(this.filter)
      this.products = Object.values(products)
      this.loadingService.hide()
    } catch (error) {
      console.log('An error occurred getting the products: ',error)
    }
  }

}
