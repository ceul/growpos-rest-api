import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import CustomerModel from 'src/app/models/customer.model';
import { CustomerBusiness } from 'src/app/business/customer/customer.business';
import { ToastService } from 'src/app/services/base-services/toast.service';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'app-find-customer',
  templateUrl: './find-customer.component.html',
  styleUrls: ['./find-customer.component.scss']
})
export class FindCustomerComponent implements OnInit {

  @Output() public selectedCustomer: EventEmitter<CustomerModel> = new EventEmitter();
  
  emit: boolean = false
  filter: CustomerModel
  customers: CustomerModel[];
  filterForm: FormGroup;
  customer: CustomerModel

  constructor(public findCustomerModal: BsModalRef,
    private loadingService: LoadingSpinnerService,
    private customerBusiness: CustomerBusiness,
    private toast: ToastService) {
    this.filterForm = new FormGroup({
      'name': new FormControl(''),
      'searchkey': new FormControl(''),
      'email': new FormControl('', [Validators.email]),
      'phone': new FormControl(''),
      'postal': new FormControl(''),
    })
  }

  ngOnInit() {
    this.customers = []
  }

  okBtn() {
    try {
      this.selectedCustomer.emit(this.customer)
      this.findCustomerModal.hide()
    } catch (error) {
      console.log('An error occurred in ok button')
    }
  }

  selectCustomer(customer) {
    try {
      this.customer = customer
    } catch (e) {
      console.log('An error ocurred selecting a customer')
    }
  }

  reset() {
    try {
      this.customers = []
    } catch (e) {
      console.log('An error occurred resetting the form')
    }
  }

  clearForm() {
    try {
      let form = {
        'name': '',
        'searchkey': '',
        'email': '',
        'phone': '',
        'postal': '',
      }
      this.filterForm.patchValue(form)
    } catch (error) {
      console.log('An error occurred clearing the form')
    }
  }

  async getCustomers() {
    try {
      this.loadingService.show()
      this.filter = new CustomerModel(this.filterForm.value)
      let customers = await this.customerBusiness.getFiltered(this.filter)
      this.customers = Object.values(customers)
      this.loadingService.hide()
    } catch (error) {
      console.log('An error occurred getting the customers: ',error)
    }
  }

}
