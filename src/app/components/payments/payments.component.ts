import { Component, OnInit, HostListener, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective, BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import TicketModel from 'src/app/models/ticket.model';
import PaymentModel from 'src/app/models/payment.model';
import * as uuid from "uuid";
import { TicketBusiness } from 'src/app/business/ticket/ticket.business';
import { ChangeComponent } from '../change/change.component';
import { PrintService } from 'src/app/services/print-service/print.service';
import TicketEnum from 'src/app/models/ticket.enum';
import HostConfigModel from 'src/app/models/host-config.model';
import { Storage } from '@ionic/storage';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  @Input() ticket: TicketModel
  @Input() note: string
  @Input() origin: string
  @Input() print: boolean = true

  @Output() completed: EventEmitter<{ isCompleted: boolean, ticketId: number }> = new EventEmitter();

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.bigScreen = window.innerWidth >= 993;
  }

  bigScreen: boolean;
  remaining: number;
  given: number
  givenTouched: boolean;
  payment: string;
  change: number;
  hostConfig: HostConfigModel

  constructor(public paymentsModal: BsModalRef,
    private modalService: BsModalService,
    public printService: PrintService,
    private ticketBusiness: TicketBusiness,
    private loadingService: LoadingSpinnerService,
    private storage: Storage) {

  }

  async ngOnInit() {
    try {
      this.ticket.payments = []
      this.bigScreen = window.innerWidth >= 993;
      this.remaining = this.ticket.total
      this.given = this.remaining
      this.givenTouched = false
      this.payment = this.origin === 'refund' ? 'cashrefund' : 'cash'
      this.calculateChange()
      this.hostConfig = await this.storage.get('config')
    } catch (error) {
      console.log(`An error ocurred on ngOnInit => PaymentsComponent`)
    }
  }

  addPayment(final: boolean) {
    try {
      let payment = new PaymentModel()
      payment.id = uuid.v4()
      payment.notes = this.note != undefined ? this.note : null
      this.remaining = this.note != undefined ? this.ticket.total : this.remaining
      payment.receipt = this.ticket.id
      payment.payment = this.payment
      payment.tip = 0
      payment.transid = 'no ID'
      payment.isprocessed = false
      if (this.payment === 'cash' && final) {
        payment.total = this.remaining
        payment.tendered = this.given
      } else if (this.payment === 'cash') {
        payment.total = this.given
        payment.tendered = this.given
        this.calculateChange()
      } else if (this.payment === 'debt') {
        if (this.remaining + this.ticket.customer.curdebt < this.ticket.customer.maxdebt) {
          payment.total = this.remaining
          payment.tendered = 0
          this.ticket.customer.curdebt = this.ticket.customer.curdebt + this.remaining
          this.calculateChange()
        } else {
          throw new Error('the client exceeds the maximum debt allowed')
        }
      } else {
        payment.total = this.remaining
        payment.tendered = 0
        this.calculateChange()
      }
      this.ticket.payments.push(payment)
      this.remaining -= payment.total
      this.given = this.remaining
      this.givenTouched = false
    } catch (error) {
      throw new Error()
    }
  }

  deletePayment() {
    try {
      let deletePayment = this.ticket.payments.pop()
      this.remaining += deletePayment.total
      this.given = this.remaining
    } catch (error) {
      console.log('An error occurred deletting a payment')
    }
  }

  async pay() {
    try {
      this.loadingService.show()
      this.addPayment(true)
      let ticketId = await this.ticketBusiness.save(this.ticket, '0')
      this.ticket.ticketId = Object.values(ticketId)[0]
      this.paymentsModal.hide()
      this.completed.emit({ isCompleted: true, ticketId: Object.values(ticketId)[0] })
      if (this.hostConfig.activePrint) {
        if (this.print) {
          switch (this.ticket.ticketType) {
            case TicketEnum.RECEIPT_NORMAL:
              this.printService.printDocument('invoice', this.ticket)
              break;
            case TicketEnum.RECEIPT_PAYMENT:
              this.printService.printDocument('customer-paid', this.ticket)
              break;
            case TicketEnum.RECEIPT_REFUND:
              this.printService.printDocument('refund', this.ticket)
              break;
          }
        }
      }
      this.openChangeModal()
      this.loadingService.hide()
    } catch (error) {
      this.paymentsModal.hide()
      this.completed.emit({ isCompleted: true, ticketId: 0 })
      console.log('An error occurred during the payment')
    }
  }

  givenKeyBoard(value: string) {
    try {
      if (value === '') {
        this.given = this.remaining
        this.givenTouched = false
      } else {
        this.given = parseFloat(value)
        this.givenTouched = true
      }
      this.calculateChange()
    } catch (error) {
      console.log('An error occurred in givenFunction')
    }
  }

  givenCash(value: number) {
    try {
      if (this.givenTouched) {
        this.given += value
      } else {
        this.given = value
        this.givenTouched = true
      }
      this.calculateChange()
    } catch (error) {
      console.log('An error occurred in givenCash')
    }
  }

  calculateChange() {
    try {
      this.change = this.given - this.remaining
    } catch (error) {
      console.log('An error occurred calculating the change')
    }
  }

  openChangeModal() {
    try {
      this.modalService.show(ChangeComponent, {
        initialState: {
          change: this.change
        }
      });
    } catch (error) {
      console.log('An error ocurred opening change modal: ', error);

    }
  }

}
