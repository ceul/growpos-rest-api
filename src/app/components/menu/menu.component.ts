import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserBusiness } from '../../business/user/user.business';
import { take } from 'rxjs/operators';
import { HeaderBodyService } from 'src/app/services/header-body/header-body.service';

export interface NavData {
  url?: string;
  title?: boolean;
}

@Component({
  selector: 'growpos-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})

export class MenuComponent implements OnInit {

  module: {
    name: String,
    manage: String,
    reports: String,
    manageItems: any,
    reportsItems: any
  };

  constructor(private activatedRoute: ActivatedRoute, private userBusiness: UserBusiness, private router: Router, private headerBodyService: HeaderBodyService) { 
    this.module = {
      name: 'Administración',
      manage: 'Tablas',
      reports: 'Reportes',
      manageItems: [],
      reportsItems: []
    };
    
    // Configure the navigation bar
    headerBodyService.navbarFunction();
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(take(1)).subscribe(async params => {

      let elements = Object.values(await  this.userBusiness.getSubMenu(params['id']));
      for (let index = 0; index < elements.length; index++) {
          const element = elements[index];
          this.module.manageItems.push({
            name: elements[index]['name'],
            icon: "fa " + elements[index]['icon'] + " fa-lg",
            url: elements[index]['url']
          });
        }
      });
  }

}
