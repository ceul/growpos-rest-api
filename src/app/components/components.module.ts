import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumericKeyboardComponent } from './numeric-keyboard/numeric-keyboard.component';
import { PaymentsComponent } from './payments/payments.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ModuleMenuComponent } from './module-menu/module-menu.component';
import { ItemListComponent } from './item-list/item-list.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { FindSupplierComponent } from './find-supplier/find-supplier.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateSupplierComponent } from './create-supplier/create-supplier.component';
import { FindCustomerComponent } from './find-customer/find-customer.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SalesKeyboardComponent } from './sales-keyboard/sales-keyboard.component';
import { ToastMessagesComponent } from './toast-messages/toast-messages.component';
import { CatalogComponent } from './catalog/catalog.component';
import { FindProductComponent } from './find-product/find-product.component';
import { ChangeComponent } from './change/change.component';
import { MasterDetailTableComponent } from './master-detail-table/master-detail-table.component';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule } from '@angular/material';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PipeModule } from '../pipes/pipes.module';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TipComponent } from './tip/tip.component';
import { VariablePriceComponent } from './variable-price/variable-price.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgSelectModule } from '@ng-select/ng-select';
import { CreateAttributeComponent } from './create-attribute/create-attribute.component';
import { PlacesComponent } from './places/places.component';
import { SetAttributeValuesComponent } from './set-attribute-values/set-attribute-values.component';
import { FindVehicleComponent } from './find-vehicle/find-vehicle.component';
import { CreateVehicleComponent } from './create-vehicle/create-vehicle.component';

@NgModule({
  declarations: [
    NumericKeyboardComponent, 
    PaymentsComponent, 
    FindCustomerComponent, 
    CreateCustomerComponent, 
    ProductListComponent, 
    ModuleMenuComponent, 
    ItemListComponent,
    CreateSupplierComponent,
    ToolBarComponent,
    SalesKeyboardComponent,
    CatalogComponent,
    FindProductComponent,
    ChangeComponent,
    FindSupplierComponent,
    MasterDetailTableComponent,
    LoadingSpinnerComponent,
    TipComponent,
    VariablePriceComponent,
    CreateAttributeComponent,
    PlacesComponent,
    SetAttributeValuesComponent,
    CreateVehicleComponent,
    FindVehicleComponent
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot() ,
    TabsModule,
    AlertModule.forRoot(),
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    NgxCurrencyModule,
    BsDropdownModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
    PipeModule.forRoot(),
    LottieAnimationViewModule.forRoot(),
    NgSelectModule
  ],
  exports: [
    NumericKeyboardComponent,
    PaymentsComponent, 
    FindCustomerComponent, 
    CreateCustomerComponent, 
    ProductListComponent, 
    ModuleMenuComponent, 
    ItemListComponent,
    CreateSupplierComponent,
    ToolBarComponent,
    SalesKeyboardComponent,
    CatalogComponent,
    FindProductComponent,
    FindSupplierComponent,
    ChangeComponent,
    MasterDetailTableComponent,
    LoadingSpinnerComponent,
    TipComponent,
    VariablePriceComponent,
    CreateAttributeComponent,
    PlacesComponent,
    SetAttributeValuesComponent,
    CreateVehicleComponent,
    FindVehicleComponent
  ]
})
export class ComponentsModule { }
