import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingSpinner } from 'src/app/models/loading-spinner.interface';
import { LoadingSpinnerService } from 'src/app/services/loading-spinner/loading-spinner.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { PrintService } from 'src/app/services/print-service/print.service';

@Component({
  selector: 'growpos-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.scss'],
})
export class LoadingSpinnerComponent implements OnInit {

  show = false;
  public lottieConfig: Object;
  private anim: any;
  private animationSpeed: number = 1;

  private subscription: Subscription;
  private subscription2: Subscription;

  constructor(
    private loaderService: LoadingSpinnerService,
    private router: Router,
    public printService: PrintService
  ) { }

  ngOnInit() {
    this.lottieConfig = {
      path: 'assets/animations/pinjump.json',
      autoplay: true,
      loop: true
    };
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoadingSpinner) => {
        this.show = state.show;
        if (this.show) {
          this.play()
        } else {
          this.stop()
        }
      });

    this.subscription2 = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.play()
      } else if (event instanceof NavigationEnd) {
        this.stop()
      }
    })

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }


  stop() {
    if (this.anim !== undefined) {
      this.anim.stop();
    }
  }

  play() {
    if (this.anim !== undefined) {
      this.anim.play();
    }
  }

  handleAnimation(anim: any) {
    this.anim = anim;
  }

}
