import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'growpos-variable-price',
  templateUrl: './variable-price.component.html',
  styleUrls: ['./variable-price.component.scss'],
})
export class VariablePriceComponent implements OnInit {

  @Input() originalPrice: number
  @Output() finalPrice: EventEmitter<number> = new EventEmitter();

  public price: number

  constructor(public variablePriceModal: BsModalRef,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.price = this.originalPrice
  }

  givenKeyBoard(value: string) {
    try {
      if (value === '') {
        this.price = 0
      } else {
        this.price = parseFloat(value)
      }
    } catch (error) {
      console.log('An error occurred in givenFunction')
    }
  }

  setOriginalPrice() {
    try {
      this.price = this.originalPrice
    } catch (error) {
      console.log('An error occurred in setOriginalPrice')
    }
  }

  okBtn() {
    try {
      this.finalPrice.emit(this.price)
      this.variablePriceModal.hide()
    } catch (e) {
      console.log('An error occurred in okBtn')
    }
  }

}
